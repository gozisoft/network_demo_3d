## Controls
1. left click mouse button and drag the mouse to look.
2. use the arrow keys to move back, fowards and strafe left or right.

Extra Info
==========
## server.exe:
1. Views the game world
3. Is the master game world
2. Does not use dead reckoning for visual update

## client.exe:
1. Each client is relient on the server
2. The client will connect to the server via TCP. 
3. Packets after connection is established are sent via UDP.
4. The clients port is auto assigned by the server,
   so multiple clients from the same machine can join the server. (Cool)
5. Views the world using dead reckoning, so movement is smooth

## remoteclient.exe:
1. Same as client, only can not be controlled. Purpose is to demonstrate
   dead reckoning.

## IP Address and Port Info
1. Clients by default connect to the localhost (127.0.0.1) address, as set by Serverport.txt
2. The port number is used by both clients and the server, as set by Serverport.txt 

File Structure
--------------
1. The exe's can be found within .\bin\win32
2. The serverport.txt is found within .\bin\media