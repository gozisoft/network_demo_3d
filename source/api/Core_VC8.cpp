#include "apiafx.h"

#ifdef COMPILER_MSVC_8

#include "Core_VC8.h"

#ifdef USING_STREAMS
	#include <iostream> // for std::[w]cin and std::[w]cout

namespace Engine
{
	#ifdef UNICODE
	  OStream& Cout = std::wcout;
	  IStream& Cin = std::wcin;
	#else
	  OStream& Cout = std::cout;
	  IStream& Cin = std::cin;
	#endif
}

#endif


#endif // COMPILER_MSVC_8