#include "apiafx.h"

#ifdef COMPILER_MSVC_9

#include "Core_VC9.h"

#if defined (USING_STL) || defined(_USING_STL)

	#include <iostream> // for std::[w]cin and std::[w]cout

	#if defined (UNICODE) || defined(_UNICODE)
		OStream& Cout = std::wcout;
		IStream& Cin = std::wcin;
	#else
		OStream& Cout = std::cout;
		IStream& Cin = std::cin;
	#endif

#endif // USING_STL

#endif // COMPILER_MSVC_8