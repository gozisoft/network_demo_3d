#ifndef __STRING_FUNCTIONS_INL__
#define __STRING_FUNCTIONS_INL__

template< class T >
String ToString(const T& t)
{
	OStringStream stream;
	stream << t;
	return stream.str();
}

template< class T >
T FromString(const String& s)
{
	IStringStream stream(s);
	T t;
	stream >> t;
	return t;
}





#endif