#ifndef __CBUFFER_IO_INL__
#define __CBUFFER_IO_INL__

//----------------------------------------------------------------------------
inline CBufferIO::operator bool () const
{
    return m_mode != BM_NONE;
}
//----------------------------------------------------------------------------
inline const char* CBufferIO::GetBuffer () const
{
    return m_pBuffer;
}
//----------------------------------------------------------------------------
inline size_t CBufferIO::GetNumBytesTotal () const
{
    return m_numBytesTotal;
}
//----------------------------------------------------------------------------
inline size_t CBufferIO::GetNumBytesProcessed () const
{
    return m_numBytesProcessed;
}
//----------------------------------------------------------------------------
inline CBufferIO::BufferMode CBufferIO::GetMode () const
{
    return m_mode;
}
//----------------------------------------------------------------------------


#endif