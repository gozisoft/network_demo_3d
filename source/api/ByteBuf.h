#ifndef __CBYTE_BUF_H__
#define __CBYTE_BUF_H__

#include "Core.h"

_STD_BEGIN

template <class _Elem, class _Traits> 
class basic_bytebuf : public basic_streambuf<_Elem, _Traits>
{
public:
	typedef basic_filebuf<_Elem, _Traits> _Myt;
	typedef basic_streambuf<_Elem, _Traits> _Mysb;
	typedef typename _Traits::state_type _Myst;
	typedef codecvt<_Elem, char, typename _Traits::state_type> _Cvt;

	typedef vector<_Elem> _AryT;


	basic_bytebuf() :
	_Mysb()
	{	

	}	

private:

	_AryT m_buffer;

};




_STD_END



#endif