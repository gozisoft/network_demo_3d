#ifndef __DATA_FUNCTIONS_INL__
#define __DATA_FUNCTIONS_INL__

inline UInt8 UpperByte(UInt8 byte)
{
	return static_cast<UInt8>( (byte >> 4) & 0x0F );
}

inline UInt8 LowerByte(UInt8 byte)
{
	return static_cast<UInt8>( byte & 0x0F );
}

inline UInt8 UpperWord(UInt16 word)
{
	return static_cast<UInt8>( (word >> 8) & 0xFF );
}

inline UInt8 LowerWord(UInt16 word)
{
	return static_cast<UInt8>( word & 0xFF );
}

inline UInt16 MakeWord(UInt8 upperbyte, UInt8 lowerbyte)
{
	return ( ( (upperbyte << 8) & 0xFF00 ) | (lowerbyte & 0xFF) );
}

// bit twiddling method for counting used bits
inline ULong Popcnt(ULong i) 
{
	ULong c = 0;
	for(; i; c++) 
	{
		i &= i - 1;
	}
	return c;
}

inline UInt16 LowerDoubleWord(UInt32 dword)
{
	return ( dword & 0xFFFF );
}

inline UInt16 UpperDoubleWord(UInt32 dword)
{
	return ( (dword >> 16) & 0xFFFF );
}





#endif