#ifndef __CORE_H__
#define __CORE_H__

#if defined(_WIN32) || defined(WIN32)

#ifndef _WIN32
#define _WIN32
#endif

#ifndef WIN32
#define WIN32
#endif

#define LITTLE_ENDIAN

#elif defined(_WIN64) || defined(WIN64)

#ifndef _WIN64
#define _WIN64
#endif

#ifndef WIN64
#define WIN64
#endif

#define LITTLE_ENDIAN

#endif



#if defined(_MSC_VER)

	#if _MSC_VER > 1000
	#pragma once
	#endif

    #if (_MSC_VER >= 1600 && _MSC_VER < 1700) 
        // Define to indicate the compiler is MS Visual Studio Version 10.                                                         
        #define COMPILER_MSVC_10
		#define USING_C99
		#define USING_STL
        #include "Core_VC10.h"
	#elif (_MSC_VER >= 1500 && _MSC_VER < 1600) 
	    // Define to indicate the compiler is MS Visual Studio Version 9.                                                         
        #define COMPILER_MSVC_9
		#define USING_C99
		#define USING_STL
        #include "Core_VC9.h"
	#elif (_MSC_VER >= 1400 && _MSC_VER < 1500) 
	    // Define to indicate the compiler is MS Visual Studio Version 9.                                                         
        #define COMPILER_MSVC_8
		#define USING_BOOST
		#define USING_BASIC_TYPES
		#define USING_STREAMS
        #include "Core_VC8.h"
    #endif

#endif

//#define USING_BOOST

// Place boost headers that you want to use in here, this will disable any TR1 use
#ifdef USING_BOOST

	#include <boost/config.hpp>
	#include <boost/smart_ptr.hpp>
	#include <boost/noncopyable.hpp>

	using boost::shared_ptr;
	using boost::weak_ptr;
	using boost::static_pointer_cast;
	using boost::dynamic_pointer_cast;

#endif

// Removes warnings for unused paramaters
#define UNUSED_PARAMETER(P) (P)

// Enables extra asserts for the BufferIO class
#if defined(DEBUG) || defined(_DEBUG)
	#define BUFFERIO_VALIDATE_OPERATION
#endif

// namespace Engine defines
#define _ENGINE_BEGIN namespace Engine {
#define _ENGINE_END	  }
#define _ENGINE		  ::Engine::

//#define USING_MEMORY_MGR
#include "MemoryMgr.h"
#include "Helper.h"

//------------------------------------------------------------------------------
// Basic Storage Type Declares (Arraytors)
//------------------------------------------------------------------------------
namespace Engine
{

	// basic type typedefs for Vector
	typedef std::vector<Int8>   Int8Vector;
	typedef std::vector<Int16>  Int16Vector;
	typedef std::vector<Int32>  Int32Vector;
	typedef std::vector<Int64>  Int64Vector;
	typedef std::vector<Int>	IntVector;

	typedef std::vector<UInt8>  UInt8Vector;
	typedef std::vector<UInt16> UInt16Vector;
	typedef std::vector<UInt32> UInt32Vector;
	typedef std::vector<UInt64> UInt64Vector;
	typedef std::vector<UInt>   UIntVector;


} // namespace Engine





#endif