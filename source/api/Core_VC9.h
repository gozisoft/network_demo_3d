#pragma once
#ifndef __CORE_VC90_H__
#define __CORE_VC90_H__


#if defined(COMPILER_MSVC_9) || defined(_COMPILER_MSVC_9)

	//-------------------------------------------------------------------------------------------
	// Basic Types	(Integers, Chars)
	//-------------------------------------------------------------------------------------------
	#include <climits>


	// For Visual Studio 6 in C++ mode and for many Visual Studio versions when
	// compiling for ARM we should wrap <wchar.h> include with 'extern "C++" {}'
	// or compiler give many errors like this:
	//   error C2733: second C linkage of overloaded function 'wmemchr' not allowed
	#ifdef __cplusplus
	extern "C"
	{
		#endif
		#include <cwchar>
		#ifdef __cplusplus
	}
	#endif

	typedef __int8					Int8;
	typedef __int16					Int16;
	typedef __int32					Int32;
	typedef __int64					Int64;
	typedef unsigned __int8		    UInt8;
	typedef unsigned __int16	    UInt16;
	typedef unsigned __int32	    UInt32;
	typedef unsigned __int64        UInt64;

	typedef short					Short;
	typedef long					Long;
	typedef long long				LLong;

	typedef unsigned short			UShort;
	typedef unsigned long			ULong;
	typedef unsigned long long		ULLong;
			
	#ifdef _WIN64 
	typedef UInt64    UInt;
	typedef Int64     Int;
	const UInt        _UINT_MAX     = _UI64_MAX;
	const Int         _INT_MAX      = _I64_MAX;
	const Int         _INT_MIN      = _I64_MIN;
	#else
	typedef UInt32    UInt;
	typedef Int32     Int;
	const UInt        _UINT_MAX     = _UI32_MAX;
	const Int         _INT_MAX      = _I32_MAX;
	const Int         _INT_MIN      = _I32_MIN;
	#endif

	// Integer types capable of holding object pointers
	#if defined (WIN64) || defined(_WIN64) 
	typedef __int64	 IntPtr;
	typedef unsigned __int64  UIntPtr;
	#else 
	typedef _W64 int IntPtr;
	typedef _W64 unsigned int UIntPtr;
	#endif // WIN64

	// Use unicode versions of Microsoft API
	#if defined (UNICODE) || defined(_UNICODE)
		typedef wchar_t Char;
		#define ieS__(s) L##s		// string macro - used in conjunction with UNICODE and USING_WIDE_CHAR.  
		#define ieS(s) ieS__(s)		
	#else
		typedef char Char;
		#define ieS(s) (s) 			// string macro - used in conjunction with ASCII and NOT_USING_WIDE_CHAR.    							
	#endif // UNICODE

	// Main char typedef (wchar_t, char)
	typedef Char* pChar;
	typedef const pChar Const_pChar;
	//-------------------------------------------------------------------------------------------


	//-------------------------------------------------------------------------------------------
	// C99 standard types and headers
	//-------------------------------------------------------------------------------------------
	#if defined(USING_C99) || defined(_USING_C99)

		// C++ 99 standard
		#include <cassert>
		#include <cstring>
		#include <cstdio>
		#include <cstdlib>

	#endif
	//-------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------
	// STL Types and headers
	//-------------------------------------------------------------------------------------------
	#if defined(USING_STL) || defined(_USING_STL)

		// C++ Common STL headers
		#include <vector>
		#include <list>
		#include <map>
		#include <set>
		#include <utility>
		#include <iterator>

		// String Types, with support for streaming (wchar_t, char)
		#include <fstream>
		#include <sstream>
		#include <istream> // for std::basic_istream
		#include <ostream> // for std::basic_ostream
		#include <locale>  // for converting to UTF-8 format
		#include <string>

		#if defined (UNICODE) || defined(_UNICODE) // Use unicode versions of Microsoft API
			typedef std::wstring _StringBase;
			typedef std::wstringstream _StringStreamBase;
			typedef std::wostringstream _OutputStringStreamBase;
			typedef std::wistringstream _InputStringStreamBase;
			typedef std::wifstream _InputFileStreamBase;
			typedef std::wofstream _OutputFileStreamBase;
			typedef std::wostream _OutputStream;
			typedef std::wistream _InputStream;
		#else
			typedef std::string _StringBase;
			typedef std::stringstream _StringStreamBase;
			typedef std::ostringstream _OutputStringStreamBase;
			typedef std::istringstream _InputStringStreamBase;
			typedef std::ifstream _InputFileStreamBase;
			typedef std::ofstream _OutputFileStreamBase;
			typedef std::ostream _OutputStream;
			typedef std::istream _InputStream;
		#endif

		// Final typedef's with compatibility for wchar_t and char
		typedef _StringBase String;
		typedef _StringStreamBase StringStream;
		typedef _OutputStringStreamBase OStringStream;
		typedef _InputStringStreamBase IStringStream;
		typedef _InputFileStreamBase IfStream;
		typedef _OutputFileStreamBase OfStream;
		typedef _OutputStream OStream;
		typedef _InputStream IStream;

		extern OStream &Cout;
		extern IStream &Cin;

	#endif
	//-------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------
	// Includes for C++ 0x shared_ptr and more, this will change in future
	//-------------------------------------------------------------------------------------------
	// includes the TR1 shared_ptr, alternative to boost.
	#if defined(_HAS_TR1) || defined(HAS_TR1)

		// includes the TR1 shared_ptr, alternative to boost.
		#include <memory> 

		using std::tr1::shared_ptr;
		using std::tr1::weak_ptr;
		using std::tr1::static_pointer_cast;
		using std::tr1::dynamic_pointer_cast;
		
	#endif // _HAS_TR1


#endif // COMPILER_MSVC_90




#endif // __CORE_VC90_H__