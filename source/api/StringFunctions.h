#ifndef __STRING_FUNCTIONS_H__
#define __STRING_FUNCTIONS_H__

#include "Core.h"



namespace Engine
{
	String GetFullPath(const Char* path); 
	String GetWorkingDirectory(const Char* path);

	template< class T > String ToString(const T& t);
	template< class T > T FromString(const String& s);

	std::wstring Widen(const std::string& str);
	std::string Narrow(const std::wstring& str);

	int AnsiToWide(wchar_t* dest, const char* src, int inputLen, int outputLen);
	int WideToAnsi(char* dest, const wchar_t* src, int inputLen, int outputLen);
	int GenericToAnsi(char* dest, const Char* src, int inputLen, int outputLen); 
	int AnsiToGeneric(Char* dest, const char* src, int inputLen, int outputLen); 
	
	//int GenericToWide(wchar_t* dest, const Char* src, int inputLen, int outputLen); 
	//int WideToGeneric(Char* dest, const wchar_t* src, int charCount);

#pragma warning(push)
#pragma warning(disable : 4311)

	class CHashedString
	{
	public:
		explicit CHashedString(const char* pIdentString);

		ULong GetHashValue() const;

		const std::string & GetStr() const;

		static void* hash_name(const char* pIdentStr);

		bool operator < (const CHashedString& o) const;

		bool operator == (const CHashedString& o) const;

	private:
		// note: m_ident is stored as a void* not an int, so that in
		// the debugger it will show up as hex-values instead of
		// integer values. This is a bit more representative of what
		// we're doing here and makes it easy to allow external code
		// to assign event types as desired.

		void*              m_ident;
		std::string		   m_identStr;
	};
	//Remove the warning for warning #4311...

#pragma warning(pop)


#include "StringFunctions.inl"

}

#endif




/*
	// AProgrammer response to widen a char to wchar_t
	// http://stackoverflow.com/questions/1791578/how-do-i-convert-a-char-string-to-a-wchar-t-string
	std::wstring Widen(const std::string& s, std::locale loc);
*/