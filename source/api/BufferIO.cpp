#include "apiafx.h"
#include "BufferIO.h"
#include "Endian.h"


using namespace Engine;

//----------------------------------------------------------------------------
CBufferIO::CBufferIO ()
    :
    m_pBuffer(0),
    m_numBytesTotal(0),
    m_numBytesProcessed(0),
    m_mode(BM_NONE)
{
}

//----------------------------------------------------------------------------
CBufferIO::CBufferIO (BufferMode mode)
    :
    m_pBuffer(0),
    m_numBytesTotal(0),
    m_numBytesProcessed(0),
    m_mode(mode)
{


}

//----------------------------------------------------------------------------
CBufferIO::CBufferIO (size_t numBytesTotal, char* buffer, BufferMode mode)
    :
    m_pBuffer(0),
    m_numBytesTotal(0),
    m_numBytesProcessed(0),
    m_mode(BM_NONE)
{
    Open(numBytesTotal, buffer, mode);
}
//----------------------------------------------------------------------------
CBufferIO::~CBufferIO ()
{
    if (m_mode != BM_NONE)
    {
        Close();
    }
}
//----------------------------------------------------------------------------
bool CBufferIO::Open (size_t numBytesTotal, char* buffer, BufferMode mode)
{
    if (m_mode == BM_NONE)
    {
        if (buffer && numBytesTotal > 0)
        {
            m_pBuffer = buffer;
            m_numBytesTotal = numBytesTotal;
            m_numBytesProcessed = 0;
            m_mode = mode;
            return true;
        }

        assert( false && ieS("Failed to open buffer\n") );
    }
    else
    {
        assert( false && ieS("Buffer %s is already open\n") );
    }

    return false;
}
//----------------------------------------------------------------------------
bool CBufferIO::Close ()
{
    m_pBuffer = 0;
    m_numBytesTotal = 0;
    m_numBytesProcessed = 0;
    m_mode = BM_NONE;
    return false;
}
//----------------------------------------------------------------------------
bool CBufferIO::IncrementNumBytesProcessed (size_t numBytes)
{
#ifdef BUFFERIO_VALIDATE_OPERATION
    if (numBytes <= 0)
    {
        assert( false && ieS("Increment must be positive\n") );
        return false;
    }
#endif

    size_t nextBytesProcessed = m_numBytesProcessed + numBytes;
    if (nextBytesProcessed <= m_numBytesTotal)
    {
        m_numBytesProcessed = nextBytesProcessed;
        return true;
    }

#ifdef BUFFERIO_VALIDATE_OPERATION
    assert( false && ieS("Increment exceeds buffer size\n") );
#endif

    m_numBytesProcessed = m_numBytesTotal;
    return false;
}
//----------------------------------------------------------------------------
bool CBufferIO::Read (size_t itemSize, void* datum)
{
#ifdef BUFFERIO_VALIDATE_OPERATION
    if ((m_mode != BM_READ && m_mode != BM_READ_AND_SWAP) || !datum
    ||  (itemSize != 1 && itemSize != 2 && itemSize != 4 && itemSize != 8))
    {
        assert( false && ieS("Invalid CBufferIO::Read\n") );
        return false;
    }
#endif

    size_t nextBytesProcessed = m_numBytesProcessed + itemSize;
    if (nextBytesProcessed <= m_numBytesTotal)
    {
        char* source = m_pBuffer + m_numBytesProcessed;
        m_numBytesProcessed = nextBytesProcessed;
        std::memcpy(datum, source, itemSize);
        if (m_mode == BM_READ_AND_SWAP && itemSize > 1)
        {
            CEndian::Swap(itemSize, datum);
        }
        return true;
    }

#ifdef BUFFERIO_VALIDATE_OPERATION
    assert( false && ieS("Invalid CBufferIO::Read\n") );
#endif

    m_numBytesProcessed = m_numBytesTotal;
    return false;
}
//----------------------------------------------------------------------------
bool CBufferIO::Read (size_t itemSize, size_t numItems, void* data)
{
#ifdef BUFFERIO_VALIDATE_OPERATION
    if ((m_mode != BM_READ && m_mode != BM_READ_AND_SWAP) || numItems <= 0
    ||  !data
    ||  (itemSize != 1 && itemSize != 2 && itemSize != 4 && itemSize != 8))
    {
        assert( false && ieS("Invalid CBufferIO::Read\n") );
        return false;
    }
#endif

    size_t numToCopy = itemSize * numItems;
    size_t nextBytesProcessed = m_numBytesProcessed + numToCopy;
    if (nextBytesProcessed <= m_numBytesTotal)
    {
        char* source = m_pBuffer + m_numBytesProcessed;
        m_numBytesProcessed = nextBytesProcessed;
        std::memcpy(data, source, numToCopy);
        if (m_mode == BM_READ_AND_SWAP && itemSize > 1)
        {
            CEndian::Swap(itemSize, numItems, data);
        }
        return true;
    }

#ifdef BUFFERIO_VALIDATE_OPERATION
    assert( false && ieS("Invalid CBufferIO::Read\n") );
#endif

    m_numBytesProcessed = m_numBytesTotal;
    return false;
}
//----------------------------------------------------------------------------
bool CBufferIO::Write (size_t itemSize, const void* datum)
{
#ifdef BUFFERIO_VALIDATE_OPERATION
    if ((m_mode != BM_WRITE && m_mode != BM_WRITE_AND_SWAP) || !datum
    ||  (itemSize != 1 && itemSize != 2 && itemSize != 4 && itemSize != 8))
    {
        assert( false && ieS("Invalid CBufferIO::Write\n") );
        return false;
    }
#endif

    size_t nextBytesProcessed = m_numBytesProcessed + itemSize;
    if (nextBytesProcessed <= m_numBytesTotal)
    {
        char* target = &m_pBuffer[0] + m_numBytesProcessed;
        m_numBytesProcessed = nextBytesProcessed;
        std::memcpy(target, datum, itemSize);
        if (m_mode == BM_WRITE_AND_SWAP && itemSize > 1)
        {
            CEndian::Swap(itemSize, target);
        }
        return true;
    }

#ifdef BUFFERIO_VALIDATE_OPERATION
    assert( false && ieS("Invalid CBufferIO::Write\n") );
#endif

    m_numBytesProcessed = m_numBytesTotal;
    return false;
}
//----------------------------------------------------------------------------
bool CBufferIO::Write (size_t itemSize, size_t numItems, const void* data)
{
#ifdef BUFFERIO_VALIDATE_OPERATION
    if ((m_mode != BM_WRITE && m_mode != BM_WRITE_AND_SWAP) || numItems <= 0
    ||  !data
    ||  (itemSize != 1 && itemSize != 2 && itemSize != 4 && itemSize != 8))
    {
        assert( false && ieS("Invalid CBufferIO::Write\n") );
        return false;
    }
#endif

    size_t numToCopy = itemSize * numItems;
    size_t nextBytesProcessed = m_numBytesProcessed + numToCopy;
    if (nextBytesProcessed <= m_numBytesTotal)
    {
        char* target = m_pBuffer + m_numBytesProcessed;
        m_numBytesProcessed = nextBytesProcessed;
        std::memcpy(target, data, numToCopy);
        if (m_mode == BM_WRITE_AND_SWAP && itemSize > 1)
        {
            CEndian::Swap(itemSize, numItems, target);
        }
        return true;
    }

#ifdef BUFFERIO_VALIDATE_OPERATION
    assert( false && ieS("Invalid CBufferIO::Write\n") );
#endif

    m_numBytesProcessed = m_numBytesTotal;
    return false;
}