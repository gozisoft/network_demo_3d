// apiafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#pragma warning (disable : 4996) // strcpy warnings, use strcpy_s (windows specific)

#include "apiheaders.h"



#if defined(WIN32) && defined(_DEBUG)

/*
#ifndef USING_MEMORY_MGR
#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
#define new DEBUG_NEW
#endif
*/

#endif