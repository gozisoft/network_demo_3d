#ifndef __DATA_FUNCTIONS_H__
#define __DATA_FUNCTIONS_H__

#include "Core.h"

namespace Engine
{
	UInt8 UpperByte(UInt8 byte);
	UInt8 LowerByte(UInt8 byte);
	UInt8 UpperWord(UInt16 word);
	UInt8 LowerWord(UInt16 word);

	UInt16 MakeWord(UInt8 upperbyte, UInt8 lowerbyte);
	ULong Popcnt(ULong i);

	UInt16 LowerDoubleWord(UInt32 dword);
	UInt16 UpperDoubleWord(UInt32 dword);


#include "DataFunctions.inl"

}




#endif