#ifndef __HELPER_H__
#define __HELPER_H__

// Checks if a pointer is not 0 before calling the Release()
// method on it and then setting it to 0. (used for com objects)
template <class T>
inline void SafeRelease(T& iface)
{
	if (iface)
	{
		iface->Release();
		// iface = 0;
	}
}

// Checks if a pointer is not 0 before calling the destroy() method on it and then setting it to 0.
template <class T>
inline void SafeDestroy(T& pItem)
{
	if (pItem)
	{
		pItem->Destroy();
		pItem = 0;
	}
}

template <class T>
struct sAutoZero : public T
{
	sAutoZero()
	{
		memset (this, 0, sizeof(T));
		dwSize = sizeof(T);
	}
};

// Checks if pointer is not 0 before deleting it.
template <class T>
inline void SafeDelete(T& iface)
{
	if (iface)
	{
		delete (iface);
		iface = 0;
	}
}

// Checks if pointer is not 0 before deleting it.
template <class T>
inline void SafeDeleteArray(T& iface)
{
	if (iface)
	{
		delete[] (iface);
		iface = 0;
	}
}

template < typename T >
T **Allocate2DArray(size_t nRows, size_t nCols)
{
    //(step 1) allocate memory for array of elements of column
    T **ppi = new T*[nRows];

    //(step 2) allocate memory for array of elements of each row
    T *pool = new T [nRows * nCols];

    // Now point the pointers in the right place
    T *curPtr = pool;
    for(size_t i = 0; i < nRows; i++)
    {
        *(ppi + i) = curPtr;
         curPtr += nCols;
    }
    return ppi;
}

template < typename T >
void Free2DArray(T** Array)
{
    SafeDeleteArray(*Array);
    SafeDeleteArray(Array);
}





#endif 