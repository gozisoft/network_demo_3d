#ifndef __CORE_VC8_H__
#define __CORE_VC8_H__


#ifdef COMPILER_MSVC_8

	//------------------------------------------------------------------------------
	// Includes for Shared_ptr, this will change in future
	//------------------------------------------------------------------------------
	#ifdef USING_BOOST
		#include <boost/config.hpp>
		#include <boost/smart_ptr.hpp>
		#include <boost/noncopyable.hpp>

		using boost::shared_ptr;
		using boost::weak_ptr;
		using boost::static_pointer_cast;
		using boost::dynamic_pointer_cast;

	#else
		#include <memory> // includes the TR1 shared_ptr, alternative to boost.

		using std::tr1::shared_ptr;
		using std::tr1::weak_ptr;
		using std::tr1::static_pointer_cast;
		using std::tr1::dynamic_pointer_cast;

	#endif // USING_BOOST


	//------------------------------------------------------------------------------
	// Basic Types	(Integers, Chars)
	//------------------------------------------------------------------------------
	#ifdef USING_BASIC_TYPES

		#include <climits>

		namespace Engine
		{

			typedef __int8				    Int8;
			typedef __int16				    Int16;
			typedef __int32				    Int32;
			typedef __int64                 Int64;
			typedef unsigned __int8		    UInt8;
			typedef unsigned __int16	    UInt16;
			typedef unsigned __int32	    UInt32;
			typedef unsigned __int64        UInt64;

			typedef unsigned long			ULong;
			typedef unsigned long long		ULLong;
			typedef unsigned short			UShort;

			typedef short					Short;
			typedef long					Long;
			typedef long long				LLong;

			#ifdef _WIN64 
			typedef UInt64    UInt;
			typedef Int64     Int;
			const UInt        GB_MAX_UINT     = _UI64_MAX;
			const Int         GB_MAX_INT      = _I64_MAX;
			const Int         GB_MIN_INT      = _I64_MIN;
			#else
			typedef UInt32    UInt;
			typedef Int32     Int;
			const UInt        GB_MAX_UINT     = UINT_MAX;
			const Int         GB_MAX_INT      = INT_MAX;
			const Int         GB_MIN_INT      = INT_MIN;
			#endif

			// Use unicode versions of Microsoft API
			#ifdef UNICODE 
				typedef wchar_t Char;
				#define ieS__(s) L##s		// string macro - used in conjunction with UNICODE and USING_WIDE_CHAR.  
				#define ieS(s) ieS__(s)		
			#else
				typedef char    Char;
				#define ieS(s) s 			// string macro - used in conjunction with ASCII and NOT_USING_WIDE_CHAR.    							
			#endif // UNICODE

			// Main char typedef (wchar_t, char)
			typedef Char* pChar;
			typedef const pChar Const_pChar;

		} // namespace Engine

	#endif // USING_BASIC_TYPES

	//------------------------------------------------------------------------------
	// String Types, with support for streaming (wchar_t, char)
	//------------------------------------------------------------------------------
	#ifdef USING_STREAMS

		#include <fstream>
		#include <sstream>
		#include <istream> // for std::basic_istream
		#include <ostream> // for std::basic_ostream
		#include <locale>
		#include <string>

		namespace Engine
		{

			#ifdef UNICODE // Use unicode versions of Microsoft API
				typedef std::wstring _StringBase;
				typedef std::wstringstream _StringStreamBase;
				typedef std::wostringstream _OutputStringStreamBase;
				typedef std::wistringstream _InputStringStreamBase;
				typedef std::wifstream _InputFileStreamBase;
				typedef std::wofstream _OutputFileStreamBase;
				typedef std::wostream _OutputStream;
				typedef std::wistream _InputStream;
			#else
				typedef std::string _StringBase;
				typedef std::stringstream _StringStreamBase;
				typedef std::ostringstream _OutputStringStreamBase;
				typedef std::istringstream _InputStringStreamBase;
				typedef std::ifstream _InputFileStreamBase;
				typedef std::ofstream _OutputFileStreamBase;
				typedef std::ostream _OutputStream;
				typedef std::istream _InputStream;
			#endif

			// Final typedef's with compatibility for wchar_t and char
			typedef _StringBase String;
			typedef _StringStreamBase StringStream;
			typedef _OutputStringStreamBase OStringStream;
			typedef _InputStringStreamBase IStringStream;
			typedef _InputFileStreamBase IfStream;
			typedef _OutputFileStreamBase OfStream;
			typedef _OutputStream OStream;
			typedef _InputStream IStream;

			extern OStream &Cout;
			extern IStream &Cin;

		} // namespace Engine

	#endif // USING_STREAMS


#endif // COMPILER_MSVC_8


#endif // __CORE_VC8_H__