#ifndef __CHEAP_FACTORY_H__
#define __CHEAP_FACTORY_H__

#include "Heap.h"

#ifdef USING_MEMORY_MGR

namespace Engine
{

class CHeapFactory
{
public:
	static CHeap * CreateHeap (const char * name);
	static CHeap * CreateHeap (const char * name, const char * parent);
	static void DestroyHeap (CHeap * pHeap);

	static CHeap * GetDefaultHeap ();
	static void   PrintInfo ();

	static int  GetMemoryBookmark ();
	static void ReportMemoryLeaks (int nBookmark);
	static void ReportMemoryLeaks (int nBookmark1, int nBookmark2);


private:
	static CHeap * GetRootHeap ();
	static CHeap * FindHeap (const char * name);
	static CHeap * CreateNewHeap (const char * name);
	static void PrintHeapTree(CHeap * pHeap);

	static void Initialize();

	enum { MAXHEAPS = 512 };

	static CHeap * s_pRootHeap;   
	static CHeap * s_pDefaultHeap;   
	static CHeap s_heaps[MAXHEAPS];
};

}

#endif 

#endif // __CHEAPFACTORY_H__