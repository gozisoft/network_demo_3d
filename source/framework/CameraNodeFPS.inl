#ifndef CCAMERA_NODE_FPS_INL
#define CCAMERA_NODE_FPS_INL

inline CCameraPtr CCameraNodeFPS::GetCamera()
{
	return m_pCamera;
}

inline Vector3f CCameraNodeFPS::GetUpVector() const 
{ 
	return m_up;
}

inline Vector3f CCameraNodeFPS::GetTargetVector() const 
{
	return m_target; 
}



#endif