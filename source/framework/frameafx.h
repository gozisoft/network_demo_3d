#pragma once

#include "targetver.h"

#include "frameheaders.h"

#if defined(WIN32) && defined(_DEBUG)
#ifndef USING_MEMORY_MGR
#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
#define new DEBUG_NEW
#endif
#endif