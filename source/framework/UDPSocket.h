#ifndef CUDP_SOCKET_H
#define CUDP_SOCKET_H

#include "Socket.h"

namespace Engine
{

	class CUDPSocket : public CSocket
	{
	public:
		CUDPSocket();
		CUDPSocket(SOCKET newsocket);
		virtual ~CUDPSocket();

		bool Connect(const String& ip, const String& port);
		bool Bind(const String& port);

		virtual void HandleInput(const IEventManagerPtr& pEventMgr);
		virtual void HandleOutput();

	protected:
		String m_ip;
		String m_port;

	private:
		DECLARE_HEAP;	
	};

}



#endif