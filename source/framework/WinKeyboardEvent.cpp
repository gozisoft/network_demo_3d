#include "frameafx.h"
#include "winKeyboardEvent.h"
#include "StringFunctions.h"

using namespace Engine;

DEFINE_HEAP(SKeyInput, "SKeyInput");
DEFINE_HIERARCHICALHEAP(SKeyEvent, "SKeyEvent", "IEventData");


const SKeyEvent::EventType SKeyEvent::sk_EventType("SKeyEvent");