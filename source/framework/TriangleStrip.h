#ifndef __CTRIANGLE_STRIP_H__
#define __CTRIANGLE_STRIP_H__

#include "Triangle.h"
#include "IndexBuffer.h"

namespace Engine
{

class CTriStrip : public CTriangles
{
public:
	CTriStrip(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, IndexType indextType);
	CTriStrip(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer);
	~CTriStrip();

	UInt GetTriangleCount() const;
	bool GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const;

protected:
	CTriStrip();

private:
	DECLARE_HEAP;

};


#include "TriangleStrip.inl"


}


#endif