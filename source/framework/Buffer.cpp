#include "Frameafx.h"
#include "Buffer.h"

using namespace Engine;

DEFINE_HEAP(CBuffer, "CBuffer");

CBuffer::CBuffer() : m_count(0), m_stride(0), m_usage(NUM_BUFFER_USES)
{

}

CBuffer::CBuffer(UInt count, UInt stride, BufferUse usage, BufferType bufferType) : 
m_count(count),
m_stride(stride),
m_usage(usage),
m_bufferType(bufferType)
{
	assert(count > 0);
	m_pData.resize(m_count * m_stride);
}

CBuffer::CBuffer(const CBuffer& buffer) : 
m_count(buffer.m_count),
m_stride(buffer.m_stride),
m_pData(buffer.m_pData),
m_usage(buffer.m_usage),
m_bufferType(buffer.m_bufferType)
{

}

CBuffer::~CBuffer()
{
	m_pData.clear();
}