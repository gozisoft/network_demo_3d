#ifndef __CTRIANGLE_H__
#define __CTRIANGLE_H__

#include "SceneNode.h"

namespace Engine
{

class CTriangles : public CSceneNode
{
public:
	virtual ~CTriangles();

	virtual UInt GetTriangleCount() const = 0;
	virtual bool GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const = 0;

	void UpdateModelSpace(GeoUpdate type);
	void CalculateNormals(CVertexBufferAccess& vba);
	bool CalculateTangents(UInt32 TCoordUnit, bool overwrite);

protected:
	CTriangles();
	CTriangles(PrimitiveType primativeType, CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer);

private:
	DECLARE_HEAP;

};



}


#endif // __TRIANGLE_H__