#include "Frameafx.h"
#include "RenderDataD3D9.h"
#include "MappingD3D9.h"

// HRESULT translation for Direct3D and other APIs 
#include <dxerr.h>

using namespace Engine;

DEFINE_HEAP(CRenderDataD3D9, "CRenderDataD3D9");
DEFINE_HEAP(CRenderDataD3D9::CRenderStateD3D9, "CRenderStateD3D9");

CRenderDataD3D9::CRenderDataD3D9()
{
	m_pID3D = 0;
	m_pID3DDevice = 0;
	m_winInst = 0;
	m_d3dInst = 0;
	m_hWnd = 0;
	m_deviceLost = false;

	SecureZeroMemory( &m_currentState, sizeof(CRenderStateD3D9) ); 
	SecureZeroMemory( &m_present, sizeof(D3DPRESENT_PARAMETERS) );
	SecureZeroMemory( &m_caps, sizeof(D3DCAPS9) );
}

CRenderDataD3D9::~CRenderDataD3D9()
{

}

void CRenderDataD3D9::CRenderStateD3D9::Initialise(IDirect3DDevice9* pID3DDevice, const CAlphaState* astate, const CCullState* cstate,
			const CDepthState* dstate, const COffsetState* ostate,
			const CStencilState* sstate, const CWireState* wstate)
{
	HRESULT hr = S_OK;

	// AlphaState
    m_alphaBlendEnabled = astate->m_alphaBlendEnabled ? TRUE : FALSE;
    m_alphaSrcBlend = D3D9AlphaSrcBlend[astate->m_srcBlend];
    m_alphaDstBlend = D3D9AlphaDstBlend[astate->m_dstBlend];
    m_alphaCompareEnabled = astate->m_compareEnabled ? TRUE : FALSE;
	m_compareFunction = D3D9AlphaCompare[astate->m_compareMode];
    m_alphaReference = (DWORD)(255.0f * astate->m_ref);
    m_blendColour = D3DCOLOR_COLORVALUE(
		astate->m_constantColour[0],
        astate->m_constantColour[1],
        astate->m_constantColour[2],
        astate->m_constantColour[3]);

    hr = pID3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, m_alphaBlendEnabled);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_SRCBLEND, m_alphaSrcBlend);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_DESTBLEND, m_alphaDstBlend);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_ALPHATESTENABLE, m_alphaCompareEnabled);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_ALPHAFUNC, m_compareFunction);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_ALPHAREF, m_alphaReference);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_BLENDFACTOR, m_blendColour);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));

    // CullState
	m_cullMode = cstate->m_cullEnabled ?
		D3D9CullOrder[cstate->m_ccOrder ? 1 : 0]	:
        D3DCULL_NONE;

    hr = pID3DDevice->SetRenderState(D3DRS_CULLMODE, m_cullMode);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") && 
        DXGetErrorString(hr));

    // DepthState
	m_depthEnabled = dstate->m_enabled ? D3DZB_TRUE : D3DZB_FALSE;
    m_depthCompareFunction = D3D9DepthCompare[dstate->m_compareMode];
	m_depthWriteEnabled = dstate->m_writable ? TRUE : FALSE;

    hr = pID3DDevice->SetRenderState(D3DRS_ZENABLE, m_depthEnabled);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_ZFUNC, m_depthCompareFunction);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_ZWRITEENABLE, m_depthWriteEnabled);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));

    // OffsetState
	if (ostate->m_fillEnabled)
    {
		m_slopeScaleDepthBias = *(DWORD*)&ostate->m_offsetScale;
		float bias = ostate->m_offsetBias / 16777216.0f;
        m_depthBias = *(DWORD*)&bias;
    }
    else
    {
        m_slopeScaleDepthBias = 0;
        m_depthBias = 0;
    }

    hr = pID3DDevice->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS,
        m_slopeScaleDepthBias);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_DEPTHBIAS, m_depthBias);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));

    // StencilState
	m_stencilEnabled = sstate->m_enabled ? TRUE : FALSE;
	m_stencilCompareFunction = D3D9StencilCompare[sstate->m_compare];
	m_stencilReference = (DWORD)sstate->m_reference;
	m_stencilMask = (DWORD)sstate->m_mask;
    m_stencilWriteMask = (DWORD)sstate->m_writeMask;
	m_stencilOnFail = D3D9StencilOperation[sstate->m_onFail];
    m_stencilOnZFail = D3D9StencilOperation[sstate->m_onZFail];
    m_stencilOnZPass = D3D9StencilOperation[sstate->m_onZPass];

    hr = pID3DDevice->SetRenderState(D3DRS_STENCILENABLE, m_stencilEnabled);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") && DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILFUNC, m_stencilCompareFunction);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILREF, m_stencilReference);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILMASK, m_stencilMask);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILWRITEMASK, m_stencilWriteMask);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILFAIL, m_stencilOnFail);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILZFAIL, m_stencilOnZFail);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));
    hr = pID3DDevice->SetRenderState(D3DRS_STENCILPASS, m_stencilOnZPass);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));

    // WireState
    m_fillMode = wstate->m_enable ? D3DFILL_WIREFRAME : D3DFILL_SOLID;

    hr = pID3DDevice->SetRenderState(D3DRS_FILLMODE, m_fillMode);
    assert(hr == D3D_OK && ieS("SetRenderState failed: %s\n") &&
        DXGetErrorString(hr));

}