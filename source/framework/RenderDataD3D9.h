#ifndef CRENDER_DATA_D3D9_H
#define CRENDER_DATA_D3D9_H

#include "FrameFwd.h"

// Direct3D9 includes
#include <d3d9.h>


namespace Engine
{

class CRenderDataD3D9
{
public:
	CRenderDataD3D9();
	~CRenderDataD3D9();

	// Maintain current render states to avoid redundant state changes.
	class CRenderStateD3D9
	{
	public:
		void Initialise (IDirect3DDevice9* pID3DDevice, const CAlphaState* astate, const CCullState* cstate,
			const CDepthState* dstate, const COffsetState* ostate,
			const CStencilState* sstate, const CWireState* wstate);

		// AlphaState
		DWORD	 m_alphaBlendEnabled;
		DWORD	 m_alphaSrcBlend;
		DWORD	 m_alphaDstBlend;
		DWORD	 m_alphaCompareEnabled;
		DWORD    m_compareFunction;
		DWORD	 m_alphaReference;
		D3DCOLOR m_blendColour;

		// CullState
		DWORD	 m_cullMode;

		// DepthState
		DWORD	 m_depthEnabled;
		DWORD	 m_depthWriteEnabled;
		DWORD	 m_depthCompareFunction;

		// OffsetState
		DWORD	 m_slopeScaleDepthBias;
		DWORD	 m_depthBias;

		// StencilState
		DWORD    m_stencilEnabled;
		DWORD    m_stencilCompareFunction;
		DWORD    m_stencilReference;
		DWORD    m_stencilMask;
		DWORD    m_stencilWriteMask;
		DWORD    m_stencilOnFail;
		DWORD    m_stencilOnZFail;
		DWORD    m_stencilOnZPass;

		// WireState
		DWORD	 m_fillMode;

	private:
		DECLARE_HEAP

	};

	CRenderStateD3D9 m_currentState;

	// Direct3d device
	IDirect3D9*	m_pID3D;
	IDirect3DDevice9* m_pID3DDevice;

	D3DPRESENT_PARAMETERS m_present;
	D3DCAPS9 m_caps;

	HINSTANCE m_winInst;
	HINSTANCE m_d3dInst;

	HWND m_hWnd;

	bool m_deviceLost;

private:
	DECLARE_HEAP

};

}


#endif