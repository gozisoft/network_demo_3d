#ifndef __CSPATIAL_H__
#define __CSPATIAL_H__

#include "FrameFwd.h"
#include "Matrix.h"
#include "Sphere.h"

namespace Engine
{

class CSpatial
{
public:
	typedef std::vector<ISceneControllerPtr> ISceneControllerVector;

	enum CullMode
	{
		// Determine visibility state by comparing the world bounding volume
		// to culling planes.
		CM_DYNAMIC,
		// Force the object to be culled.  If a Node is culled, its entire
		// subtree is culled.
		CM_ALWAYS,
		// Never cull the object.  If a Node is never culled, its entire
		// subtree is never culled.  To accomplish this, the first time such
		// a Node is encountered, the bNoCull parameter is set to 'true' in
		// the recursive chain GetVisibleSet/OnGetVisibleSet.
		CM_NEVER,
		NUM_CULL_MODES
	};

	virtual ~CSpatial();

	// update the node
	void Update(double deltaTime = 0, bool initiator = true);

	// Parent Access
	CSpatial* GetParent();
	void SetParent(CSpatial* pParent);

	// Controller Use.
	void UpdateControllers(double deltaTime);
	void AddController(ISceneControllerPtr pController);
	void RemoveController(ISceneControllerPtr pController);

	// Culling
	CullMode GetCullMode() const;
	void SetCullMode(CullMode mode);
	Spheref GetWorldBound() const;

	// Functions to set the positional information of the node.
	const Matrix4f& GetWorldTransform() const;
	Matrix4f& GetWorldTransform();

	// Local transform functions
	Matrix4f GetLocalTransform() const;
	void SetPosition(const Vector3f& pos);
	void SetRotation(const Vector3f& rot);
	void SetScale(const Vector3f& scale);
	const Vector3f& GetPostion() const;
	const Vector3f& GetRotation() const;
	const Vector3f& GetScale() const;

protected:
	CSpatial();

	// Pure abstract
	virtual void UpdateWorldBound() = 0;

	// Abstract
	virtual void UpdateWorldData(double deltaTime);
	
	// Internal
	void PropagateBoundToRoot();

	// List of animators
	ISceneControllerVector m_animators;

	// Bounding volume
	Spheref m_worldBound;
	bool m_worldBoundIsCurrent; 

	// absolute transform
	Matrix4f m_world;

	// Local transform
	Matrix4f m_local; 
	Vector3f m_localPostion;
	Vector3f m_localRotation;
	Vector3f m_localScale;

	// Culling mode
	CullMode m_cullMode;
	
	// Pointer to parent node
	CSpatial* m_pParent;

private:
	DECLARE_HEAP;

};


#include "Spatial.inl"


}


// void OnGetVisible(CCuller& culler, bool notCull);

#endif