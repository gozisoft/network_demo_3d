#ifndef __CBUFFER_INL__
#define __CBUFFER_INL__

inline const UInt8* CBuffer::GetData() const
{
	return &m_pData[0];
}

inline UInt CBuffer::GetBytesStride() const
{
	return m_stride;
}

inline UInt CBuffer::GetNumElements() const
{
	return m_count;
}

inline UInt CBuffer::GetBytesSize() const
{
	return m_pData.size();
}

inline CBuffer::BufferUse CBuffer::GetUsage() const
{
	return m_usage;
}

inline CBuffer::BufferType CBuffer::GetBufferType() const
{
	return m_bufferType;
}


#endif