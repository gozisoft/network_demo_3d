#include "Frameafx.h"
#include "IndexBufferD3D9.h"
#include "IndexBuffer.h"
#include "RendererD3D9.h"
#include "MappingD3D9.h"

// HRESULT translation for Direct3D and other APIs 
#include <dxerr.h>

using namespace Engine;

DEFINE_HEAP(CIndexBufferD3D9, "CIndexBufferD3D9");

CIndexBufferD3D9::CIndexBufferD3D9(IDirect3DDevice9* pDevice, CIndexBufferPtr pIBuffer) : 
m_pIBuffer(pIBuffer),
m_pBuffer(0)
{
	 D3DPOOL pool; 
	 DWORD usage = D3D9BufferUsage[ pIBuffer->GetUsage() ]; 
	
	 switch (usage)
	 {  
	 case D3DUSAGE_WRITEONLY:
		 pool = D3DPOOL_MANAGED; // D3DPOOL_MANAGED for static buffers
		 break;

	 case D3DUSAGE_DYNAMIC:
	 case D3DUSAGE_RENDERTARGET:
	 case D3DUSAGE_DEPTHSTENCIL:
		 pool = D3DPOOL_DEFAULT; // D3DPOOL_DEFAULT for dynamic buffers
		 break;

	 default:
		 pool = D3DPOOL_DEFAULT;
		 break;
	 } 

	 D3DFORMAT format = D3D9GetIndexFormat( pIBuffer->GetType() );

	 HRESULT hr = pDevice->CreateIndexBuffer( pIBuffer->GetBytesSize(),
		 D3D9BufferUsage[ pIBuffer->GetUsage() ], format, pool, &m_pBuffer, 0 );

	 if (FAILED(hr))
	 {
		 String msg = DXGetErrorString(hr);
		 assert( false && ieS("Unable to create d3d9 index buffer: ") && msg.c_str() );
	 }

	 hr = m_pBuffer->GetDesc(&m_buffDesc);
	 if (FAILED(hr))
	 {
		 String msg = DXGetErrorString(hr);
		 assert( false && ieS("Unable to get d3d9 index buffer descriptiom: ") && msg.c_str() );
	 }

	 void* data = Lock(CVertexBuffer::BL_WRITE_ONLY);
	 std::memcpy( data, pIBuffer->GetData(), pIBuffer->GetBytesSize() );
	 Unlock();

}

CIndexBufferD3D9::~CIndexBufferD3D9()
{
	SafeRelease(m_pBuffer);
}

// Vertex buffer operations.
void CIndexBufferD3D9::Enable(IDirect3DDevice9* pDevice)
{
	// Enable the buffer by setting the state.
    HRESULT hr = pDevice->SetIndices(m_pBuffer);

    if (FAILED(hr) )
	{
		String msg = DXGetErrorString(hr);
		assert( false && "Failed to enable index buffer: \n" && msg.c_str() );
	}        
}

void CIndexBufferD3D9::Disable(IDirect3DDevice9* pDevice)
{
	HRESULT hr = S_OK;

#if defined(DEBUG) || defined(_DEBUG)
    IDirect3DIndexBuffer9 *activeBuffer = 0;

    hr = pDevice->GetIndices(&activeBuffer);

	if ( FAILED(hr) )
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Failed to get stream source: ") && msg.c_str() );
	}

	if ( activeBuffer != m_pBuffer )
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Mismatched index buffers ") && msg.c_str() );
		SafeRelease(activeBuffer);
	}
#endif

    // Disable the buffer by clearing the state.
    hr = pDevice->SetIndices(0);

	if (FAILED(hr))
	{
		String msg = DXGetErrorString(hr);
		assert( false && msg.c_str() );
	}
}

void* CIndexBufferD3D9::Lock(CVertexBuffer::Locking mode)
{
	// D3DLOCK_NOOVERWRITE
	// D3DLOCK_DISCARD
	// used with dynamic index/vertex buffers, although they can also be used with dynamic textures.

	// D3DLOCK_READONLY
	// used with static index/vertex buffers. Indicates that your program will never write to the resource
    void *videoMemory = 0;
    HRESULT hr = m_pBuffer->Lock( 0, 0, &videoMemory, D3D9BufferLockingMode[mode] );
    
	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && ieS("Failed to lock vertex buffer: \n") && msg.c_str() );
	}

    return videoMemory;
}

void CIndexBufferD3D9::Unlock()
{
   HRESULT hr = m_pBuffer->Unlock();

   if ( FAILED(hr) )
   {
	   String msg = DXGetErrorDescription(hr);
	   assert( false && ieS("Failed to unlock vertex buffer: \n") && msg.c_str() );
   }
}

