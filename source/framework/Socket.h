#ifndef CSOCKET_H
#define CSOCKET_H

#include "framefwd.h"

#include <winsock2.h>

namespace Engine
{


	// Maintains an open socket as well as the ipaddress to that connection/
	// Furthermore it contains the packets for sending and recieving.
	// This also acts as a client socket if used in this form
	class CSocket
	{
	public:
		typedef std::vector<char> CharVector;
		typedef std::list<IPacketPtr> PacketList;
		typedef PacketList::iterator PacketItor;

		enum PacketConstraints
		{ 
			MAX_PACKET_SIZE = 256, 
			RECV_BUFFER_SIZE = (MAX_PACKET_SIZE * 512) 
		};

		enum DeleteFlags
		{
			DF_DESTROY = 1,
			DF_CLOSE = 2
		};

	public:
		CSocket();
		CSocket(SOCKET socket);
		virtual ~CSocket();

		void Send(IPacketPtr pkt, bool clearTimeOut = true);

		void SetDeleteFlag(DeleteFlags flag, bool enable);
		bool GetDeleteFlag(DeleteFlags flag) const;

		const PacketList& GetInList() const;

		SOCKET GetSocket() const;
		u_int GetTimeOut() const;

		bool HasOutput() const;

	protected:
		SOCKET m_socket;

		CharVector m_recvBuf;

		PacketList m_outList;	// packet list out 
		PacketList m_inList;	// packet list in

		int m_sendOfs;
		int m_timeOut;		// time in which a packet is sent

		// If deleteFlag has bit 2 set, exceptions only close the
		// socket and set to INVALID_SOCKET, and do not delete the NetSocket
		u_long m_deleteFlag;   

		bool m_binaryProtocol;

	private:
		DECLARE_HEAP;
	};


#include "socket.inl"



}



#endif 