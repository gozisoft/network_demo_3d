#ifndef __CSPATIAL_INL__
#define __CSPATIAL_INL__

inline CSpatial* CSpatial::GetParent()
{
	return m_pParent;
}

inline void CSpatial::SetParent(CSpatial* pParent)
{
	m_pParent = pParent;
}

inline const Matrix4f& CSpatial::GetWorldTransform() const
{
	return m_world;
}

inline Matrix4f& CSpatial::GetWorldTransform()
{
	return m_world;
}

inline void CSpatial::SetPosition(const Vector3f& pos)
{
	m_localPostion = pos;
}

inline void CSpatial::SetRotation(const Vector3f& rot)
{
	m_localRotation = rot;
}

inline void CSpatial::SetScale(const Vector3f& scale)
{
	m_localScale = scale;
}

inline Spheref CSpatial::GetWorldBound() const
{
	return m_worldBound;
}

inline CSpatial::CullMode CSpatial::GetCullMode() const
{
	return m_cullMode;
}

inline void CSpatial::SetCullMode(CullMode mode)
{
	m_cullMode = mode;
}

inline const Vector3f& CSpatial::GetPostion() const
{
	return m_localPostion;
}

inline const Vector3f& CSpatial::GetRotation() const
{
	return m_localRotation;
}

inline const Vector3f& CSpatial::GetScale() const
{
	return m_localScale;
}


#endif