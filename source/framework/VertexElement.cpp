#include "Frameafx.h"
#include "VertexElement.h"

using namespace Engine;

DEFINE_HEAP(CVertexElement, "CVertexElement");

CVertexElement::CVertexElement(UInt sourceIndex, UInt offset, VertexType type, VertexSemantic semantic, UInt index) :
m_streamIndex(sourceIndex),
m_stride(offset),
m_vertexType(type),
m_semantic(semantic),
m_semanticIndex(index)
{
}