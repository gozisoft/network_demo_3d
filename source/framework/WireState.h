#ifndef __CWIRE_STATE_H__
#define __CWIRE_STATE_H__

#include "Core.h"

namespace Engine
{


class CWireState
{
public:
	CWireState();
	~CWireState();

	bool m_enable; // default : false

private:
	DECLARE_HEAP;

};


}


#endif