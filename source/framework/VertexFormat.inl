#ifndef __CVERTEX_FORMAT_INL__
#define __CVERTEX_FORMAT_INL__

/*
inline const CVertexElement* CVertexFormat::GetElements() const
{
	CVertexElementPtr pElement = m_elements;
	return pElement.get();
}
*/

inline UInt CVertexFormat::GetElementCount() const
{
	return m_elements.size();
}

inline CVertexElementPtr CVertexFormat::GetElement(UInt index)
{
	if ( index < m_elements.size() )
	{
		return m_elements[index];
	}
	else
	{
		assert(!"Invalid index called for GetElement");
		return CVertexElementPtr();
	}
}

inline UInt	CVertexFormat::GetTotalStride() const
{
	return m_totalStride;
}

inline void CVertexFormat::SetEnabled(VertexSemantic semantic, bool enable)
{
	assert(semantic >= VS_POSITION && semantic <= VS_PSIZE);
	m_enableFlags[semantic] = enable;
}


inline bool CVertexFormat::IsEnabled(VertexSemantic semantic) const
{
	assert(semantic >= VS_POSITION && semantic <= VS_PSIZE);
	return m_enableFlags[semantic];
}



#endif