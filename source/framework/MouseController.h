#ifndef CMOUSE_CONTROLLER_H
#define CMOUSE_CONTROLLER_H

#include "IMouseController.h"

// Exclude rarely-used stuff from Windows headers
#define WIN32_LEAN_AND_MEAN		
#include <Windows.h>


namespace Engine
{

	class CMouseController : public IMouseController
	{
	public:
		CMouseController(const Point2l& wsize, HWND hwnd, bool windowed);
		~CMouseController();

		void SetVisible(bool visible);

		// Check if cursor is visible
		bool IsVisible() const;

		// Set new pos of cursor
		void SetMousePos(const Point2l& pos);

		// Sets the relative pos 
		// Value lies between [0.0, 1.0]
		// where (0.0f, 0.0f) is the top left corner and
		// (1.0f, 1.0f) is the bottom right corner of the render window.
		void SetRelativePos(const Point2f& pos);

		// Get pos of coursor
		// Piexl Pos of the mouse. ie pos based upon entire monitor area
		const Point2l& GetMousePos();

		// Pos of mouse relative to the size of the active window.
		// Value lies between [0.0, 1.0]
		// where (0.0f, 0.0f) is the top left corner and
		// (1.0f, 1.0f) is the bottom right corner of the render window.
		Point2f GetRelativeMousePos();

		// retrieve the stored client screen size
		Point2l GetClientSize() const;

		// Notify on window resize
		void OnResize(const Point2l& size);
		
		// Notifies cursur on that resizable sttings have changed
		void UpdateBorderSize(bool windowed, bool resizable);

		// Set the current window handle on window input
		void SetHWND(HWND hwnd);

	private:
		// update internal cursor position
		void UpdateCursorPos();

		Point2l m_cursorPos;

		Point2f m_invClientSize;
		Point2l m_winSize;

		Int32 m_borderX;
		Int32 m_borderY;

		bool m_isVisible;

		HWND m_hwnd;

	private:
		DECLARE_HEAP;

	};


#include "MouseController.inl"


} // namespace Engine




#endif