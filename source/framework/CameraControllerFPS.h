#ifndef CCAMERA_CONTROLLER_FPS_H
#define CCAMERA_CONTROLLER_FPS_H

#include "ISceneController.h"
#include "Vector.h"

namespace Engine
{

class CCameraControllerFPS : public ISceneControllerFPS
{
public:
	typedef std::vector<SKeyMap> KeyMapVector;
	

	CCameraControllerFPS( IMouseControllerPtr pMouse, float roateSpeed=0.01f, float moveSpeed=0.008f, SKeyMap* pKeyMapVector=0,
		UInt KeyMapSize=0, bool freeLook=true, bool centerMouse=false, bool invert=true );

	virtual ~CCameraControllerFPS();

	// function moves the camera by given speed.
	virtual void AnimateNode(CSpatial* pSpatial = 0, double timeMS = 0);

	// Event reciever
	virtual bool HandleEvent(const IEventData& event);

	// units per millisecond
	virtual float GetMoveSpeed() const; 
	virtual void SetMoveSpeed(float speed);

	virtual float GetRotateSpeed() const;
	virtual void SetRotateSpeed(float speed);

	virtual void setVerticalMovement(bool allow);
	virtual void SetInverseMouse(bool invert);

	// Used to set a custom keymap
	virtual void SetKeyMap(SKeyMap *pKeyMap, UInt size);
	virtual void SetKeyMap(const KeyMapVector& keyMap);

	// virtual SceneControllerType GetType() const;

private:
	void SetAllKeysUp();
	void SetAllButtonsUp();

	IMouseControllerPtr m_pMouse;

	KeyMapVector m_keyMap;

	// Previous frame mouse pos
	Point2l m_lastCursorPos;

	// Change in mouse pos, floating point
	Point2f m_mouseDelta;

	// Speed of the mouse rotation after smoothing
	Point2f m_rotVelocity;

	// Azimoth of the mouse, accumlated through the frames
	float m_pitch;
	float m_yaw;

	float m_maxVertAngle;

	float m_rotateSpeed;	
	float m_moveSpeed;
	float m_mouseYDirection; // -1.0f for inverted, 1.0f for default

	// Check on each animation
	bool m_cursorkeys[KA_COUNT];
	bool m_mouseButtons[MA_COUNT];

	bool m_vertMovement; // allow vertical movement
	bool m_firstUpdate;
	bool m_centerMouse;

private:
	DECLARE_HEAP;

};



#include "CameraControllerFPS.inl"


}

#endif