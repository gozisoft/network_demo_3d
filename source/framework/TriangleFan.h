#ifndef __CTRIANGLE_FAN_H__
#define __CTRIANGLE_FAN_H__

#include "Triangle.h"
#include "IndexBuffer.h"

namespace Engine
{

class CTriFan : public CTriangles
{
public:
	CTriFan(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, IndexType indextType);
	CTriFan(CVertexFormatPtr pVFormat,CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer);
	~CTriFan();

	UInt GetTriangleCount() const;
	bool GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const;

private:
	DECLARE_HEAP;

};


#include "TriangleFan.inl"

}

#endif