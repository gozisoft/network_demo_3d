#include "frameafx.h"
#include "Culler.h"
#include "SceneNode.h"
#include "RootNode.h"
#include "Sphere.h"
#include "Intersections.h"


using namespace Engine;

DEFINE_HEAP(CCuller, "CCuller");

CCuller::CCuller(CCameraPtr pCamera) : 
m_pCamera(pCamera),
m_planeStates(0)
{

}

CCuller::~CCuller()
{
	m_visibleSet.clear();
}

void CCuller::SetFrustum(const float* frustum)
{
	if (!m_pCamera)
	{
		assert( false && ieS("requires valid camera for frustum culling") );
		return;
	}

	// copy fustrum values
	std::memcpy( &m_frustum[0], &frustum[0], sizeof(float) * CCamera::NUM_PLANES );

    float near2 = Sqr(m_frustum[CCamera::S_NEAR]);
    float bottom2 = Sqr(m_frustum[CCamera::S_BOTTOM]);
    float top2 = Sqr(m_frustum[CCamera::S_TOP]);
    float left2 = Sqr(m_frustum[CCamera::S_LEFT]);
    float right2 = Sqr(m_frustum[CCamera::S_RIGHT]);

    // Get the camera coordinate frame.
    Vector3f position = m_pCamera->GetPosition();
	Vector3f forward = m_pCamera->GetFVector();
    Vector3f up		 = m_pCamera->GetUVector();
    Vector3f right	 = m_pCamera->GetRVector();
    float dirDotEye  = Dot(position, forward); // viewing angle

    // Update the near plane.
    m_plane[CCamera::S_NEAR].Set( forward, (dirDotEye + m_frustum[CCamera::S_NEAR]) );

    // Update the far plane.
    m_plane[CCamera::S_FAR].Set( -forward, -(dirDotEye + m_frustum[CCamera::S_FAR]) );

    // Update the bottom plane
	float invLength = 1 / std::sqrt(near2 + bottom2);
    float c0 = -m_frustum[CCamera::S_BOTTOM] * invLength;  // D component
    float c1 = +m_frustum[CCamera::S_NEAR] * invLength;	 // U component
    Vector3f normal = c0 * forward + c1 * up;
    m_plane[CCamera::S_BOTTOM].Set( normal, Dot(position, normal) );

    // Update the top plane.
    invLength = 1 / std::sqrt(near2 + top2);
    c0 = +m_frustum[CCamera::S_TOP] * invLength;  // D component
    c1 = -m_frustum[CCamera::S_NEAR] * invLength;  // U component
    normal = c0 * forward + c1 * up;
    m_plane[CCamera::S_TOP].Set( normal, Dot(position, normal) );

    // Update the left plane.
    invLength = 1 / std::sqrt(near2 + left2);
    c0 = -m_frustum[CCamera::S_LEFT] * invLength;  // D component
    c1 = +m_frustum[CCamera::S_NEAR] * invLength;  // R component
    normal = c0 * forward + c1* right;
    m_plane[CCamera::S_LEFT].Set( normal, Dot(position, normal) );

    // Update the right plane.
    invLength = 1 / std::sqrt(near2 + right2);
    c0 = +m_frustum[CCamera::S_RIGHT] * invLength;  // D component
    c1 = -m_frustum[CCamera::S_NEAR] * invLength;  // R component
    normal = c0 * forward + c1 * right;
    m_plane[CCamera::S_RIGHT].Set( normal, Dot(position, normal) );

    // All planes are active initially.
    m_planeStates = 0xFF;
}

void CCuller::SetFrustum(const Matrix4f& viewMatrix, const Matrix4f& projMatrix)
{
   // Extracts the view frustum clipping planes from the combined
    // view-projection matrix in world space. The extracted planes will
    // have their normals pointing towards the inside of the view frustum.
    //
    // References:
    //  Gil Gribb, Klaus Hartmann, "Fast Extraction of Viewing Frustum
    //  Planes from the World-View-Projection Matrix,"
    //  http://crazyjoke.free.fr/doc/3D/plane%20extraction.pdf

    Matrix4f m(viewMatrix * projMatrix);
    Planef *pPlane = 0;

    // Left clipping plane. (col3 + col0)
	pPlane = &m_plane[CCamera::S_LEFT];
    pPlane->m_normal = Vector3f( m(0,3) + m(0,0), m(1,3) + m(1,0), m(2,3) + m(2,0) );
	pPlane->m_disOrigin = m(3,3) + m(3,0);
	pPlane->PlaneNormalise();

    // Right clipping plane. (col3 - col0)
    pPlane = &m_plane[CCamera::S_RIGHT];
    pPlane->m_normal = Vector3f( m(0,3) - m(0,0), m(1,3) - m(1,0), m(2,3) - m(2,0) );
	pPlane->m_disOrigin = m(3,3) - m(3,0);
    pPlane->PlaneNormalise();

    // Top clipping plane. (col3 - col1)
    pPlane = &m_plane[CCamera::S_TOP];
    pPlane->m_normal = Vector3f( m(0,3) - m(0,1), m(1,3) - m(1,1), m(2,3) - m(2,1) );
	pPlane->m_disOrigin = m(3,3) - m(3,1);
	pPlane->PlaneNormalise();

    // Bottom clipping plane. (col3 + col1)
    pPlane = &m_plane[CCamera::S_BOTTOM];
    pPlane->m_normal = Vector3f( m(0,3) + m(0,1), m(1,3) + m(1,1), m(2,3) + m(2,1) );
	pPlane->m_disOrigin = m(3,3) + m(3,1);
	pPlane->PlaneNormalise();

	switch( m_pCamera->GetDepthType() )
	{
	case CCamera::DT_ZERO_TO_ONE:
		{
			// Near clipping plane. (col2)
			pPlane = &m_plane[CCamera::S_NEAR];
			pPlane->m_normal = Vector3f( m(0,2), m(1,2), m(2,2) );
			pPlane->m_disOrigin = m(3,2);
			pPlane->PlaneNormalise();
		}
		break;

	case CCamera::DT_MINUS_ONE_TO_ONE:
		{
			// Near clipping plane. (col3 + col2)
			pPlane = &m_plane[CCamera::S_NEAR];
			pPlane->m_normal = Vector3f( m(0,3) + m(0,2), m(1,3) + m(1,2), m(2,3) + m(2,2) );
			pPlane->m_disOrigin = m(3,3) + m(3,2);
			pPlane->PlaneNormalise();
		}
		break;
	};

	// Far clipping plane. (col3 - col2)
	pPlane = &m_plane[CCamera::S_FAR];
	pPlane->m_normal = Vector3f( m(0,3) - m(0,2), m(1,3) - m(1,2), m(2,3) - m(2,2) );
	pPlane->m_disOrigin = m(3,3) - m(3,2);
    pPlane->PlaneNormalise();

}

bool CCuller::IsVisible(const Spheref& volume)
{
	// If radius is zero, there is no object visible
	if ( IsZero( volume.m_radius ) )
		return false;

	UInt32 index = CCamera::NUM_PLANES - 1;
	UInt32 mask = (1 << index);
	// Access the array of planes in revese as the far plane
	// is normally the most frequent true result
	for (UInt32 i = 0; i < CCamera::NUM_PLANES; ++i)
	{
		float val = PlaneDotCoord( m_plane[i], volume.m_center );

		if (val <= -volume.m_radius)
		{
			// Object on negative side of plane
			// so cull it.

			//OStringStream ss;
			//ss << ieS("Object culled: ") << i << std::endl;
			//OutputDebugString( String(ss.str()).c_str() );

			return false;
		}

		if (val >= volume.m_radius)
		{
			// Object within positive side of plane.
			// No need to compare subobjects against this plane.
			m_planeStates &= ~mask;
		}
	}

	// object within frustum
	return true;
}

void CCuller::ComputeVisibleSet(CRootNodePtr pRootNode)
{
	if (m_pCamera && pRootNode)
	{
		// Update the frustum.
		SetFrustum( m_pCamera->GetFrustum() );	// SetFrustum( m_pCamera->GetViewMatrix(), m_pCamera->GetProjectionMatrix() );
		// Clear the list of visible objects.
		m_visibleSet.clear(); 
		// Build a new list of visible objcets.
		OnGetVisible(pRootNode, false);
	}
	else
		assert(false && ieS("Valid Camera and scene required to use culling.\n") );

}

void CCuller::OnGetVisible(CRootNodePtr pRootNode, bool notCull)
{
	CSpatial::CullMode cullMode = pRootNode->GetCullMode();

	if (cullMode == CSpatial::CM_ALWAYS)
		return;
	
	if (cullMode == CSpatial::CM_NEVER)
		notCull = true;

	// Apply culling testing against "test worthy" planes
	if ( notCull || IsVisible( pRootNode->GetWorldBound() ) )
	{
		pRootNode->GetVisible(*this, notCull);
	}
}

void CCuller::OnGetVisible(CSceneNodePtr pSceneNode, bool notCull)
{
	CSpatial::CullMode cullMode = pSceneNode->GetCullMode();

	if (cullMode == CSpatial::CM_ALWAYS)
		return;
	
	if (cullMode == CSpatial::CM_NEVER)
		notCull = true;

	// save plane states
	UInt8 planeStates = GetPlaneStates();

	// Apply culling testing against "test worthy" planes
	if ( notCull || IsVisible( pSceneNode->GetWorldBound() ) )
	{
		pSceneNode->GetVisible(*this, notCull);
		CSceneNode *pNode = pSceneNode.get();
		m_visibleSet.push_back(pNode);
	}
	
	// apply saved plane states
	SetPlaneStaets(planeStates);
}