#ifndef __CCULLER_INL__
#define __CCULLER_INL__

inline const float* CCuller::GetFrustum() const
{
	return &m_frustum[0];
}

inline const std::vector<CSceneNode*>& CCuller::GetVisibleArray() const
{
	return m_visibleSet;
}


inline size_t CCuller::GetVisibleCount() const
{
	return m_visibleSet.size();
}

inline UInt8 CCuller::GetPlaneStates() const
{
	return m_planeStates;
}

inline void CCuller::SetPlaneStaets(UInt8 states)
{
	m_planeStates = states;
}


#endif