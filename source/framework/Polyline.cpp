#include "frameafx.h"
#include "Polyline.h"
#include "VertexBuffer.h"

using namespace Engine;


DEFINE_HIERARCHICALHEAP(CPolyline, "CPolyline", "CSceneNode");


CPolyline::CPolyline(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, bool contiguous) :
CSceneNode( contiguous ? PT_POLYLINE_OPEN : PT_POLYLINE_CLOSED, pVFormat, pVBuffer, CIndexBufferPtr() ),
m_numLines(0),
m_contiguous(contiguous)
{
	UInt numVertices = m_pVBuffer->GetNumElements();
	assert( numVertices >= 2 && ieS("Polylines must have at least 2 vertex points \n") );

	if (m_contiguous)
	{
		m_numLines = numVertices - 1;
	}
	else
	{
		assert( numVertices & 1 && ieS("Disconnected lines require an even number of vertices.\n") );
		m_numLines = numVertices / 2;
	}
}

CPolyline::~CPolyline()
{

}

UInt CPolyline::GetMaxNumLines() const
{
	UInt numVertices = m_pVBuffer->GetNumElements();
	return m_contiguous ? numVertices - 1 : numVertices / 2;
}


void CPolyline::SetNumLines(UInt numLines)
{
	UInt numVertices = m_pVBuffer->GetNumElements();
	if (m_contiguous)
	{
		UInt numVertices1 = numVertices - 1;
		// Number of lines greator than 0
		// Number of lines <= number of vertices
		if (numLines > 0 && numVertices1 > numLines) 
			m_numLines = numLines;
		else
			m_numLines = numVertices1;
	}
	else
	{
		UInt numVertices2 = numVertices / 2;
		// Number of lines greator than 0
		// Number of lines <= number of vertices
		if (numLines > 0 && numVertices2 > numLines) 
			m_numLines = numLines;
		else
			m_numLines = numVertices2;
	}

}