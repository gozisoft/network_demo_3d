#ifndef __CPACKET_INL__
#define __CPACKET_INL__
	

inline const char* CBinaryPacket::GetData() const 
{ 
	return &m_pData[0]; 
}

inline char* CBinaryPacket::GetData() 
{ 
	return const_cast<char*>( static_cast<const CBinaryPacket&>(*this).GetData() );
}

inline const char* CSafeBinaryPacket::GetData() const
{
	return &m_pData[0]; 
}

inline char* CSafeBinaryPacket::GetData()
{
	return const_cast<char*>( static_cast<const CSafeBinaryPacket&>(*this).GetData() );
}








#endif