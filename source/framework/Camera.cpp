#include "Frameafx.h"
#include "Camera.h"
#include "Const.h"


using namespace Engine;

DEFINE_HEAP(CCamera, "CCamera");

/*
#if defined(USING_DIRECT3D9)
CCamera::DepthType CCamera::m_defaultDepthType = DT_ZERO_TO_ONE;
#endif
*/

#if defined(USING_OPENGL) || defined(_USING_OPENGL)
CCamera::DepthType CCamera::m_defaultDepthType = DT_MINUS_ONE_TO_ONE;
#endif

CCamera::CCamera() :
m_depthType(m_defaultDepthType),
m_maxVelocity(1.0f),
m_maxPitch( 88.0f * Const<float>::TO_RAD() ),
m_pitch(0.0f),
m_yaw(0.0f)
{
	std::memset( &m_frustum[0], 0, sizeof(float)*sizeof(m_frustum) );
	SetFrame(Vector3f::ZERO, Vector3f::UNIT_Y, Vector3f::UNIT_X, Vector3f::UNIT_Z);
}

CCamera::~CCamera()
{

}

void CCamera::SetCamera(const Vector3f& pos, const Vector3f& upVector, const Vector3f& lookat)
{
	// determine the zaxis from the lookat
	Vector3f zaxis = Normalised( lookat - pos );
	Vector3f xaxis = Normalised( Cross(upVector, zaxis) );
	Vector3f yaxis = Cross(zaxis, xaxis);

	SetFrame(pos, upVector, xaxis, zaxis);
}

void CCamera::SetAxis(const Vector3f& upVector, const Vector3f& rightVector,
	const Vector3f& lookVector)
{
	m_yaxis = upVector;
	m_xaxis = rightVector;
	m_zaxis = lookVector;

	const float epsilon = 0.001f;
	float det = Dot( m_xaxis, Cross(m_yaxis, m_zaxis) );
	if (Abs(1.0f - det) > epsilon)
	{
		OrthoNormaliseVectors(m_zaxis, m_yaxis, m_xaxis);
	}

	BuildViewMatrix();
}


void CCamera::SetFrustum(float upFovDegrees, float aspectRatio, float zNear, float zFar)
{
	// aspectRatio = width / height
	// f = cotangent(fov / 2)
	float halfAngleRadians	= 0.5f * ( upFovDegrees * Const<float>::TO_RAD() ); 
	m_frustum[S_TOP]	= zNear * tan(halfAngleRadians);
	m_frustum[S_RIGHT]	= aspectRatio * m_frustum[S_TOP];
	m_frustum[S_BOTTOM] = -m_frustum[S_TOP];
	m_frustum[S_LEFT]	= -m_frustum[S_RIGHT];
	m_frustum[S_NEAR]	= zNear;
	m_frustum[S_FAR]	= zFar;

	OnFrustrumChange();
}

void CCamera::Update()
{
  // Cap velocity to max velocity
    if ( Mag(m_velocity) > m_maxVelocity )
    {
		m_velocity = m_velocity.Normalise() * m_maxVelocity;
    }

    // Move the camera
    m_pos += m_velocity;
    // Could decelerate here. I'll just stop completely.
    m_velocity = Vector3f( 0.0f, 0.0f, 0.0f );
    m_lookat = m_pos + m_xaxis;
    
    // Calculate the new view matrix
    Vector3f up = Vector3f( 0.0f, 1.0f, 0.0f );
	SetCamera(m_pos, up, m_lookat);

    // Calculate yaw and pitch
    float lookLengthOnXZ = sqrtf( m_zaxis.z() * m_zaxis.z() + m_zaxis.x() * m_zaxis.x() );
    m_pitch = atan2f( m_zaxis.y(), lookLengthOnXZ );
    m_yaw   = atan2f( m_zaxis.x(), m_zaxis.z() );
}

void CCamera::MoveForward(float units)
{
	m_velocity += m_zaxis * units;
}

// Move the camera across left and right
void CCamera::Strafe(float units)
{
	m_velocity += m_xaxis * units;
}

// Move camera up and down
void CCamera::MoveUp(float units)
{
	m_pos.y() += units;
}

void CCamera::Yaw(float rads)
{
	if (rads == 0.0f)
		return;

	Matrix4f rotation;
	rotation.SetRotationAxis(m_yaxis, rads);
	rotation.TransformVectNormal(m_xaxis);
	rotation.TransformVectNormal(m_zaxis);
}

void CCamera::Pitch(float rads)
{
	if (rads == 0.0f)
		return;

    m_pitch -= rads;
    if ( m_pitch > m_maxPitch )
    {
        rads += m_pitch - m_maxPitch;
    }
    else if ( m_pitch < -m_maxPitch )
    {
        rads += m_pitch + m_maxPitch;
    }

	Matrix4f rotation;
	rotation.SetRotationAxis(m_xaxis, rads);
	rotation.TransformVectNormal(m_yaxis);
	rotation.TransformVectNormal(m_zaxis);
}

void CCamera::Roll(float rads)
{

}

// Build the view matrix LH 
void CCamera::BuildViewMatrix()
{
	m_viewMatrix(0,0) = m_xaxis.x();
	m_viewMatrix(0,1) = m_yaxis.x();
	m_viewMatrix(0,2) = m_zaxis.x();
	m_viewMatrix(0,3) = 0.0f;
	m_viewMatrix(1,0) = m_xaxis.y();
	m_viewMatrix(1,1) = m_yaxis.y();
	m_viewMatrix(1,2) = m_zaxis.y();
	m_viewMatrix(1,3) = 0.0f;
	m_viewMatrix(2,0) = m_xaxis.z();
	m_viewMatrix(2,1) = m_yaxis.z();
	m_viewMatrix(2,2) = m_zaxis.z();
	m_viewMatrix(2,3) = 0.0f;
	m_viewMatrix(3,0) = -Dot(m_pos, m_xaxis);
	m_viewMatrix(3,1) = -Dot(m_pos, m_yaxis);
	m_viewMatrix(3,2) = -Dot(m_pos, m_zaxis);
	m_viewMatrix(3,3) = 1.0f;

	UpdatePVMatrix();
}

// Directx http://www.codeguru.com/cpp/misc/misc/math/article.php/c10123__3/
void CCamera::OnFrustrumChange()
{
	Matrix4f& proj = m_projectionMatrix[m_depthType];

	switch (m_depthType)
	{
	case (DT_ZERO_TO_ONE):
		{
			proj = MatrixPerspectiveNLH( m_frustum[S_LEFT], m_frustum[S_RIGHT], m_frustum[S_TOP],
				m_frustum[S_BOTTOM], m_frustum[S_NEAR], m_frustum[S_FAR] );
		}
		break;
	case (DT_MINUS_ONE_TO_ONE):
		{
			proj = MatrixPerspectiveLH( m_frustum[S_LEFT], m_frustum[S_RIGHT], m_frustum[S_TOP],
				m_frustum[S_BOTTOM], m_frustum[S_NEAR], m_frustum[S_FAR] );
		}
		break;
	}

	UpdatePVMatrix();
}	


void CCamera::UpdatePVMatrix()
{
	Matrix4f& pMatrix = m_projectionMatrix[m_depthType];
	Matrix4f& pvMatrix = m_projectionViewMatrix[m_depthType];

	pvMatrix = pMatrix * m_viewMatrix;
}



/*



*/



/*

void CCamera::OnFrustrumChange()
{
	switch (m_depthType)
	{
	case DT_ZERO_TO_ONE:
		{
			Matrix4f& proj = m_projectionMatrix[DT_ZERO_TO_ONE];

			proj(0,0) = 2.0f * m_frustum[S_NEAR] / m_frustum[S_RIGHT] - m_frustum[S_LEFT];
			proj(0,1) = 0.0f;
			proj(0,2) = -(m_frustum[S_RIGHT] + m_frustum[S_LEFT] / m_frustum[S_RIGHT] - m_frustum[S_LEFT]);
			proj(0,3) = 0.0f;

			proj(1,0) = 0.0f;
			proj(1,1) = 2.0f * m_frustum[S_NEAR] / m_frustum[S_TOP] - m_frustum[S_BOTTOM];
			proj(1,2) = -(m_frustum[S_TOP] + m_frustum[S_BOTTOM] / m_frustum[S_TOP] - m_frustum[S_BOTTOM]);
			proj(1,3) = 0.0f;

			proj(2,0) = 0.0f;
			proj(2,1) = 0.0f;
			proj(2,2) = m_frustum[S_FAR] / m_frustum[S_FAR] - m_frustum[S_NEAR];
			proj(2,3) = -(m_frustum[S_NEAR] * m_frustum[S_FAR] / m_frustum[S_FAR] - m_frustum[S_NEAR]);

			proj(3,0) = 0.0f;
			proj(3,1) = 0.0f;
			proj(3,2) = 1.0f;
			proj(3,3) = 0.0f;
		}
		break;

	case DT_MINUS_ONE_TO_ONE:
		{
			Matrix4f& proj = m_projectionMatrix[DT_MINUS_ONE_TO_ONE];

			proj(0,0) = 2.0f * m_frustum[S_NEAR] / m_frustum[S_RIGHT] - m_frustum[S_LEFT];
			proj(0,1) = 0.0f;
			proj(0,2) = -(m_frustum[S_RIGHT] + m_frustum[S_LEFT] / m_frustum[S_RIGHT] - m_frustum[S_LEFT]);
			proj(0,3) = 0.0f;

			proj(1,0) = 0.0f;
			proj(1,1) = 2.0f * m_frustum[S_NEAR] / m_frustum[S_TOP] - m_frustum[S_BOTTOM];
			proj(1,2) = -(m_frustum[S_TOP] + m_frustum[S_BOTTOM] / m_frustum[S_TOP] - m_frustum[S_BOTTOM]);
			proj(1,3) = 0.0f;

			proj(2,0) = 0.0f;
			proj(2,1) = 0.0f;
			proj(2,2) = m_frustum[S_FAR] + m_frustum[S_NEAR] / m_frustum[S_FAR] - m_frustum[S_NEAR];
			proj(2,3) = -(2 * m_frustum[S_NEAR] * m_frustum[S_FAR] / m_frustum[S_FAR] - m_frustum[S_NEAR]);

			proj(3,0) = 0.0f;
			proj(3,1) = 0.0f;
			proj(3,2) = 1.0f;
			proj(3,3) = 0.0f;
		}
		break;
	};

	UpdatePVMatrix();
}	

*/