#include "frameafx.h"
#include "WinNet.h"
#include "Packet.h"
#include "StringFunctions.h"

#include <WS2tcpip.h>

using std::list;
using std::vector;



_ENGINE_BEGIN



namespace TCP
{


	// Creates a TCP socket and sets it up to listen
	// By default its is non-blocking
	int NETListenSock(SOCKET& newSocket, const String& protNum, bool block)
	{
		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(ADDRINFOT) );
		hints.ai_family = AF_INET;		// Use AF_UNSPEC for IPv4 or IPv6 compatibility
		hints.ai_socktype = SOCK_STREAM;	// Specify TCP usage
		hints.ai_protocol = IPPROTO_TCP;	// Specify TCP usage
		hints.ai_flags = AI_PASSIVE;		// The socket address will be used in a call to the bind function.

		ADDRINFOT *pResult = 0;
		GetAddrInfo( NULL, protNum.c_str(), &hints, &pResult );

		bool optVal = true;
		int optLen = sizeof(bool);

		for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
		{
			// Create the socket
			newSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol); 

			if (newSocket == INVALID_SOCKET)
			{
				Cout << ieS("Creating socket TCP failed") << std::endl;
				continue; // skip this loop and go to ai_next
			}

			// Allows other sockets to bind() to this port, unless there is an
			// active listening socket bound to the port already. This enables
			// you to get around those "Address already in use" error messages
			// when you try to restart your server after a crash.
			if ( setsockopt( newSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal, optLen ) == SOCKET_ERROR ) 
			{
				Cout << ieS("setsockopt has TCP failed") << std::endl;
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			// Bind to port
			// Before sending and receiving data using a socket, 
			// it must first be associated with a local source port
			// and a network interface address.
			if ( bind(newSocket, pResult->ai_addr, (int)pResult->ai_addrlen) == SOCKET_ERROR )
			{
				Cout << ieS("Could not bind TCP socket") << std::endl;
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			break;
		}

		// all done with this structure
		FreeAddrInfo(pResult); 

		if (newSocket == INVALID_SOCKET)
		{
			Cout << ieS("Unable to create socket also failed to bind socket") << std::endl;
			assert(0);
			return 1;
		}

		// set nonblocking - accept() blocks under some odd circumstances otherwise
		NETSetBlocking(newSocket, block);

		// start listening
		if ( listen(newSocket, SOMAXCONN) == SOCKET_ERROR ) 
		{
			closesocket(newSocket);
			newSocket = INVALID_SOCKET;
			assert(0);
			return 1;
		}

		Cout << ieS("Listening on socket") << std::endl;

		return 0;
	}

	// Create a TCP socket and attemp to connect to a server ib the
	// specificed IP address and Port number.
	// if coalesce is false Nagle algorithim is turned off : socket is set to not TCP_NODELAY
	int NETConnectSock(SOCKET& newSocket, const String& ip, const String& port, bool coalesce)
	{
		bool optVal = TRUE;
		int optLen = sizeof(bool);

		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(ADDRINFOT) );
		hints.ai_family = AF_UNSPEC; 		// Use AF_UNSPEC for IPv4 or IPv6 compatibility
		hints.ai_socktype = SOCK_STREAM;	// Specify TCP usage
		hints.ai_protocol = IPPROTO_TCP;	// Specify TCP usage

		ADDRINFOT* pResult = 0;
		if ( GetAddrInfo( ip.c_str(), port.c_str(), &hints, &pResult ) )
		{
			Cout << ieS("Could not call GetAddrInfo().") << WSAGetLastError() << std::endl;
			NETPrintError();
			return 1;
		}

		for (ADDRINFOT* p = pResult; p != NULL; p = pResult->ai_next)
		{
			// Create a SOCKET for connecting to server
			newSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol); 

			if (newSocket == INVALID_SOCKET)
			{
				Cout << ieS("Creating socket TCP failed.") << WSAGetLastError() << std::endl;
				NETPrintError();
				continue; // skip this loop and go to ai_next
			}

			// if coalesce is false Nagle algorithim is turned off
			if (!coalesce)
			{
				if ( setsockopt( newSocket, IPPROTO_TCP, TCP_NODELAY, (char *)&optVal, optLen ) == SOCKET_ERROR )
				{
					Cout << ieS("Could not set TCP_NODELAY with setsockopt.") << std::endl;
					NETPrintError();
					closesocket(newSocket);
					newSocket = INVALID_SOCKET;
					continue;
				}
			}

			// This function establishes an Internet connection with the server at socket address address
			if ( connect(newSocket, p->ai_addr, (int)p->ai_addrlen) == SOCKET_ERROR )
			{
				// Free up used resources
				Cout << ieS("Could not connect TCP socket: ") << WSAGetLastError() << std::endl;
				NETPrintError();
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			break;
		} 

		// Free up used resources
		FreeAddrInfo(pResult);

		if (newSocket == INVALID_SOCKET)
		{
			Cout << ieS("Socket Invalid.") << std::endl;
			NETPrintError();
			return 1;
		}
		Cout << ieS("Connnected to: ") << std::endl; 
		Cout << ieS("IP: ") << ip << std::endl; 
		Cout << ieS("Port: ") << port << std::endl;  

		return 0;
	}

	// Accept a connection over TCP and return the new socket
	SOCKET NETAcceptConnection(String& IPaddress, String& port, SOCKET listenSocket)
	{
		SOCKADDR_STORAGE sockStorage;
		std::memset( &sockStorage, 0, sizeof(SOCKADDR_STORAGE) );
		socklen_t size = sizeof(SOCKADDR_STORAGE);

		// Accept the connection using blank sockStorage
		SOCKET new_sock = accept(listenSocket, (SOCKADDR *)&sockStorage, &size);
		if (new_sock == INVALID_SOCKET)
			return INVALID_SOCKET;

		// Use custom inet_ntop() to get string address
		size_t stringSize = ( (sockStorage.ss_family == AF_INET) ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN );
		Char* pIPStr = new Char[stringSize];

		// NETInetNtop(sockStorage.ss_family, (SOCKADDR *)&sockStorage, pIPStr, stringSize);
		NETGetIpStr( (SOCKADDR *)&sockStorage, pIPStr, stringSize );

		// Copy string over and delete the old one
		IPaddress = pIPStr;
		SafeDeleteArray(pIPStr);

		// Use custom function to get port number
		NETGetPortStr( (SOCKADDR *)&sockStorage, port );

		// Set socketopertion to SO_DONTLINGER:
		// Does not block close waiting for unsent data to be sent.
		// Setting this option is equivalent to setting SO_LINGER with l_onoff set to zero.
		int x = 1;
		setsockopt(new_sock, SOL_SOCKET, SO_DONTLINGER, (char *)&x, sizeof(x));

		// Output the IP Address for clarity.
		//cock cock cock cock cock cock cock
		OStringStream oss;
		oss << ieS("Accepted IP: ") << IPaddress << std::endl;
		oss << ieS("Accepted Port: ") << port << std::endl;
		Cout << oss.str() << std::endl;

		return new_sock;
	}

	int NETHandleOutput(list<IPacketPtr>& outList, SOCKET openSocket, int sendOfs)
	{
		bool sent = false;
		int errorFlag = 0;

		do
		{
			// assert( !outList.empty() );
			char* pData = 0;
			u_long length = 0;

			if ( !outList.empty() )
			{
				list<IPacketPtr>::iterator itor = outList.begin();
				IPacketPtr pIPacket = *itor;
				pData = pIPacket->GetData();
				length = pIPacket->GetSize();
			}

			int result = send(openSocket, pData+sendOfs, length-sendOfs, 0);
			if (result == SOCKET_ERROR)
			{
				Cout << "send() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
				errorFlag |= 1;
				sent = false;
			}
			else if (WSAGetLastError() != WSAEWOULDBLOCK)
			{
				NETPrintError();
				errorFlag |= 1;
				sent = false;
			}
			else if (result >= 0)
			{
				sendOfs += result; // bytes sent, increase the offset.
				sent = true;
			}
			else
			{
				sent = false;
			}

			// if the amount of sent data has reace the same length of that of the packet
			// size, then pop the top packet and send the next one.
			if ( sendOfs == length && !outList.empty() )
			{
				outList.pop_front();
				sendOfs = 0; // reset the sent offset
			}

		} while( sent && !outList.empty() );

		return errorFlag;
	}


	static int recvBegin = 0;
	static int recvOff = 0;

	int NETHandleInput(list<IPacketPtr>& inList, vector<char>& recvBuf, SOCKET openSocket, size_t maxRecvSize)
	{
		int errorFlag = 0;

		if (!recvBuf.capacity())
		{
			recvBuf.resize(maxRecvSize);
		}

		vector<char>::pointer pRecBuff = &recvBuf[0];
		pRecBuff += recvBegin;
		pRecBuff += recvOff;

		int result = recv(openSocket, pRecBuff, int(maxRecvSize) - recvOff, 0);
		if (result == SOCKET_ERROR)
		{
			Cout << "recv() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
			NETPrintError();
			errorFlag |= 1;
			return errorFlag;
		}
		else if (result == 0)
		{
			Cout << ieS("Connection closed") << std::endl;
			errorFlag = 1;
			return errorFlag;
		}

#if defined(DEBUG) | defined(_DEBUG)	
		OStringStream oss;
		oss << ieS("Incoming: ") << result << ieS(" bytes.") << ieS(" ");
		oss << ieS("Begin ") << recvBegin << ieS(" ");
		oss << ieS("Offset ") << recvOff;
		Cout << oss.str() << std::endl;
#endif

		// Current size in buffer + next lot of bytes recieved
		int newData = recvOff + result;
		int packetSize = 0;
		int processedData = 0;
		bool pktRecieved = false;

		while ( newData > (sizeof(u_short) * 2) )
		{
			vector<char>::pointer pRead = &recvBuf[0];

			// extract packet size from data.
			packetSize = *(reinterpret_cast<u_short *>(pRead));
			packetSize = static_cast<int>( ntohs(packetSize) );

			// Don't have enough new data to grab the next packet
			if (packetSize > newData)
				break;

			if (packetSize > (int)maxRecvSize)
			{
				errorFlag = 1;
				return errorFlag; // Prevent buffer overflow error
			}

			// we know how big the packet is...and we have the whole thing
			if (newData >= packetSize)
			{
				// Advance the pointer along 2 bytes to get the type
				u_short sType = ntohs( *(reinterpret_cast<u_short *>( pRead + sizeof(u_short) )) );
				IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

				const size_t dataOffset = sizeof(u_short) * 2;

				switch (type)
				{
				case IPacket::PT_BINARY:
					{
						// we know how big the packet is...and we have the whole thing
						CBinaryPacketPtr pkt = CBinaryPacketPtr(new CBinaryPacket( &pRecBuff[recvBegin + dataOffset], packetSize - dataOffset ));
						inList.push_back(pkt);
						pktRecieved = true;
						processedData += packetSize;
						recvBegin += packetSize;
						newData -= packetSize;
					}
					break;
				case IPacket::PT_TEXT:
					{
						// Convert the recieved text to generic form (Unicode or Ansi)
						String recievedText;
						recievedText.resize( packetSize - dataOffset ); // Dont need the packet size or its type. Also no null character needed
						AnsiToGeneric( &recievedText[0], &pRecBuff[recvBegin + dataOffset], packetSize - dataOffset, packetSize - dataOffset );

						if ( !recievedText.empty() )
						{			
							// push back the text packet
							CTextPacketPtr pkt = CTextPacketPtr( new CTextPacket( recievedText ) );
							inList.push_back(pkt);
							pktRecieved = true;
							processedData += packetSize;
							recvBegin += packetSize;
							newData -= packetSize;
						}
					}
					break;
				default:
					assert( false && ieS("Unkown packet type") );
					break;
				}
			} // if

		}

		recvOff = newData;

		if (pktRecieved)
		{
			if (newData == 0)
			{
				recvOff = 0;
				recvBegin = 0;
			}
			/*		else if (MAX_PACKET_SIZE > maxRecvSize)
			{
			// we don't want to overrun the buffer - so we copy the leftover bits 
			// to the beginning of the recieve buffer and start over
			int leftover = m_recvOfs;
			memcpy(&recvBuf[0], &recvBuf[recvBuf.size()], m_recvOfs);
			m_recvBegin = 0;
			} */
		}

		return errorFlag;
	}

} // namespace TCP

_ENGINE_END// namespace Engine



/*
// Accept a connection over TCP and return the new socket
SOCKET NETAcceptConnection(String& address, SOCKET listenSocket)
{
SOCKADDR_STORAGE sockStorage;
std::memset( &sockStorage, 0, sizeof(SOCKADDR_STORAGE) );
socklen_t size = sizeof(SOCKADDR_STORAGE);
SOCKET new_sock = accept(listenSocket, (SOCKADDR *)&sockStorage, &size);

if (new_sock == INVALID_SOCKET)
return INVALID_SOCKET;

#if defined(DEBUG) | defined(_DEBUG)		
// Additional Debug information, containing the hostname and its service.
Char hostname[NI_MAXHOST];
Char servInfo[NI_MAXSERV];

if ( GetNameInfo( (SOCKADDR *)&sockStorage, size, &hostname[0],
NI_MAXHOST, &servInfo[0], NI_MAXSERV, NI_NAMEREQD ) == SOCKET_ERROR )
{
closesocket(new_sock);
return INVALID_SOCKET;
}

Cout << ieS("DEBUG: Hostname: ") << hostname << std::endl;
Cout << ieS("DEBUG: Service: ") << servInfo << std::endl;			
#endif

// Determine if using IPV4 or IPV6 and assign appropriate space
// for the string : ipstr. 
Char* ipstr = 0;
switch (sockStorage.ss_family)
{
case AF_INET:
ipstr = new Char[INET_ADDRSTRLEN];
break;
case AF_INET:
ipstr = new Char[INET6_ADDRSTRLEN];
break;
default:
assert(0);
break;
};

// The WSAAddressToString function converts all components of a sockaddr structure into a human-readable string representation of the address.
if ( WSAAddressToString( (SOCKADDR *)&sockStorage, (DWORD)size,
NULL, &ipstr[0], (LPDWORD)&size ) != 0 )
{
NETPrintError();
closesocket(new_sock);
return INVALID_SOCKET;
}

// If the address being passed in is not empty, clear it
if (!address.empty())
address.clear();

address.assign(&ipstr[0], (size_t)size-1);

// Rid of the ipstr, its been copied over to address.
if (ipstr)
{
delete ipstr;
ipstr = 0;
}

// Output the IP Address for clarity.
Cout << ieS("Accepted IP: ") << address << std::endl;

return new_sock;
}


*/