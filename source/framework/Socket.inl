#ifndef CSOCKET_INL
#define CSOCKET_INL

inline void CSocket::SetDeleteFlag(DeleteFlags flag, bool enable)
{
	m_deleteFlag &= ~(1 << flag);
	m_deleteFlag |= ((!!enable) << flag);
}

inline bool CSocket::GetDeleteFlag(DeleteFlags flag) const
{
	return !!( m_deleteFlag & (1 << flag) );
}

inline const CSocket::PacketList& CSocket::GetInList() const
{
	return m_inList;
}

inline SOCKET CSocket::GetSocket() const
{
	return m_socket;
}

inline u_int CSocket::GetTimeOut() const
{
	return m_timeOut;
}

inline bool CSocket::HasOutput() const
{
	return !m_outList.empty();
}

#endif