#ifndef __COFFSET_STATE_H__
#define __COFFSET_STATE_H__

#include "Core.h"

namespace Engine
{


class COffsetState
{
public:
	COffsetState();
	~COffsetState();

	bool m_fillEnabled;		// default: false
	bool m_lineEnabled;		// default: false
	bool m_pointEnabled;	// default: false
	float m_offsetScale;	// default: 0
	float m_offsetBias;		// default: 0

private:
	DECLARE_HEAP;

};



} // namespace Engine




#endif