#ifndef __CVERTEX_ELEMENT_H__
#define __CVERTEX_ELEMENT_H__

#include "FrameFwd.h"
#include "VertexTypes.h"

namespace Engine
{

class CVertexElement
{
public:
	CVertexElement(UInt sourceIndex, UInt stride, VertexType type, VertexSemantic semantic, UInt index = 0);

	UInt GetStreamIndex() const;

	UInt GetStride() const;

	// Returns the type of vertex, V_UNKNOWN = ~0, V_BYTE = 0, V_UBYTE,
	// V_SHORT, V_USHORT, V_INT, V_UINT, V_FLOAT, V_DOUBLE.
	VertexType GetVertexType() const;

	VertexSemantic GetVertexSemantic() const;

	// Returns the number of vertex channels. These range from 1 - 4.
	UInt GetVertexChannels() const;

	UInt GetSemanticIndex() const;

private:
	UInt			m_streamIndex;
	UInt			m_stride;
	VertexType		m_vertexType;
	VertexSemantic	m_semantic;
	UInt			m_semanticIndex;

private:
	DECLARE_HEAP;
};




#include "VertexElement.inl"



}



#endif