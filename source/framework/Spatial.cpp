#include "Frameafx.h"
#include "Spatial.h"
#include "ISceneController.h"
#include "Culler.h"

using namespace Engine;


DEFINE_HEAP(CSpatial, "CSpatial");


CSpatial::CSpatial() :
m_pParent(0),
m_cullMode(CM_DYNAMIC),
m_world(Matrix4f::IDENTITY),
m_localScale(Vector3f::ONE),
m_worldBoundIsCurrent(false)
{

}


CSpatial::~CSpatial()
{

}

void CSpatial::Update(double deltaTime, bool initiator)
{
	UpdateWorldData(deltaTime);
	UpdateWorldBound();

	if (initiator)
		PropagateBoundToRoot();
}

void CSpatial::UpdateControllers(double deltaTime)
{
	if ( !m_animators.empty() )
	{
		ISceneControllerVector::iterator itor = m_animators.begin();
		for (/**/; itor != m_animators.end(); ++itor)
		{
			(*itor)->AnimateNode(this, deltaTime);
		}
	}
}

void CSpatial::AddController(ISceneControllerPtr pController)
{
	if (pController)
	{
		m_animators.push_back(pController);
	}
}

void CSpatial::RemoveController(ISceneControllerPtr pController)
{
	if (pController)
	{
		ISceneControllerVector::iterator itor = m_animators.begin();

		for (/**/; itor != m_animators.end(); ++itor)
		{
			if ( (*itor) == pController )
			{
				(*itor).reset();
				m_animators.erase(itor);
				return;
			}
		}
	}
}

void CSpatial::UpdateWorldData(double deltaTime)
{
	// update any controllers
	UpdateControllers(deltaTime);

	if (m_pParent)
	{
		m_world = m_pParent->GetWorldTransform() * GetLocalTransform();
	}
	else
	{
		m_world = GetLocalTransform();
	}

}


void CSpatial::PropagateBoundToRoot()
{
	if (m_pParent)
	{
		m_pParent->UpdateWorldBound();
		m_pParent->PropagateBoundToRoot();
	}
}


// As this is a post-mutiply maths library the order of transfrom
// is Scale * Rotation * Translation [S*R*T].
Matrix4f CSpatial::GetLocalTransform() const
{
	Matrix4f out = Matrix4f::IDENTITY;

	// If left as S*R*T, this is expensive. The operation can be reduced in 
	// cost by doing S * R. However you do not always need to do this
	// operation as the scale is sometimes never changed.
	// In order to add an optimisation the R and S can be combined
	// only when the S on all axis is not equal to one. 
	// This can be further optimised by changing the order of mulitplication
	// to R * S, which is what would be done if the library used was
	// matrix pre-multiply.
	// The order of multiply can then be corrected
	// by transposing the result of Rotation * Scale.
	out.SetRotationDegrees(m_localRotation);
	if (m_localScale != Vector3f::ONE)
	{
		Matrix4f scale = Matrix4f::IDENTITY;
		scale.SetScale(m_localScale);
		out *= scale;
	}
	out = Transpose(out);

	out.SetTranslation(m_localPostion);

	return out;
}

/*
void CSpatial::OnGetVisible(CCuller& culler, bool notCull)
{
	if (m_cullMode == CM_ALWAYS)
		return;
	
	if (m_cullMode == CM_NEVER)
		notCull = true;

	// save plane states
	UInt8 planeStates = culler.GetPlaneStates();

	// Apply culling testing against "test worthy" planes
	if ( notCull || culler.IsVisible(m_worldBound) )
		GetVisible(culler, notCull);
	
	// apply saved plane states
	culler.SetPlaneStaets(planeStates);
}
*/

/*
void CSpatial::Accept(CSpatialVistor& visitor)
{
	visitor.Visit(this);
}
*/

/*
// where the object sits in the world
Matrix4f CSpatial::GetLocalTransform()
{
	Matrix4f out(Matrix4f::IDENTITY);

	Matrix4f scale(Matrix4f::IDENTITY), rotation(Matrix4f::IDENTITY), translation(Matrix4f::IDENTITY);

	scale.SetScale(m_localScale);
	rotation.SetRotationDegrees(m_localRotation);
	translation.SetTranslation(m_localTranslation);

	out = scale * rotation * translation;

	// out.SetRotationDegrees(m_localRotation);
	// out.SetTranslation(m_localTranslation);

	// if (m_localScale != Vector3f::ONE)
	// {
	//	Matrix4f scale;
	//	scale.SetScale(m_localScale);
	//	out = smat * out;
	// }

	return out;
}
*/