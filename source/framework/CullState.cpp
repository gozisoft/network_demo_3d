#include "frameafx.h"
#include "CullState.h"

using namespace Engine;


DEFINE_HEAP(CCullState, "CCullState");

CCullState::CCullState() : m_cullEnabled(true), m_ccOrder(false)
{

}

CCullState::CCullState(bool enableCull, bool ccOrder) : 
m_cullEnabled(enableCull), m_ccOrder(ccOrder)
{

}

CCullState::~CCullState()
{

}