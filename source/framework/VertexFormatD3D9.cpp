#include "Frameafx.h"
#include "VertexFormatD3D9.h"
#include "RendererD3D9.h"
#include "MappingD3D9.h"
#include "VertexFormat.h"
#include "VertexElement.h"
#include "Helper.h"

// HRESULT translation for Direct3D and other APIs 
#include <dxerr.h>

using namespace Engine;

DEFINE_HEAP(CVertexFormatD3D9, "CVertexFormatD3D9");

CVertexFormatD3D9::CVertexFormatD3D9(IDirect3DDevice9* pDevice, CVertexFormatPtr pVFromat)
{
	UInt numElements = pVFromat->GetElementCount();
	for (UInt i = 0; i < numElements; ++i)
	{
		// Get the platform-independent attribute.
		CVertexElementPtr pElement = pVFromat->GetElement(i);

		// Set the DX9 attribute.
		D3DVERTEXELEMENT9 newElement;
		newElement.Stream = static_cast<WORD>( pElement->GetStreamIndex() );
		newElement.Offset = static_cast<WORD>( pElement->GetStride() );
		newElement.Type = static_cast<BYTE>( D3D9VertexType( pElement->GetVertexType() ) );
		newElement.Method = D3DDECLMETHOD_DEFAULT;
		newElement.Usage = static_cast<BYTE>( D3D9SemanticUsage[ pElement->GetVertexSemantic() ] );
        newElement.UsageIndex = static_cast<BYTE>( pElement->GetSemanticIndex() );

		m_elements.push_back(newElement);
	}

	// The last DX9 element must be D3DDECL_END().
    D3DVERTEXELEMENT9 endElement;
    endElement.Stream = 0xFF;
    endElement.Offset = 0;
    endElement.Type = D3DDECLTYPE_UNUSED;
    endElement.Method = 0;
    endElement.Usage = 0;
    endElement.UsageIndex = 0;

	m_elements.push_back(endElement);

	HRESULT hr = pDevice->CreateVertexDeclaration( &m_elements[0] , &m_pDeclaration );

	if (FAILED(hr))
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Failed to create vertex declaration: %s\n") && msg.c_str() );
	}

}

CVertexFormatD3D9::~CVertexFormatD3D9()
{
	SafeRelease(m_pDeclaration);
	m_elements.clear();
}

void CVertexFormatD3D9::Enable(IDirect3DDevice9* pDevice)
{
	// Enable the declaration by setting the state.
	HRESULT hr = pDevice->SetVertexDeclaration(m_pDeclaration);

	if ( FAILED(hr) )
	{
		assert( false && ieS("Failed to enable vertex declartation %s\n") && DXGetErrorDescription(hr) );
	}
}

void CVertexFormatD3D9::Disable(IDirect3DDevice9* pDevice)
{
	HRESULT hr = S_OK;

#if defined(DEBUG) || defined(_DEBUG)
    // Verify that the active declaration is the one making the disable
    // request.
	IDirect3DVertexDeclaration9* pActiveDec = 0;
	hr = pDevice->GetVertexDeclaration(&pActiveDec);

	if (FAILED(hr))
	{
		assert( false && DXGetErrorDescription(hr) );
	}

	if ( pActiveDec != m_pDeclaration )
	{
		assert( false && ieS("Mismatched vertex declarations\n") );
		SafeRelease(pActiveDec);
	}
#endif

#if defined(NDEBUG) || defined(_NDEBUG)
    // Disable the declaration by clearing the state.  TODO:  DirectX9
    // using debug drivers warns that the vertex declaration is set to
    // null.  For now, let's not call it.
    hr = pDevice->SetVertexDeclaration(0);
    assert(hr == D3D_OK, ieS("Failed to set vertex declaration: %s\n"),
        DXGetErrorString(hr));
#endif

}