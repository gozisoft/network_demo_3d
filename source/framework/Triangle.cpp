#include "Frameafx.h"
#include "Triangle.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexBufferAccess.h"
#include "Vector.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CTriangles, "CTriangles", "CSceneNode");

CTriangles::CTriangles() : 
CSceneNode()
{}

CTriangles::CTriangles(PrimitiveType primativeType, CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer) : 
CSceneNode(primativeType, pVFormat, pVBuffer, pIBuffer)
{

}

CTriangles::~CTriangles()
{

}

void CTriangles::UpdateModelSpace(GeoUpdate type)
{
	UpdateModelBound();
	if (type == GU_MODEL_BOUND_ONLY)
	{
		return;
	}
	
	CVertexBufferAccess vba( m_pVFormat, m_pVBuffer );
	if (vba.HasNormal())
	{
		CalculateNormals(vba);
	}

	/*
	if (type != GU_NORMALS)
	{
	if (vba.HasTangent() || vba.HasBinormal())
	{
	if (type == GU_USE_GEOMETRY)
	{
	UpdateModelTangentsUseGeometry(vba);
	}
	else
	{
	UpdateModelTangentsUseTCoords(vba);
	}
	}
	}*/


}

void CTriangles::CalculateNormals(CVertexBufferAccess& vba)
{
	UInt numVertices = vba.GetNumVertices();
	for (UInt i = 0; i < numVertices; ++i)
	{
		vba.Normal<Vector3f>(i) = Vector3f::ZERO;
	}

	for (UInt i = 0; i < GetTriangleCount(); ++i)
	{
		UInt Pos = 0; UInt32 I0 = 0;
		UInt32 I1 = 0; UInt32 I2 = 0;

		if (!GetTriangleIndex(Pos, I0, I1, I2))
		{
			continue;
		}

		Vector3f E0 = vba.Position<Vector3f>(I1) - vba.Position<Vector3f>(I0);
		Vector3f E1 = vba.Position<Vector3f>(I2) - vba.Position<Vector3f>(I0);
		Vector3f Normal = Cross(E0, E1);

		vba.Normal<Vector3f>(I0) += Normal;
		vba.Normal<Vector3f>(I1) += Normal;			
		vba.Normal<Vector3f>(I2) += Normal;
	}

	// Normalise all normal vectors.
	for (UInt i = 0; i < numVertices; ++i)
	{
		vba.Normal<Vector3f>(i).Normalise();
	}

}

