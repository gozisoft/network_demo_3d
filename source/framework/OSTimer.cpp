#include "frameafx.h"
#include "OSTimer.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>


_ENGINE_BEGIN

namespace OS
{


static BOOL multicore = FALSE;
static BOOL highPerformanceTimerSupport = FALSE;
static double secondsPerCount = 0;


DEFINE_HEAP(Timer, "Timer");

double Timer::m_deltaTime = -1.0;
Int64 Timer::m_baseTime = 0;
Int64 Timer::m_pauseTime = 0;
Int64 Timer::m_stopTime = 0;
Int64 Timer::m_prevTime = 0;
Int64 Timer::m_currTime = 0;
bool Timer::m_stopped = false;
bool Timer::m_isVistaOrLater = false;

void Timer::InitTimer()
{
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	multicore = (sysinfo.dwNumberOfProcessors > 1);

	LARGE_INTEGER HighPerformanceFreq;
	highPerformanceTimerSupport = QueryPerformanceFrequency(&HighPerformanceFreq);
	secondsPerCount = 1.0 / static_cast<double>(HighPerformanceFreq.QuadPart);

	// Not a multicore CPU, check windows version for correct use of
	// GetTickCount().
	OSVERSIONINFO osvi;
	SecureZeroMemory( &osvi, sizeof(OSVERSIONINFO) );
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	// dwMajorVersion	
	// Windows 7 = 6.0
	// Windows Vista = 6.0
	// Windows XP = 5.0
    m_isVistaOrLater = ( (osvi.dwMajorVersion > 6) || ( (osvi.dwMajorVersion == 6) && (osvi.dwMinorVersion >= 0) ));
}

// Returns the total time elapsed since reset() was called, NOT counting any
// time when the clock is stopped.
double Timer::GetGameTime()
{
	// If we are stopped, do not count the time that has passed since we stopped.
	//
	// ----*---------------*------------------------------*------> time
	//  m_baseTime       m_stopTime                      m_currTime
	if( m_stopped )
	{
		return static_cast<double>(m_stopTime - m_baseTime);
	}
	// The distance m_currTime - m_baseTime includes paused time,
	// which we do not want to count.  To correct this, we can subtract 
	// the paused time from m_currTime:  
	//
	//  (m_currTime - m_pauseTime) - m_baseTime 
	//
	//                     |<-------d------->|
	// ----*---------------*-----------------*------------*------> time
	//  m_baseTime       m_stopTime        startTime     m_currTime
	else
	{
		return static_cast<double>(((m_currTime-m_pauseTime)-m_baseTime));
	}
}


double Timer::GetDeltaTime()
{
	return m_deltaTime;
}

double Timer::GetTime()
{
	Int64 currTime = GetRealTime();
    double appTime = static_cast<double>(currTime - m_baseTime);
    return appTime;
}

void Timer::Reset()
{
	Int64 currTime = GetRealTime();

	m_baseTime = currTime;
	m_prevTime = currTime;
	m_stopTime = 0;
	m_stopped  = false;
}

void Timer::Start()
{
	Int64 startTime = GetRealTime();
	// Accumulate the time elapsed between stop and start pairs.
	//
	//                     |<-------d------->|
	// ----*---------------*-----------------*------------> time
	//  m_baseTime       m_stopTime        startTime     
	if (m_stopped)
	{
		m_pauseTime += (startTime - m_stopTime);	
	}
	m_prevTime = startTime;
	m_stopTime = 0;
	m_stopped  = false;	
}

void Timer::Stop()
{
	if( !m_stopped )
	{
		Int64 currTime = GetRealTime();

		m_stopTime = currTime;
		m_prevTime = currTime;
		m_stopped  = true;
	}
}

void Timer::Update()
{
	if( m_stopped )
	{
		m_deltaTime = 0.0;
		return;
	}

	m_currTime = GetRealTime();

	// Time difference between this frame and the previous.
	m_deltaTime = static_cast<double>(m_currTime - m_prevTime);

	// Prepare for next frame.
	m_prevTime = m_currTime;

	// Force nonnegative.  The DXSDK's CDXUTTimer mentions that if the 
	// processor goes into a power save mode or we get shuffled to another
	// processor, then m_deltaTime can be negative.
	if(m_deltaTime < 0.0)
	{
		m_deltaTime = 0.0;
	}
}

Int64 Timer::GetRealTime()
{
	if (highPerformanceTimerSupport)
	{
		// Avoid potential timing inaccuracies across multiple cores by
		// temporarily setting the affinity of this process to one core.
		DWORD_PTR affinityMask = 0;
		if(multicore)
		{
			// Get the processor affinity mask for this process
			DWORD_PTR dwProcessAffinityMask = 0;
			DWORD_PTR dwSystemAffinityMask = 0;

			if( ::GetProcessAffinityMask( ::GetCurrentProcess(), &dwProcessAffinityMask, &dwSystemAffinityMask ) != 0 &&
				dwProcessAffinityMask )
			{
				// Find the lowest processor that our process is allows to run against
				DWORD_PTR dwAffinityMask = ( dwProcessAffinityMask & ( ( ~dwProcessAffinityMask ) + 1 ) );

				// Set this as the processor that our thread must always run against
				// This must be a subset of the process affinity mask
				HANDLE hCurrentThread = ::GetCurrentThread();
				if(INVALID_HANDLE_VALUE != hCurrentThread)
				{
					affinityMask = ::SetThreadAffinityMask(hCurrentThread, dwAffinityMask);
					::CloseHandle(hCurrentThread);
				}
			}
		}

		// Get the time with QueryPerformanceCounter
		LARGE_INTEGER nTime;
		BOOL queriedOK = ::QueryPerformanceCounter(&nTime);

		// Restore the true affinity.
		if(multicore)
		{
			HANDLE hCurrentThread = ::GetCurrentThread();
			if(INVALID_HANDLE_VALUE != hCurrentThread)
			{
				(void)::SetThreadAffinityMask(hCurrentThread, affinityMask);
				::CloseHandle(hCurrentThread);
			}
		}

		if(queriedOK)
			return Int64( (nTime.QuadPart * 1000) * secondsPerCount ); // convert to miliseconds
	}

	if(IsVistaOrLater)
		return static_cast<Int64>( ::GetTickCount64() );
	else
		return static_cast<Int64>( ::GetTickCount() );
}

bool Timer::IsVistaOrLater()
{
	return m_isVistaOrLater;
}


//--------------------------------------------------------------------------------------
// Limit the current thread to one processor (the current one). This ensures that timing code 
// runs on only one processor, and will not suffer any ill effects from power management.
// See "Game Timing and Multicore Processors" for more details. (Taken from DXUT)
//--------------------------------------------------------------------------------------
void Timer::LimitThreadAffinityToCurrentProc()
{
    HANDLE hCurrentProcess = GetCurrentProcess();

    // Get the processor affinity mask for this process
    DWORD_PTR dwProcessAffinityMask = 0;
    DWORD_PTR dwSystemAffinityMask = 0;

    if( GetProcessAffinityMask( hCurrentProcess, &dwProcessAffinityMask, &dwSystemAffinityMask ) != 0 &&
        dwProcessAffinityMask )
    {
        // Find the lowest processor that our process is allows to run against
        DWORD_PTR dwAffinityMask = ( dwProcessAffinityMask & ( ( ~dwProcessAffinityMask ) + 1 ) );

        // Set this as the processor that our thread must always run against
        // This must be a subset of the process affinity mask
        HANDLE hCurrentThread = GetCurrentThread();
        if( INVALID_HANDLE_VALUE != hCurrentThread )
        {
            SetThreadAffinityMask( hCurrentThread, dwAffinityMask );
            CloseHandle( hCurrentThread );
        }
    }

    CloseHandle( hCurrentProcess );
}


} // namespace OS

_ENGINE_END