#ifndef WIN_CREATE_EVENT_H
#define WIN_CREATE_EVENT_H

#include "IEventReciever.h"

namespace Engine
{

	struct SWindowCreateEvent : public IEventData
	{
		static const EventType sk_EventType;

		const EventType& GetEventType() const
		{
			return sk_EventType;	
		}

		virtual ~SWindowCreateEvent() {}

		explicit SWindowCreateEvent(IRendererPtr pRenderer, IMouseControllerPtr pMouse) :
		m_pRenderer(pRenderer),
			m_pMouse(pMouse)
		{

		}

		void Serialise(OStream &out) const
		{

		}

		IRendererPtr m_pRenderer;
		IMouseControllerPtr m_pMouse;

	private:
		DECLARE_HEAP;
	};

}

#endif