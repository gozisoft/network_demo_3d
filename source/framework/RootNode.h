#ifndef __CROOT_NODE_H__
#define __CROOT_NODE_H__

#include "Spatial.h"

namespace Engine
{

// Each Node contains a list of child spatials.
class CRootNode : public CSpatial
{
public:
	CRootNode();
	virtual ~CRootNode();

	// Child node related
	UInt GetChildCount() const;
	UInt AddChild(CSpatialPtr pChild);
	UInt RemoveChild(CSpatialPtr pChild);
	UInt RemoveChild(UInt index);

	// World Data
	virtual void UpdateWorldData(double time);
	void UpdateWorldBound();

	// Return child pointer at the specifed index
	// of the array.
	CSpatialPtr GetChild(UInt index);

	// Sets a new child at the index location,
	// returns the previous child at the index location.
	CSpatialPtr SetChild(CSpatialPtr pChild, UInt index);

	// Culling visible set determination
	void GetVisible(CCuller& culler, bool notCull);

private:
	typedef std::vector<CSpatialPtr> SpatialList;
	typedef SpatialList::iterator SpatialItor;
	typedef SpatialList::const_iterator Const_SpatialItor;

	SpatialList m_childList;

private:
	DECLARE_HEAP;
};

#include "RootNode.inl"


}

#endif