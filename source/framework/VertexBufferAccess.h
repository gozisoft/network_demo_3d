#ifndef __CVERTEX_BUFFER_ACCESS_H__
#define __CVERTEX_BUFFER_ACCESS_H__

#include "VertexElement.h"
#include "VertexFormat.h"
#include "VertexBuffer.h"

namespace Engine
{


class CVertexBufferAccess
{		
public:
	CVertexBufferAccess(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer);
	~CVertexBufferAccess();

public:
	// Use these functions after default construction of an accessor object
	// or when you want to create an accessor object once and use it to
	// process multiple vertex buffers.
	void Apply (CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer);

	const UInt8* GetData () const;
	UInt GetNumVertices () const;
	UInt GetStride () const;

	// Generic accessors to the vertex buffer data.  You must know the type T
	// for the particular attributes.  The Get*Channels() functions are valid
	// only for FLOAT{1,2,3,4} data and return the number of floats for the
	// attributes.
	template <typename T> T& Position(UInt i);
	template <typename T> T* GetPosition(UInt i);
	bool HasPosition () const;
	UInt GetPositionChannels () const;

	template <typename T> T& Normal(UInt i);
	template <typename T> T* GetNormal(UInt i);
	bool HasNormal () const;
	UInt GetNormalChannels () const;

	template <typename T> T& Tangent(UInt i);
	template <typename T> T* GetTangent(UInt i);
	bool HasTangent () const;
	UInt GetTangentChannels () const;

	template <typename T> T& Binormal (UInt i);
	template <typename T> T* GetBinormal (UInt i);
	bool HasBinormal () const;
	UInt GetBinormalChannels () const;

	template <typename T> T& TCoord (UInt unit, UInt i);
	template <typename T> T* GetTCoord (UInt unit, UInt i);
	bool HasTCoord (UInt unit) const;
	UInt GetTCoordChannels (UInt unit) const;

	template <typename T> T& Colour (UInt unit, UInt i);
	template <typename T> T* GetColour (UInt unit, UInt i);
	bool HasColour (UInt unit) const;
	UInt GetColourChannels (UInt unit) const;

	template <typename T> T& BlendIndices (UInt i);
	bool HasBlendIndices () const;

	template <typename T> T& BlendWeight (UInt i);
	bool HasBlendWeight () const;

private:
	// Called by the constructors.
	void Initialise ();

	CVertexFormatPtr m_pVertexFormat;
	CVertexBufferPtr m_pVertexBuffer;

	// Data is not stored here, only manipulated.
	// The vertex buffer itself stores the data, and
	// the accessor acts like a visitor class for access
	// to elements of the buffer.
	UInt8* m_data;
	UInt8* m_position;
	UInt8* m_normal;
	UInt8* m_tangent;
	UInt8* m_binormal;
	UInt8* m_tCoord[CVertexFormat::MAX_TCOORDS];
	UInt8* m_colour[CVertexFormat::MAX_COLOURS];
	UInt8* m_blendIndices;
	UInt8* m_blendWeight;

private:
	DECLARE_HEAP;

};



#include "VertexBufferAccess.inl"


}



#endif