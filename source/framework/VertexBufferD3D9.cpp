#include "Frameafx.h"
#include "VertexBufferD3D9.h"
#include "RendererD3D9.h"
#include "MappingD3D9.h"
#include "Helper.h"

// HRESULT translation for Direct3D and other APIs 
#include <dxerr.h>

using namespace Engine;

DEFINE_HEAP(CVertexBufferD3D9, "CVertexBufferD3D9");

CVertexBufferD3D9::CVertexBufferD3D9(IDirect3DDevice9* pDevice, CVertexBufferPtr pVBuffer) : 
m_pVBuffer(pVBuffer),
m_pBuffer(0)
{
	 D3DPOOL pool; 
	 DWORD usage = D3D9BufferUsage[ pVBuffer->GetUsage() ]; 
	
	 switch (usage)
	 {  
	 case D3DUSAGE_WRITEONLY:
		 pool = D3DPOOL_MANAGED; // D3DPOOL_MANAGED for static buffers
		 break;

	 case D3DUSAGE_DYNAMIC:
	 case D3DUSAGE_RENDERTARGET:
	 case D3DUSAGE_DEPTHSTENCIL:
		 pool = D3DPOOL_DEFAULT; // D3DPOOL_DEFAULT for dynamic buffers
		 break;

	 default:
		 pool = D3DPOOL_DEFAULT;
		 break;
	 } 

	 HRESULT hr = pDevice->CreateVertexBuffer( pVBuffer->GetBytesSize(),
		 D3D9BufferUsage[ pVBuffer->GetUsage() ], 0, pool, &m_pBuffer, 0 );
	 if (FAILED(hr))
	 {
		 String msg = DXGetErrorString(hr);
		 assert( false && ieS("Unable to create d3d9 vertex buffer: ") && msg.c_str() );
	 }

	 SecureZeroMemory( &m_buffDesc, sizeof(D3DVERTEXBUFFER_DESC) );
	 hr = m_pBuffer->GetDesc(&m_buffDesc);
	 if (FAILED(hr))
	 {
		 String msg = DXGetErrorString(hr);
		 assert( false && ieS("Unable to get d3d9 vertex buffer descriptiom: ") && msg.c_str() );
	 }

	 // copy data into the d3d9 vertex buffer
	 void* data = Lock(CVertexBuffer::BL_WRITE_ONLY);
	 std::memcpy( data, pVBuffer->GetData(), pVBuffer->GetBytesSize() );
	 Unlock();
}

CVertexBufferD3D9::~CVertexBufferD3D9()
{
	SafeRelease(m_pBuffer);
}

// Vertex buffer operations.
void CVertexBufferD3D9::Enable(IDirect3DDevice9* pDevice, UInt vertexSize,
    UInt streamIndex, UInt offset)
{
	// Enable the buffer by setting the state.
    HRESULT hr = pDevice->SetStreamSource(streamIndex, m_pBuffer, offset, 
        vertexSize);

    if (FAILED(hr) )
	{
		String msg = DXGetErrorString(hr);
		assert( false && "Failed to enable vertex buffer: " && msg.c_str() );
	}        
}

void CVertexBufferD3D9::Disable(IDirect3DDevice9* pDevice, UInt streamIndex)
{
	HRESULT hr = S_OK;

#if defined(DEBUG) || defined(_DEBUG)
    IDirect3DVertexBuffer9 *activeBuffer = 0;
    UINT activeOffset = 0, activeStride = 0;
    hr = pDevice->GetStreamSource(streamIndex, &activeBuffer, &activeOffset,
		&activeStride);

	if ( FAILED(hr) )
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Failed to get stream source: ") && msg.c_str() );
	}

	if ( activeBuffer != m_pBuffer )
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Mismatched vertex buffers ") && msg.c_str() );
		SafeRelease(activeBuffer);
	}
#endif

    // Disable the buffer by clearing the state.
    hr = pDevice->SetStreamSource(streamIndex, 0, 0, 0);

	if (FAILED(hr))
	{
		String msg = DXGetErrorString(hr);
		assert( false && msg.c_str() );
	}
}

void* CVertexBufferD3D9::Lock(CVertexBuffer::Locking mode)
{
	// D3DLOCK_NOOVERWRITE
	// D3DLOCK_DISCARD
	// used with dynamic index/vertex buffers, although they can also be used with dynamic textures.

	// D3DLOCK_READONLY
	// used with static index/vertex buffers. Indicates that your program will never write to the resource
    void *videoMemory = 0;
    HRESULT hr = m_pBuffer->Lock( 0, 0, &videoMemory, D3D9BufferLockingMode[mode] );
    
	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && ieS("Failed to lock vertex buffer: \n") && msg.c_str() );
	}

    return videoMemory;
}

void CVertexBufferD3D9::Unlock()
{
   HRESULT hr = m_pBuffer->Unlock();
   if ( FAILED(hr) )
   {
	   String msg = DXGetErrorDescription(hr);
	   assert( false && ieS("Failed to unlock vertex buffer: \n") && msg.c_str() );
   }
}

