#include "Frameafx.h"
#include "VertexBufferAccess.h"
#include "VertexBuffer.h"

using namespace Engine;

DEFINE_HEAP(CVertexBufferAccess, "CVertexBufferAccess");



CVertexBufferAccess::CVertexBufferAccess(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer) :
m_pVertexFormat(pVFormat),
m_pVertexBuffer(pVBuffer)
{
	Initialise();
}


CVertexBufferAccess::~CVertexBufferAccess()
{

}

void CVertexBufferAccess::Apply(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer)
{
	m_pVertexFormat = pVFormat;
	m_pVertexBuffer = pVBuffer;
	Initialise();
}

void CVertexBufferAccess::Initialise()
{
	//UInt stride = m_pVertexFormat->GetTotalStride();
	UInt8* pData = m_pVertexBuffer->GetData();

	// if true
	CVertexElementPtr pElement;

	// set postion
	if ( m_pVertexFormat->IsEnabled(VS_POSITION) )
	{
		for (UInt i = 0; i < m_pVertexFormat->GetElementSemanitcCount(VS_POSITION); ++i)
		{
			pElement = m_pVertexFormat->GetElementBySemantic(VS_POSITION);
			m_position = ( pElement ) ? pData + pElement->GetStride() : 0;
		}
	}

	// set Normal
	if ( m_pVertexFormat->IsEnabled(VS_NORMAL) )
	{
		for (UInt i = 0; i < m_pVertexFormat->GetElementSemanitcCount(VS_NORMAL); ++i)
		{
			pElement = m_pVertexFormat->GetElementBySemantic(VS_NORMAL);
			m_normal = ( pElement ) ? pData + pElement->GetStride() : 0;
		}
	}

	// set Tanagent
	if ( m_pVertexFormat->IsEnabled(VS_TANGENT) )
	{
		for (UInt i = 0; i < m_pVertexFormat->GetElementSemanitcCount(VS_TANGENT); ++i)
		{
			pElement = m_pVertexFormat->GetElementBySemantic(VS_TANGENT);
			m_tangent = ( pElement ) ? pData + pElement->GetStride() : 0;
		}
	}

	// set Bitangent
	if ( m_pVertexFormat->IsEnabled(VS_BINORMAL) )
	{
		for (UInt i = 0; i < m_pVertexFormat->GetElementSemanitcCount(VS_BINORMAL); ++i)
		{
			pElement = m_pVertexFormat->GetElementBySemantic(VS_BINORMAL);
			m_binormal = ( pElement ) ? pData + pElement->GetStride() : 0;
		}
	}

	// set Textures
	if ( m_pVertexFormat->IsEnabled(VS_TEXTURE_COORDINATES) )
	{
		for (UInt i = 0; i < CVertexFormat::MAX_TCOORDS; ++i)
		{
			pElement = m_pVertexFormat->GetElementBySemantic(VS_TEXTURE_COORDINATES, i);
			m_tCoord[i] = ( pElement ) ? pData + pElement->GetStride() : 0;
		}
	}

	// set Colours
	if ( m_pVertexFormat->IsEnabled(VS_COLOUR) )
	{
		for (UInt i = 0; i < CVertexFormat::MAX_COLOURS; ++i)
		{
			pElement = m_pVertexFormat->GetElementBySemantic(VS_COLOUR, i);
			m_colour[i] = ( pElement ) ? pData + pElement->GetStride() : 0;
		}
	}

	// set Bitangent
	if ( m_pVertexFormat->IsEnabled(VS_BLEND_INDICES) )
	{
		pElement = m_pVertexFormat->GetElementBySemantic(VS_BLEND_INDICES);
		m_blendIndices = ( pElement ) ? pData + pElement->GetStride() : 0;
	}

	// Bitangent
	if ( m_pVertexFormat->IsEnabled(VS_BLEND_WEIGHTS) )
	{
		pElement = m_pVertexFormat->GetElementBySemantic(VS_BLEND_WEIGHTS);
		m_blendWeight = ( pElement ) ? pData + pElement->GetStride() : 0;
	}
}