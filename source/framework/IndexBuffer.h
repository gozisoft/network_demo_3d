#ifndef __CINDEX_BUFFER_H__
#define __CINDEX_BUFFER_H__

#include "VertexTypes.h"
#include "Buffer.h"

namespace Engine
{
// This index buffer is of UInt32 only as video cards only support
// 32 Bit integer indexing. Possibly in future 64 Bit integer will be added.
// As 32 Bit Integer is the "Optimum" type for an x86 CPU the indices are of
// type Int32. If you want to use 16 Bit Int on the video card, just cast the values over.

class CIndexBuffer : public CBuffer
{
public:
	// Default Ctor, set all to 0. IndexType = UINT
	CIndexBuffer();
	// Allocate memory to the index buffer prior to filling
	// the buffer space. A bit like std::vector::reserve()
	CIndexBuffer(UInt reserve, IndexType indextype = IT_UINT, BufferUse usage = BU_STATIC); 

	CIndexBuffer(const CIndexBuffer &ibuffer);

	// Retrive all the indices from the buffer.
	// This is used for passing all the indices down to the
	// GPU when it comes to creating a GPU IndexBuffer.
	UInt8* GetData();

	// Index buffer can be either 16 or 32 bits.
	// This returns the type, UShort or UInt32
	IndexType GetType() const;

	// Function to fill an index buffer up to the numValues.
	// ie SetIndexBuffer(5); will set 0,1,2,3,4,5 as indices.
	void SetIndexBuffer(UInt32 numValues);

	// Use this for when multiple gemetric primatives shared the same
	// index buffer.
	void SetIndexOffset(UInt offset);

	// This will be used with SetIndexOffset() in order to share indices
	// with geometric primatives that use the same index buffer.
	void SetIndexCount(UInt indexCount);

private:
	IndexType m_type;

private:
	DECLARE_HEAP;

};


#include "IndexBuffer.inl"

}

// UInt8Vector  *m_indices;
// UInt			m_offset;
// UInt			m_numIndices;


#endif