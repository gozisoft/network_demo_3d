#ifndef __CPOLYLINE_H__
#define __CPOLYLINE_H__

#include "SceneNode.h"

namespace Engine
{

class CPolyline : public CSceneNode
{
public:
	// Construction and destruction.
	CPolyline(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer,
		bool contiguous);
	~CPolyline();

	UInt GetMaxNumLines() const;
	// You can lower the max number of lines but not increase the number
	// higher than that of the number of vertex buffer elements.
	void SetNumLines(UInt numLines); 
	UInt GetNumLines() const;

	bool IsContiguous() const;

private:
	// Number of active lines
	UInt m_numLines;

	// Poly lines are in segments or contiguous
	bool m_contiguous;

	DECLARE_HEAP;

};

#include "Polyline.inl"


}



#endif