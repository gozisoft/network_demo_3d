#ifndef __CSCENE_NODE_H__
#define __CSCENE_NODE_H__


#include "Spatial.h"

namespace Engine
{

class CSceneNode : public CSpatial
{
public:
	enum PrimitiveType
	{
		PT_NONE = 0,
		PT_POLYPOINTS,		// Renders the vertices as a collection of isolated points
		PT_POLYLINE_CLOSED,	// Renders the vertices as a list of isolated straight line segments 
		PT_POLYLINE_OPEN,	// Renders the vertices as a single polyline.
		PT_TRIANGLE,
		PT_TRIMESH,
		PT_TRISTRIP,
		PT_TRIFAN,
		NUM_GEOMETRIC_TYPES
	};

	enum GeoUpdate
	{
		GU_MODEL_BOUND_ONLY = -3,
		GU_NORMALS = -2,
		GU_USE_GEOMETRY = -1,
		GU_USE_TCOORD_CHANNEL = 0
	};

	virtual ~CSceneNode();

	// Object geometric updating [tranform, modelbound, mesh normals]
	virtual void UpdateModelSpace(GeoUpdate type);
	// Culling
	virtual void GetVisible(CCuller& culler, bool notCull);

	PrimitiveType GetType() const;

	void SetVertexFormat(CVertexFormatPtr pVFormat);
	CVertexFormatPtr GetVertexFormat() const;

	void SetVertexBuffer(CVertexBufferPtr pVBuffer);
	CVertexBufferPtr GetVertexBuffer() const;

	void SetIndexBuffer(CIndexBufferPtr pIBuffer);
	CIndexBufferPtr GetIndexBuffer() const;

	// unsafe function, however allows modification of modelbound
	Spheref& GetModelBound();

protected:
	CSceneNode();
	CSceneNode(PrimitiveType primativeType, CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer);

	// Geometric update
	void UpdateModelBound();
	void UpdateWorldBound();

	// Vertex Format
	CVertexFormatPtr m_pVFormat; 
	// Vertex Buffer
	CVertexBufferPtr m_pVBuffer; 
	 // Index Buffer
	CIndexBufferPtr m_pIBuffer; 
	// Bounding volume to model
	Spheref	m_modelBound;		 
	// Ranges from Triangles, Quads and Polygon points.
	PrimitiveType m_type;
	// Set visible if calculated as such
	bool m_isVisible;

private:
	DECLARE_HEAP

};



#include "SceneNode.inl"


}



#endif // __CGEOMETRY_H__