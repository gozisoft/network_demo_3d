#ifndef WIN_SIZE_EVENT_H
#define WIN_SIZE_EVENT_H

#include "IEventReciever.h"
#include "Vector.h"


namespace Engine
{


	struct SWindowSizeEvent : public IEventData
	{
		static const EventType sk_EventType;

		const EventType& GetEventType() const
		{
			return sk_EventType;	
		}

		virtual ~SWindowSizeEvent() {}

		explicit SWindowSizeEvent(const Point2l& clientSize, const Point2l& winSize) :
		m_clientSize(clientSize),
		m_winSize(winSize)
		{

		}

		void Serialise(OStream &out) const
		{

		}

		Point2l m_clientSize;
		Point2l m_winSize;

	private:
		DECLARE_HEAP;
	};




}

#endif