#ifndef __CVERTEX_FORMAT_H__
#define __CVERTEX_FORMAT_H__

#include "FrameFwd.h"
#include "VertexTypes.h"

#include <bitset>

namespace Engine
{

class CVertexFormat
{
public:
	typedef std::vector<CVertexElementPtr> ElementList;
	typedef ElementList::iterator ElementItor; 
	typedef ElementList::const_iterator Const_ElementItor;
	typedef std::bitset<NUM_VS_SEMANTICS> EnableBSet;

public:
	CVertexFormat();
	~CVertexFormat();

	CVertexElementPtr AddElement(UInt sourceIndex, VertexType type, VertexSemantic semantic, UInt index = 0);
	CVertexElementPtr InsertElement(UInt pos, UInt sourceIndex, VertexType type, VertexSemantic semantic, UInt index = 0);

	void RemoveElement(UInt pos);
	void RemoveElement(VertexSemantic semantic, UInt semanticIndex = 0);
	void RemoveAllElements();

	// Returns a list of elements that make up the vertex format
	// const CVertexElement*  GetElements() const;

	// Number of elements within the vertex format, best used with GetElements()
	// for copying the list of elements
	UInt GetElementCount() const;

	// Lookup the number of active element semantics. Use when looking for the number
	// of textures or colours semenatics within the vertex format.
	UInt GetElementSemanitcCount(VertexSemantic semantic) const;

	CVertexElementPtr GetElementBySemantic(VertexSemantic semantic, UInt semanticIndex = 0);
	CVertexElementPtr GetElement(UInt pos);
	ElementList GetElementByStream(UInt source);

	UInt GetTotalStride() const;

	void SetEnabled(VertexSemantic semantic, bool enable);
	bool IsEnabled(VertexSemantic semantic) const;

	static const UInt MAX_TCOORDS = 8; // default: 8
	static const UInt MAX_COLOURS = 2; // default: 2

protected:
	ElementList	m_elements;
	EnableBSet m_enableFlags;
	UInt m_totalStride;

private:
	DECLARE_HEAP;

};


#include "VertexFormat.inl"



}



#endif