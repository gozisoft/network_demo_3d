#ifndef __CPOLYPOINT_H__
#define __CPOLYPOINT_H__

#include "SceneNode.h"

namespace Engine
{

class CPolypoint : public CSceneNode
{
public:
	// Construction and destruction.
	CPolypoint(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer);
	~CPolypoint();

	UInt GetMaxNumPoints() const;
	// You can lower the max number of points but not increase the number
	// higher than that of the number of vertex buffer elements.
	void SetMaxNumPoints(UInt numPoints); 
	UInt GetNumPoints() const;

private:
	// number of active points
	UInt m_numPoints;

private:
	DECLARE_HEAP;
};

#include "Polypoint.inl"


}


#endif // __CPOLYPOINT_H__