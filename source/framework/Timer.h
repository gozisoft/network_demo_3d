#ifndef CTIMER_H
#define CTIMER_H

#include "OSTimer.h"

_ENGINE_BEGIN

class CTimer
{
public:
	CTimer()
	{
		OS::Timer::InitTimer();
	}

	void Start()
	{
		OS::Timer::Start();
	}
	void Stop()	
	{
		OS::Timer::Stop();
	}

	void Update()
	{
		OS::Timer::Update();
	}

	void Reset()
	{
		OS::Timer::Reset();
	}

	double GetDeltaTime()
	{
		return OS::Timer::GetDeltaTime();
	}

	double GetGameTime()
	{
		return OS::Timer::GetGameTime();
	}

	double GetTime()
	{
		return OS::Timer::GetTime();
	}

	Int64 GetRealTime()
	{
		return OS::Timer::GetRealTime();
	}

private:
	DECLARE_HEAP;

};

DEFINE_HEAP(CTimer, "CTimer");



_ENGINE_END

#endif