#ifndef WIN_KEYBOARD_EVENT_H
#define WIN_KEYBOARD_EVENT_H

#include "IEventReciever.h"
#include "Keycodes.h"

namespace Engine
{


	// use of bit fields
	struct SKeyInput 
	{
		KeyCode key;
		bool pressedDown:1;	// if not true key is up
		bool shift:1;		// if true shift was also pressed
		bool ctrl:1;		// if true ctrl was also pressed

	private:
		DECLARE_HEAP;
	};

	struct SKeyEvent : public IEventData
	{
		static const EventType sk_EventType;

		const EventType& GetEventType() const 
		{
			return sk_EventType;	
		}

		virtual ~SKeyEvent() {}

		SKeyEvent(const SKeyInput& input)
		{
			m_keyInput.key = input.key;
			m_keyInput.pressedDown = input.pressedDown;
			m_keyInput.shift = input.shift;
			m_keyInput.ctrl = input.ctrl;
		}

		void Serialise(std::ostream &out) const
		{
			out << m_keyInput.key;
			out << m_keyInput.pressedDown;
			out << m_keyInput.shift;
			out << m_keyInput.ctrl;
		}

		void Serialise(CBufferIO & data) const
		{


		};

		SKeyInput m_keyInput;

	private:
		DECLARE_HEAP;
	};


}

#endif