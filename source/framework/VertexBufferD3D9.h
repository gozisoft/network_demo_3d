#ifndef VERTEX_BUFFER_D3D9_H
#define VERTEX_BUFFER_D3D9_H

#include "framefwd.h"
#include "VertexBuffer.h"

// Direct3D9 includes
#include <d3d9.h>

namespace Engine
{


class CVertexBufferD3D9
{
public:
	CVertexBufferD3D9(IDirect3DDevice9* pDevice, CVertexBufferPtr pVBuffer);
	~CVertexBufferD3D9();

	// Vertex buffer operations.
	void Enable(IDirect3DDevice9* pDevice, UInt vertexSize,
		UInt streamIndex, UInt offset);
	void Disable(IDirect3DDevice9* pDevice, UInt streamIndex);

	void* Lock(CVertexBuffer::Locking mode);
	void Unlock();

private:
	IDirect3DVertexBuffer9* m_pBuffer;
	D3DVERTEXBUFFER_DESC m_buffDesc;

	CVertexBufferPtr m_pVBuffer; // keep a pointer to the data on CPU

	DECLARE_HEAP;

};


}


#endif