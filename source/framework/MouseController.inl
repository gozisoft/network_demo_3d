#ifndef CMOUSE_CONTROLLER_INL
#define CMOUSE_CONTROLLER_INL

inline void CMouseController::SetRelativePos(const Point2f& pos)
{
	SetMousePos( Point2l( pos.x() * m_winSize.x(), pos.y() * m_winSize.y() ) );
}

inline const Point2l& CMouseController::GetMousePos()
{
	UpdateCursorPos();
	return m_cursorPos;
}

inline Point2f CMouseController::GetRelativeMousePos()
{
	UpdateCursorPos();
	return Point2f(  m_invClientSize.x() * m_cursorPos.x(),  m_invClientSize.x() * m_cursorPos.y() );
}

inline bool CMouseController::IsVisible() const
{
	return m_isVisible;
}

inline Point2l CMouseController::GetClientSize() const
{
	return m_winSize;
}

inline void CMouseController::SetHWND(HWND hwnd)
{
	m_hwnd = hwnd;
}

#endif