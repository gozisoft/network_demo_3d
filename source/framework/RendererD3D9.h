#ifndef CRENDERER_D3D9_H
#define CRENDERER_D3D9_H

#include "IRenderer.h"
#include "Colour.h"

// Enable extra D3D debugging in debug builds if using the debug DirectX runtime.  
// This makes D3D objects work well in the debugger watch window, but slows down 
// performance slightly.
#if defined(DEBUG) || defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

// Direct3D9 includes
#include <d3d9.h>



namespace Engine
{


class CRendererD3D9 : public IRenderer
{
public:
	typedef std::map< CIndexBufferPtr, CIndexBufferD3D9Ptr > CIndexBufferMap;
	typedef std::map< CVertexBufferPtr, CVertexBufferD3D9Ptr > VertexBufferMap;
	typedef std::map< CVertexFormatPtr, CVertexFormatD3D9Ptr > VertexFormatMap;

public:
    CRendererD3D9();
    ~CRendererD3D9();

    bool Initialise(HWND hwnd, HINSTANCE hinst, bool windowed);
    void Release();

	// Hardware buffer management

	// Vertex buffer
	void Bind(CVertexBufferPtr pVBuffer);
	void Unbind(CVertexBufferPtr pVBuffer);
	void Enable(CVertexBufferPtr pVBuffer, UInt streamIndex = 0, UInt offset = 0);
	void Disable(CVertexBufferPtr pVBuffer, UInt streamIndex = 0);

	// Vertex format
	void Bind(CVertexFormatPtr pVFormat);
	void Unbind(CVertexFormatPtr pVFormat);
	void Enable(CVertexFormatPtr pVFormat);
	void Disable(CVertexFormatPtr pVFormat);

	// Index buffer
	void Bind(CIndexBufferPtr pIBuffer);
	void Unbind(CIndexBufferPtr pIBuffer);
	void Enable(CIndexBufferPtr pIBuffer);
	void Disable(CIndexBufferPtr pIBuffer);

	// Camera Settings
	void SetCamera(CCameraPtr pCamera);
	CCameraPtr GetCamera();

	// Render functions
	bool PreDraw();
	void PostDraw();
	void Draw(CSceneNodePtr pNode);
	void DrawPrimitive(CSceneNodePtr pNode);

	// Frame Control
	void SetViewport(const Viewporti& view);
	void Resize(Int32 width, Int32 height);
	Viewporti GetViewport() const;
	bool EnterFullscreen(const Point2l& fullSize) { return false;  };
	bool ExitFullscreen() { return false; };

	// Buffer Control
	void ClearColorBuffer();
	void ClearDepthBuffer();
	void ClearStencilBuffer();
	void ClearBuffers();
	void ClearColorBuffer(Int32 x, Int32 y, Int32 w, Int32 h);
	void ClearDepthBuffer(Int32 x, Int32 y, Int32 w, Int32 h);
	void ClearStencilBuffer(Int32 x, Int32 y, Int32 w, Int32 h);
	void ClearBuffers(Int32 x, Int32 y, Int32 w, Int32 h);
	void DisplayColorBuffer();

private:
	void IntialiseSubSystems();

	CRenderDataD3D9Ptr m_pData;

	bool m_windowed;
	bool m_vsync;
	bool m_stencilBuffer;
	bool m_backBuffer;

	UInt8 m_bits;
	UInt8 m_antiAlias;

	// Framebuffer clearing.
	Colour4f m_clearColor;
	float m_clearDepth;
	UInt32 m_clearStencil;

	CCameraPtr m_pCamera;

	// States
	CAlphaState* m_pDefaultAlphaState;
    CCullState* m_pDefaultCullState;
    CDepthState* m_pDefaultDepthState;
    COffsetState* m_pDefaultOffsetState;
    CStencilState* m_pDefaultStencilState;
    CWireState*	m_pDefaultwireState;

	// Resources
	CIndexBufferMap m_indexBuffers;
	VertexBufferMap m_vertexBuffers;
	VertexFormatMap	m_vertexFormats;

};


extern IRendererPtr CreateDirect3D9Renderer();


#include "RendererD3D9.inl"

}

#endif