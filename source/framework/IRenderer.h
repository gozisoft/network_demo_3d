#ifndef IRENDERER_GL_H
#define IRENDERER_GL_H

#include "FrameFwd.h"
#include "Viewport.h"


#define WIN32_LEAN_AND_MEAN
#include <windows.h>

namespace Engine
{


class IRenderer
{
public:
	virtual ~IRenderer() {}

	enum RendererType
	{
		RT_OPENGL,
		RT_DIRECT3D9
	};

	static IRendererPtr Create(RendererType type);

    virtual bool Initialise(HWND hwnd, HINSTANCE hinst, bool windowed) = 0;
    virtual void Release() = 0;

	// Picking
	virtual bool GetPickRay (Int32 x, Int32 y, Vector3f& origin, Vector3f& direction) const = 0;

	// Camera Settings
	virtual void SetCamera(CCameraPtr pCamera) = 0;
	virtual CCameraPtr GetCamera() = 0;

	// Render functions
	virtual bool PreDraw() = 0;
	virtual void PostDraw() = 0;
	virtual void Draw(const std::vector<CSceneNode*>& visibleSet) = 0;
	virtual void Draw(const CSceneNode* pNode) = 0;
	virtual void DrawPrimitive(const CSceneNode* pNode) = 0;

	// Frame Control
	virtual void SetViewport(const Viewporti& view) = 0;
	virtual void Resize(Int32 width, Int32 height) = 0;
	virtual Viewporti GetViewport() const = 0;
	virtual bool EnterFullscreen(const Point2l& fullSize) = 0;
	virtual bool ExitFullscreen() = 0;

	// Buffer Control
	virtual void ClearColorBuffer() = 0;
	virtual void ClearDepthBuffer() = 0;
	virtual void ClearStencilBuffer() = 0;
	virtual void ClearBuffers() = 0;
	virtual void ClearColorBuffer(Int32 x, Int32 y, Int32 w, Int32 h) = 0;
	virtual void ClearDepthBuffer(Int32 x, Int32 y, Int32 w, Int32 h) = 0;
	virtual void ClearStencilBuffer(Int32 x, Int32 y, Int32 w, Int32 h) = 0;
	virtual void ClearBuffers(Int32 x, Int32 y, Int32 w, Int32 h) = 0;
	virtual void DisplayColorBuffer()= 0;

private:
	DECLARE_HEAP;

};



} // namespace Engine

#endif