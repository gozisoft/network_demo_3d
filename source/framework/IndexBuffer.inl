#ifndef __CINDEX_BUFFER_INL__
#define __CINDEX_BUFFER_INL__


inline UInt8* CIndexBuffer::GetData()
{
	return const_cast<UInt8*>( static_cast<const CBuffer&>(*this).GetData() );
}

inline IndexType CIndexBuffer::GetType() const
{
	return m_type;
}

inline void CIndexBuffer::SetIndexOffset(UInt offset)
{
	m_stride = offset;
}



#endif