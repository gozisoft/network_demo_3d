#ifndef __CSCENE_NODE_INL__
#define __CSCENE_NODE_INL__

inline CSceneNode::PrimitiveType CSceneNode::GetType() const
{
	return m_type;
}

inline void CSceneNode::SetVertexFormat(CVertexFormatPtr pVFormat)
{
	m_pVFormat.reset();
	m_pVFormat = pVFormat;
}

inline CVertexFormatPtr CSceneNode::GetVertexFormat() const
{
	return m_pVFormat;
}

inline void CSceneNode::SetVertexBuffer(CVertexBufferPtr pVBuffer)
{
	m_pVBuffer.reset();
	m_pVBuffer = pVBuffer;
}

inline CVertexBufferPtr CSceneNode::GetVertexBuffer() const
{
	return m_pVBuffer;
}

inline void CSceneNode::SetIndexBuffer(CIndexBufferPtr pIBuffer)
{
	m_pIBuffer.reset();
	m_pIBuffer = pIBuffer;
}

inline CIndexBufferPtr CSceneNode::GetIndexBuffer() const
{
	return m_pIBuffer;
}

inline Spheref& CSceneNode::GetModelBound()
{
	return m_modelBound;
}

#endif

/*
inline CVertexFormatPtr CSceneNode::GetVertexFormat()
{
	return const_cast<CVertexFormat*>( static_cast<const CSceneNode&>(*this).GetVertexFormat() );
}

inline CVertexBuffer* CSceneNode::GetVertexBuffer()
{
	return const_cast<CVertexBuffer*>( static_cast<const CSceneNode&>(*this).GetVertexBuffer() );
}

inline CIndexBuffer* CSceneNode::GetIndexBuffer()
{
	return const_cast<CIndexBuffer*>( static_cast<const CSceneNode&>(*this).GetIndexBuffer() ); 
}

*/