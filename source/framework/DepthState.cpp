#include "frameafx.h"
#include "DepthState.h"

using namespace Engine;


DEFINE_HEAP(CDepthState, "CDepthState");

CDepthState::CDepthState() : m_enabled(true), m_writable(true), m_compareMode(CM_LEQUAL)
{


}

CDepthState::CDepthState(bool enable, bool writable, CompareMode compareMode) :
m_enabled(enable),
m_writable(writable),
m_compareMode(compareMode)
{


}

CDepthState::~CDepthState()
{

}