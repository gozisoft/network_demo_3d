#include "Frameafx.h"
#include "RootNode.h"
#include "SceneNode.h"
#include "Intersections.h"
#include "Culler.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CRootNode, "CRootNode", "CSpatial");

CRootNode::CRootNode() : CSpatial()
{

}

CRootNode::~CRootNode()
{
	for (SpatialItor itor = m_childList.begin(); itor != m_childList.end(); ++itor)
	{
		if (*itor)
		{
			(*itor)->SetParent(0);
			(*itor) = CSpatialPtr();
		}
	}

	m_childList.clear();
}


UInt CRootNode::AddChild(CSpatialPtr pChild)
{
	if (!pChild)
	{
		assert( false && ieS("No child present, cannot attach to a Node") );
		return 0;
	}

	if ( pChild->GetParent() ) // does the pointer exist
	{
		assert( false && ieS("Child has already got a parent") );
		return 0;
	}

	// Set the child as having this root as a parent
	pChild->SetParent(this);

	// search for empty slot and insert child to that pos.
	if ( !m_childList.empty() )
	{
		SpatialItor itorBegin = m_childList.begin();
		SpatialItor itorEnd	= m_childList.end();
		for (UInt i = 0; itorBegin != itorEnd; ++itorBegin, ++i)
		{
			if ( *itorBegin == 0 )
			{
				m_childList.insert(itorBegin, pChild);
				return i;
			}
		}
	}

	// all slot used, so add child to end of array.
	UInt numChildren = static_cast<UInt>( m_childList.size() );
	m_childList.push_back(pChild);
	return numChildren;
}


UInt CRootNode::RemoveChild(CSpatialPtr pChild)
{
	if (pChild)
	{
		SpatialItor itorBegin = m_childList.begin();
		SpatialItor itorEnd	= m_childList.end();
		for (UInt i = 0; itorBegin != itorEnd; ++itorBegin, ++i)
		{
			if ( *itorBegin == pChild )
			{
				(*itorBegin)->SetParent(0);
				*itorBegin = CSpatialPtr();
				return i;
			}
		}
	}
	return 0;
}


CSpatialPtr CRootNode::GetChild(UInt index)
{
	if (index >= 0 && index < m_childList.size())
	{
		return m_childList[index];
	}
	return CSpatialPtr();
}


CSpatialPtr CRootNode::SetChild(CSpatialPtr pChild, UInt index)
{
	if (pChild)
	{
		assert( pChild->GetParent() && ieS("No parent found") );

		if (index >= 0 && index < m_childList.size())
		{
			// detach child currently in list 
			CSpatialPtr pPreviousChild = m_childList[index];
			if (pPreviousChild)
				pPreviousChild->SetParent(0);

			if (pChild)
				pChild->SetParent(this);

			m_childList[index] = pChild;
			return pPreviousChild;
		}

		// index out of range, append the child to the end of the list
		pChild->SetParent(this);
		m_childList.push_back(pChild);
	}

	return CSpatialPtr();
}

void CRootNode::UpdateWorldData(double time)
{
	CSpatial::UpdateWorldData(time);

    for (SpatialItor itor = m_childList.begin(); itor != m_childList.end(); ++itor)
    {
        CSpatialPtr child = *itor;
        if (child)
        {
            child->Update(time, false);
        }
    }
}

void CRootNode::UpdateWorldBound()
{
	if (!m_worldBoundIsCurrent)
	{
		bool firstBound = true;
		for (SpatialItor itor = m_childList.begin(); itor != m_childList.end(); ++itor)
		{
			CSpatialPtr pChild = *itor;
			if (pChild)
			{
				if ( firstBound )
				{
					// Set the world bound to the world bound of the first
                    // non-null child.
					firstBound = false;
					m_worldBound = pChild->GetWorldBound();
				}
				else
				{
					// Merge the current world bound with the child world
					// bound.
					MergeWithSphere( m_worldBound, m_worldBound, pChild->GetWorldBound() );
				}
			}
		}
	}
}

void CRootNode::GetVisible(CCuller& culler, bool notCull)
{
	for (SpatialItor itor = m_childList.begin(); itor != m_childList.end(); ++itor)
	{
		CSceneNodePtr pChild = dynamic_pointer_cast<CSceneNode>(*itor);
		if (pChild)
		{
			culler.OnGetVisible(pChild , notCull );
		}

		CRootNodePtr pRoot = dynamic_pointer_cast<CRootNode>(*itor);
		if (pRoot)
		{
			culler.OnGetVisible(pRoot , notCull );
		}

	}
}

