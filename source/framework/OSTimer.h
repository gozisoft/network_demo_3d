#ifndef OS_TIMER_H
#define OS_TIMER_H

#include "Core.h"

_ENGINE_BEGIN

namespace OS
{

	class Timer
	{
	public:
		static void InitTimer();
		static void Start();
		static void Stop();
		static void Update();
		static void Reset();
		static void LimitThreadAffinityToCurrentProc();

		static double GetDeltaTime();
		static double GetGameTime();
		static double GetTime();

		static Int64 GetRealTime();

		// This is made public for anyone else curious about this
		static bool IsVistaOrLater();


	private:
		static double m_deltaTime;

		static Int64 m_baseTime;
		static Int64 m_pauseTime;
		static Int64 m_stopTime;
		static Int64 m_prevTime;
		static Int64 m_currTime;

		static bool m_stopped;
		static bool m_isVistaOrLater; 

	private:
		DECLARE_HEAP;

	};

}


_ENGINE_END

#endif