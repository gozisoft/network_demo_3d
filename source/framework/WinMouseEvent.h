#ifndef WIN_MOUSE_EVENT_H
#define WIN_MOUSE_EVENT_H

#include "IEventReciever.h"

namespace Engine
{

	enum MouseInputEvent
	{
		MIE_UNKOWN,
		MIE_RMOUSE_DOWN,
		MIE_LMOUSE_DOWN,
		MIE_MMOUSE_DOWN,
		MIE_RMOUSE_UP,
		MIE_LMOUSE_UP,
		MIE_MMOUSE_UP,
		MIE_MOUSE_MOVED,
		MIE_MOUSE_WHEEL,
		MIE_RMOUSE_DOUBLE_CLICK,
		MIE_LMOUSE_DOUBLE_CLICK,
		MIE_MMOUSE_DOUBLE_CLICK,
		MIE_RMOUSE_TRIPLE_CLICK,
		MIE_LMOUSE_TRIPLE_CLICK,
		MIE_MMOUSE_TRIPLE_CLICK,
		MAX_NUM_MOUSE_EVENTS
	};

	enum MouseButtonState
	{
		MBS_LEFT    = 0x01,
		MBS_RIGHT   = 0x02,
		MBS_MIDDLE  = 0x04,
		//! currently only on windows
		MBS_EXTRA1  = 0x08,
		//! currently only on windows
		MBS_EXTRA2  = 0x10
	};

	struct SMouseInput
	{
		Long x;
		Long y;
		float Wheel;		   // usually between [-1.0, 1.0]
		UInt32 ButtonStates;
		bool shift:1;		   // if true shift was also pressed
		bool ctrl:1;		   // if true ctrl was also pressed

		// Is the left button pressed down?
		bool isLeftPressed() const { return 0 != ( ButtonStates & MBS_LEFT ); }

		// Is the right button pressed down?
		bool isRightPressed() const { return 0 != ( ButtonStates & MBS_RIGHT ); }

		// Is the middle button pressed down?
		bool isMiddlePressed() const { return 0 != ( ButtonStates & MBS_MIDDLE ); }

		MouseInputEvent mouseEvent; // Type of mouse event

	private:
		DECLARE_HEAP;
	};

	struct SMouseEvent : public IEventData
	{
		static const EventType sk_EventType;

		const EventType& GetEventType() const
		{
			return sk_EventType;	
		}

		virtual ~SMouseEvent() {}

		explicit SMouseEvent(const SMouseInput& input)
		{
			m_mouseInput.x = input.x;
			m_mouseInput.y = input.y;
			m_mouseInput.Wheel = input.Wheel;
			m_mouseInput.ButtonStates = input.ButtonStates;
			m_mouseInput.shift = input.shift;
			m_mouseInput.ctrl = input.ctrl;
		}

		void Serialise(std::ostream &out) const
		{
			out << m_mouseInput.x;
			out << m_mouseInput.y;
			out << m_mouseInput.Wheel;
			out << m_mouseInput.ButtonStates;
			out << m_mouseInput.shift;
			out << m_mouseInput.ctrl;
		}

		void Serialise(CBufferIO & data) const
		{

		}

		SMouseInput m_mouseInput;

	private:
		DECLARE_HEAP;
	};

}



#endif