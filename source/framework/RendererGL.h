#ifndef CRENDERER_GL_H
#define CRENDERER_GL_H

#include "IRenderer.h"
#include "Colour.h"



#include "GL/glew.h"

namespace Engine
{

class CRendererGL : public IRenderer
{
public:
	typedef std::map< CIndexBufferPtr, CIndexBufferGLPtr > CIndexBufferMap;
	typedef std::map< CVertexBufferPtr, CVertexBufferGLPtr > VertexBufferMap;
	typedef std::map< CVertexFormatPtr, CVertexFormatGLPtr > VertexFormatMap;

	typedef std::vector<CSceneNode*> SceneNodeVector;

public:
    CRendererGL();
    virtual ~CRendererGL();

    bool Initialise(HWND hwnd, HINSTANCE hinst, bool windowed);
    void Release();

	// Picking
	bool GetPickRay (Int32 x, Int32 y, Vector3f& origin, Vector3f& direction) const;

	// Hardware buffer management

	// Vertex buffer
	void Enable(CVertexBufferPtr pVBuffer);
	void Disable(CVertexBufferPtr pVBuffer);

	// Vertex format
	void Enable(CVertexFormatPtr pVFormat);
	void Disable(CVertexFormatPtr pVFormat);

	// Index buffer
	void Enable(CIndexBufferPtr pIBuffer);
	void Disable(CIndexBufferPtr pIBuffer);

	// Camera Settings
	void SetCamera(CCameraPtr pCamera);
	CCameraPtr GetCamera();

	// Render functions
	bool PreDraw();
	void PostDraw();
	void Draw(const SceneNodeVector& visibleSet);
	void Draw(const CSceneNode* pNode);
	void DrawPrimitive(const CSceneNode* pNode);

	// Frame Control
	void SetViewport(const Viewporti& view);
	void Resize(Int32 width, Int32 height);
	Viewporti GetViewport() const;
	bool EnterFullscreen(const Point2l& fullSize);
	bool ExitFullscreen();

	// Buffer Control
	void ClearColorBuffer();
	void ClearDepthBuffer();
	void ClearStencilBuffer();
	void ClearBuffers();
	void ClearColorBuffer (Int32 x, Int32 y, Int32 w, Int32 h);
	void ClearDepthBuffer (Int32 x, Int32 y, Int32 w, Int32 h);
	void ClearStencilBuffer (Int32 x, Int32 y, Int32 w, Int32 h);
	void ClearBuffers (Int32 x, Int32 y, Int32 w, Int32 h);
	void DisplayColorBuffer();
 
private: // Private functions
	void BuildPixelFormatDescriptor(PIXELFORMATDESCRIPTOR &pfd);
	void BuildGLDevice(HWND hwnd, PIXELFORMATDESCRIPTOR &pfd, GLint &pixelformat, HDC &dc);
	HGLRC BuildContextLevel(HDC &dc);
	HWND CreateAAWindow();
	bool BuildAASupport(GLint &pixelformat);
	void IntialiseSubSystems();
	
private:
	// Renderstate data
	CRenderDataGLPtr m_pData;

	// Resources
	CIndexBufferMap m_indexBuffers;
	VertexBufferMap m_vertexBuffers;
	VertexFormatMap	m_vertexFormats;

	// States
	CAlphaState* m_pDefaultAlphaState;
    CCullState* m_pDefaultCullState;
    CDepthState* m_pDefaultDepthState;
    COffsetState* m_pDefaultOffsetState;
    CStencilState* m_pDefaultStencilState;
    CWireState*	m_pDefaultwireState;

	// Camera
	CCameraPtr m_pCamera;

	// Renderwindow details
	bool m_windowed;
	bool m_multisampleSupport;
	UInt8 m_antiAlias;
	UInt8 m_bits;					// Minimum Bits per pixel of the color buffer in fullscreen mode. Ignored if windowed mode. Default: 32.
	UInt8 m_zBufferBits; 			// Minimum Bits per pixel of the depth buffer. Default: 32.
	bool m_vsync;
	bool m_stencilBuffer;
	bool m_stereoBuffer;			// opengl only
	bool m_doubleBuffer;

	// Framebuffer clearing.
	Colour4f m_clearColor;
	float m_clearDepth;
	UInt32 m_clearStencil;

private:
	DECLARE_HEAP;

};



}

#endif



/*

class CRendererGL
{
public:
    CRendererGL();
    ~CRendererGL() { Release(); }
    BOOL Initialise( HWND hWnd, BOOL bWindowed );
    void Release();
    BOOL Reset();
    LPDIRECT3D9       GetD3D()    { return m_pD3D9; }
    LPDIRECT3DDEVICE9 GetDevice() { return m_pDevice; }

    BOOL Windowed;

private:
    BOOL BuildPresentParameters();
    HWND                    m_hWnd;
    HINSTANCE               m_hInstance;
    LPDIRECT3D9             m_pD3D9;
    LPDIRECT3DDEVICE9       m_pDevice;
    D3DCAPS9                m_caps;
    D3DPRESENT_PARAMETERS   m_pp;
    D3DDISPLAYMODE          m_displayMode;
};


*/