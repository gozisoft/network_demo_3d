#ifndef IEVENT_MANAGER_H
#define IEVENT_MANAGER_H

#include "IEventReciever.h"

namespace Engine
{
	class IEventManager
	{
	public:
		typedef IEventData::EventType EventType;

		virtual ~IEventManager() {}

		virtual void AddRegisteredEventType(const EventType & eventType) = 0;

		virtual bool AddListener(const IEventListenerPtr& inListener, const EventType& inType) = 0;

		virtual bool DelListener(const IEventListenerPtr& inListener, const EventType& inType) = 0;

		virtual bool Trigger(const IEventData& inEvent) const = 0;

		virtual bool QueueEvent(const IEventDataPtr& inEvent) = 0;

		virtual bool AbortEvent(const EventType& inType, bool allOfType) = 0;

		virtual bool Tick(ULong maxMillis) = 0;

		virtual bool ValidateType(const EventType& inType) const = 0;

	protected:
		IEventManager();
		IEventManager(const char* pName, bool setAsGlobabl);

	private:
		DECLARE_HEAP;
	};

}


#endif