#ifndef __CZBUFFER_STATE_H__
#define __CZBUFFER_STATE_H__

#include "Core.h"

namespace Engine
{

class CZBufferState
{
public:
	enum CompareTypes
	{
		CT_NEVER = 0,
		CT_LESS,
		CT_EQUAL,
		CT_LEQUAL,
		CT_GREATER,
		CT_NOTEQUAL,
		CT_GEQUAL,
		CT_ALWAYS,
		CT_QUANTITY
	};

	CZBufferState();
	~CZBufferState();

	bool		 m_isEnabled;	// default: true
	bool		 m_isWritable;	// default: true
	CompareTypes m_compare;		// default: CF_LEQUAL

private:
	DECLARE_HEAP;
};


}


#endif