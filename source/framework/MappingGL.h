#ifndef MAPPING_GL_H
#define MAPPING_GL_H

#include "AlphaState.h"
#include "CullState.h"
#include "DepthState.h"
#include "OffsetState.h"
#include "StencilState.h"
#include "WireState.h"

#include "VertexTypes.h"
#include "Buffer.h"
#include "SceneNode.h"

#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif
#include "GL/glew.h"

namespace Engine
{

//----------------------------------------------------------------------------
// Buffer Mapping
//----------------------------------------------------------------------------
extern GLenum GLBufferLockingMode[CBuffer::NUM_LOCKING_STATES];
extern GLenum GLBufferUsage[CBuffer::NUM_BUFFER_USES];
extern GLenum GLPrimitiveType[CSceneNode::NUM_GEOMETRIC_TYPES];

//----------------------------------------------------------------------------
// States Mapping
//----------------------------------------------------------------------------
// Alpha
extern GLenum GLAlphaSrcBlend[CAlphaState::NUM_SRC_BLENDS];
extern GLenum GLAlphaDstBlend[CAlphaState::NUM_DST_BLENDS];
extern GLenum GLAlphaCompare[CAlphaState::NUM_CMP_MODES];

// Depth
extern GLenum GLDepthCompare[CDepthState::NUM_CMP_MODES];

// Stencil
extern GLenum GLStencilCompare[CStencilState::NUM_CMP_MODES];
extern GLenum GLStencilOperation[CStencilState::NUM_OP_MODES];


extern GLuint GLVertexType(VertexType vtype);



}

#endif