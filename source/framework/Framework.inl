#ifndef CFRAMEWORK_INL
#define CFRAMEWORK_INL

inline HWND CFramework::GetHWND() const
{
	return m_hwnd;
}

inline void CFramework::SetHWND(HWND hwnd)
{
	m_hwnd = hwnd;
}

inline HINSTANCE CFramework::GetHINSTANCE() const
{
	return m_hInstance;
}


#endif