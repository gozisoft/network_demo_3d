#include "Frameafx.h"
#include "VertexFormatGL.h"
#include "MappingGL.h"
#include "VertexFormat.h"
#include "VertexElement.h"


using namespace Engine;

DEFINE_HEAP(CVertexFormatGL, "CVertexFormatGL");

CVertexFormatGL::CVertexFormatGL(CVertexFormatPtr pVFromat) : 
m_pVFormat(pVFromat),
m_numActiveCols(0),
m_numActiveTextures(0)
{


}

CVertexFormatGL::~CVertexFormatGL()
{

}

void CVertexFormatGL::Enable()
{
	CVertexElementPtr pElement = CVertexElementPtr();

	if ( m_pVFormat->IsEnabled(VS_POSITION) )
	{
		pElement = m_pVFormat->GetElementBySemantic(VS_POSITION);
		glEnableClientState(GL_VERTEX_ARRAY);

		glVertexPointer( pElement->GetVertexChannels(), GLVertexType( pElement->GetVertexType() ), m_pVFormat->GetTotalStride(),
			BufferOffset( pElement->GetStride() ) );
	}

	if ( m_pVFormat->IsEnabled(VS_NORMAL) )
	{
		pElement = m_pVFormat->GetElementBySemantic(VS_NORMAL);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, pElement->GetVertexChannels(), GLVertexType( pElement->GetVertexType() ), GL_FALSE, m_pVFormat->GetTotalStride(),
			BufferOffset( pElement->GetStride() ) );	
	}

	if ( m_pVFormat->IsEnabled(VS_COLOUR) )
	{
		for (UInt i = 0; i < CVertexFormat::MAX_COLOURS; ++i)
		{
			pElement = m_pVFormat->GetElementBySemantic(VS_COLOUR, i);
			if (pElement)
			{
				++m_numActiveCols; // increment counter for active colours

				(i == 0) ? glEnableClientState(GL_COLOR_ARRAY) : glEnableClientState(GL_SECONDARY_COLOR_ARRAY);

				glColorPointer( pElement->GetVertexChannels(), GLVertexType( pElement->GetVertexType() ), m_pVFormat->GetTotalStride(),
					BufferOffset( pElement->GetStride() ) );
			}
		}
	}

}

void CVertexFormatGL::Disable()
{
	if ( m_pVFormat->IsEnabled(VS_COLOUR) )
	{
		if (m_numActiveCols == 1) 
			glDisableClientState(GL_COLOR_ARRAY);

		if (m_numActiveCols == 2) 
			glDisableClientState(GL_SECONDARY_COLOR_ARRAY);
	}

	if ( m_pVFormat->IsEnabled(VS_NORMAL) )
	{
		glDisableVertexAttribArray(1);
	}

	if ( m_pVFormat->IsEnabled(VS_POSITION) )
	{
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}