#include "frameafx.h"
#include "ISceneController.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(ISceneController, "ISceneController", "IEventListener");
DEFINE_HIERARCHICALHEAP(ISceneControllerFPS, "ISceneControllerFPS", "ISceneController");
