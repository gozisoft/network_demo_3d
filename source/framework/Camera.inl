#ifndef CCAMERA_INL
#define CCAMERA_INL

inline Matrix4f CCamera::GetViewMatrix() const
{
	return m_viewMatrix;
}

inline Matrix4f CCamera::GetProjectionMatrix() const
{
	return m_projectionMatrix[m_depthType];
}

inline Matrix4f CCamera::GetProjViewMatrix() const
{
	return m_projectionViewMatrix[m_depthType];
}

inline void CCamera::SetFrame(const Vector3f& pos, const Vector3f& upVector, 
	const Vector3f& rightVector, const Vector3f& lookVector)
{
	SetPosition(pos);
	SetAxis(upVector, rightVector, lookVector);
}

inline void CCamera::SetPosition(const Vector3f& pos)
{
	m_pos = pos;
}

inline const float* CCamera::GetFrustum() const
{
	return &m_frustum[0];
}

inline CCamera::DepthType CCamera::GetDepthType() const
{
	return m_depthType;
}

inline Vector3f CCamera::GetPosition() const
{
	return m_pos;
}

inline Vector3f CCamera::GetUVector() const
{
	return m_yaxis;
}
	
inline Vector3f CCamera::GetRVector() const
{
	return m_xaxis;		
}

inline Vector3f CCamera::GetFVector() const
{
	return m_zaxis;
}




#endif