#ifndef __CTRIANGLE_MESH_H__
#define __CTRIANGLE_MESH_H__

#include "Triangle.h"
#include "IndexBuffer.h"

namespace Engine
{

class CTriMesh : public CTriangles
{
public:
	CTriMesh(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer);
	~CTriMesh();
	UInt GetTriangleCount() const;
	bool GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const;

protected:
	CTriMesh(); // protected ctor

private:
	DECLARE_HEAP;

};


#include "TriangleMesh.inl"

}


#endif