#ifndef CVERTEX_FORMAT_D3D9_H
#define CVERTEX_FORMAT_D3D9_H

#include "framefwd.h"

// Direct3D9 includes
#include <d3d9.h>

namespace Engine
{

class CVertexFormatD3D9
{
	
public:
	CVertexFormatD3D9(IDirect3DDevice9* pDevice, CVertexFormatPtr pVFromat);
	~CVertexFormatD3D9();

	void Enable(IDirect3DDevice9* pDevice);
	void Disable(IDirect3DDevice9* pDevice);

	typedef std::vector<D3DVERTEXELEMENT9> Elements;

private:
	Elements m_elements;
	IDirect3DVertexDeclaration9* m_pDeclaration;

	DECLARE_HEAP;

};


}


#endif