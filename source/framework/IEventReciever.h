#ifndef IEVENT_RECIEVER_H
#define IEVENT_RECIEVER_H

#include "framefwd.h"
#include "apifwd.h"


namespace Engine
{

	class IEventData
	{
	public:
		typedef CHashedString EventType;

		virtual ~IEventData() {}
		virtual const EventType& GetEventType() const = 0;
		virtual void Serialise(std::ostream & os) const = 0;
		virtual void Serialise(CBufferIO & data) const = 0;

	private:
		DECLARE_HEAP;

	};

	class IEventListener
	{
	public:
		explicit IEventListener() {}

		virtual ~IEventListener() {}

		// return true to indicate that this listener consumed the event
		// and it should not be continue to be propogated.
		virtual bool HandleEvent(const IEventData& event) = 0
		{
			UNREFERENCED_PARAMETER(event);
			// Pure virtual function, makes it easier to wire
			// up derived classes to have a default implementaiton
			// of HandleEvent.
			return true;
		}

	private:
		DECLARE_HEAP;

	};

}

#endif