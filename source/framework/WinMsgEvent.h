#ifndef CWINDOW_MESSAGE_EVENT_H
#define CWINDOW_MESSAGE_EVENT_H

#include "WinMouseEvent.h"
#include "WinKeyboardEvent.h"
#include "WinSizeEvent.h"

namespace Engine
{

	class CWinEvent
	{
	public:
		enum WinEventType
		{
			W_MOUSE_EVENT,
			W_KEYBOARD_EVENT,
			W_JOYPAD_EVENT,
			W_NETWORK_EVENT
		};

		WinEventType EventType;
		union
		{
			struct SMouseInput MouseInput;
			struct SKeyInput KeyInput;
		};

	};


}

#endif