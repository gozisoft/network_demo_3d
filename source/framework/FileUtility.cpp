#include "frameafx.h"
#include "FileUtility.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#pragma warning(disable : 4996)

#include <tchar.h>

using namespace Engine;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Searches through folders to find a media file. Adapted from the DirectX Sample Framework.
Parameters:
[in] file - Name of the file we're looking for.</param>
Returns: The path of the file or NULL if the file wasn't found
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
String CFileUtility::GetMediaFile(const String& file)
{
    Char exeName[MAX_PATH] = {0};
    Char exeFolder[MAX_PATH] = {0};

    // Get full executable path
    GetModuleFileName( NULL, exeFolder, MAX_PATH );
    exeFolder[MAX_PATH - 1] = 0;

    // Get pointer to beginning of executable file name
    // which is after the last slash
    Char* pCutPoint = NULL;
    for ( int i = 0; i < MAX_PATH; ++i )
    {
        if ( exeFolder[i] == TEXT('\\') )
        {
            pCutPoint = &exeFolder[i + 1];
        }
    }

    if ( pCutPoint )
    {
        // Copy over the exe file name
        _tcscpy( exeName, pCutPoint );

        // Chop off the exe file name from the path so we
        // just have the exe directory
        *pCutPoint = 0;

        // Get pointer to start of the .exe extension 
        pCutPoint = NULL;
        for ( int i = 0; i < MAX_PATH; i++ )
        {
            if ( exeName[i] == TEXT('.') )
            {
                pCutPoint = &exeName[i];
            }
        }

        // Chop the .exe extension from the exe name
        if ( pCutPoint )
        {
            *pCutPoint = 0;
        }

        // Add a slash
        _tcscat( exeName, TEXT("\\") );
    }

    // Search all the folders in searchFolders
	String path = SearchFolders( file, exeFolder, exeName );
	if ( !path.empty()  )
    {
        return path;
    }

    // Search all the folders in searchFolders with media\ appended to the end
    Char mediaFile[MAX_PATH] = TEXT("media\\");
    _tcscat( mediaFile, &file[0] );
	path = SearchFolders(mediaFile, exeFolder, exeName);
	if ( !path.empty()  )
    {
        return path;
    }

	OutputDebugString( TEXT("Can not find the specified file.") );
    return String();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Searches through folders to find a media file. Adapted from the DirectX Sample Framework.
Parameters:
[in] filename - File we are looking for
[in] exeFolder - Folder of the executable
[in] exeName - Name of the executable
[in] fullPath - Returned path if file is found.
Returns: true if the file was found, false otherwise.
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
String CFileUtility::SearchFolders(const String& filename, Char* exeFolder, Char* exeName)
{
    Char* searchFolders[] = 
    { 
        TEXT(".\\"), TEXT("..\\"), TEXT("..\\..\\"), TEXT("%s"), TEXT("%s..\\"), TEXT("%s..\\..\\"), TEXT("%s..\\%s"), TEXT("%s..\\..\\%s")
    };

    // Look through each folder to find the file
    Char currentPath[MAX_PATH] = {0};
    for ( int i = 0; i < 8; ++i )
    {
        _stprintf(currentPath, searchFolders[i], exeFolder, exeName);
        _tcscat(currentPath, &filename[0]);
        if (GetFileAttributes( currentPath ) != INVALID_FILE_ATTRIBUTES)
        {
			String fullPath = currentPath;
            return fullPath;
        }
    }

    // Crap...didn't find it
    return String();
}
