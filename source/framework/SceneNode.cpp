#include "Frameafx.h"
#include "SceneNode.h"
#include "IndexBuffer.h"
#include "VertexBuffer.h"
#include "VertexFormat.h"
#include "VertexElement.h"
#include "Culler.h"
#include "Intersections.h"
#include "GeometryCreator.h"


using namespace Engine;

DEFINE_HIERARCHICALHEAP(CSceneNode, "CSceneNode", "CSpatial");

CSceneNode::CSceneNode() : CSpatial()
{

}

CSceneNode::CSceneNode(PrimitiveType primativeType, CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer) : 
CSpatial(),
m_pVFormat(pVFormat),
m_pVBuffer(pVBuffer),
m_pIBuffer(pIBuffer),
m_type(primativeType),
m_isVisible(true)
{
	UpdateModelSpace(GU_MODEL_BOUND_ONLY);
}

CSceneNode::~CSceneNode()
{


}

void CSceneNode::UpdateModelSpace(GeoUpdate type)
{
	UNREFERENCED_PARAMETER(type);
	UpdateModelBound();
}

void CSceneNode::UpdateModelBound()
{
	CVertexElementPtr pElement = m_pVFormat->GetElementBySemantic(VS_POSITION);
	if (!pElement)
	{
		assert( false && ieS("Requires position element to compute bounding sphere\n") );
		return;
	}

	VertexType type = pElement->GetVertexType();
	if ( (type != VT_FLOAT3) && (type != VT_FLOAT4) )
	{
		assert( false && ieS("Can only be used on 3 or 4 float elements\n") );
		return;
	}

	// Number of vertex positions
	UInt numVertices = m_pVBuffer->GetNumElements();

	// Shift interleved position data to non interleved memory
	std::vector<Vector3f> testPos( numVertices );
	std::vector<Vector3f>::pointer itor = &testPos[0];
	for ( UInt i = 0; i < numVertices; ++i )
	{
		const float *pos = reinterpret_cast<const float*>( ( m_pVBuffer->GetData() + pElement->GetStride() ) + ( i * m_pVFormat->GetTotalStride() ) );
		std::memcpy( itor, &pos[0], SizeOfType(type) );
		++itor;
	}

	CGeometry(m_pVFormat).ComputeBoundingSphere(m_pVBuffer->GetData(), m_pVBuffer->GetNumElements(), m_modelBound);
}

void CSceneNode::UpdateWorldBound()
{
	m_modelBound.TrasnfromBy(m_world, m_worldBound);
}

void CSceneNode::GetVisible(CCuller& culler, bool notCull)
{
	// insert scene node into the culler's visible list
	
}