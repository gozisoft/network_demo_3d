#ifndef __CVERTEX_BUFFER_ACCESS_INL__
#define __CVERTEX_BUFFER_ACCESS_INL__


inline const UInt8* CVertexBufferAccess::GetData () const
{
	return m_pVertexBuffer->GetData();
}

inline UInt CVertexBufferAccess::GetNumVertices () const
{
	return m_pVertexBuffer->GetNumElements();
}

inline UInt CVertexBufferAccess::GetStride () const
{
	return m_pVertexFormat->GetTotalStride();
}

//----------------------------------------------------------------------------
// Position
//----------------------------------------------------------------------------
template <typename T> 
inline T& CVertexBufferAccess::Position(UInt i)
{
	return *(T*)( m_position + i * m_pVertexFormat->GetTotalStride() );
}

template <typename T> 
inline T* CVertexBufferAccess::GetPosition(UInt i)
{
	return static_cast<T*>( m_position + i * m_pVertexFormat->GetTotalStride() );
}

inline bool CVertexBufferAccess::HasPosition() const
{
	return m_pVertexFormat->IsEnabled(VS_POSITION);
}

inline UInt CVertexBufferAccess::GetPositionChannels() const
{
	CVertexElementPtr pElement = m_pVertexFormat->GetElementBySemantic(VS_POSITION);
	return ( pElement ) ? pElement->GetVertexChannels() : 0;
}

//----------------------------------------------------------------------------
// Normal
//----------------------------------------------------------------------------
template <typename T> 
inline T& CVertexBufferAccess::Normal(UInt i)
{
	return *(T*)( m_normal + i * m_pVertexFormat->GetTotalStride() );
}

template <typename T>
inline T* CVertexBufferAccess::GetNormal(UInt i)
{
	return (T*)( m_normal + i * m_pVertexFormat->GetTotalStride() );
}

inline bool CVertexBufferAccess::HasNormal() const
{
	return m_normal != 0;
}

inline UInt CVertexBufferAccess::GetNormalChannels() const
{
	CVertexElementPtr pElement = m_pVertexFormat->GetElementBySemantic(VS_NORMAL);
	return ( pElement ) ? pElement->GetVertexChannels() : 0;
}

//----------------------------------------------------------------------------
// Tangent
//----------------------------------------------------------------------------
template <typename T>
inline T& CVertexBufferAccess::Tangent(UInt i)
{
	return *(T*)( m_tangent + i * m_pVertexFormat->GetTotalStride() ); 
}

template <typename T>
inline T* CVertexBufferAccess::GetTangent(UInt i)
{
	return static_cast<T*>( m_tangent + i * m_pVertexFormat->GetTotalStride() );
}

inline bool CVertexBufferAccess::HasTangent() const
{
	return m_tangent != 0;
}

inline UInt CVertexBufferAccess::GetTangentChannels() const
{
	CVertexElementPtr pElement = m_pVertexFormat->GetElementBySemantic(VS_TANGENT);
	return ( pElement ) ? pElement->GetVertexChannels() : 0;
}

//----------------------------------------------------------------------------
// Binormal
//----------------------------------------------------------------------------
template <typename T>
inline T& CVertexBufferAccess::Binormal(UInt i)
{
	return *(T*)( m_binormal + i * m_pVertexFormat->GetTotalStride() ); 
}

template <typename T>
inline T* CVertexBufferAccess::GetBinormal(UInt i)
{
	return static_cast<T*>( m_binormal + i * m_pVertexFormat->GetTotalStride() ); 
}

inline bool CVertexBufferAccess::HasBinormal() const
{
	return m_binormal != 0;
}

inline UInt CVertexBufferAccess::GetBinormalChannels() const
{
	CVertexElementPtr pElement = m_pVertexFormat->GetElementBySemantic(VS_BINORMAL);
	return ( pElement ) ? pElement->GetVertexChannels() : 0;
}

//----------------------------------------------------------------------------
// Texture
//----------------------------------------------------------------------------
template <typename T> 
inline T& CVertexBufferAccess::TCoord (UInt unit, UInt i)
{
	return *(T*)( m_tCoord[unit] + i * m_pVertexFormat->GetTotalStride() ); 
}

template <typename T> 
inline T* CVertexBufferAccess::GetTCoord (UInt unit, UInt i)
{
	return static_cast<T*>( m_tCoord[unit] + i * m_pVertexFormat->GetTotalStride() );
}

inline bool CVertexBufferAccess::HasTCoord (UInt unit) const
{
	return m_tCoord[unit] != 0;
}

inline UInt CVertexBufferAccess::GetTCoordChannels (UInt unit) const
{
	CVertexElementPtr pElement = m_pVertexFormat->GetElementBySemantic(VS_TEXTURE_COORDINATES, unit);
	return ( pElement ) ? pElement->GetVertexChannels() : 0;
}

//----------------------------------------------------------------------------
// Colour
//----------------------------------------------------------------------------
template <typename T> 
inline T& CVertexBufferAccess::Colour(UInt unit, UInt i)
{
	return *(T*)( m_colour[unit] + i * m_pVertexFormat->GetTotalStride() ); 
}

template <typename T>
inline T* CVertexBufferAccess::GetColour(UInt unit, UInt i)
{
	return static_cast<T*>( m_colour[unit] + i * m_pVertexFormat->GetTotalStride() );
}

inline bool CVertexBufferAccess::HasColour(UInt unit) const
{
	return  m_colour[unit] != 0;
}

inline UInt CVertexBufferAccess::GetColourChannels(UInt unit) const
{
	CVertexElementPtr pElement = m_pVertexFormat->GetElementBySemantic(VS_TEXTURE_COORDINATES, unit);
	return ( pElement ) ? pElement->GetVertexChannels() : 0;
}

//----------------------------------------------------------------------------
// Indices
//----------------------------------------------------------------------------
template <typename T> 
inline T& CVertexBufferAccess::BlendIndices(UInt i)
{
	return *(T*)( m_blendIndices + i * m_pVertexFormat->GetTotalStride() ); 
}

inline bool CVertexBufferAccess::HasBlendIndices() const
{
	return m_blendIndices != 0;
}

//----------------------------------------------------------------------------
// Blend weight
//----------------------------------------------------------------------------
template <typename T> 
inline T& CVertexBufferAccess::BlendWeight(UInt i)
{
	return *(T*)( m_blendWeight + i * m_pVertexFormat->GetTotalStride() ); 
}

inline bool CVertexBufferAccess::HasBlendWeight() const
{
	return m_blendWeight != 0;
}






#endif