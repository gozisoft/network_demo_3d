#include "Frameafx.h"
#include "TriangleMesh.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CTriMesh, "CTriMesh", "CTriangles");


CTriMesh::CTriMesh(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer) : 
CTriangles(PT_TRIMESH, pVFormat, pVBuffer, pIBuffer)
{

}

CTriMesh::~CTriMesh()
{

}

bool CTriMesh::GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const
{
	if (pos >= 0 && pos < GetTriangleCount())
	{
		switch (m_pIBuffer->GetType())
		{
		case IT_USHORT:
			{
				const UShort *pIBuffer = (3 * pos) + reinterpret_cast<const UShort*>( m_pIBuffer->GetData() );
				i0 = *pIBuffer++;
				i1 = *pIBuffer++;
				i2 = *pIBuffer;
				return true;
			}
			break;

		case IT_UINT:
			{
				const UInt32 *pIBuffer = (3 * pos) + reinterpret_cast<const UInt32*>( m_pIBuffer->GetData() );
				i0 = *pIBuffer++;
				i1 = *pIBuffer++;
				i2 = *pIBuffer;
				return true;
			}
			break;
		};
	}

	return false;
}
