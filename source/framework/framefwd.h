#ifndef NET_FORWARD_H
#define NET_FORWARD_H

#include "Core.h"


namespace Engine
{

	//----------------------------------------------------------------------------
	// Aplication
	//----------------------------------------------------------------------------
	class IBaseApp;
	class IEventData;
	class IEventListener;
	class IMouseController;
	class IEventManager;

	struct SKeyEvent;
	struct SMouseEvent;
	struct SWindowSizeEvent;
	struct SWindowCreateEvent;

	//----------------------------------------------------------------------------
	// Win32
	//----------------------------------------------------------------------------
	class CWinEvent;
	class CFramework;
	class CTimer;
	class CMouseController; 

	//----------------------------------------------------------------------------
	// System
	//----------------------------------------------------------------------------
	class CEventManager;

	//----------------------------------------------------------------------------
	// Pointer types
	//----------------------------------------------------------------------------
	// Interfaces
	typedef shared_ptr<IEventData> IEventDataPtr;
	typedef shared_ptr<IEventListener> IEventListenerPtr;
	typedef shared_ptr<IMouseController> IMouseControllerPtr;
	typedef shared_ptr<IEventManager> IEventManagerPtr;

	// Application
	typedef shared_ptr<IBaseApp> IBaseAppPtr;

	// Framework
	typedef shared_ptr<CFramework> CFrameworkPtr;
	typedef shared_ptr<CTimer> CTimerPtr;
	typedef shared_ptr<CMouseController> CMouseControllerPtr;

	// Events
	typedef shared_ptr<CEventManager> CEventManagerPtr;


	//----------------------------------------------------------------------------
	// Rendering
	//----------------------------------------------------------------------------
	class IRenderer;
	class ISceneController;
	class ISceneControllerFPS;

	// Buffer
	class CBuffer;
	class CIndexBuffer;
	class CVertexBuffer;
	class CVertexBufferAccess;
	class CVertexFormat;
	class CVertexElement;

	// States (generic)
	class CGlobalState;
	class CAlphaState;
	class CCullState;
	class CDepthState;
	class COffsetState;
	class CStencilState;
	class CWireState;

	// OpenGL
	class CRendererGL;
	class CRenderDataGL;
	class CVertexBufferGL;
	class CVertexFormatGL;
	class CIndexBufferGL;

	// Direct3d 9
	class CRendererD3D9;
	class CRenderDataD3D9;
	class CIndexBufferD3D9;
	class CVertexBufferD3D9;
	class CVertexFormatD3D9;

	//----------------------------------------------------------------------------
	// Pointer types :: Renderer
	//----------------------------------------------------------------------------
	typedef shared_ptr<IRenderer> IRendererPtr;

	// Scene based event recivers
	typedef shared_ptr<ISceneController> ISceneControllerPtr;
	typedef shared_ptr<ISceneControllerFPS> ISceneControllerFPSPtr;

	// Buffer
	typedef shared_ptr<CIndexBuffer>   CIndexBufferPtr;
	typedef shared_ptr<CVertexBuffer>  CVertexBufferPtr;
	typedef shared_ptr<CVertexElement> CVertexElementPtr;
	typedef shared_ptr<CVertexFormat>  CVertexFormatPtr;

	// OpenGL
	typedef shared_ptr<CRendererGL> CRendererGLPtr;
	typedef shared_ptr<CRenderDataGL> CRenderDataGLPtr;
	typedef shared_ptr<CIndexBufferGL> CIndexBufferGLPtr;
	typedef shared_ptr<CVertexBufferGL> CVertexBufferGLPtr;
	typedef shared_ptr<CVertexFormatGL> CVertexFormatGLPtr;

	// Direct3d 9
	typedef shared_ptr<CRendererD3D9> CRendererD3D9Ptr;
	typedef shared_ptr<CRenderDataD3D9> CRenderDataD3D9Ptr;
	typedef shared_ptr<CIndexBufferD3D9> CIndexBufferD3D9Ptr;
	typedef shared_ptr<CVertexBufferD3D9> CVertexBufferD3D9Ptr;
	typedef shared_ptr<CVertexFormatD3D9> CVertexFormatD3D9Ptr;


	//----------------------------------------------------------------------------
	// Scenegraph
	//----------------------------------------------------------------------------
	class CSpatial;
	class CRootNode;
	class CSceneNode;
	class CTriangles;
	class CTriFan;
	class CTriMesh;
	class CTriStrip;
	class CPolypoint;
	class CPolyline;
	class CCamera;
	class CCameraNodeFPS;
	class CCuller;

	// Controllers
	class CCameraControllerFPS;

	// Geometry
	class CGeometry;

	// visitors
	class CSpatialVistor;
	class CTransformVisitor;

	//----------------------------------------------------------------------------
	// Pointer types :: Scenegraph
	//----------------------------------------------------------------------------
	typedef shared_ptr<CSpatial> CSpatialPtr;
	typedef shared_ptr<CRootNode> CRootNodePtr;
	typedef shared_ptr<CSceneNode> CSceneNodePtr;
	typedef shared_ptr<CTriangles> CTrianglesPtr;
	typedef shared_ptr<CTriFan>	CTriFanPtr;
	typedef shared_ptr<CTriMesh> CTriMeshPtr;
	typedef shared_ptr<CTriStrip> CTriStripPtr;
	typedef shared_ptr<CPolypoint> CPolypointPtr;
	typedef shared_ptr<CPolyline> CPolylinePtr;

	typedef shared_ptr<CCamera>	CCameraPtr;
	typedef shared_ptr<CCameraNodeFPS> CCameraNodeFPSPtr;
	typedef shared_ptr<CCuller> CCullerPtr;

	// Controllers
	typedef shared_ptr<CCameraControllerFPS> CCameraControllerFPSPtr;

	//----------------------------------------------------------------------------
	// Networking
	//----------------------------------------------------------------------------
	// Packets
	class IPacket;
	class CBinaryPacket;
	class CTextPacket;
	class CSafeBinaryPacket;

	typedef shared_ptr<IPacket> IPacketPtr;
	typedef shared_ptr<CBinaryPacket> CBinaryPacketPtr;
	typedef shared_ptr<CTextPacket> CTextPacketPtr;
	typedef shared_ptr<CSafeBinaryPacket> CSafeBinaryPacketPtr;

	// Net based
	class CSocket;
	class CUDPSocket;
	class CUDPEventSocket;
	class CTCPSocket;
	class CTCPEventSocket;

	typedef shared_ptr<CSocket> CSocketPtr;
	typedef shared_ptr<CUDPSocket> CUDPSocketPtr;
	typedef shared_ptr<CTCPSocket> CTCPSocketPtr;





}









#endif