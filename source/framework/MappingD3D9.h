#ifndef MAPPING_D3D9_H
#define MAPPING_D3D9_H

#include "AlphaState.h"
#include "CullState.h"
#include "DepthState.h"
#include "OffsetState.h"
#include "StencilState.h"
#include "WireState.h"

#include "VertexTypes.h"
#include "Buffer.h"

#include "SceneNode.h"

// Direct3D9 includes
#include <d3d9.h>


namespace Engine
{


//----------------------------------------------------------------------------
// Buffer Mapping
//----------------------------------------------------------------------------
extern DWORD D3D9BufferLockingMode[CBuffer::NUM_LOCKING_STATES];
extern DWORD D3D9BufferUsage[CBuffer::NUM_BUFFER_USES];
extern D3DPRIMITIVETYPE D3D9PrimitiveType[CSceneNode::NUM_GEOMETRIC_TYPES];
extern D3DDECLUSAGE D3D9SemanticUsage[NUM_VS_SEMANTICS];


//----------------------------------------------------------------------------
// States Mapping
//----------------------------------------------------------------------------
// Alpha
extern DWORD D3D9AlphaSrcBlend[CAlphaState::NUM_SRC_BLENDS];
extern DWORD D3D9AlphaDstBlend[CAlphaState::NUM_DST_BLENDS];
extern DWORD D3D9AlphaCompare[CAlphaState::NUM_CMP_MODES];

// Depth
extern DWORD D3D9DepthCompare[CDepthState::NUM_CMP_MODES];

// Culling
extern DWORD D3D9CullOrder[2];

// Stencil
extern DWORD D3D9StencilCompare[CStencilState::NUM_CMP_MODES];
extern DWORD D3D9StencilOperation[CStencilState::NUM_OP_MODES];

D3DFORMAT D3D9GetIndexFormat(IndexType itype);
D3DDECLTYPE D3D9VertexType(VertexType vtype);

}


#endif // MAPPING_D3D9_H