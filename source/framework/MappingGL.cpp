#include "FrameAfx.h"
#include "MappingGL.h"

namespace Engine
{

GLenum GLBufferLockingMode[CBuffer::NUM_LOCKING_STATES] =
{
	GL_READ_ONLY,   // BL_READ_ONLY
	GL_WRITE_ONLY,  // BL_WRITE_ONLY
	GL_READ_WRITE   // BL_READ_WRITE
};

GLenum GLBufferUsage[CBuffer::NUM_BUFFER_USES] =
{
	GL_STATIC_DRAW,   // BU_GPU,		// CPU cannont read or write to GPU 
	GL_STATIC_DRAW,   // BU_STATIC,		// CPU can write to GPU on creation 
	GL_DYNAMIC_DRAW,  // BU_DYNAMIC,	// CPU can write to GPU frequently  
	GL_STREAM_DRAW,   // BU_STAGING,	// GPU can read and write to the GPU
};


GLenum GLAlphaSrcBlend[CAlphaState::NUM_SRC_BLENDS] =
{
	GL_ZERO,                        // SB_ZERO
	GL_ONE,                         // SB_ONE
	GL_DST_COLOR,                   // SB_DST_COLOR
	GL_ONE_MINUS_DST_COLOR,         // SB_ONE_MINUS_DST_COLOR
	GL_SRC_ALPHA,                   // SB_SRC_ALPHA
	GL_ONE_MINUS_SRC_ALPHA,         // SB_ONE_MINUS_SRC_ALPHA
	GL_DST_ALPHA,                   // SB_DST_ALPHA
	GL_ONE_MINUS_DST_ALPHA,         // SB_ONE_MINUS_DST_ALPHA
	GL_SRC_ALPHA_SATURATE,          // SB_SRC_ALPHA_SATURATE
	GL_CONSTANT_COLOR,              // SB_CONSTANT_COLOR
	GL_ONE_MINUS_CONSTANT_COLOR,    // SB_ONE_MINUS_CONSTANT_COLOR
	GL_CONSTANT_ALPHA,              // SB_CONSTANT_ALPHA
	GL_ONE_MINUS_CONSTANT_ALPHA     // SB_ONE_MINUS_CONSTANT_ALPHA
};

GLenum GLAlphaDstBlend[CAlphaState::NUM_DST_BLENDS] =
{
	GL_ZERO,                        // DB_ZERO
	GL_ONE,                         // DB_ONE
	GL_SRC_COLOR,                   // DB_SRC_COLOR
	GL_ONE_MINUS_SRC_COLOR,         // DB_ONE_MINUS_SRC_COLOR
	GL_SRC_ALPHA,                   // DB_SRC_ALPHA
	GL_ONE_MINUS_SRC_ALPHA,         // DB_ONE_MINUS_SRC_ALPHA
	GL_DST_ALPHA,                   // DB_DST_ALPHA
	GL_ONE_MINUS_DST_ALPHA,         // DB_ONE_MINUS_DST_ALPHA
	GL_CONSTANT_COLOR,              // DB_CONSTANT_COLOR
	GL_ONE_MINUS_CONSTANT_COLOR,    // DB_ONE_MINUS_CONSTANT_COLOR
	GL_CONSTANT_ALPHA,              // DB_CONSTANT_ALPHA
	GL_ONE_MINUS_CONSTANT_ALPHA     // DB_ONE_MINUS_CONSTANT_ALPHA
};

GLenum GLAlphaCompare[CAlphaState::NUM_CMP_MODES] =
{
	GL_NEVER,       // CM_NEVER
	GL_LESS,        // CM_LESS
	GL_EQUAL,       // CM_EQUAL
	GL_LEQUAL,      // CM_LEQUAL
	GL_GREATER,     // CM_GREATER
	GL_NOTEQUAL,    // CM_NOTEQUAL
	GL_GEQUAL,      // CM_GEQUAL
	GL_ALWAYS       // CM_ALWAYS
};

GLenum GLDepthCompare[CDepthState::NUM_CMP_MODES] = 
{
	GL_NEVER,       // CM_NEVER
	GL_LESS,        // CM_LESS
	GL_EQUAL,       // CM_EQUAL
	GL_LEQUAL,      // CM_LEQUAL
	GL_GREATER,     // CM_GREATER
	GL_NOTEQUAL,    // CM_NOTEQUAL
	GL_GEQUAL,      // CM_GEQUAL
	GL_ALWAYS       // CM_ALWAYS
};

GLenum GLStencilCompare[CStencilState::NUM_CMP_MODES] = 
{
	GL_NEVER,       // CM_NEVER
	GL_LESS,        // CM_LESS
	GL_EQUAL,       // CM_EQUAL
	GL_LEQUAL,      // CM_LEQUAL
	GL_GREATER,     // CM_GREATER
	GL_NOTEQUAL,    // CM_NOTEQUAL
	GL_GEQUAL,      // CM_GEQUAL
	GL_ALWAYS       // CM_ALWAYS
};

GLenum GLStencilOperation[CStencilState::NUM_OP_MODES] =
{
	GL_KEEP,    // OT_KEEP
	GL_ZERO,    // OT_ZERO
	GL_REPLACE, // OT_REPLACE
	GL_INCR,    // OT_INCREMENT
	GL_DECR,    // OT_DECREMENT
	GL_INVERT   // OT_INVERT
};

GLenum GLPrimitiveType[CSceneNode::NUM_GEOMETRIC_TYPES] =
{
    0,                  // PT_NONE (not used)
    GL_POINTS,          // PT_POLYPOINT
    GL_LINES,           // PT_POLYSEGMENTS_DISJOINT
    GL_LINE_STRIP,      // PT_POLYSEGMENTS_CONTIGUOUS
    0,                  // PT_TRIANGLES (not used)
    GL_TRIANGLES,       // PT_TRIMESH
    GL_TRIANGLE_STRIP,  // PT_TRISTRIP
    GL_TRIANGLE_FAN     // PT_TRIFAN
};

GLuint GLVertexType(VertexType vtype)
{
	switch (vtype)
	{
	case VT_BYTE1:
	case VT_BYTE2:
	case VT_BYTE4:
		return GL_BYTE;
	case VT_UBYTE1:
	case VT_UBYTE2:
	case VT_UBYTE4:
		return GL_UNSIGNED_BYTE;
	case VT_SHORT1:
	case VT_SHORT2:
	case VT_SHORT4:
		return GL_SHORT;
	case VT_USHORT1:
	case VT_USHORT2:
	case VT_USHORT4:
		return GL_UNSIGNED_SHORT;
	case VT_INT1:
	case VT_INT2:
	case VT_INT3:
	case VT_INT4:
		return GL_INT;
	case VT_UINT1:
	case VT_UINT2:
	case VT_UINT3:
	case VT_UINT4:
		return GL_UNSIGNED_INT;
	case VT_FLOAT1:
	case VT_FLOAT2:
	case VT_FLOAT3:
	case VT_FLOAT4:
		return GL_FLOAT;
	};

	// keep the compiler happy
	return 0;
}


}