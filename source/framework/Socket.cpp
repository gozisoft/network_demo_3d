#include "frameafx.h"
#include "Socket.h"
#include "Packet.h"
#include "StringFunctions.h"


#include <WS2tcpip.h>
#include <cstring>

using namespace Engine;

DEFINE_HEAP(CSocket, "CSocket");

CSocket::CSocket() : 
m_socket(0),
m_sendOfs(0),
m_timeOut(0),
m_deleteFlag(0),
m_binaryProtocol(true)
{
 
}

CSocket::CSocket(SOCKET socket) : 
m_socket(socket),
m_sendOfs(0),
m_timeOut(0),
m_deleteFlag(0),
m_binaryProtocol(true)
{


}

CSocket::~CSocket()
{
	if (m_socket != INVALID_SOCKET) 
	{
		closesocket(m_socket);
		m_socket = INVALID_SOCKET;
	}

	m_outList.clear();
	m_inList.clear();
}

void CSocket::Send(IPacketPtr pkt, bool clearTimeOut)
{
	if (clearTimeOut)
		m_timeOut = 0;

	m_outList.push_back(pkt);
}