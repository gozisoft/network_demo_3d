#ifndef ISCENE_CONTROLLER_H
#define ISCENE_CONTROLLER_H

#include "IEventReciever.h"
#include "WinKeyboardEvent.h"

namespace Engine
{



class ISceneController : public IEventListener
{
public:
	enum SceneControllerType
	{
		SCT_CAMERA_UNKNOWN = 0,
		SCT_CAMERA_FPS,
		SCT_CAMERA_SPLINE
	};

	virtual ~ISceneController() {}

	// Animates the node (observer pattern only does not store the observed object
	// and instead recives it thourgh AnimateNode)
	// Delta time is in milliseconds.
	virtual void AnimateNode(CSpatial* pSpatial, double deltaTime) = 0;

	////virtual SceneControllerType GetType() const
	//{
	//	return SCT_CAMERA_UNKNOWN;
	//}

	virtual bool HasFinished() const 
	{
		return false;
	}

private:
	DECLARE_HEAP;

};

class ISceneControllerFPS : public ISceneController
{
public:
	enum KeyAction
	{
		KA_MOVE_FORWARD = 0,
		KA_MOVE_BACKWARD,
		KA_STRAFE_LEFT,
		KA_STRAFE_RIGHT,
		KA_JUMP_UP,
		KA_CROUCH,
		KA_COUNT
	};

	enum MouseAction
	{
		MA_LOOK = 0,
		MA_PICK,
		MA_COUNT
	};

	// struct storeing which key belongs to which action
	struct SKeyMap
	{
		SKeyMap() {};
		SKeyMap(KeyAction a, KeyCode k) : action(a), keycode(k) {}

		KeyAction action;
		KeyCode keycode;
	};

	virtual ~ISceneControllerFPS() {}

	virtual float GetMoveSpeed() const = 0; // units per millisecond
	virtual void SetMoveSpeed(float speed) = 0;

	virtual float GetRotateSpeed() const = 0;
	virtual void SetRotateSpeed(float speed) = 0;

	virtual void setVerticalMovement(bool allow) = 0;
	virtual void SetInverseMouse(bool invert) = 0;

	virtual void SetKeyMap(SKeyMap *pKeyMap, UInt32 count) = 0;

private:
	DECLARE_HEAP;
};



} // namespace Engine

#endif