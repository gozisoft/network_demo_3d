#include "Frameafx.h"
#include "VertexTypes.h"

namespace Engine
{


UInt SizeOfType(VertexType type)
{
	switch (type)
	{
	case VT_BYTE1:
	case VT_UBYTE1:
		return sizeof(UInt8);
	case VT_BYTE2:
	case VT_UBYTE2:
		return sizeof(UInt8) * 2;
	case VT_BYTE4:
	case VT_UBYTE4:
		return sizeof(UInt8) * 4;
	case VT_SHORT1:
	case VT_USHORT1:
		return sizeof(UInt16);
	case VT_SHORT2:
	case VT_USHORT2:
		return sizeof(UInt16) * 2;
	case VT_SHORT4:
	case VT_USHORT4:
		return sizeof(UInt16) * 4;
	case VT_INT1:
	case VT_UINT1:
		return sizeof(UInt32);
	case VT_INT2:
	case VT_UINT2:
		return sizeof(UInt32) * 2;
	case VT_INT3:
	case VT_UINT3:
		return sizeof(UInt32) * 3;
	case VT_INT4:
	case VT_UINT4:
		return sizeof(UInt32) * 4;
	case VT_FLOAT1:
		return sizeof(float);
	case VT_FLOAT2:
		return sizeof(float) * 2;
	case VT_FLOAT3:
		return sizeof(float) * 3;
	case VT_FLOAT4:
		return sizeof(float) * 4;
	};

	assert( false && ieS("Unkown VertexType type : SizeOfType()\n") );
	return 0;
}

UInt GetChannelCount(VertexType type)
{
	switch (type)
	{
	case VT_BYTE1:
	case VT_UBYTE1:
	case VT_SHORT1:
	case VT_USHORT1:
	case VT_INT1:
	case VT_UINT1:
	case VT_FLOAT1:
		return 1;
	case VT_BYTE2:
	case VT_UBYTE2:
	case VT_SHORT2:
	case VT_USHORT2:
	case VT_INT2:
	case VT_UINT2:
	case VT_FLOAT2:
		return 2;
	case VT_INT3:
	case VT_UINT3:
	case VT_FLOAT3:
		return 3;
	case VT_BYTE4:
	case VT_UBYTE4:
	case VT_SHORT4:
	case VT_USHORT4:
	case VT_INT4:
	case VT_UINT4:
	case VT_FLOAT4:
		return 4;
	};

	assert( false && ieS("Unkown VertexType type : GetChannelCount()\n") );
	return 0;
}

UInt SizeOfType(IndexType type)
{
	switch(type)
	{
	case IT_USHORT:
		return sizeof(UInt16);
	case IT_UINT:
		return sizeof(UInt32);
	};

	assert( false && ieS("Unkown IndexType type : SizeOfType()\n") );
	return 0;
}


}