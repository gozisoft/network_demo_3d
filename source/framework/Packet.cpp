#include "frameafx.h"
#include "packet.h"
#include "Helper.h"

#include "StringFunctions.h"


using namespace Engine;


CBinaryPacket::CBinaryPacket(const char* data, size_t size)
{
	assert( data && size );

	// Packet Header Info
	// 1) ushort total size
	// 2) ushort packet type

	// Calculate extra space required for data and packet info
	size_t extraSpace = sizeof(u_short) * 2;

	// Create the memory space
	m_pData.resize(size + extraSpace);

	// Assign the total size (size + 2 bytes) of the packet
	// to the first 2 bytes of the data.
	CharVector::pointer pData = &m_pData[0];
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( size + extraSpace ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Assign the packet type to actual data.
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( PT_BINARY ) );

	// Copy data passed in to the member data member
	std::memcpy( &m_pData[0] + extraSpace, data, size );
}

CBinaryPacket::CBinaryPacket(size_t size)
{
	// Packet Header Info
	// 1) ushort total size
	// 2) ushort packet type

	// Calculate extra space required for data and packet info
	size_t extraSpace = sizeof(u_short) * 2;

	// Create the memory space
	m_pData.resize( size + extraSpace );

	// Assign the total size (size + 2 bytes) of the packet
	// to the first four bytes of the data.
	CharVector::pointer pData = &m_pData[0];
	*( reinterpret_cast<u_short *>(pData) ) = htons( size + extraSpace );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Assign the packet type to actual data.
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( PT_BINARY ) );
}

// Do nothing constuctor thats protected
CBinaryPacket::CBinaryPacket()
{

}

CBinaryPacket::~CBinaryPacket()
{
	m_pData.clear();
}

// This looks complicated but its not.
// 1) Cast away the const char*
// 2) reinterpret_cast the char* to usigned short*
// 3) static_cast unsinged short to size_t.
size_t CBinaryPacket::GetSize() const 
{ 
	return static_cast<size_t>( ntohs( *reinterpret_cast<u_short *>( const_cast<char*>(&m_pData[0]) ) ) );
}

IPacket::PacketType CBinaryPacket::GetType() const 
{ 
	return static_cast<IPacket::PacketType>( ntohs( *reinterpret_cast<u_short *>( const_cast<char*>( &m_pData[0] + sizeof(u_short) ) ) ) );
}

size_t CBinaryPacket::GetOffset() const
{
	return size_t(sizeof(u_short) * 2);
}

void CBinaryPacket::MemCpy(const char* data, size_t size, int destOffset)
{
	// Calculate extra space required for data and packet info
	size_t extraSpace = sizeof(u_short) * 2;

	// If size + offset is greator than the array size - ulong, throw an
	// error as we're pointing past the end of the array.
	assert( size + destOffset <= GetSize() - extraSpace );

	// Copy data passed in to the member data member
	std::memcpy(&m_pData[0] + destOffset + extraSpace, data, size);
}


CSafeBinaryPacket::CSafeBinaryPacket(const char* data, size_t size, size_t packetNumber, size_t ack, size_t ack_bits)
{
	assert( data && size );

	// Packet Header Info
	// 1) ushort total size
	// 2) ushort packet type
	// 3) ushort packet sequence
	// 4) ulong standard ack
	// 5) ulong extra ack

	// Calculate extra space required for data and packet info
	size_t extraSpace = (sizeof(u_short) * 3) + (sizeof(u_long) * 2);

	// Create the memory space
	m_pData.resize(size + extraSpace);

	// Assign the total size (size + 2 bytes) of the packet
	// to the first 2 bytes of the data.
	CharVector::pointer pData = &m_pData[0];
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( size + extraSpace ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Assign the packet type to actual data.
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( PT_SAFEBINARY ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Store the sequence number
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( packetNumber ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Store the sequence ack
	*( reinterpret_cast<u_long *>(pData) ) = htonl( static_cast<u_long>( ack ) );

	// Advance the pointer 4 bytes 
	pData += sizeof(u_long);

	// Store the ack_bits
	*( reinterpret_cast<u_long *>(pData) ) = htonl( static_cast<u_long>( ack_bits ) );

	// Copy data passed in to the member data member
	std::memcpy( &m_pData[0] + extraSpace, data, size );
}

CSafeBinaryPacket::CSafeBinaryPacket(size_t size, size_t packetNumber, size_t ack, size_t ack_bits)
{
	// Packet Header Info
	// 1) ushort total size
	// 2) ushort packet type
	// 3) ushort packet sequence
	// 4) ulong standard ack
	// 5) ulong extra ack

	// Calculate extra space required for data and packet info
	size_t extraSpace = (sizeof(u_short) * 3) + (sizeof(u_long) * 2);

	// Create the memory space
	m_pData.resize(size + extraSpace);

	// Assign the total size (size + 2 bytes) of the packet
	// to the first 2 bytes of the data.
	CharVector::pointer pData = &m_pData[0];
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( size + extraSpace ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Assign the packet type to actual data.
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( PT_SAFEBINARY ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Store the sequence number
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( packetNumber ) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Store the sequence ack
	*( reinterpret_cast<u_long *>(pData) ) = htonl( static_cast<u_long>( ack ) );

	// Advance the pointer 4 bytes 
	pData += sizeof(u_long);

	// Store the ack_bits
	*( reinterpret_cast<u_long *>(pData) ) = htonl( static_cast<u_long>( ack_bits ) );
}

// Do nothing constuctor thats protected
CSafeBinaryPacket::CSafeBinaryPacket()
{

}

CSafeBinaryPacket::~CSafeBinaryPacket()
{
	m_pData.clear();
}

// This looks complicated but its not.
// 1) Cast away the const char*
// 2) reinterpret_cast the char* to usigned short*
// 3) static_cast unsinged short to size_t.
size_t CSafeBinaryPacket::GetSize() const 
{ 
	return static_cast<size_t>( ntohs( *reinterpret_cast<u_short *>( const_cast<char*>(&m_pData[0]) ) ) );
}

IPacket::PacketType CSafeBinaryPacket::GetType() const 
{ 
	return static_cast<IPacket::PacketType>( ntohs( *reinterpret_cast<u_short *>( const_cast<char*>( &m_pData[0] + sizeof(u_short) ) ) ) );
}

size_t CSafeBinaryPacket::GetOffset() const
{
	return size_t( (sizeof(u_short) * 3) + (sizeof(u_long) * 2) );
}

void CSafeBinaryPacket::MemCpy(const char* data, size_t size, int destOffset)
{
	// Calculate extra space required for data and packet info
	size_t extraSpace = (sizeof(u_short) * 3) + (sizeof(u_long) * 2);

	// If size + offset is greator than the array size - ulong, throw an
	// error as we're pointing past the end of the array.
	assert( size + destOffset <= GetSize() - extraSpace );

	// Copy data passed in to the member data member
	std::memcpy(&m_pData[0] + destOffset + extraSpace, data, size);
}


CTextPacket::CTextPacket(const String& text) : 
CBinaryPacket()
{
	// Add on 'carrige return' and 'new line'
	std::string strdest( text.size(), '\0' );

	// Convert the char to UTF-8
	GenericToAnsi( &strdest[0], &text[0], text.size(), strdest.size() * sizeof(std::string::value_type) );

	// Calculate extra space required for text and packet info
	size_t extraSpace = sizeof(u_short) * 2;

	// Create the memory space (+1 for null terminating char)
	m_pData.resize( ( strdest.size() * sizeof(std::string::value_type) ) + extraSpace );

	// Assign the total size (size + 2 bytes) of the packet
	// to the first four bytes of the data.
	CharVector::pointer pData = &m_pData[0];
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( ( strdest.size() * sizeof(std::string::value_type) ) + extraSpace) );

	// Advance the pointer 2 bytes 
	pData += sizeof(u_short);

	// Assign the packet type to actual data.
	*( reinterpret_cast<u_short *>(pData) ) = htons( static_cast<u_short>( PT_TEXT ) );

	// Copy data passed in to the member data member
	std::memcpy( &m_pData[0] + extraSpace, &strdest[0], strdest.size() * sizeof(std::string::value_type) );
}






/*
CTextPacket::CTextPacket(const char* text) :
CBinaryPacket( static_cast<u_short>( std::strlen(text) + sizeof(char) * 3 ), PT_TEXT )
{
	CharVector::pointer pData = &m_pData[0] + sizeof(u_short);
	std::strcat(pData, text);
	std::strcat(pData, "\r\n");
}
*/


