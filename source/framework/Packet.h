#ifndef __CPACKET_H__
#define __CPACKET_H__


#include "Core.h"

namespace Engine
{
	// Signature used to identify packet for this engine
	const UInt32 ENGINE_SIGNATURE = 0xDEADC0DE;

	// Two types of basic packet make up this network system :
	// Binary - for use of holding game data ie positions.
	// Text - for use of holding game text.
	class IPacket
	{
	public:
		enum PacketType
		{
			PT_BINARY = 0x00,
			PT_TEXT = 0x01,
			PT_SAFEBINARY = 0x02
		};

		virtual ~IPacket() {}

		virtual const char* GetData() const = 0;
		virtual char* GetData() = 0;
		virtual size_t GetSize() const = 0;
		virtual size_t GetOffset() const = 0;
		virtual PacketType GetType() const = 0;
	};


	// Binary packet class allocates its own bytes and
	// stores is size of the buffer in the first four bytes.
	// These four bytes are stored in network order (Big-Endian)
	class CBinaryPacket : public IPacket
	{
	public:
		// Packet Header Info
		// 1) ushort total size
		// 2) ushort packet type
		CBinaryPacket(const char* data, size_t size);
		CBinaryPacket(size_t size);

		virtual ~CBinaryPacket();

		const char* GetData() const;
		char* GetData();
		size_t GetSize() const;
		size_t GetOffset() const;
		PacketType GetType() const;

		void MemCpy(const char* data, size_t size, int destOffset);

	protected:
		typedef std::vector<char> CharVector;
		CBinaryPacket();

		CharVector m_pData;
	};

	class CSafeBinaryPacket : public IPacket
	{
	public:
		// Packet Header Info
		// 1) ushort total size
		// 2) ushort packet type
		// 3) ushort packet sequence
		// 4) ulong standard ack
		// 5) ulong extra ack
		CSafeBinaryPacket(const char* data, size_t size, size_t packetNumber, size_t ack, size_t ack_bits);
		CSafeBinaryPacket(size_t size, size_t packetNumber, size_t ack, size_t ack_bits);

		virtual ~CSafeBinaryPacket();

		const char* GetData() const;
		char* GetData();
		size_t GetSize() const;
		size_t GetOffset() const;
		PacketType GetType() const;

		void MemCpy(const char* data, size_t size, int destOffset);

	protected:
		typedef std::vector<char> CharVector;
		CSafeBinaryPacket();

		CharVector m_pData;
	};

	class CTextPacket : public CBinaryPacket
	{
	public:
		CTextPacket(const String& text);

	};





#include "Packet.inl"


}


#endif


/*
class CTextPacket : public CBinaryPacket
{
public:
CTextPacket(const char* text);

};
*/