#ifndef CINDEX_BUFFER_D3D9_H
#define CINDEX_BUFFER_D3D9_H

#include "framefwd.h"

// Direct3D9 includes
#include <d3d9.h>

namespace Engine
{


class CIndexBufferD3D9
{
public:
	CIndexBufferD3D9(IDirect3DDevice9* pDevice, CIndexBufferPtr pIBuffer);
	~CIndexBufferD3D9();

	// Vertex buffer operations.
	void Enable(IDirect3DDevice9* pDevice);
	void Disable(IDirect3DDevice9* pDevice);

	void* Lock(CVertexBuffer::Locking mode);
	void Unlock();

private:
	IDirect3DIndexBuffer9* m_pBuffer;
	D3DINDEXBUFFER_DESC m_buffDesc;

	CIndexBufferPtr m_pIBuffer; // keep a pointer to the data on CPU

	DECLARE_HEAP;

};


}


#endif