#include "Frameafx.h"
#include "MappingD3D9.h"

namespace Engine
{

//----------------------------------------------------------------------------
// Buffer Mapping
//----------------------------------------------------------------------------
DWORD D3D9BufferLockingMode[CBuffer::NUM_LOCKING_STATES] = 
{
	D3DLOCK_READONLY,		// BL_READ_ONLY
	0,						// BL_WRITE_ONLY
	0,						// BL_READ_WRITE
	D3DLOCK_DISCARD,		// BL_DISCARD
	D3DLOCK_NOOVERWRITE		// BL_NO_OVERWRITE
};

DWORD D3D9BufferUsage[CBuffer::NUM_BUFFER_USES] = 
{
	D3DUSAGE_RENDERTARGET,      // BU_GPU	
    D3DUSAGE_WRITEONLY,         // BU_STATIC
    D3DUSAGE_DYNAMIC,           // BU_DYNAMIC
    D3DUSAGE_DEPTHSTENCIL		// BU_STAGING
};

D3DPRIMITIVETYPE D3D9PrimitiveType[CSceneNode::NUM_GEOMETRIC_TYPES] = 
{
    D3DPT_FORCE_DWORD,          // PT_NONE (not used)
    D3DPT_POINTLIST,            // PT_POLYPOINTS
    D3DPT_LINELIST,             // PT_POLYLINE_CLOSED
    D3DPT_LINESTRIP,            // PT_POLYLINE_OPEN
    D3DPT_FORCE_DWORD,          // PT_TRIANGLES (not used)
    D3DPT_TRIANGLELIST,         // PT_TRIMESH
    D3DPT_TRIANGLESTRIP,        // PT_TRISTRIP
    D3DPT_TRIANGLEFAN           // PT_TRIFAN
};

D3DDECLUSAGE D3D9SemanticUsage[NUM_VS_SEMANTICS] = 
{	
	D3DDECLUSAGE_POSITION,		//	VS_POSITION		
	D3DDECLUSAGE_NORMAL,		//	VS_NORMAL			
	D3DDECLUSAGE_TANGENT,		//	VS_TANGENT		
	D3DDECLUSAGE_BINORMAL,		//	VS_BINORMAL		
	D3DDECLUSAGE_COLOR,			//	VS_COLOUR		
	D3DDECLUSAGE_BLENDINDICES,	//	VS_BLEND_INDICES	
	D3DDECLUSAGE_BLENDWEIGHT,	//	VS_BLEND_WEIGHTS	
	D3DDECLUSAGE_TEXCOORD,		//	VS_TEXTURE_COORDINATES
	D3DDECLUSAGE_FOG,			//	VS_FOG			
	D3DDECLUSAGE_PSIZE			//	VS_PSIZE				
};

//----------------------------------------------------------------------------
// States Mapping
//----------------------------------------------------------------------------
// Alpha
DWORD D3D9AlphaSrcBlend[CAlphaState::NUM_SRC_BLENDS] = 
{
    D3DBLEND_ZERO,              // AlphaState::SBM_ZERO
    D3DBLEND_ONE,               // AlphaState::SBM_ONE
    D3DBLEND_DESTCOLOR,         // AlphaState::SBM_DST_COLOR
    D3DBLEND_INVDESTCOLOR,      // AlphaState::SBM_ONE_MINUS_DST_COLOR
    D3DBLEND_SRCALPHA,          // AlphaState::SBM_SRC_ALPHA
    D3DBLEND_INVSRCALPHA,       // AlphaState::SBM_ONE_MINUS_SRC_ALPHA
    D3DBLEND_DESTALPHA,         // AlphaState::SBM_DST_ALPHA
    D3DBLEND_INVDESTALPHA,      // AlphaState::SBM_ONE_MINUS_DST_ALPHA
    D3DBLEND_SRCALPHASAT,       // AlphaState::SBM_SRC_ALPHA_SATURATE
    0,                          // AlphaState::SBM_CONSTANT_COLOR
    0,                          // AlphaState::SBM_ONE_MINUS_CONSTANT_COLOR
    0,                          // AlphaState::SBM_CONSTANT_ALPHA
    0                           // AlphaState::SBM_ONE_MINUS_CONSTANT_ALPHA
};

DWORD D3D9AlphaDstBlend[CAlphaState::NUM_DST_BLENDS] = 
{
    D3DBLEND_ZERO,              // AlphaState::DBM_ZERO
    D3DBLEND_ONE,               // AlphaState::DBM_ONE
    D3DBLEND_SRCCOLOR,          // AlphaState::DBM_SRC_COLOR
    D3DBLEND_INVSRCCOLOR,       // AlphaState::DBM_ONE_MINUS_SRC_COLOR
    D3DBLEND_SRCALPHA,          // AlphaState::DBM_SRC_ALPHA
    D3DBLEND_INVSRCALPHA,       // AlphaState::DBM_ONE_MINUS_SRC_ALPHA
    D3DBLEND_DESTALPHA,         // AlphaState::DBM_DST_ALPHA
    D3DBLEND_INVDESTALPHA,      // AlphaState::DBM_ONE_MINUS_DST_ALPHA
    0,                          // AlphaState::DBM_CONSTANT_COLOR
    0,                          // AlphaState::DBM_ONE_MINUS_CONSTANT_COLOR
    0,                          // AlphaState::DBM_CONSTANT_ALPHA
    0                           // AlphaState::DBM_ONE_MINUS_CONSTANT_ALPHA
};

DWORD D3D9AlphaCompare[CAlphaState::NUM_CMP_MODES] = 
{
    D3DCMP_NEVER,               // AlphaState::CM_NEVER
    D3DCMP_LESS,                // AlphaState::CM_LESS
    D3DCMP_EQUAL,               // AlphaState::CM_EQUAL
    D3DCMP_LESSEQUAL,           // AlphaState::CM_LEQUAL
    D3DCMP_GREATER,             // AlphaState::CM_GREATER
    D3DCMP_NOTEQUAL,            // AlphaState::CM_NOTEQUAL
    D3DCMP_GREATEREQUAL,        // AlphaState::CM_GEQUAL
    D3DCMP_ALWAYS               // AlphaState::CM_ALWAYS
};

// Depth
DWORD D3D9DepthCompare[CDepthState::NUM_CMP_MODES] = 
{
    D3DCMP_NEVER,               // DepthState::CM_NEVER
    D3DCMP_LESS,                // DepthState::CM_LESS
    D3DCMP_EQUAL,               // DepthState::CM_EQUAL
    D3DCMP_LESSEQUAL,           // DepthState::CM_LEQUAL
    D3DCMP_GREATER,             // DepthState::CM_GREATER
    D3DCMP_NOTEQUAL,            // DepthState::CM_NOTEQUAL
    D3DCMP_GREATEREQUAL,        // DepthState::CM_GEQUAL
    D3DCMP_ALWAYS               // DepthState::CM_ALWAYS
};

// Culling
DWORD D3D9CullOrder[2] = 
{
    D3DCULL_CCW,                // CullState::mCCWOrder = true
    D3DCULL_CW                  // CullState::mCCWOrder = false
};

// Stencil
DWORD D3D9StencilCompare[CStencilState::NUM_CMP_MODES] = 
{
    D3DCMP_NEVER,               // StencilState::CM_NEVER
    D3DCMP_LESS,                // StencilState::CM_LESS
    D3DCMP_EQUAL,               // StencilState::CM_EQUAL
    D3DCMP_LESSEQUAL,           // StencilState::CM_LEQUAL
    D3DCMP_GREATER,             // StencilState::CM_GREATER
    D3DCMP_NOTEQUAL,            // StencilState::CM_NOTEQUAL
    D3DCMP_GREATEREQUAL,        // StencilState::CM_GEQUAL
    D3DCMP_ALWAYS               // StencilState::CM_ALWAYS
};

DWORD D3D9StencilOperation[CStencilState::NUM_OP_MODES] = 
{
    D3DSTENCILOP_KEEP,          // StencilState::OM_KEEP
    D3DSTENCILOP_ZERO,          // StencilState::OM_ZERO
    D3DSTENCILOP_REPLACE,       // StencilState::OM_REPLACE
    D3DSTENCILOP_INCR,          // StencilState::OM_INCREMENT
    D3DSTENCILOP_DECR,          // StencilState::OM_DECREMENT
    D3DSTENCILOP_INVERT         // StencilState::OM_INVERT
};


D3DFORMAT D3D9GetIndexFormat(IndexType itype)
{
	switch (itype)
	{
	case IT_UINT:
		return D3DFMT_INDEX32;
	case IT_USHORT:
		return D3DFMT_INDEX16;
	}

	// keep the compiler happy
	return D3DFMT_INDEX32;
}


D3DDECLTYPE D3D9VertexType(VertexType vtype)
{
	switch (vtype)
	{
	case VT_UBYTE4:
		return D3DDECLTYPE_UBYTE4;
	case VT_SHORT4:
		return D3DDECLTYPE_SHORT4;
	case VT_SHORT2:
		return D3DDECLTYPE_SHORT2;
	case VT_FLOAT1:
		return D3DDECLTYPE_FLOAT1;
	case VT_FLOAT2:
		return D3DDECLTYPE_FLOAT2;
	case VT_FLOAT3:
		return D3DDECLTYPE_FLOAT3;
	case VT_FLOAT4:
		return D3DDECLTYPE_FLOAT4;
	};

	assert( false && ieS("Non recognised vertex type \n") );

	return D3DDECLTYPE_UNUSED;
}

} // namespace Engine