#include "frameafx.h"

// opengl
#include "RendererGL.h"
#include "RenderDataGL.h"
#include "VertexBufferGL.h"
#include "IndexBufferGL.h"
#include "VertexFormatGL.h"
#include "MappingGL.h"

// scenegraph
#include "Camera.h"
#include "Triangle.h"
#include "Polyline.h"
#include "Polypoint.h"

// Core
#include "Helper.h"

// Glew
#include "GL\wglew.h"


using namespace Engine;
using std::vector;

DEFINE_HIERARCHICALHEAP(CRendererGL, "CRendererGL", "IRenderer");

// -------------------------------------------------------------------------------------------
// Summary: Default constructor
// -------------------------------------------------------------------------------------------
CRendererGL::CRendererGL() 
{
	m_pDefaultAlphaState = 0;
	m_pDefaultCullState = 0;
	m_pDefaultDepthState = 0;
	m_pDefaultOffsetState = 0;
	m_pDefaultStencilState = 0;
	m_pDefaultwireState = 0;

	m_multisampleSupport = false;
	m_antiAlias = 4;
	m_bits = 32;
	m_zBufferBits = 16;
	m_vsync = true;
	m_stencilBuffer = true;
	m_stereoBuffer = false;
	m_doubleBuffer = true;
	m_clearDepth = 1.0f;
	m_clearStencil = 0;

	m_clearColor = Colour4f::BLACK;
}

CRendererGL::~CRendererGL()
{
	// LIFO
	m_indexBuffers.clear();
	m_vertexBuffers.clear();
	m_vertexFormats.clear();

	SafeDelete(m_pDefaultwireState);
	SafeDelete(m_pDefaultStencilState);
	SafeDelete(m_pDefaultOffsetState);
	SafeDelete(m_pDefaultDepthState);
	SafeDelete(m_pDefaultCullState);
	SafeDelete(m_pDefaultAlphaState);

	if (m_pData)
	{
		if (m_pData->m_HRc || m_pData->m_HDc)
		{
			if (!::wglMakeCurrent(0, 0))
				assert( false && ieS("Release of dc and rc failed.\n") );

			if (!::wglDeleteContext(m_pData->m_HRc))
				assert( false && ieS("Release of rendering context failed.\n") );
		}

		if (m_pData->m_HDc)
			ReleaseDC(m_pData->m_hWnd, m_pData->m_HDc);
	}
}

bool CRendererGL::GetPickRay (Int32 x, Int32 y, Vector3f& origin, Vector3f& direction) const
{
	if (!m_pCamera)
	{
		return false;
	}

	// Get the current viewport and test whether (x,y) is in it.
	Viewporti view = GetViewport();
	if (x < view[0] || x > view[0] + view[2] || y < view[1] || y > view[1] + view[3])
	{
		return false;
	}

	// Get the [0,1]^2-normalized coordinates of (x,y).
	float r = ((float)(x - view[0]))/(float)view[2];
	float u = ((float)(y - view[1]))/(float)view[3];

	// Pointer to the frustum
	const float* pFrustum = m_pCamera->GetFrustum();

	float rBlend = (1.0f - r)*pFrustum[CCamera::S_LEFT] + r*pFrustum[CCamera::S_RIGHT];
	float uBlend = (1.0f - u)*pFrustum[CCamera::S_BOTTOM] + u*pFrustum[CCamera::S_TOP];
	float dBlend = pFrustum[CCamera::S_NEAR];

	origin = m_pCamera->GetPosition();

	direction = dBlend*m_pCamera->GetFVector() + rBlend * m_pCamera->GetRVector()
		+ uBlend*m_pCamera->GetUVector();

	direction.Normalise();
	return true;
}

//----------------------------------------------------------------------------
// Rendering TODO : iterate over the shaders, set approriate shaders then draw that primitive
//----------------------------------------------------------------------------
bool CRendererGL::PreDraw()
{
	return true;
}

void CRendererGL::PostDraw()
{

}

void CRendererGL::Draw(const SceneNodeVector& visibleSet)
{
	// Nothing to draw, just leave
	///if ( !numVisible )
	//	return;

	SceneNodeVector::const_iterator itor = visibleSet.begin();
	for (/**/; itor != visibleSet.end(); ++itor)
	{
		Draw( *itor );
	}
}

void CRendererGL::Draw(const CSceneNode* pNode)
{
	CIndexBufferPtr pIBuffer = pNode->GetIndexBuffer();
	CVertexBufferPtr pVBuffer = pNode->GetVertexBuffer();
	CVertexFormatPtr pVFormat = pNode->GetVertexFormat();

	// Enable geometry
	Enable( pVBuffer );
	Enable( pVFormat );
	if (pIBuffer)
	{
		Enable( pIBuffer );
	}

	// Set up projection matrix
	::glMatrixMode(GL_PROJECTION);
	::glLoadIdentity();

	Matrix4f proj = m_pCamera->GetProjectionMatrix();
	::glLoadMatrixf( static_cast<const GLfloat*>(&proj[0]) );

	// Set up view matrix
	::glMatrixMode(GL_MODELVIEW);	
	::glLoadIdentity();

    // Set up world world view matrix
	Matrix4f view = m_pCamera->GetViewMatrix(); 	
	Matrix4f world = pNode->GetWorldTransform();
	Matrix4f myworldview = world * view;
	::glLoadMatrixf( static_cast<const GLfloat*>(&myworldview[0]) );
		
	// Draw the node
	DrawPrimitive(pNode);

	::glMatrixMode(GL_MODELVIEW);
	::glPopMatrix();
	::glMatrixMode(GL_PROJECTION);
	::glPopMatrix();

	// Disable geometry
	if (pIBuffer)
	{
		Disable( pIBuffer );
	}
	Disable( pVFormat );
	Disable( pVBuffer );

}
//----------------------------------------------------------------------------
void CRendererGL::DrawPrimitive(const CSceneNode* pNode)
{
	CSceneNode::PrimitiveType type = pNode->GetType();
	CVertexBufferPtr pVBuffer = pNode->GetVertexBuffer();
	CVertexFormatPtr pVFormat = pNode->GetVertexFormat();
	CIndexBufferPtr pIBuffer = pNode->GetIndexBuffer();

	switch (type)
	{
	case CSceneNode::PT_TRISTRIP:
	case CSceneNode::PT_TRIMESH:
	case CSceneNode::PT_TRIFAN:
		{
			GLsizei numVerts = (GLsizei)pVBuffer->GetNumElements();
			GLsizei numIndices = (GLsizei)pIBuffer->GetNumElements();

			if (numVerts && numIndices)
			{
				GLenum indexType = 0;
				const GLvoid* pIndexData;
				switch ( pIBuffer->GetType() )
				{
				case IT_USHORT:
					indexType = GL_UNSIGNED_SHORT;
					pIndexData = (UShort*)0 + pIBuffer->GetBytesStride();
					break;
				case IT_UINT:
					indexType = GL_UNSIGNED_INT;
					pIndexData = (UInt32*)0 + pIBuffer->GetBytesStride();
					break;
				}

				 ::glDrawRangeElements( GLPrimitiveType[type], 0, numVerts-1, numIndices, indexType, 0 );
			}
		}
		break;
	case CSceneNode::PT_POLYLINE_OPEN:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINE_STRIP, 0, numVerts);
			}
		}
		break;
	case CSceneNode::PT_POLYLINE_CLOSED:
		{
			const CPolyline* pPolyline = static_cast<const CPolyline*>(pNode);
			GLsizei numVerts = (GLsizei)pPolyline->GetNumLines();

			if (numVerts)
			{
				::glDrawArrays(GL_LINES, 0, numVerts);
			}
		}
		break;
	case CSceneNode::PT_POLYPOINTS:
		{
			const CPolypoint* pPolypoint = static_cast<const CPolypoint*>(pNode);
			GLsizei numVerts = (GLsizei)pPolypoint->GetNumPoints();

			if (numVerts)
			{
				::glDrawArrays(GL_POINTS, 0, numVerts);
			}
		}
		break;
	default:
		assert( false && ieS("Invalid type\n") );
		break;
		
	} // switch

}

//----------------------------------------------------------------------------
// Camera Settings
//----------------------------------------------------------------------------
void CRendererGL::SetCamera(CCameraPtr pCamera)
{
	m_pCamera = pCamera;
}

CCameraPtr CRendererGL::GetCamera()
{
	return m_pCamera;
}

//----------------------------------------------------------------------------
// Frame Control
//----------------------------------------------------------------------------
void CRendererGL::SetViewport(const Viewporti& view)
{
	::glViewport( view.left(), view.top(), view.right(), view.bottom() );
}

void CRendererGL::Resize(Int32 width, Int32 height)
{
	int param[4];
	::glGetIntegerv(GL_VIEWPORT, param);
	::glViewport(param[0], param[1], width, height);
}

Viewporti CRendererGL::GetViewport() const
{
	Viewporti view;
	::glGetIntegerv(GL_VIEWPORT, &view[0]);
	return view;
}

bool CRendererGL::EnterFullscreen(const Point2l& fullSize)
{
	if (!m_windowed)
		return false;

	DEVMODE dm;
	std::memset( &dm, 0, sizeof(DEVMODE) );
	dm.dmSize = sizeof(dm);
	// use default values from current setting
	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);
	dm.dmPelsWidth = fullSize.x();
	dm.dmPelsHeight = fullSize.y();
	dm.dmBitsPerPel = m_bits;
	dm.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFREQUENCY;

	LONG res = ChangeDisplaySettings(&dm, CDS_FULLSCREEN);
	if (res != DISP_CHANGE_SUCCESSFUL)
	{ // try again without forcing display frequency
		dm.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		res = ChangeDisplaySettings(&dm, CDS_FULLSCREEN);
	}

	bool ret = false;
	switch(res)
	{
	case DISP_CHANGE_SUCCESSFUL:
		m_windowed = false;
		ret = true;
		break;
	case DISP_CHANGE_RESTART:
		assert( false && ieS("Switch to fullscreen: The computer must be restarted in order for the graphics mode to work.") );
		break;
	case DISP_CHANGE_BADFLAGS:
		assert( false && ieS("Switch to fullscreen: An invalid set of flags was passed in.") );
		break;
	case DISP_CHANGE_BADPARAM:
		assert( false && ieS("Switch to fullscreen: An invalid parameter was passed in. This can include an invalid flag or combination of flags.") );
		break;
	case DISP_CHANGE_FAILED:
		assert( false && ieS("Switch to fullscreen: The display driver failed the specified graphics mode.") );
		break;
	case DISP_CHANGE_BADMODE:
		assert( false && ieS("Switch to fullscreen: The graphics mode is not supported.") );
		break;
	default:
		assert( false && ieS("An unknown error occured while changing to fullscreen.") );
		break;
	}

	return ret;
}

bool CRendererGL::ExitFullscreen()
{
	m_windowed = ChangeDisplaySettings(NULL, CDS_RESET) == DISP_CHANGE_SUCCESSFUL;
	return m_windowed;
}

// ----------------------------------------------------------------------------
// Support for clearing the color, depth, and stencil buffers.
// ----------------------------------------------------------------------------
void CRendererGL::ClearColorBuffer()
{
    ::glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2],
        m_clearColor[3]);

    ::glClear(GL_COLOR_BUFFER_BIT);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearDepthBuffer()
{
    ::glClearDepth( (GLclampd)m_clearDepth );

    ::glClear(GL_DEPTH_BUFFER_BIT);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearStencilBuffer()
{
    ::glClearStencil( (GLint)m_clearStencil );

    ::glClear(GL_STENCIL_BUFFER_BIT);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearBuffers ()
{
    ::glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2],
        m_clearColor[3]);

    ::glClearDepth( (GLclampd)m_clearDepth );

    ::glClearStencil( (GLint)m_clearStencil );

    ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearColorBuffer (Int32 x, Int32 y, Int32 w, Int32 h)
{
    ::glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2],
        m_clearColor[3]);

    ::glEnable(GL_SCISSOR_TEST);
    ::glScissor(x, y, w, h);
    ::glClear(GL_COLOR_BUFFER_BIT);
    ::glDisable(GL_SCISSOR_TEST);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearDepthBuffer (Int32 x, Int32 y, Int32 w, Int32 h)
{
    ::glClearDepth( (GLclampd)m_clearDepth );

    ::glEnable(GL_SCISSOR_TEST);
    ::glScissor(x, y, w, h);
    ::glClear(GL_DEPTH_BUFFER_BIT);
    ::glDisable(GL_SCISSOR_TEST);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearStencilBuffer (Int32 x, Int32 y, Int32 w, Int32 h)
{
    ::glClearStencil((GLint)m_clearStencil);

    ::glEnable(GL_SCISSOR_TEST);
    ::glScissor(x, y, w, h);
    ::glClear(GL_STENCIL_BUFFER_BIT);
    ::glDisable(GL_SCISSOR_TEST);
}
// ----------------------------------------------------------------------------
void CRendererGL::ClearBuffers (Int32 x, Int32 y, Int32 w, Int32 h)
{
    ::glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2],
        m_clearColor[3]);

    ::glClearDepth((GLclampd)m_clearDepth);

    ::glClearStencil((GLint)m_clearStencil);

    ::glEnable(GL_SCISSOR_TEST);
    ::glScissor(x, y, w, h);
    ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    ::glDisable(GL_SCISSOR_TEST);
}
//----------------------------------------------------------------------------
void CRendererGL::DisplayColorBuffer()
{
    SwapBuffers(m_pData->m_HDc);
}

// -------------------------------------------------------------------------------------------
// VertexBuffer
// -------------------------------------------------------------------------------------------
void CRendererGL::Enable(CVertexBufferPtr pVBuffer)
{
	// search to find if vertex buffer is active
	VertexBufferMap::iterator itor = m_vertexBuffers.find( pVBuffer );
	CVertexBufferGLPtr pVBufferGL = CVertexBufferGLPtr();
	if ( itor != m_vertexBuffers.end() )
	{
		pVBufferGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVBufferGL = CVertexBufferGLPtr( new CVertexBufferGL( pVBuffer ) );
		m_vertexBuffers[pVBuffer] = pVBufferGL;
	}

	pVBufferGL->Enable();
}

void CRendererGL::Disable(CVertexBufferPtr pVBuffer)
{
	// search to find if vertex buffer is active
	VertexBufferMap::iterator itor = m_vertexBuffers.find( pVBuffer );
	if ( itor != m_vertexBuffers.end() )
	{
		CVertexBufferGLPtr pVBufferGL = (*itor).second;
		pVBufferGL->Disable();
	}
}

// -------------------------------------------------------------------------------------------
// VertexFormat
// -------------------------------------------------------------------------------------------
void CRendererGL::Enable(CVertexFormatPtr pVFormat)
{
	// search to find if vertex buffer is active
	VertexFormatMap::iterator itor = m_vertexFormats.find( pVFormat );
	CVertexFormatGLPtr pVFormatGL = CVertexFormatGLPtr();
	if ( itor != m_vertexFormats.end() )
	{
		pVFormatGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVFormatGL = CVertexFormatGLPtr( new CVertexFormatGL( pVFormat ) );
		m_vertexFormats[pVFormat] = pVFormatGL;
	}

	pVFormatGL->Enable();
}

void CRendererGL::Disable(CVertexFormatPtr pVFormat)
{
	VertexFormatMap::iterator itor = m_vertexFormats.find( pVFormat );
	if ( itor != m_vertexFormats.end() )
	{
		CVertexFormatGLPtr pVFormatGL = (*itor).second;
		pVFormatGL->Disable();
	}
}

// -------------------------------------------------------------------------------------------
// IndexBuffer
// -------------------------------------------------------------------------------------------
void CRendererGL::Enable(CIndexBufferPtr pIBuffer)
{
	CIndexBufferMap::iterator itor = m_indexBuffers.find(pIBuffer);
	CIndexBufferGLPtr pIBufferGL = CIndexBufferGLPtr();
	if ( itor != m_indexBuffers.end() )
	{
		pIBufferGL = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pIBufferGL = CIndexBufferGLPtr( new CIndexBufferGL( pIBuffer ) );
		m_indexBuffers[pIBuffer] = pIBufferGL;
	}

	pIBufferGL->Enable();
}

void CRendererGL::Disable(CIndexBufferPtr pIBuffer)
{
	CIndexBufferMap::iterator itor = m_indexBuffers.find(pIBuffer);
	if ( itor != m_indexBuffers.end() )
	{
		CIndexBufferGLPtr pIBufferGL = (*itor).second;
		pIBufferGL->Disable();
	}
}



// -------------------------------------------------------------------------------------------
// Summary: Cleans up OpenGL resources
// -------------------------------------------------------------------------------------------
void CRendererGL::Release()
{
	// LIFO
	m_indexBuffers.clear();
	m_vertexBuffers.clear();
	m_vertexFormats.clear();

	SafeDelete(m_pDefaultwireState);
	SafeDelete(m_pDefaultStencilState);
	SafeDelete(m_pDefaultOffsetState);
	SafeDelete(m_pDefaultDepthState);
	SafeDelete(m_pDefaultCullState);
	SafeDelete(m_pDefaultAlphaState);

	if (m_pData)
	{
		if (m_pData->m_HRc || m_pData->m_HDc)
		{
			if (!::wglMakeCurrent(0, 0))
				assert( false && ieS("Release of dc and rc failed.\n") );

			if (!::wglDeleteContext(m_pData->m_HRc))
				assert( false && ieS("Release of rendering context failed.\n") );
		}

		if (m_pData->m_HDc)
			ReleaseDC(m_pData->m_hWnd, m_pData->m_HDc);
	}
}

// -------------------------------------------------------------------------------------------
// Summary: Initializes Direct3D.
// Parameters:
// [in] hWnd - Handle to the window
// [in] windowed - TRUE for windowed mode, FALSE for fullscreen mode
// Returns: TRUE on success. FALSE on failure
// -------------------------------------------------------------------------------------------
bool CRendererGL::Initialise(HWND hwnd, HINSTANCE hinst, bool windowed)
{
	// Create the render data container
	m_pData = CRenderDataGLPtr( new CRenderDataGL() );
	// Set render data
	m_pData->m_hWnd = hwnd;
	m_pData->m_hInst = hinst;

	// set window data
	m_windowed = windowed;

	HWND temp_hwnd = CreateAAWindow();
	if (!temp_hwnd)
		return false;

	PIXELFORMATDESCRIPTOR pfd;
	SecureZeroMemory( &pfd, sizeof(PIXELFORMATDESCRIPTOR) );
	BuildPixelFormatDescriptor(pfd);	

	// When running under Windows Vista or later support desktop composition.
	OSVERSIONINFO osvi;
	SecureZeroMemory( &osvi, sizeof(OSVERSIONINFO) );
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if ( !GetVersionEx(&osvi) )
		assert(!"GetVersionEx() failed.");

	if ( osvi.dwMajorVersion > 6 || (osvi.dwMajorVersion == 6 && osvi.dwMinorVersion >= 0) )
		pfd.dwFlags |=  PFD_SUPPORT_COMPOSITION;

	HDC	HDc = 0;
	GLint pixelformat = 0; 

	// register pixelformat, w::glCreateContext, w::glMakeCurrent
	BuildGLDevice(temp_hwnd, pfd, pixelformat, HDc);
	
	// create basic rendering context (2.1)
	HGLRC hrc = ::wglCreateContext(HDc);

	// activate rendering context
	if (!::wglMakeCurrent(HDc, hrc))
	{
		Release();
		assert(!"Cannot activate GL rendering context");
		return 0;
	}

	// Intailise Glew
	GLenum err = ::glewInit();
	if (GLEW_OK != err)
	{
		assert(!"Glew not supported");
	}
	
	// Attempt to test for multisample
	BuildAASupport(pixelformat);

	//---------------------------------------------------------------------
	// Destory temp window and release its device.
	::wglMakeCurrent(HDc, 0);
	::wglDeleteContext(hrc);
	ReleaseDC(temp_hwnd, HDc);
	DestroyWindow(temp_hwnd);
	//---------------------------------------------------------------------

	// Build device on actual window
	BuildGLDevice(hwnd, pfd, pixelformat, HDc);

	// create rendering context
	hrc = BuildContextLevel(HDc);

	// activate rendering context
	if (!::wglMakeCurrent(HDc, hrc))
	{
		Release();
		assert(!"Cannot activate GL rendering context");
		return 0;
	}

	// set vsync
	if (::wglSwapIntervalEXT)
		::wglSwapIntervalEXT(m_vsync? 1 : 0);


	//Checking GL version
	/* const UInt8 *GLVersionString = ::glGetString(GL_VERSION);

	//Or better yet, use the GL3 way to get the version number
	int OpenGLVersion[2];
	::glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	::glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]); */

	if (!hrc) 
		return false;

	// Set contexts for opengl
	m_pData->m_HRc = hrc;
	m_pData->m_HDc = HDc;

    IntialiseSubSystems();

	return true;
}

void CRendererGL::BuildPixelFormatDescriptor(PIXELFORMATDESCRIPTOR &pfd)
{
	// Set up pixel format descriptor with desired parameters
	PIXELFORMATDESCRIPTOR PFD =
	{
		sizeof(PIXELFORMATDESCRIPTOR),					// Size Of This Pixel Format Descriptor
		1,												// Version Number
		PFD_DRAW_TO_WINDOW |							// Format Must Support Window
		PFD_SUPPORT_OPENGL |							// Format Must Support OpenGL
		(m_doubleBuffer ? PFD_DOUBLEBUFFER : 0) |		// Must Support Double Buffering
		(m_stereoBuffer ? PFD_STEREO : 0),				// Must Support Stereo Buffer
		PFD_TYPE_RGBA,									// Request An RGBA Format
		m_bits,											// Select Our Color Depth
		0, 0, 0, 0, 0, 0,								// color bits ignored  
		0,												// no alpha buffer  
		0,												// shift bit ignored  
		0,												// no accumulation buffer  
		0, 0, 0, 0,										// accum bits ignored  
		m_zBufferBits,                       			// Z-Buffer (Depth Buffer)
		m_stencilBuffer ? 1 : 0,						// Stencil Buffer Depth
		0,                                         		// No Auxiliary Buffer
		PFD_MAIN_PLANE,                            		// Main Drawing Layer
		0,                                         		// Reserved
		0, 0, 0                                    		// Layer Masks Ignored
	};

	std::memcpy( &pfd, &PFD, sizeof(PIXELFORMATDESCRIPTOR) );
}

void CRendererGL::BuildGLDevice(HWND hwnd, PIXELFORMATDESCRIPTOR &pfd, GLint &pixelformat, HDC &dc)
{
	// get hdc
	dc = GetDC(hwnd);
	if (!dc)
	{
		assert( false && ieS("Cannot create a GL device context.\n") );
		return;
	}

	if (!m_multisampleSupport)
	{
		// search for pixel format the simple way
		for (UInt32 i=0; i<5; ++i)
		{
			if (i == 1)
			{
				if (m_stencilBuffer)
				{
					assert( false && ieS("Cannot create a GL device with stencil buffer, disabling stencil shadows.\n") );
					m_stencilBuffer = false;
					pfd.cStencilBits = 0;
				}
				else
					continue;
			}
			else if (i == 2)
			{
				pfd.cDepthBits = 24;
			}
			if (i == 3)
			{
				if (m_bits!=16)
					pfd.cDepthBits = 16;
				else
					continue;
			}
			else if (i == 4)
			{
				assert( false && ieS("Cannot create a GL device context\n") && ieS("No suitable format.\n") );
				return;
			}

			// choose pixelformat
			pixelformat = ChoosePixelFormat(dc, &pfd);
			if (pixelformat)
				break;
		}
	}

	// set pixel format
	if (!SetPixelFormat(dc, pixelformat, &pfd))
	{
		Release();				
		assert( false && ieS("Cannot set the pixel format.\n") );
		return;
	}
}


// -------------------------------------------------------------------------------------------
// Summary: Builds the D3DPRESENT_PARAMETERS structure.
// Returns: TRUE on success. FALSE on failure
// -------------------------------------------------------------------------------------------
bool CRendererGL::BuildAASupport(GLint &pixelformat)
{
	if (::wglChoosePixelFormatARB)
	{
		// Get Our Current Device Context
		HDC hDC = GetDC(m_pData->m_hWnd);

		// This value determines the number of samples used for antialiasing
		// My experience is that 8 does not show a big
		// improvement over 4, but 4 shows a big improvement
		// over 2.
		if(m_antiAlias > 32)
			m_antiAlias = 32;

		float fAttributes[] = { 0.0f, 0.0f };
		GLint iAttributes[] =
		{
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
			WGL_COLOR_BITS_ARB, (m_bits == 32) ? 24 : 15,
			WGL_ALPHA_BITS_ARB, (m_bits == 32) ? 8 : 1,
			WGL_DEPTH_BITS_ARB, m_zBufferBits, // 10,11
			WGL_STENCIL_BITS_ARB, (m_stencilBuffer) ? 1 : 0,
			WGL_DOUBLE_BUFFER_ARB, (m_doubleBuffer) ? GL_TRUE : GL_FALSE,
			WGL_STEREO_ARB, (m_stereoBuffer) ? GL_TRUE : GL_FALSE,
			WGL_SAMPLE_BUFFERS_ARB, 1,
			WGL_SAMPLES_ARB, m_antiAlias, // 20,21
			WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
			0,0
		};

		GLint pfmt = 0;
		GLuint numFormats = 0;
		GLint valid = ::wglChoosePixelFormatARB(hDC, iAttributes, fAttributes, 1, &pfmt, &numFormats);

		// if We Returned True, And Our Format Count Is Greater Than 1
		if (valid && numFormats >= 1)
		{
			m_multisampleSupport = true;
			pixelformat = pfmt;
		}
	}
	else
	{
		m_antiAlias = 0;
		m_multisampleSupport = false;
	}

	return m_multisampleSupport;
}

HGLRC CRendererGL::BuildContextLevel(HDC &dc)
{
	HGLRC rc = 0;

	// create rendering context
	if (::wglCreateContextAttribsARB) 
	{
		GLint iAttribs[] =
		{
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 1,
			// WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, // comment out this line to enable old functionality 
			0 
		};
		rc = ::wglCreateContextAttribsARB(dc, 0, iAttribs);
	}
	else
	{
		// If It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		rc = ::wglCreateContext(dc);
	}

	return rc;
}

HWND CRendererGL::CreateAAWindow()
{
	// Create a window to test antialiasing support
	const pChar ClassName = TEXT("OpenGLDevice");

	// get handle to exe file
	if (!m_pData->m_hInst)
		m_pData->m_hInst = GetModuleHandle(0);

	// Register Class
	WNDCLASSEX wcex;
	wcex.cbSize        = sizeof(WNDCLASSEX);
	wcex.style         = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc   = (WNDPROC)DefWindowProc;
	wcex.cbClsExtra    = 0;
	wcex.cbWndExtra    = 0;
	wcex.hInstance     = m_pData->m_hInst;
	wcex.hIcon         = 0;
	wcex.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName  = 0;
	wcex.lpszClassName = ClassName;
	wcex.hIconSm       = 0;
	wcex.hIcon         = 0;

	RegisterClassEx(&wcex);

	DWORD style = WS_POPUP;

	if (m_windowed)
		style = WS_OVERLAPPEDWINDOW;

	RECT rcClient = { 0, 0, 0, 0 };
	GetClientRect(m_pData->m_hWnd, &rcClient);
	AdjustWindowRect(&rcClient, style, FALSE);

	const LONG realWidth = rcClient.right - rcClient.left;
	const LONG realHeight = rcClient.bottom - rcClient.top;

	LONG windowLeft = (GetSystemMetrics(SM_CXSCREEN) - realWidth) / 2;
	LONG windowTop = (GetSystemMetrics(SM_CYSCREEN) - realHeight) / 2;

	HWND temp_hwnd = CreateWindow(ClassName, ClassName, style, windowLeft, windowTop,
		realWidth, realHeight, 0, 0, m_pData->m_hInst, this);

	if (!temp_hwnd)
	{
		assert( false && ieS("Cannot create a temporary window.\n") );
		return 0;
	}

	ShowWindow(temp_hwnd, SW_HIDE);

	return temp_hwnd;
}


void CRendererGL::IntialiseSubSystems()
{
	m_pDefaultAlphaState = new CAlphaState();
	m_pDefaultCullState = new CCullState();
	m_pDefaultDepthState = new CDepthState();
	m_pDefaultOffsetState = new COffsetState();
	m_pDefaultStencilState = new CStencilState();
	m_pDefaultwireState	= new CWireState();

	m_pData->m_currentState.Initialise(m_pDefaultAlphaState, m_pDefaultCullState, m_pDefaultDepthState, m_pDefaultOffsetState, m_pDefaultStencilState, m_pDefaultwireState);
}


