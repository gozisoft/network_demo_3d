#ifndef CCAMERA_NODE_FPS_H
#define CCAMERA_NODE_FPS_H

#include "RootNode.h"

namespace Engine
{

class CCameraNodeFPS : public CRootNode
{
public:
	CCameraNodeFPS(CCameraPtr pCamera);
	~CCameraNodeFPS();

	void SetCamera(CCameraPtr pCamera); 
	CCameraPtr GetCamera();

	// Geometric update
	void UpdateWorldData(double time);

	// Process an event, usually a key press or
	// camera movement.
	bool OnEvent(const IEventData& event);

	// Sets the look at target of the camera
	void SetTarget(const Vector3f& target);

	Vector3f GetUpVector() const;
	Vector3f GetTargetVector() const;

protected:
	CCameraPtr m_pCamera;

	Vector3f m_up;
	Vector3f m_target;

private:
	DECLARE_HEAP;
};


#include "CameraNodeFPS.inl"

}

#endif