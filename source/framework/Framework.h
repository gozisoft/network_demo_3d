#ifndef CFRAMEWORK_H
#define CFRAMEWORK_H

#include "framefwd.h"
#include "Vector.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <functional>

namespace Engine
{

	class CFramework
	{
	public:
		typedef void (CALLBACK *LPRENDERFRAME) (IRendererPtr* pRenderer, double time, double elapsedTime);
		typedef std::tr1::function< void (LPRENDERFRAME) > RenderFrame;

		CFramework(IBaseAppPtr pGameApp, const String& title, bool windowed=true, HINSTANCE hInstance=0,
			const Point2l& windowedSize=Point2l(640,480), const Point2l& fullSize=Point2l::ZERO);

		~CFramework();

		bool Initialise();
		void Release();
		int Run(HACCEL hAccel);

		// Accessors
		HWND GetHWND() const;
		void SetHWND(HWND hwnd);
		HINSTANCE GetHINSTANCE() const;

		// Window messege handler
		static LRESULT CALLBACK StaticWndProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

	private:
		LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
		void OnCreateDevice();
		void OnResetDevice();
		void OnLostDevice();
		void OnDestroyDevice();
		void OnUpdateFrame();
		void OnRenderFrame();
		void OnWindowEvent(const CWinEvent& event);

		void Pause(bool rendering, bool timer);
		void CheckForWindowSizeChange();
		void CheckForWindowChangingMonitors();
		void ToggleFullScreen();

		RenderFrame RenderFrameFunc;

		HWND m_hwnd;
		HINSTANCE m_hInstance;
		WINDOWPLACEMENT m_wp;

		// Window Details
		Point2l m_clientSize;
		Point2l m_winSize;
		Point2l m_fullSize;
		String m_title;
		bool m_active;
		bool m_windowed;
		bool m_minimised;
		bool m_maximised;
		bool m_borderDrag;
		bool m_closing;

		// Timing
		bool m_renderingPaused;
		bool m_timerPaused;
		Int m_renderingPauseCount;
		Int m_timerPauseCount;

		// Window owns these
		CTimerPtr m_pTimer;
		CMouseControllerPtr m_pMouse;
		IRendererPtr m_pRenderer;
		
		// Window does not own these
		IBaseAppPtr m_pGameApp;

	private:
		DECLARE_HEAP;

	};


#include "Framework.inl"



}

#endif