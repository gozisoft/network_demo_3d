#include "Frameafx.h"
#include "TriangleFan.h"
#include "VertexBuffer.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CTriFan, "CTriFan", "CTriangles");



CTriFan::CTriFan(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, IndexType indextType) : 
CTriangles( PT_TRIFAN, pVFormat, pVBuffer, CIndexBufferPtr() )
{
	UInt PosCount = pVBuffer->GetNumElements();
	m_pIBuffer = CIndexBufferPtr( new CIndexBuffer(PosCount, indextType) );
	m_pIBuffer->SetIndexBuffer(PosCount);
}


CTriFan::CTriFan(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer) : 
CTriangles(PT_TRIFAN, pVFormat, pVBuffer, pIBuffer)
{

}

CTriFan::~CTriFan()
{

}

bool CTriFan::GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const
{
	if (pos >= 0 && pos < GetTriangleCount())
	{
		switch (m_pIBuffer->GetType())
		{
		case IT_USHORT:
			{
				const UShort *pIBuffer = reinterpret_cast<const UShort*>( m_pIBuffer->GetData() );
				i0 = pIBuffer[0];
				i1 = pIBuffer[pos + 1];
				i2 = pIBuffer[pos + 2];
				return true;
			}
			break;

		case IT_UINT:
			{
				const UInt32 *pIBuffer = reinterpret_cast<const UInt32*>( m_pIBuffer->GetData() );
				i0 = pIBuffer[0];
				i1 = pIBuffer[pos + 1];
				i2 = pIBuffer[pos + 2];
				return true;
			}
			break;
		};
	}

	return false;
}
