#ifndef VERTEX_BUFFER_GL_H
#define VERTEX_BUFFER_GL_H

#include "framefwd.h"
#include "VertexBuffer.h"

#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif
#include "GL/glew.h"

namespace Engine
{

class CVertexBufferGL
{
public:
	CVertexBufferGL(CVertexBufferPtr pVBuffer);
	~CVertexBufferGL();

	// Vertex buffer operations.
	void Enable();
	void Disable();
	void* Lock(CVertexBuffer::Locking mode);
	void Unlock();

private:
	GLuint m_buffer;
	CVertexBufferPtr m_pVBuffer; // keep a pointer to the data on CPU

private:
	DECLARE_HEAP;

};

}



#endif