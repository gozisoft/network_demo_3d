#ifndef __CCULLER_H__
#define __CCULLER_H__

#include "FrameFwd.h"
#include "Camera.h"
#include "Plane.h"


namespace Engine
{

	class CCuller
	{
	public:
		typedef std::vector<CSceneNode*> SceneNodeVector;

		CCuller(CCameraPtr pCamera);
		~CCuller();

		// Camera access, set frustrum etc
		void SetCamera(CCameraPtr pCamera); 
		CCameraPtr GetCamera() const;

		void SetFrustum(const float* frustum);
		void SetFrustum(const Matrix4f& viewMatrix, const Matrix4f& projMatrix);
		const float* GetFrustum() const;

		bool IsVisible(const Spheref& volume);
		void ComputeVisibleSet(CRootNodePtr pRootNode);

		// Access to the visible array
		const SceneNodeVector& GetVisibleArray() const;
		size_t GetVisibleCount() const;

		// Access to the frustrum planes o
		UInt8 GetPlaneStates() const;
		void SetPlaneStaets(UInt8 states);

		void OnGetVisible(CRootNodePtr pRootNode, bool notCull);
		void OnGetVisible(CSceneNodePtr pSceneNode, bool notCull);

	private:
		// Vector of all the objects calcluated to be within
		// the frustum. These will be rendered.
		SceneNodeVector m_visibleSet;

		// Pointer to the camera.
		CCameraPtr m_pCamera;

		// Updated frustum per frame.
		float m_frustum[CCamera::NUM_PLANES];

		// Planes calculated from the eular vectors of camera and frustum.
		Planef m_plane[CCamera::NUM_PLANES];

		// Bitset - active planes.
		UInt8 m_planeStates;

	private:
		DECLARE_HEAP;
	};


#include "Culler.inl"


}




#endif