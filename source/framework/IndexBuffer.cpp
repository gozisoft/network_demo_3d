#include "Frameafx.h"
#include "IndexBuffer.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CIndexBuffer, "CIndexBuffer", "CBuffer");


CIndexBuffer::CIndexBuffer() :
CBuffer(),
m_type(IT_UINT)
{}

CIndexBuffer::CIndexBuffer(UInt reserve, IndexType indextype, BufferUse usage) : 
CBuffer(reserve, SizeOfType(indextype), usage, BT_INDEX),
m_type(indextype)
{

}

CIndexBuffer::CIndexBuffer(const CIndexBuffer &ibuffer) :
CBuffer( ibuffer ),
m_type(ibuffer.GetType())
{

}


void CIndexBuffer::SetIndexBuffer(UInt32 numValues)
{
	switch (m_type)
	{
	case IT_USHORT:
		{
			Short* pDest = reinterpret_cast<Short*>( GetData() );
			for (Short i = 0; i < static_cast<Short>(numValues); ++i)
			{
				pDest[i] = i;
			}
		}
		break;
	case IT_UINT:
		{
			UInt* pDest = reinterpret_cast<UInt*>( GetData() );
			for (UInt i = 0; i < numValues; ++i)
			{
				pDest[i] = i;
			}
		}
		break;
	};		
}


