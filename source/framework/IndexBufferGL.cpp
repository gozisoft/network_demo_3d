#include "Frameafx.h"
#include "IndexBufferGL.h"

#include "MappingGL.h"

using namespace Engine;

DEFINE_HEAP(CIndexBufferGL, "CIndexBufferGL");

CIndexBufferGL::CIndexBufferGL(CIndexBufferPtr pIBuffer) :
m_pIBuffer(pIBuffer),
m_buffer(0)
{
	glGenBuffers(1, &m_buffer);

	if (!m_buffer)
		assert( false && ieS("Cannot create GL vertex buffer\n") );

	// generate a new VBO and get the associated ID
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer);

	// bind VBO in order to use
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_pIBuffer->GetBytesSize(), (GLvoid*)m_pIBuffer->GetData(),
		GLBufferUsage[ m_pIBuffer->GetUsage() ] );

	// check data size in VBO is same as input array, if not return 0 and delete VBO
	GLint bufferSize = 0;
	glGetBufferParameterivARB(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &bufferSize);
	if( m_pIBuffer->GetBytesSize() != static_cast<UInt>(bufferSize) )
	{
		glDeleteBuffersARB(1, &m_buffer);
		m_buffer = 0;
		assert( false && ieS("[createVBO()] Data size is mismatch with input array\n") );
	}

	// disable buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


CIndexBufferGL::~CIndexBufferGL()
{
	glDeleteBuffers(1, &m_buffer);
}

// Vertex buffer operations.
void CIndexBufferGL::Enable()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer);
}

void CIndexBufferGL::Disable()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void* CIndexBufferGL::Lock(CIndexBuffer::Locking mode)
{
	// Use glMapBuffer
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_buffer );

	if(mode == CIndexBuffer::BL_DISCARD)
	{
		// Discard the buffer
		glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, m_pIBuffer->GetBytesSize(), 0, GLBufferUsage[ m_pIBuffer->GetUsage() ] );
	}

	void* pBuffer = glMapBuffer( GL_ELEMENT_ARRAY_BUFFER, GLBufferLockingMode[mode] );

	if(pBuffer == 0)
	{
		assert( false && ieS("Index Buffer: Out of memory\n") );
	}

    return pBuffer;
}

void CIndexBufferGL::Unlock()
{
	 glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer);

	 glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	 glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
