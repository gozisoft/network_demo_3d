#include "frameafx.h"
#include "WinNet.h"
#include "Packet.h"

#include "StringFunctions.h"

#include <WS2tcpip.h>

using std::list;

_ENGINE_BEGIN

namespace UDP
{

	// Creates a new UDP socket and binds it to a particular port
	// for sending and recieving data over.
	// Note : If this is used to create the socket the sendto and
	// recievefrom functions have to be used in order to correctly
	// send data over the network.
	int NETBind(SOCKET& newSocket, const String& port, bool block)
	{
		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(ADDRINFOT) );
		hints.ai_family = AF_INET;				// Use AF_UNSPEC for IPv4 or IPv6 compatibility
		hints.ai_socktype = SOCK_DGRAM;			// Specify UDP usage
		hints.ai_protocol = IPPROTO_UDP;		// Specify UDP usage
		hints.ai_flags = AI_PASSIVE;			// The socket address will be used in a call to the bind function

		PADDRINFOT pResult = 0;
		GetAddrInfo( 0, port.c_str(), &hints, &pResult );

		// Used in setsockopt.
		bool optVal = TRUE;
		int optLen = sizeof(bool);

		for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
		{
			// Create the socket
			newSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol); 

			if (newSocket == INVALID_SOCKET)
			{
				Cout << ieS("Creating socket UDP failed") << std::endl;
				continue; // skip this loop and go to ai_next
			}

			// Allows other sockets to bind() to this port, unless there is an
			// active listening socket bound to the port already. This enables
			// you to get around those "Address already in use" error messages
			// when you try to restart your server after a crash.
			if ( setsockopt( newSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal, optLen ) == SOCKET_ERROR ) 
			{
				Cout << ieS("setsockopt UDP has failed") << std::endl;
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			// Setup a default place to send packets to, for UDP.
			if ( bind(newSocket, p->ai_addr, p->ai_addrlen) )
			{
				Cout << ieS("Could not bind UDP socket") << std::endl;
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			break;
		}

		// Free up resources from created by GetAddrInfo
		FreeAddrInfo(pResult);

		// set nonblocking - accept() blocks under some odd circumstances otherwise
		NETSetBlocking(newSocket, block);

		if (newSocket == INVALID_SOCKET)
		{
			assert( false && ieS("Unable to create socket also failed to bind socket") );
			return 1;
		}

		return 0;
	}

	// Creates a new UDP socket without calling bind.
	int NETCreateSock(SOCKET& newSocket, const String& port, bool block)
	{
		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(ADDRINFOT) );
		hints.ai_family = AF_INET;				// Use AF_UNSPEC for IPv4 or IPv6 compatibility
		hints.ai_socktype = SOCK_DGRAM;			// Specify UDP usage
		hints.ai_protocol = IPPROTO_UDP;		// Specify UDP usage

		PADDRINFOT pResult = 0;
		GetAddrInfo( 0, port.c_str(), &hints, &pResult );

		// Used in setsockopt.
		bool optVal = TRUE;
		int optLen = sizeof(bool);

		for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
		{
			// Create the socket
			newSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol); 

			if (newSocket == INVALID_SOCKET)
			{
				Cout << ieS("Creating socket UDP failed") << std::endl;
				continue; // skip this loop and go to ai_next
			}

			// Allows other sockets to bind() to this port, unless there is an
			// active listening socket bound to the port already. This enables
			// you to get around those "Address already in use" error messages
			// when you try to restart your server after a crash.
			if ( setsockopt( newSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&optVal, optLen ) == SOCKET_ERROR ) 
			{
				Cout << ieS("setsockopt UDP has failed") << std::endl;
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			break;
		}

		// Free up resources from created by GetAddrInfo
		FreeAddrInfo(pResult);

		// set nonblocking - accept() blocks under some odd circumstances otherwise
		NETSetBlocking(newSocket, block);

		if (newSocket == INVALID_SOCKET)
		{
			assert( false && ieS("Unable to create socket also failed to bind socket") );
			return 1;
		}

		return 0;
	}

	// Creates a new UDP socket and calls connect() in order to
	// allow for the use of recv/send as opposed to recvfrom/sendto.
	int NETConnectSock(SOCKET& newSocket, const String& ip, const String& port, bool block)
	{
		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(ADDRINFOT) );
		hints.ai_family = AF_INET;				// Use AF_UNSPEC for IPv4 or IPv6 compatibility
		hints.ai_socktype = SOCK_DGRAM;			// Specify UDP usage
		hints.ai_protocol = IPPROTO_UDP;		// Specify UDP usage

		PADDRINFOT pResult = 0;
		GetAddrInfo( ip.c_str(), port.c_str(), &hints, &pResult );

		// Used in setsockopt.
		bool optVal = TRUE;
		int optLen = sizeof(bool);

		for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
		{
			// Create the socket
			newSocket = socket(p->ai_family, p->ai_socktype, p->ai_protocol); 

			if (newSocket == INVALID_SOCKET)
			{
				Cout << ieS("Creating socket failed") << std::endl;
				NETPrintError();
				continue; // skip this loop and go to ai_next
			}

			// Setup a default place to send packets to, for UDP.
			if ( connect(newSocket, p->ai_addr, p->ai_addrlen) )
			{
				// Free up used resources
				Cout << ieS("Could not connect UDP socket") << std::endl;
				NETPrintError();
				closesocket(newSocket);
				newSocket = INVALID_SOCKET;
				continue;
			}

			break;
		}

		FreeAddrInfo(pResult);
		if (newSocket == INVALID_SOCKET)
		{
			NETPrintError();
			assert( false && ieS("Unable to create socket also failed to connect socket") );
			return 1;
		}

		return 0;
	}

	int NETHandleOutput(list<IPacketPtr>& outList, SOCKET openSocket, const String& ip, const String& port)
	{
		typedef list<IPacketPtr>::iterator PacketItor;

		assert( !outList.empty() );

		bool packetSent = false;
		u_int sendOfs = 0;
		u_int errorFlag = 0;

		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(hints) );
		hints.ai_family = AF_UNSPEC;			// use IPv4 or IPv6, whichever
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_flags = AI_PASSIVE;			// fill in my IP for me

		ADDRINFOT* pResult = 0;
		GetAddrInfo( ip.c_str(), port.c_str(), &hints, &pResult );

		for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
		{
			do
			{
				PacketItor itor = outList.begin();
				IPacketPtr pIPacket = *itor;
				const char* pData = pIPacket->GetData();
				int length = (int)pIPacket->GetSize();

				// For message-oriented sockets (address family of AF_INET or AF_INET,
				// type of SOCK_DGRAM, and protocol of IPPROTO_UDP, for example), care
				// must be taken not to exceed the maximum packet size of the underlying provider
				int optVal = 0;
				int optLen = sizeof(int);
				if ( getsockopt(openSocket, SOL_SOCKET, SO_MAX_MSG_SIZE, (char*)&optVal, &optLen) == SOCKET_ERROR )
				{
					Cout << ieS("Problem with determining max message size of the provider") << std::endl;
					NETPrintError();
					return 0;
				}
				if (length > optVal)
				{
					Cout << ieS("Packet is bigger than the SO_MAX_MSG_SIZE of the socket") << std::endl;
					return 0;
				}

				// For message-oriented sockets (address family of AF_INET or AF_INET,
				// type of SOCK_DGRAM, and protocol of IPPROTO_UDP, for example), care
				// must be taken not to exceed the maximum packet size of the underlying provider
				int result = sendto(openSocket, pData+sendOfs, length-sendOfs, 0, p->ai_addr, p->ai_addrlen);
				if (result == SOCKET_ERROR)
				{
					Cout << ieS("send() SOCKET_ERROR failed with error: ") << WSAGetLastError() << std::endl;
					NETPrintError();
					packetSent = false;
				}
				else if (result >= 0)
				{
					sendOfs += result; // bytes sent, increase the offset.
					packetSent = true;
				}
				else
				{
					packetSent = false;
				}

				NETPrintError();

				// if the amount of sent data has reace the same length of that of the packet
				// size, then pop the top packet and send the next one.
				if ( sendOfs == length )
				{		
					outList.pop_front();
					sendOfs = 0; // reset the sent offset
				}

			} while( packetSent && !outList.empty() );
		}

		return errorFlag;
	}

} // namespace UDP

_ENGINE_END // namespace Engine



	/*
	for ( PacketItor itor = outList.begin(); itor != outList.end();  )
	{
	IPacketPtr pIPacket = *itor;
	const char* pData = pIPacket->GetData();
	u_int length = pIPacket->GetSize();

	for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
	{
	while ( int result = sendto( openSocket, pData+sendOfs, length-sendOfs,
	0, p->ai_addr, p->ai_addrlen )  )
	{
	if (result > 0)
	{
	sendOfs += result; // bytes sent, increase the offset.
	packetSent = true;
	}
	else if (WSAGetLastError() != WSAEWOULDBLOCK)
	{
	errorFlag |= 1;
	packetSent = false;
	}
	else
	{
	packetSent = false;
	}

	// if the amount of sent data has reached the same length of that of the packet
	// size, break the while loop and send the same packet to the next ADDRINFOT.
	if (sendOfs == length)
	{
	sendOfs = 0; // reset the sent offset
	result = 0;
	break;
	}
	}

	}
	*/