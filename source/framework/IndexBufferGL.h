#ifndef CINDEX_BUFFER_GL_H
#define CINDEX_BUFFER_GL_H

#include "framefwd.h"
#include "IndexBuffer.h"

#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif

#include "GL/glew.h"

namespace Engine
{

class CIndexBufferGL 
{
public:
	CIndexBufferGL(CIndexBufferPtr pIBuffer);
	~CIndexBufferGL();

	// Index buffer operations.
	void Enable();
	void Disable();
	void* Lock(CIndexBuffer::Locking mode);
	void Unlock();


private:
	GLuint m_buffer;
	CIndexBufferPtr m_pIBuffer;

private:
	DECLARE_HEAP;
};


}




#endif