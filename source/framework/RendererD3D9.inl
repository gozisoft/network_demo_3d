#ifndef CRENDERER_D3D9_INL
#define CRENDERER_D3D9_INL

/*
inline IDirect3DDevice9* CRendererD3D9::GetDevice()
{ 
	assert( m_pData->m_pID3DDevice && ieS("No valid D3D9 device found") );
	return m_pData->m_pID3DDevice;
}

inline HWND CRendererD3D9::GetHWND() const
{ 
	return m_pData->m_hWnd;
}
*/

inline void CRendererD3D9::SetCamera(CCameraPtr pCamera)
{
	m_pCamera = pCamera;
}

CCameraPtr CRendererD3D9::GetCamera()
{
	return m_pCamera;
}


#endif