#include "Frameafx.h"
#include "RenderDataGL.h"
#include "MappingGL.h"

using namespace Engine;

DEFINE_HEAP(CRenderDataGL, "CGLRenderData");
DEFINE_HEAP(CRenderDataGL::CRenderStateGL, "CRenderStateGL");

CRenderDataGL::CRenderDataGL ()
{
	m_HDc = 0;
	m_HRc = 0;
	m_hWnd = 0;
	m_hInst = 0;

	SecureZeroMemory( &m_currentState, sizeof(m_currentState) ); // just set all to zero first
}

CRenderDataGL::~CRenderDataGL()
{


}

void CRenderDataGL::CRenderStateGL::Initialise(const CAlphaState* astate, const CCullState* cstate,
			const CDepthState* dstate, const COffsetState* ostate,
			const CStencilState* sstate, const CWireState* wstate)
{
	// Alpha
	m_alphaBlendEnabled = astate->m_alphaBlendEnabled;
	m_alphaSrcBlend = GLAlphaSrcBlend[astate->m_srcBlend];
	m_alphaDstBlend = GLAlphaDstBlend[astate->m_dstBlend];
	m_alphaCompareEnabled = astate->m_compareEnabled;
	m_compareFunction = GLAlphaCompare[astate->m_compareMode];
	m_alphaReference = astate->m_ref;
	m_blendColour = astate->m_constantColour;

	m_alphaBlendEnabled ? glEnable(GL_BLEND) : glDisable(GL_BLEND);
    glBlendFunc(m_alphaSrcBlend, GL_ONE_MINUS_SRC_ALPHA);
    m_alphaCompareEnabled ? glEnable(GL_ALPHA_TEST) : glDisable(GL_ALPHA_TEST);
    glAlphaFunc(m_compareFunction, m_alphaReference);
    glBlendColor(m_blendColour[0], m_blendColour[1], m_blendColour[2], m_blendColour[3]);

	// Cull
	m_cullEnabled = cstate->m_cullEnabled;
	m_CCWOrder = cstate->m_ccOrder;

    m_cullEnabled ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
	glCullFace(m_CCWOrder ? GL_BACK : GL_FRONT);

	// DepthState
	m_depthEnabled = dstate->m_enabled;
	m_depthWriteEnabled = dstate->m_writable;
	m_depthCompareFunction = GLDepthCompare[dstate->m_compareMode];

	m_depthEnabled ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
	glDepthMask(m_depthWriteEnabled ? GL_TRUE : GL_FALSE);
	glDepthFunc(m_depthCompareFunction);

	// OffsetState
	m_fillEnabled = ostate->m_fillEnabled;
	m_lineEnabled = ostate->m_lineEnabled;
	m_pointEnabled = ostate->m_pointEnabled;
	m_offsetScale = ostate->m_offsetScale;
	m_offsetBias = ostate->m_offsetBias;

	m_fillEnabled ? glEnable(GL_POLYGON_OFFSET_FILL) :
		glDisable(GL_POLYGON_OFFSET_FILL);
	m_lineEnabled ? glEnable(GL_POLYGON_OFFSET_LINE) :
		glDisable(GL_POLYGON_OFFSET_LINE);
	m_pointEnabled ? glEnable(GL_POLYGON_OFFSET_POINT) :
		glDisable(GL_POLYGON_OFFSET_POINT);
	glPolygonOffset(m_offsetScale, m_offsetBias);

	// StencilState
	m_stencilEnabled = sstate->m_enabled;
	m_stencilCompareFunction = GLStencilCompare[sstate->m_compare];
	m_stencilReference = sstate->m_reference;
	m_stencilMask = sstate->m_mask;
	m_stencilWriteMask = sstate->m_writeMask;
	m_stencilOnFail = GLStencilOperation[sstate->m_onFail];
	m_stencilOnZFail = GLStencilOperation[sstate->m_onZFail];
	m_stencilOnZPass = GLStencilOperation[sstate->m_onZPass];

	// WireState
    m_wireEnabled = wstate->m_enable;

    glPolygonMode(GL_FRONT_AND_BACK, m_wireEnabled ? GL_LINE : GL_FILL);

}