#include "Frameafx.h"
#include "CameraControllerFPS.h"
#include "CameraNodeFPS.h"
#include "IMouseController.h"
#include "IEventReciever.h"
#include "StringFunctions.h"
#include "WinMouseEvent.h"
#include "WinKeyboardEvent.h"


using namespace Engine;

DEFINE_HIERARCHICALHEAP(CCameraControllerFPS, "CCameraControllerFPS", "ISceneControllerFPS");

CCameraControllerFPS::CCameraControllerFPS(IMouseControllerPtr pMouse, float roateSpeed, float moveSpeed, SKeyMap* pKeyMapVector,
	UInt KeyMapSize, bool freeLook, bool centerMouse, bool invert) :
m_pMouse(pMouse),
m_maxVertAngle(80.0f),
m_rotateSpeed(roateSpeed),
m_moveSpeed(moveSpeed),
m_vertMovement(freeLook),
m_centerMouse(centerMouse),
m_pitch(0.0f),
m_yaw(0.0f),
m_mouseYDirection( invert ? -1.0f : 1.0f ),
m_firstUpdate(true)
{
	// zero the keys
	SetAllKeysUp();

	// zero the buttons
	SetAllButtonsUp();

	if (!pKeyMapVector || !KeyMapSize)
	{
		// create default key map
		m_keyMap.push_back( SKeyMap(KA_MOVE_FORWARD, KEY_UP) );
		m_keyMap.push_back( SKeyMap(KA_MOVE_BACKWARD, KEY_DOWN) );
		m_keyMap.push_back( SKeyMap(KA_STRAFE_LEFT, KEY_LEFT) );
		m_keyMap.push_back( SKeyMap(KA_STRAFE_RIGHT, KEY_RIGHT) );
	}
	else
	{
		// create custom 
		SetKeyMap(pKeyMapVector, KeyMapSize);
	}

}


CCameraControllerFPS::~CCameraControllerFPS()
{
	m_keyMap.clear();
}


// function moves the camera by given speed.
void CCameraControllerFPS::AnimateNode(CSpatial* pSpatial, double timeMS)
{
	if (!pSpatial)
		return;

	// cast the spatial to a camera
	CCameraNodeFPS *pCameraNode = static_cast<CCameraNodeFPS*>(pSpatial);
	CCameraPtr pCamera = pCameraNode->GetCamera();

	if (m_firstUpdate)
	{
		if (pCameraNode && m_pMouse)
		{
			m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
			m_lastCursorPos = m_pMouse->GetMousePos();
		}

		m_firstUpdate = false;
	}

	if (m_mouseButtons[0] == true)
	{
		// Process mouse input
		{
			// Get current position of mouse
			Point2l curMousePos = m_pMouse->GetMousePos();

			// Calc how far it's moved since last frame
			Point2f curMouseDelta = curMousePos - m_lastCursorPos;

			// Record current position for next time
			m_lastCursorPos = curMousePos;

			// Reset cursor position to the centre of the window.
			if (m_centerMouse)
			{
				m_pMouse->SetRelativePos( Point2f(0.5f, 0.5f) );
				m_lastCursorPos = m_pMouse->GetMousePos();
			}

			// Smooth the relative mouse data over a few frames so it isn't 
			// jerky when moving slowly at low frame rates.
			float fPercentOfNew = 1.0f / 2.0f;
			float fPercentOfOld = 1.0f - fPercentOfNew;
			m_mouseDelta = (m_mouseDelta * fPercentOfOld) + (curMouseDelta * fPercentOfNew);
			//m_mouseDelta =  curMouseDelta;

			// Assign the rotational velocity of this frame
			m_rotVelocity = m_mouseDelta * m_rotateSpeed;
		}

		//
		{
			// Update the pitch & yaw angle based on mouse movement
			float angleY = m_rotVelocity.y() * m_mouseYDirection;
			float angleX = m_rotVelocity.x();

			m_pitch += angleY;
			m_yaw += angleX;

			// Limit pitch to straight up or straight down
			m_pitch = Max( -Const<float>::PI() / 3.0f, m_pitch );
			m_pitch = Min( +Const<float>::PI() / 3.0f, m_pitch );
		}

	}
	else
	{
		m_lastCursorPos = m_pMouse->GetMousePos();
	}

	// Get latest pos.
	Vector3f posDelta = pCameraNode->GetWorldTransform().GetTranslation();
	// Determine last zaxis vector.
	Vector3f target = pCameraNode->GetTargetVector() - posDelta;

	target.SetXYZ( 0, 0, Max( 1.0f, Mag(posDelta) ) );
	Vector3f moveDir = target;

	// Make a rotation matrix based on the camera's yaw & pitch
	Matrix4f rotation = Matrix4f::IDENTITY;
	rotation.SetRotationRads( Vector3f(m_pitch, m_yaw, 0.0f) ); 
	rotation.TransformVectCoord(target);

	// Transform the position delta by the camera's rotation 
	if (!m_vertMovement)
	{
		// If restricting Y movement, do not include pitch
        // when transforming position delta vector.
		rotation.SetRotationRads( Vector3f(0.0f, m_yaw, 0.0f) ); 
		rotation.TransformVectCoord(moveDir);
	}
	else
	{
		moveDir = target;
	}

	moveDir.Normalise();

	if (m_cursorkeys[KA_MOVE_FORWARD])
		posDelta += moveDir * timeMS * m_moveSpeed;

	if (m_cursorkeys[KA_MOVE_BACKWARD])
		posDelta -= moveDir * timeMS * m_moveSpeed;

	// strafing
	Vector3f strafe = Cross(pCameraNode->GetUpVector(), target);

	if (!m_vertMovement)
		strafe.y() = 0.0f;

	strafe.Normalise();

	if (m_cursorkeys[KA_STRAFE_RIGHT])
		posDelta += strafe * timeMS * m_moveSpeed;

	if (m_cursorkeys[KA_STRAFE_LEFT])
		posDelta -= strafe * timeMS * m_moveSpeed;

	// Set the local position for next frame update
	pCameraNode->SetPosition(posDelta);

	// write target
	Vector3f lookat = posDelta + target;
	pCameraNode->SetTarget(lookat);
}

bool CCameraControllerFPS::HandleEvent(const IEventData& event)
{
	if ( SKeyEvent::sk_EventType == event.GetEventType() )
	{
		const SKeyEvent& ed = static_cast<const SKeyEvent&>(event);
		SKeyInput keyInput = ed.m_keyInput;

		for (KeyMapVector::iterator i = m_keyMap.begin(); i != m_keyMap.end(); ++i)
		{
			if ( (*i).keycode == keyInput.key )
			{
				m_cursorkeys[(*i).action] = keyInput.pressedDown;
				return true; // event absorbed
			}
		}
	}
	else if ( SMouseEvent::sk_EventType == event.GetEventType() )
	{
		const SMouseEvent& ed = static_cast<const SMouseEvent&>(event);
		SMouseInput mouseInput = ed.m_mouseInput;

		/*if (mouseInput.mouseEvent == MIE_MOUSE_MOVED)
		{
			// Get current position of mouse
			m_curMousePos = m_pMouse->GetMousePos();
		}*/

		m_mouseButtons[0] = mouseInput.isLeftPressed();
		m_mouseButtons[1] = mouseInput.isRightPressed();
		return true;
	}


	return false;
}


void CCameraControllerFPS::SetKeyMap(SKeyMap *pKeyMap, UInt size)
{
	// clear keymap
	m_keyMap.clear();

	// loop through keymap
	for (UInt32 i = 0; i < size; ++i)
	{
		// find the assigned action
		// and assign the approriate
		// keycode
		switch (pKeyMap[i].action)
		{
		case KA_MOVE_FORWARD:
			m_keyMap.push_back( SKeyMap(KA_MOVE_FORWARD, pKeyMap[i].keycode) );
			break;
		case KA_MOVE_BACKWARD:
			m_keyMap.push_back( SKeyMap(KA_MOVE_BACKWARD, pKeyMap[i].keycode) );
			break;
		case KA_STRAFE_LEFT:
			m_keyMap.push_back( SKeyMap(KA_STRAFE_LEFT, pKeyMap[i].keycode) );
			break;
		case KA_STRAFE_RIGHT:
			m_keyMap.push_back( SKeyMap(KA_STRAFE_RIGHT, pKeyMap[i].keycode) );
			break;
		};
	}
}
