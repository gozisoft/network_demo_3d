#ifndef __CVERTEX_BUFFER_H__
#define __CVERTEX_BUFFER_H__

#include "Buffer.h"

namespace Engine
{

class CVertexBuffer : public CBuffer
{		
public:
	CVertexBuffer();
	CVertexBuffer(UInt numVertices, UInt sizeInBytes, BufferUse usage);
	CVertexBuffer(const CVertexBuffer &verterxBuffer);

	// const has been removed
	UInt8 *GetData();

private:
	DECLARE_HEAP;
};


#include "VertexBuffer.inl"


}


#endif


/*

class CVertexBuffer
{		
public:
enum VertexSemantic 
{
VERTEX_UNKNOWN,
VERTEX_POSITION,
VERTEX_BLEND_WEIGHTS,
VERTEX_NORMAL,
VERTEX_COLOUR,
VERTEX_FOG,
VERTEX_PSIZE,
VERTEX_BLEND_INDICES,
VERTEX_TEXTURE_COORDINATES,
VERTEX_TANGENT,
VERTEX_BITANGENT,
NUM_VERTEX_SEMANTICS
};

public:
CVertexBuffer();
CVertexBuffer(const CVertexBuffer &attSet);
~CVertexBuffer();

public:
void		SetVertexData(VertexSemantic attribute, UInt channels,
VertexType type, const void *data, UInt count);

const void *GetVertexData(VertexSemantic attribute) const;
UInt		GetVertexChannels(VertexSemantic attribute) const;
UInt		GetVertexCount(VertexSemantic attribute) const;
VertexType	GetVertexDataType(VertexSemantic attribute) const;
void		RemoveVertexData(VertexSemantic attribute);

UInt		GetTotalStride() const;

void		SetEnabled(VertexSemantic attribute, bool enable);
bool		IsEnabled(VertexSemantic attribute) const;

public:
// Position Coordinates
// Can be of 1 - 4, floats or doubles. 
void SetPosition(const Vector2f *vertices, UInt count);
void SetPosition(const Vector3f *vertices, UInt count);
void SetPosition(const Vector4f *vertices, UInt count);

const Vector2f *GetPosition2() const;
const Vector3f *GetPosition3() const;
const Vector4f *GetPosition4() const;

UInt GetPostionCount() const;

// Blendweights
// Can be of 1 - 5, floats or doubles to representing vertex blending.
template <typename T> void SetBlendWeights(const T *weights, UInt count);

const void *GetBlendWeights() const;

// Normals
// Can be of 3, float or double.
void SetNormals(const Vector3f *normals, UInt count);

const Vector3f *GetNormals() const;

UInt GetNormalCount() const;

// Colours
// Can be of 3 - 4, float, double or unsigned byte.
template <typename T> void SetColours(const ColourRGB<T> *colours, UInt count);
template <typename T> void SetColours(const ColourRGBA<T> *colours, UInt count);

const void *GetColours(UInt i) const;

// Fog
// Can be of 1 floats or doubles.
template <typename T> void SetFogCoords(const T *coords, UInt count);

const void *GetFogCoords() const;

// Point
// The size of a point.
template <typename T> void SetPointSize(const T *PSizes, UInt count);

const void *GetPointSize() const;

// Index Blending
template <typename T> void SetBlendidIndices(const T *indices, UInt count);

const void *GetBlendidIndices() const;

// Texture Coordinates
// Texture 1D, 2D, 3D, 4D
template <typename T> void SetTCoords(const T *tcoords, UInt count);
template <typename T> void SetTCoords(const Vector2<T> *tcoords, UInt count);
template <typename T> void SetTCoords(const Vector3<T> *tcoords, UInt count);
template <typename T> void SetTCoords(const Vector4<T> *tcoords, UInt count);

const void *GetTCoords(UInt i) const;

// Tangent Coordinates
template <typename T> void SetTangents(const T *tangents, UInt count);

const void *GetTangents() const;

// BiTangent Coordinates
template <typename T> void SetBiTangents(const T *biTangents, UInt count);

const void *GetBiTangents() const;

private:
typedef std::vector<CVertexAttributePtr> AttributeList;
typedef AttributeList::iterator AttItor; 
typedef AttributeList::const_iterator Const_AttItor;
typedef std::bitset<NUM_VERTEX_SEMANTICS> EnableBSet;

AttributeList m_vattribs;
EnableBSet m_enableFlags;
UInt m_totalStride;
};

*/