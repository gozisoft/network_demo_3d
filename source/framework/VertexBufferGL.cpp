#include "Frameafx.h"
#include "VertexBufferGL.h"
#include "MappingGL.h"


using namespace Engine;

DEFINE_HEAP(CVertexBufferGL, "CVertexBufferGL");

CVertexBufferGL::CVertexBufferGL(CVertexBufferPtr pVBuffer) : 
m_pVBuffer(pVBuffer),
m_buffer(0)
{
    glGenBuffers(1, &m_buffer);

	if (!m_buffer)
		assert( false && ieS("Cannot create GL vertex buffer\n") );

	// generate a new VBO and get the associated ID
    glBindBuffer(GL_ARRAY_BUFFER, m_buffer);

	// bind VBO in order to use
	glBufferData(GL_ARRAY_BUFFER, m_pVBuffer->GetBytesSize(), (GLvoid*)m_pVBuffer->GetData(),
		GLBufferUsage[ m_pVBuffer->GetUsage() ] );

	// check data size in VBO is same as input array, if not return 0 and delete VBO
	GLint bufferSize = 0;
	glGetBufferParameterivARB( GL_ARRAY_BUFFER, GL_BUFFER_SIZE_ARB, &bufferSize );
	if( m_pVBuffer->GetBytesSize() != static_cast<UInt>(bufferSize) )
	{
		glDeleteBuffersARB(1, &m_buffer);
		m_buffer = 0;
		assert( false && ieS("[createVBO()] Data size is mismatch with input array\n") );
	}

	// disable buffer until enabled
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

CVertexBufferGL::~CVertexBufferGL()
{
	glDeleteBuffers(1, &m_buffer);
}

// Vertex buffer operations.
void CVertexBufferGL::Enable()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_buffer);
}

void CVertexBufferGL::Disable()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void* CVertexBufferGL::Lock(CVertexBuffer::Locking mode)
{
	// Use glMapBuffer
	glBindBuffer( GL_ARRAY_BUFFER, m_buffer );

	if(mode == CVertexBuffer::BL_DISCARD)
	{
		// Discard the buffer
		glBufferDataARB(GL_ARRAY_BUFFER_ARB, m_pVBuffer->GetBytesSize(), NULL, GLBufferUsage[ m_pVBuffer->GetUsage() ] );
	}

	void* pBuffer = glMapBuffer( GL_ARRAY_BUFFER, GLBufferLockingMode[mode] );

	if(pBuffer == 0)
	{
		assert( false && ieS("Vertex Buffer: Out of memory\n") );
	}

    return pBuffer;
}

void CVertexBufferGL::Unlock()
{
	 glBindBuffer(GL_ARRAY_BUFFER, m_buffer);

	 glUnmapBuffer(GL_ARRAY_BUFFER);

	 glBindBuffer(GL_ARRAY_BUFFER, 0);
}

