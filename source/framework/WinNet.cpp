#include "frameafx.h"
#include "WinNet.h"
#include "Packet.h" 
#include "StringFunctions.h"
#include "OSTimer.h"

#include <tchar.h>
#include <WS2tcpip.h>

using std::list;

_ENGINE_BEGIN

// If val = 0, blocking is enabled; 
// If val != 0, non-blocking mode is enabled.
void NETSetBlocking(SOCKET sock, bool block)
{
	u_long val = block ? 0 : 1;
	int result = ioctlsocket(sock, FIONBIO, &val);

	if (result != NO_ERROR)
	{
		NETPrintError();
		OStringStream oss;
		oss << ieS("ioctlsocket failed with error: ") << result << std::endl;
		OutputDebugString( oss.str().c_str() );
	}
}

void NETPrintError()
{
	LPVOID lpMsgBuf;
	::FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL 
		);

	::OutputDebugString( (LPCTSTR)lpMsgBuf );
	LocalFree( lpMsgBuf );
}


const Char* NETGetIpStr(const sockaddr *sa, Char *str, size_t maxlen)
{
	switch(sa->sa_family) 
	{
	case AF_INET:
		{
			const sockaddr_in *pSckadd = reinterpret_cast<const sockaddr_in *>(sa);
			if (OS::Timer::IsVistaOrLater() == true)
				InetNtop(AF_INET, (void*)&pSckadd->sin_addr, str, maxlen);
			else
				NETInetNtop(AF_INET, (void*)&pSckadd->sin_addr, str, maxlen);	
		}
		break;

	case AF_INET6:
		{
			const sockaddr_in6 *pSckadd = reinterpret_cast<const sockaddr_in6 *>(sa);
			if (OS::Timer::IsVistaOrLater() == true)
				InetNtop(AF_INET6, (void*)&pSckadd->sin6_addr, str, maxlen);			
			else
				NETInetNtop(AF_INET6, (void*)&pSckadd->sin6_addr, str, maxlen);	
		}
		break;

	default:
		str = 0;
		return 0;
	}

	return str;
}




const Char* NETInetNtop(int family, const void* pSrc, Char* pStrOut, size_t strSize)
{       
	union
	{
		sockaddr base;
		sockaddr_storage storage;
		sockaddr_in v4;
		sockaddr_in6 v6;
	} address;

	socklen_t address_length = 0;

	if (family == AF_INET)
	{
		address_length = sizeof(sockaddr_in);
		address.v4.sin_family = AF_INET;
		address.v4.sin_port = 0;
		std::memcpy( &address.v4.sin_addr, pSrc, sizeof(in_addr) );
	}
	else // AF_INET6
	{
		address_length = sizeof(sockaddr_in6);
		address.v6.sin6_family = AF_INET6;
		address.v6.sin6_port = 0;
		address.v6.sin6_flowinfo = 0;
		address.v6.sin6_scope_id = 0;
		std::memcpy( &address.v6.sin6_addr, pSrc, sizeof(in6_addr) );
	}

	DWORD string_length = static_cast<DWORD>(strSize);
	if (::WSAAddressToString(&address.base, address_length, 0, (LPTSTR)pStrOut, &string_length) != 0)
	{  
		OStringStream oss;
		oss << ieS("WSAAddressToString(): ") << WSAGetLastError();
		OutputDebugString( oss.str().c_str() );
		NETPrintError();
		return 0;
	}

	return pStrOut;  
} 

UInt16 NetGetPort(const sockaddr *sa)
{
	UInt16 portNum = 0;

	switch(sa->sa_family) 
	{
	case AF_INET:
		{
			const sockaddr_in *pSckadd = reinterpret_cast<const sockaddr_in *>(sa);
			portNum = static_cast<UInt16>( ntohs( pSckadd->sin_port) );
		}
		break;

	case AF_INET6:
		{
			const sockaddr_in6 *pSckadd = reinterpret_cast<const sockaddr_in6 *>(sa);
			portNum = static_cast<UInt16>( ntohs(pSckadd->sin6_port ) );
		}
		break;
	}
	return portNum;
}

void NETGetPortStr(const sockaddr *sa, String &str)
{
	switch(sa->sa_family) 
	{
	case AF_INET:
		{
			const sockaddr_in *pSckadd = reinterpret_cast<const sockaddr_in *>(sa);
			str = ToString(pSckadd->sin_port);
		}
		break;

	case AF_INET6:
		{
			const sockaddr_in6 *pSckadd = reinterpret_cast<const sockaddr_in6 *>(sa);
			str = ToString(pSckadd->sin6_port);
		}
		break;

	default:
		str = String();
	}
}

	
_ENGINE_END

		/*
		const Char* NETGetIpStr(const sockaddr *sa, Char *str, size_t maxlen)
		{
		switch(sa->sa_family) 
		{
		case AF_INET:
		{
		char *ansiStr = new char[maxlen];
		const sockaddr_in *pSckadd = reinterpret_cast<const sockaddr_in *>(sa);
		inet_ntop(AF_INET, (void*)&pSckadd->sin_addr, ansiStr, maxlen);
		AnsiToGeneric( str, ansiStr, std::strlen(ansiStr), std::strlen(ansiStr) );
		}
		break;

		case AF_INET6:
		{
		char *ansiStr = new char[maxlen];
		const sockaddr_in6 *pSckadd = reinterpret_cast<const sockaddr_in6 *>(sa);
		inet_ntop(AF_INET6, (void*)&pSckadd->sin6_addr, ansiStr, maxlen);
		AnsiToGeneric( str, ansiStr, std::strlen(ansiStr), std::strlen(ansiStr) );
		}
		break;

		default:
		// _tcscpy( &str[0], ieS("Unknown AF") );
		str = 0;
		return 0;
		}

		return str;
		}
		*/

/*
const Char* NETGetIpStr(const sockaddr *sa, Char *str, size_t maxlen)
{
	switch(sa->sa_family) 
	{
	case AF_INET:
		{
			const sockaddr_in *pSckadd = reinterpret_cast<const sockaddr_in *>(sa);
			NETInetNtop(AF_INET, (void*)&pSckadd->sin_addr, str, maxlen);
		}
		break;

	case AF_INET6:
		{
			const sockaddr_in6 *pSckadd = reinterpret_cast<const sockaddr_in6 *>(sa);
			NETInetNtop(AF_INET6, (void*)&pSckadd->sin6_addr, str, maxlen);			
		}
		break;

	default:
		// _tcscpy( &str[0], ieS("Unknown AF") );
		str = 0;
		return 0;
	}

	return str;
}
*/


		/*
		const Char* NETGetIpStr(const sockaddr *sa, Char *str, size_t maxlen)
		{
		switch(sa->sa_family) 
		{
		case AF_INET:
		{
		const sockaddr_in *pSckadd = reinterpret_cast<const sockaddr_in *>(sa);
		NETInetNtop(AF_INET, (void*)&pSckadd->sin_addr, str, maxlen);
		}
		break;

		case AF_INET6:
		{
		const sockaddr_in6 *pSckadd = reinterpret_cast<const sockaddr_in6 *>(sa);
		NETInetNtop(AF_INET6, (void*)&pSckadd->sin6_addr, str, maxlen);			
		}
		break;

		default:
		// _tcscpy( &str[0], ieS("Unknown AF") );
		str = 0;
		return 0;
		}

		return str;
		}
		*/





