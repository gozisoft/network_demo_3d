#ifndef CVERTEX_FORMAT_GL_H
#define CVERTEX_FORMAT_GL_H

#include "FrameFwd.h"

namespace Engine
{

class CVertexFormatGL
{
public:
	CVertexFormatGL(CVertexFormatPtr pVFromat);
	~CVertexFormatGL();

	void Enable();
	void Disable();

private:
	UInt8* BufferOffset(const Long offset) { return ( (UInt8*)0 + offset ); }

	CVertexFormatPtr m_pVFormat;
	UInt m_numActiveCols;
	UInt m_numActiveTextures;

private:
	DECLARE_HEAP;

};


}


#endif