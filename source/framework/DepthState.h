#ifndef __CDEPTH_STATE_H__
#define __CDEPTH_STATE_H__

#include "Core.h"

namespace Engine
{


class CDepthState
{
public:
	enum CompareMode
	{
		CM_NEVER,
		CM_LESS,
		CM_EQUAL,
		CM_LEQUAL,
		CM_GREATER,
		CM_NOTEQUAL,
		CM_GEQUAL,
		CM_ALWAYS,
		NUM_CMP_MODES
	};

	CDepthState();
	CDepthState(bool enable, bool writable, CompareMode compareMode);
	~CDepthState();

	bool m_enabled;				// default: true
	bool m_writable;            // default: true
	CompareMode m_compareMode;  // default: CM_LEQUAL

private:

	DECLARE_HEAP;
};




} // namespace Engine


#endif