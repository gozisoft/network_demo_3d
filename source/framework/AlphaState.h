#ifndef __CALPHA_STATE_H__
#define __CALPHA_STATE_H__

#include "Core.h"
#include "Colour.h"

namespace Engine
{

class CAlphaState
{
public:
	enum SrcBlend
	{
		SB_ZERO,
		SB_ONE,
		SB_DST_COLOUR,
		SB_ONE_MINUS_DST_COLOUR,
		SB_SRC_ALPHA,
		SB_ONE_MINUS_SRC_ALPHA,
		SB_DST_ALPHA,
		SB_ONE_MINUS_DST_ALPHA,
		SB_SRC_ALPHA_SATURATE,
		SB_CONSTANT_COLOUR,
		SB_ONE_MINUS_CONSTANT_COLOUR,
		SB_CONSTANT_ALPHA,
		SB_ONE_MINUS_CONSTANT_ALPHA,
		NUM_SRC_BLENDS
	};

	enum DstBlend
	{
		DB_ZERO,
		DB_ONE,
		DB_DST_COLOUR,
		DB_ONE_MINUS_DST_COLOUR,
		DB_SRC_ALPHA,
		DB_ONE_MINUS_SRC_ALPHA,
		DB_DST_ALPHA,
		DB_ONE_MINUS_DST_ALPHA,
		DB_CONSTANT_COLOUR,
		DB_ONE_MINUS_CONSTANT_COLOUR,
		DB_CONSTANT_ALPHA,
		DB_ONE_MINUS_CONSTANT_ALPHA,
		NUM_DST_BLENDS
	};

	// Alpha Testing Related.
	enum CompareMode
	{
		CM_NEVER,
		CM_LESS,
		CM_EQUAL,
		CM_LEQUAL,
		CM_GREATER,
		CM_NOTEQUAL,
		CM_GEQUAL,
		CM_ALWAYS,
		NUM_CMP_MODES
	};

	CAlphaState();

	CAlphaState(bool blendEnable,
		SrcBlend srcBlend,
		DstBlend dstBlend,
		bool testEnable,
		CompareMode testval,
		float ref,
		const Colour4f& col);

	~CAlphaState();

	bool		m_alphaBlendEnabled;	// default: false
	SrcBlend	m_srcBlend;				// default: SB_SRC_ALPHA
	DstBlend	m_dstBlend;				// default: DB_ONE_MINUS_SRC_ALPHA

	// Alpha Testing Related.
	bool		m_compareEnabled;	// default: false
	CompareMode m_compareMode;		// default: TV_ALWAYS
	float		m_ref;				// default: 0 // 0 < ref < 1, lies between [0, 1]
	Colour4f	m_constantColour;

private:
	DECLARE_HEAP;
};




} // namespace Engine




#endif