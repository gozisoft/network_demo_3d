#include "Frameafx.h"
#include "VertexFormat.h"
#include "VertexElement.h"

using namespace Engine;

DEFINE_HEAP(CVertexFormat, "CVertexFormat");

CVertexFormat::CVertexFormat() : m_totalStride(0)
{

}


CVertexFormat::~CVertexFormat()
{
	m_elements.clear();
}


CVertexElementPtr CVertexFormat::AddElement(UInt sourceIndex, VertexType type, VertexSemantic semantic, UInt index)
{
	// if enabled and if not a colour or texture.
	if (IsEnabled(semantic) && ( (semantic != VS_COLOUR) || (semantic != VS_TEXTURE_COORDINATES) ) )
	{
		for (ElementItor itor = m_elements.begin(); itor != m_elements.end(); ++itor)
		{
			if ( (*itor)->GetVertexType() == semantic )
			{
				// decrement the total stide as this vertexattribute is going to be removed
				m_totalStride -= (*itor)->GetStride();

				// remove the vertex
				m_elements.erase(itor);

				// create the new vertex and increment the totalstride with the new attribute
				CVertexElementPtr pVertex = CVertexElementPtr( new CVertexElement( sourceIndex, m_totalStride, type, semantic, index ) );
				m_totalStride += SizeOfType(type);

				// insert the new vertex over the location of the
				// removed one.
				itor = m_elements.insert( itor, pVertex );
				return (*itor);
			}
		}
	}
	else // First time adding this attribute to the list. (or it is a colour or a texture)
	{
		// Create the vertex attribute and set its data.
		m_elements.push_back( CVertexElementPtr( new CVertexElement( sourceIndex, m_totalStride, type, semantic, index) ) );
		m_totalStride += SizeOfType(type);
		SetEnabled(semantic, true);
		return m_elements.back();
	}

	assert( false && ieS("Error occured when adding vertex element\n") );
	return CVertexElementPtr();
}


CVertexElementPtr CVertexFormat::InsertElement(UInt pos, UInt sourceIndex, VertexType type, VertexSemantic semantic, UInt index)
{
	if ( pos >= m_elements.size() || IsEnabled(semantic) )
		return AddElement(sourceIndex, type, semantic, index);

	ElementItor itor = m_elements.begin();
	for (UInt i = 0; i < pos; ++i)
		++itor;

	// create the new vertex and increment the totalstride with the new attribute
	CVertexElementPtr pVertex = CVertexElementPtr( new CVertexElement( sourceIndex, m_totalStride, type, semantic, index ) );
	m_totalStride += SizeOfType(type);

	itor = m_elements.insert( itor, pVertex );
	return (*itor);
}


void CVertexFormat::RemoveElement(UInt pos)
{
	assert(pos < m_elements.size());

	ElementItor itor = m_elements.begin();
	for (UInt i = 0; i < pos; ++i)
		++itor;

	m_elements.erase(itor);
}

void CVertexFormat::RemoveElement(VertexSemantic semantic, UInt semanticIndex)
{
	if (IsEnabled(semantic))
	{
		for (ElementItor itor = m_elements.begin(); itor != m_elements.end(); ++itor)
		{
			if ( (*itor)->GetVertexSemantic() == semantic &&  (*itor)->GetSemanticIndex() == semanticIndex)
			{
				m_elements.erase(itor);
				break;
			}
		}
	}
}

void CVertexFormat::RemoveAllElements()
{
	m_elements.clear();
}

UInt CVertexFormat::GetElementSemanitcCount(VertexSemantic semantic) const 
{
	UInt i = 0;

	for (Const_ElementItor itor = m_elements.begin(); itor != m_elements.end(); ++itor)
	{
		if ( (*itor)->GetVertexSemantic() == semantic)
		{
			++i;
		}
	}

	return i;
}

CVertexElementPtr CVertexFormat::GetElementBySemantic(VertexSemantic semantic, UInt semanticIndex)
{
	for (ElementItor itor = m_elements.begin(); itor != m_elements.end(); ++itor)
	{
		if ( (*itor)->GetVertexSemantic() == semantic &&  (*itor)->GetSemanticIndex() == semanticIndex)
		{
			return (*itor);
		}
	}

	return CVertexElementPtr();
}

CVertexFormat::ElementList CVertexFormat::GetElementByStream(UInt streamindex)
{
		ElementList retList;
		for (Const_ElementItor itor = m_elements.begin(); itor != m_elements.end(); ++itor)
		{
			if ( (*itor)->GetStreamIndex() == streamindex)
			{
				retList.push_back(*itor);
			}
		}
		return retList;
}