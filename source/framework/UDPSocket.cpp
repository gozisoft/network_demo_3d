#include "frameafx.h"
#include "UDPSocket.h"
#include "Packet.h"
#include "Winnet.h"
#include "IEventManager.h"
#include "StringFunctions.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CUDPSocket, "CUDPSocket", "CSocket");

CUDPSocket::CUDPSocket() : 
CSocket()
{

}

CUDPSocket::CUDPSocket(SOCKET newsocket) : 
CSocket(newsocket)
{

}

CUDPSocket::~CUDPSocket()
{

}

bool CUDPSocket::Connect(const String& ip, const String& port)
{
/*	int result = UDP::NETBind(m_socket, port);
	if (result != 0) 
	{
		Cout << ieS("UDP NETBind failed: ") << result << std::endl;
		return false;
	} */

	int result = UDP::NETConnectSock(m_socket, ip, port);
	if (result != 0) 
	{
		Cout << ieS("UDP NETConnectSock failed: ") << result << std::endl;
		return false;
	}

	m_ip = ip;
	m_port = port;

	return true;
}

bool CUDPSocket::Bind(const String& port)
{
	int result = UDP::NETBind(m_socket, port);
	if (result != 0) 
	{
		Cout << ieS("UDP NETBind failed: ") << result << std::endl;
		return false;
	}

	return true;
}

void CUDPSocket::HandleInput(const IEventManagerPtr& pEventMgr)
{
	UNREFERENCED_PARAMETER(pEventMgr);
	int errorFlag = 0;

	m_recvBuf.resize( MAX_PACKET_SIZE );
	CharVector::pointer pRecBuff = &m_recvBuf[0];
	int result = recv(m_socket, pRecBuff, MAX_PACKET_SIZE, 0);
	if (result == SOCKET_ERROR)
	{
		Cout << "recv() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
		NETPrintError();
		m_recvBuf.clear();
		SetDeleteFlag(DF_DESTROY, true);
		return;
	}
	else if (result == 0)
	{
		Cout << ieS("Connection closed") << std::endl;
		NETPrintError();
		m_recvBuf.clear();
		errorFlag = 1;
	}

#if defined(DEBUG) | defined(_DEBUG)	
	OStringStream oss;
	oss << ieS("Incoming: ") << result << ieS(" bytes.") << ieS(" ");
	Cout << oss.str() << std::endl;
#endif

	// Current size in buffer + next lot of bytes recieved
	int newData = result;
	int packetSize = 0;
	int processedData = 0;
	bool pktRecieved = false;

	while ( newData > (sizeof(u_short) * 2) )
	{
		CharVector::pointer pRead = &m_recvBuf[0];

		// measured offset of the base header
		size_t dataOffset = 0;

		// extract packet size from data.
		packetSize = *( reinterpret_cast<u_short *>(pRead + dataOffset) );
		packetSize = static_cast<int>( ntohs(packetSize) );

		// increase the offset by 2 bytes
		dataOffset += sizeof(u_short);

		// Don't have enough new data to grab the next packet
		if (packetSize > newData)
		{
			// Throw the recieved data away
			m_recvBuf.clear();
			break;
		}

		if (packetSize > MAX_PACKET_SIZE)
		{
			// prevent nasty buffer overruns!
			SetDeleteFlag(DF_DESTROY, true);
			return;
		}

		// we know how big the packet is...and we have the whole thing
		if (newData >= packetSize)
		{
			// Advance the pointer along 2 bytes to get the type
			u_short sType = ntohs( *( reinterpret_cast<u_short *>(pRead + dataOffset) ) );
			IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

			// increase the offset by 2 bytes
			dataOffset += sizeof(u_short);

			switch (type)
			{
			case IPacket::PT_BINARY:
				{
					// we know how big the packet is...and we have the whole thing
					CBinaryPacketPtr pkt = CBinaryPacketPtr(new CBinaryPacket( &pRecBuff[dataOffset], packetSize - dataOffset ));
					m_inList.push_back(pkt);
					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;
				}
				break;
			case IPacket::PT_TEXT:
				{
					// Convert the recieved text to generic form (Unicode or Windows Ansi)
					String recievedText;
					recievedText.resize( packetSize - dataOffset ); // Dont need the packet size or its type. Also no null character needed
					AnsiToGeneric( &recievedText[0], &pRecBuff[dataOffset], packetSize - dataOffset, packetSize - dataOffset );

					if ( !recievedText.empty() )
					{			
						// push back the text packet
						CTextPacketPtr pkt = CTextPacketPtr( new CTextPacket( recievedText ) );
						m_inList.push_back(pkt);
						pktRecieved = true;
						processedData += packetSize;
						newData -= packetSize;
					}			
				}
				break;
			case IPacket::PT_SAFEBINARY:
				{
					// 4 bytes in : to get the sequence num
					u_short sequenceNum = *( reinterpret_cast<u_short *>(pRead + dataOffset) );
					sequenceNum = ntohs(sequenceNum);

					// offset is 2 more bytes
					dataOffset += sizeof(u_short);

					// 6 bytes in : to get the ack
					u_long ack = *( reinterpret_cast<u_long *>(pRead + dataOffset) );
					ack = ntohl(sequenceNum);

					// offset is 4 more bytes
					dataOffset += sizeof(u_long);

					// 10 bytes in : to get the ack bit field
					u_long ack_bit = *( reinterpret_cast<u_long *>(pRead + dataOffset) );
					ack_bit = ntohl(sequenceNum);

					CSafeBinaryPacketPtr pkt = CSafeBinaryPacketPtr ( 
						new CSafeBinaryPacket( &pRecBuff[dataOffset], packetSize - dataOffset, sequenceNum, ack, ack_bit) );

					m_inList.push_back(pkt);

					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;
				}
				break;
			default:
				assert( false && ieS("Unkown packet type") );
				break;

			} // switch

		} // if newData >= packetSize

	} // while newData > (sizeof(u_short) * 2)

	m_recvBuf.clear();

}


void CUDPSocket::HandleOutput()
{
	bool sent = false;
	int errorFlag = 0;

	do
	{
		PacketList::iterator itor = m_outList.begin();
		IPacketPtr pIPacket = *itor;
		const char* pData = pIPacket->GetData();
		int length = (int)pIPacket->GetSize();
		
		// For message-oriented sockets (address family of AF_INET or AF_INET6,
		// type of SOCK_DGRAM, and protocol of IPPROTO_UDP, for example), care
		// must be taken not to exceed the maximum packet size of the underlying provider
		int optVal = 0;
		int optLen = sizeof(int);
		if ( getsockopt(m_socket, SOL_SOCKET, SO_MAX_MSG_SIZE, (char*)&optVal, &optLen) == SOCKET_ERROR )
		{
			Cout << ieS("Problem with determining max message size of the provider") << std::endl;
			NETPrintError();
			return;
		}
		if (length > optVal)
		{
			Cout << ieS("Packet is bigger than the SO_MAX_MSG_SIZE of the socket") << std::endl;
			return;
		}

		int result = send(m_socket, pData+m_sendOfs, length-m_sendOfs, 0);
		if (result == SOCKET_ERROR)
		{
			Cout << ieS("send() SOCKET_ERROR failed with error: ") << WSAGetLastError() << std::endl;
			NETPrintError();
			SetDeleteFlag(DF_DESTROY, true);
			sent = false;
		}
		else if (result >= 0)
		{
			m_sendOfs += result; // bytes sent, increase the offset.
			sent = true;
		}
		else
		{
			sent = false;
		}

		NETPrintError();

		// if the amount of sent data has reace the same length of that of the packet
		// size, then pop the top packet and send the next one.
		if ( m_sendOfs == length )
		{		
			m_outList.pop_front();
			m_sendOfs = 0; // reset the sent offset
		}

	} while( sent && !m_outList.empty() );


}