#ifndef CTCP_SOCKET_H
#define CTCP_SOCKET_H

#include "Socket.h"

namespace Engine
{

	// Maintains an open socket as well as the ipaddress to that connection/
	// Furthermore it contains the packets for sending and recieving.
	// This also acts as a client socket if used in this form
	class CTCPSocket : public CSocket
	{
	public:
		CTCPSocket();
		CTCPSocket(SOCKET newsocket);
		virtual ~CTCPSocket();

		bool Connect(const String& ip, const String& port, bool coalesce = true);
		bool Listen(const String& port);
		SOCKET AcceptConnection(String& address, String& port);

		virtual void Send(IPacketPtr pIPacket, bool clearTimeOut);

		virtual void HandleInput(IEventManagerPtr pEevntMgr);
		virtual void HandleOutput();
		
	protected:
		int m_recvBegin;
		int m_recvOfs;

		
	private:
		DECLARE_HEAP;

	};



}




#endif
