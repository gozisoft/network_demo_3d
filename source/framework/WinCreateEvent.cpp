#include "frameafx.h"
#include "WinCreateEvent.h"
#include "StringFunctions.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(SWindowCreateEvent, "SWindowCreateEvent", "IEventData");

const SWindowCreateEvent::EventType SWindowCreateEvent::sk_EventType("SWindowCreateEvent");