#include "Frameafx.h"
#include "TriangleStrip.h"
#include "VertexBuffer.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CTriStrip, "CTriStrip", "CTriangles");

CTriStrip::CTriStrip(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, IndexType indextType) : 
CTriangles( PT_TRISTRIP, pVFormat, pVBuffer, CIndexBufferPtr() )
{
	UInt PosCount = pVBuffer->GetNumElements();
	m_pIBuffer = CIndexBufferPtr( new CIndexBuffer(PosCount, indextType) );
	m_pIBuffer->SetIndexBuffer(PosCount);
}


CTriStrip::CTriStrip(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer, CIndexBufferPtr pIBuffer) : 
CTriangles(PT_TRISTRIP, pVFormat, pVBuffer, pIBuffer)
{

}

CTriStrip::CTriStrip() : CTriangles()
{

}

CTriStrip::~CTriStrip()
{


}

bool CTriStrip::GetTriangleIndex(UInt pos, UInt32 &i0, UInt32 &i1, UInt32 &i2) const
{
	if (pos >= 0 && pos < GetTriangleCount())
	{
		switch (m_pIBuffer->GetType())
		{
		case IT_USHORT:
			{
				const UShort *pIBuffer = reinterpret_cast<const UShort*>( m_pIBuffer->GetData() );
				i0 = pIBuffer[pos];
				if (pos & 1) // only look at odd numbers
				{
					i1 = pIBuffer[pos + 2];
					i2 = pIBuffer[pos + 1];
				}
				else
				{
					i1 = pIBuffer[pos + 1];
					i2 = pIBuffer[pos + 2];
				}

				// Degenerate triangles are assumed to have been added for swaps and
				// turns in the triangle strip.
				return (i0 != i1 && i0 != i2 && i1 != i2);
			}
			break;

		case IT_UINT:
			{
				const UInt32 *pIBuffer = reinterpret_cast<const UInt32*>( m_pIBuffer->GetData() );
				i0 = pIBuffer[pos];
				if (pos & 1) // only look at odd numbers
				{
					i1 = pIBuffer[pos + 2];
					i2 = pIBuffer[pos + 1];
				}
				else
				{
					i1 = pIBuffer[pos + 1];
					i2 = pIBuffer[pos + 2];
				}

				// Degenerate triangles are assumed to have been added for swaps and
				// turns in the triangle strip.
				return (i0 != i1 && i0 != i2 && i1 != i2);
			}
			break;
		};
	}

	return false;
}
