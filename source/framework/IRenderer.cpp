#include "Frameafx.h"
#include "IRenderer.h"
#include "RendererGL.h"

using namespace Engine;

DEFINE_HEAP(IRenderer, "IRenderer");

IRendererPtr IRenderer::Create(RendererType type)
{
	switch (type)
	{
	case RT_OPENGL:
		{
			CRendererGLPtr pRenderer = CRendererGLPtr( new CRendererGL );
			return static_pointer_cast<IRenderer>(pRenderer);
		}
	}

	return IRendererPtr();
}



