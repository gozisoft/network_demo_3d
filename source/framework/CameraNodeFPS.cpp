#include "Frameafx.h"
#include "CameraNodeFPS.h"
#include "CameraControllerFPS.h"
#include "Camera.h"
#include "IEventReciever.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CCameraNodeFPS, "CCameraNodeFPS", "CRootNode");


CCameraNodeFPS::CCameraNodeFPS(CCameraPtr pCamera) : 
CRootNode(),
m_target(0,0,100),
m_pCamera(pCamera)
{
	if (m_pCamera)
	{
		m_up = m_pCamera->GetUVector();
		m_target = m_pCamera->GetFVector();
		SetPosition( m_pCamera->GetPosition() );

		Update();
	}
}

CCameraNodeFPS::~CCameraNodeFPS()
{


}

void CCameraNodeFPS::SetCamera(CCameraPtr pCamera)
{
	m_pCamera = pCamera;

	if (m_pCamera)
	{
		m_up = m_pCamera->GetUVector();
		m_target = m_pCamera->GetFVector();
		SetPosition( m_pCamera->GetPosition() );

		Update();
	}
}

void CCameraNodeFPS::UpdateWorldData(double time)
{
	CRootNode::UpdateWorldData(time);

	if (m_pCamera)
	{
		Vector3f pos = m_world.GetTranslation(); // get updated position
		Vector3f lookat = m_target - pos;
		lookat.Normalise();

		Vector3f up = m_up;
		up.Normalise();

		float dp = Dot(lookat, up);

		if ( IsEqual( Abs(dp), 1.0f ) )
			up.x() += 0.5f;

		m_pCamera->SetCamera(pos, m_up, m_target);
	}
}

bool CCameraNodeFPS::OnEvent(const IEventData& event)
{
	bool absorbed = false;

	if ( !m_animators.empty() )
	{
		ISceneControllerVector::iterator itor = m_animators.begin();
		for (/**/; itor != m_animators.end(); ++itor)
		{
			absorbed = (*itor)->HandleEvent(event);
		}
	}

	return absorbed;
}

void CCameraNodeFPS::SetTarget(const Vector3f& target)
{
	m_target = target;

	Vector3f toTarget = m_target - m_world.GetTranslation();
	SetRotation( toTarget.GetHorizontalAngle() );
}