#ifndef CCAMERA_H
#define CCAMERA_H

#include "Vector.h"
#include "Matrix.h"

namespace Engine
{

// matrices (view), (proj), (ortho) :: (left handed coords)
// from http://www.codeguru.com/cpp/misc/misc/math/article.php/c10123__3/Deriving-Projection-Matrices.htm

class CCamera
{
public:
	// Access the projection matrices of the camera.  The projection matrix
	// that maps to depths [0,1] is what Direct3D uses.  The view matrix that
	// maps to depths [-1,1] is what OpenGL uses.
	enum DepthType
	{
		DT_ZERO_TO_ONE,       // [0,1]
		DT_MINUS_ONE_TO_ONE,  // [-1,1]
		NUM_DEPTH_TYPES
	};

	enum Side 
	{
		S_NEAR,
		S_FAR,
		S_BOTTOM,
		S_TOP,
		S_LEFT,
		S_RIGHT,
		NUM_PLANES
	};

	CCamera();

	~CCamera();

	// This function is like using D3DXMatrixLookAtLH
	void SetCamera(const Vector3f& pos, const Vector3f& upVector, 
		const Vector3f& lookat);

	// The camera frame is always in world coordinates.
	//   default position  P = (0, 0,  0; 1)
	//   default direction D = (0, 0, -1; 0)
	//   default up        U = (0, 1,  0; 0)
	//   default right     R = (1, 0,  0; 0)
	void SetFrame(const Vector3f& pos, const Vector3f& upVector, 
		const Vector3f& rightVector, const Vector3f& lookVector);

	void SetPosition(const Vector3f& pos);

	void SetAxis(const Vector3f& upVector, const Vector3f& rightVector,
		const Vector3f& lookVector);

	// Same as gluPerspective
	void SetFrustum(float upFovDegrees, float aspectRatio, float zNear, float zFar);

	// Self contained update method
	void Update();

	// ---------------
	// Camera control 
	// ---------------
	// Move the camera forwards and backwards
	void MoveForward(float units);

	// Move the camera across left and right
	void Strafe(float units);

	// Move camera up and down
	void MoveUp(float units);

	// look left and right
	void Yaw(float rads);

	// look up and down
	void Pitch(float rads);

	// roll like a barrell
	void Roll(float rads);

	// -------------
	// Accessors
	// -------------
	Matrix4f GetViewMatrix() const;
	Matrix4f GetProjectionMatrix() const;
	Matrix4f GetProjViewMatrix() const; // combination of proj*view

	const float* GetFrustum() const;
	DepthType GetDepthType() const;

	// Camera Coordinate Vectors
	Vector3f GetPosition() const;
	Vector3f GetUVector() const; // up vector
	Vector3f GetRVector() const; // right vector
	Vector3f GetFVector() const; // forward(lookat) vector


private:
	void BuildViewMatrix();
	void OnFrustrumChange();
	void UpdatePVMatrix();

	// Frustrum
	float m_frustum[NUM_PLANES];

	// Cameras look coordinates
	Vector3f m_yaxis;		// up
	Vector3f m_xaxis;		// right
	Vector3f m_zaxis;		// look

	// camera look at vector
	Vector3f m_lookat;

	// Cameras position
	Vector3f m_pos;

	// Cameras speed
	Vector3f m_velocity;
	float m_maxVelocity;

	float m_pitch, m_yaw; // azimoth
	float m_maxPitch;

	// Matrices
	Matrix4f m_viewMatrix; // V matrix
	Matrix4f m_projectionMatrix[NUM_DEPTH_TYPES]; // P matrix
	Matrix4f m_projectionViewMatrix[NUM_DEPTH_TYPES]; // V * P Matrix

	DepthType m_depthType;

	static DepthType m_defaultDepthType;

	DECLARE_HEAP;
};


#include "Camera.inl"

}


#endif