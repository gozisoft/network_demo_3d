#include "Frameafx.h"
#include "RendererD3D9.h"
#include "RenderDataD3D9.h"
#include "VertexBufferD3D9.h"
#include "VertexFormatD3D9.h"
#include "IndexBufferD3D9.h"
#include "MappingD3D9.h"

// scenegraph
#include "Camera.h"
#include "Triangle.h"

// core
#include "Helper.h"

// Direct3D 9

#include <d3dx9.h> // Includes the D3DX maths library and tools
#include <dxerr.h> // HRESULT translation for Direct3D and other APIs 

namespace Engine
{

IRendererPtr CreateDirect3D9Renderer()
{
	IRendererPtr pRenderer = IRendererPtr( new CRendererD3D9() );

	return pRenderer;
}

}

using namespace Engine;

CRendererD3D9::CRendererD3D9()
{
	m_windowed = true;
	m_vsync = true;
	m_stencilBuffer = true;
	m_backBuffer = true;
	m_bits = 32;
	m_antiAlias = 4;
	m_clearDepth = 1.0f;
	m_clearStencil = 0;

	m_clearColor = Colour4f::WHITE; //Colour4f(0.1, 0.1, 0.5, 0.0);

	m_pDefaultAlphaState = 0;
	m_pDefaultCullState = 0;
	m_pDefaultDepthState = 0;
	m_pDefaultOffsetState = 0;
	m_pDefaultStencilState = 0;
	m_pDefaultwireState = 0;
}

CRendererD3D9::~CRendererD3D9()
{
	Release();
}

bool CRendererD3D9::Initialise(HWND hwnd, HINSTANCE hinst, bool windowed)
{
	// Create the render data container
	m_pData = CRenderDataD3D9Ptr( new CRenderDataD3D9() );

	// Store window properties within the render data class
	m_pData->m_hWnd = hwnd;
	m_pData->m_winInst = hinst;
	m_windowed = windowed;

	if (!m_pData->m_pID3D)
	{
		m_pData->m_d3dInst = LoadLibrary( __TEXT("d3d9.dll") );

		if (!m_pData->m_d3dInst)
		{
			assert( false && ieS("Error, could not load d3d9.dll.\n") );
			return false;
		}

		typedef IDirect3D9 * (__stdcall *D3DCREATETYPE)(UINT);
		D3DCREATETYPE d3dCreate = (D3DCREATETYPE) ::GetProcAddress(m_pData->m_d3dInst, "Direct3DCreate9");

		if (!d3dCreate)
		{
			assert( false && ieS("Error, could not get proc adress of Direct3DCreate9.\n") );
			return false;
		}

		//just like pID3D = Direct3DCreate9(D3D_SDK_VERSION);
		m_pData->m_pID3D = (*d3dCreate)(D3D_SDK_VERSION);

		if (!m_pData->m_pID3D)
		{
			assert( false && ieS("Error initializing D3D.\n") );
			return false;
		}
	}

	HRESULT hr = S_OK;

	D3DDISPLAYMODE d3ddm;
	hr = m_pData->m_pID3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);
	if (FAILED(hr))
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Error: Could not get Adapter Display mode.") && msg.c_str() );
		return false;
	}

	SecureZeroMemory( &m_pData->m_present, sizeof(D3DPRESENT_PARAMETERS) );

	m_pData->m_present.BackBufferCount = 1;
	m_pData->m_present.hDeviceWindow = m_pData->m_hWnd;
	m_pData->m_present.EnableAutoDepthStencil = TRUE;
	if (m_vsync)
		m_pData->m_present.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
	else
		m_pData->m_present.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
	
	if (m_windowed)
	{
		m_pData->m_present.BackBufferFormat	= d3ddm.Format;
		m_pData->m_present.SwapEffect		= D3DSWAPEFFECT_DISCARD;
		m_pData->m_present.Windowed			= TRUE;
	}
	else
	{
		RECT rcClient = { 0, 0, 0, 0 };
		GetClientRect(m_pData->m_hWnd, &rcClient);

		const LONG realWidth = rcClient.right - rcClient.left;
		const LONG realHeight = rcClient.bottom - rcClient.top;

		m_pData->m_present.BackBufferWidth = realWidth;
		m_pData->m_present.BackBufferHeight = realHeight;

		// request 32bit mode if user specified 32 bit, added by Thomas Stuefe
		if (m_bits == 32)
			m_pData->m_present.BackBufferFormat = D3DFMT_X8R8G8B8;
		else
			m_pData->m_present.BackBufferFormat = D3DFMT_R5G6B5;

		m_pData->m_present.SwapEffect = D3DSWAPEFFECT_FLIP;
		m_pData->m_present.Windowed = FALSE;
		m_pData->m_present.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	}

	UINT adapter = D3DADAPTER_DEFAULT;
	D3DDEVTYPE devtype = D3DDEVTYPE_HAL;

	if (m_antiAlias > 0)
	{
		if(m_antiAlias > 16)
			m_antiAlias = 16;

		DWORD qualityLevels = 0;

		while(m_antiAlias > 0)
		{
			HRESULT hres = S_OK;

			hres = m_pData->m_pID3D->CheckDeviceMultiSampleType(adapter, devtype, m_pData->m_present.BackBufferFormat, m_windowed,
				(D3DMULTISAMPLE_TYPE)m_antiAlias, &qualityLevels);

			if ( SUCCEEDED(hres) )
			{
				m_pData->m_present.MultiSampleType = (D3DMULTISAMPLE_TYPE)m_antiAlias;
				m_pData->m_present.MultiSampleQuality = qualityLevels - 1;
				m_pData->m_present.SwapEffect = D3DSWAPEFFECT_DISCARD;
				break;
			}
			--m_antiAlias;
		}

		if (m_antiAlias == 0)
		{
			assert( false && ieS("Anti aliasing disabled because hardware/driver lacks necessary caps.\n") );
		}
	}

	if (m_stencilBuffer)
	{
		HRESULT hres = S_OK;

		m_pData->m_present.AutoDepthStencilFormat = D3DFMT_D24S8;

		hres = m_pData->m_pID3D->CheckDeviceFormat(adapter, devtype,	m_pData->m_present.BackBufferFormat, D3DUSAGE_DEPTHSTENCIL,
			D3DRTYPE_SURFACE, m_pData->m_present.AutoDepthStencilFormat);
				
		if ( FAILED(hres) )
		{
			m_pData->m_present.AutoDepthStencilFormat = D3DFMT_D24X4S4;

			hres = m_pData->m_pID3D->CheckDeviceFormat(adapter, devtype,	m_pData->m_present.BackBufferFormat, D3DUSAGE_DEPTHSTENCIL,
				D3DRTYPE_SURFACE, m_pData->m_present.AutoDepthStencilFormat);

			if ( FAILED(hres) )
			{
				m_pData->m_present.AutoDepthStencilFormat = D3DFMT_D15S1;

				hres = m_pData->m_pID3D->CheckDeviceFormat(adapter, devtype, m_pData->m_present.BackBufferFormat, D3DUSAGE_DEPTHSTENCIL,
					D3DRTYPE_SURFACE, m_pData->m_present.AutoDepthStencilFormat);

				if( FAILED(hres) )
				{
					assert( false && ieS("Device does not support stencilbuffer, disabling stencil buffer.") );
					m_stencilBuffer = false;
				}
			}
		}
		else
		{
			HRESULT hres = S_OK;

			hres = m_pData->m_pID3D->CheckDepthStencilMatch(adapter, devtype, m_pData->m_present.BackBufferFormat, 
				m_pData->m_present.BackBufferFormat, m_pData->m_present.AutoDepthStencilFormat);

			if( FAILED(hres) )
			{
				assert( false && ieS("Depth-stencil format is not compatible with display format, disabling stencil buffer.") );
				m_stencilBuffer = false;
			}
		}
	}
	// do not use else here to cope with flag change in previous block
	if (!m_stencilBuffer)
	{
		m_pData->m_present.AutoDepthStencilFormat = D3DFMT_D32;
		if ( FAILED( m_pData->m_pID3D->CheckDeviceFormat(adapter, devtype, m_pData->m_present.BackBufferFormat, D3DUSAGE_DEPTHSTENCIL,
			D3DRTYPE_SURFACE, m_pData->m_present.AutoDepthStencilFormat) ) )
		{
			m_pData->m_present.AutoDepthStencilFormat = D3DFMT_D24X8;
			if ( FAILED( m_pData->m_pID3D->CheckDeviceFormat(adapter, devtype, m_pData->m_present.BackBufferFormat, D3DUSAGE_DEPTHSTENCIL,
				D3DRTYPE_SURFACE, m_pData->m_present.AutoDepthStencilFormat) ) )
			{
				m_pData->m_present.AutoDepthStencilFormat = D3DFMT_D16;
				if ( FAILED( m_pData->m_pID3D->CheckDeviceFormat(adapter, devtype, m_pData->m_present.BackBufferFormat, D3DUSAGE_DEPTHSTENCIL,
					D3DRTYPE_SURFACE, m_pData->m_present.AutoDepthStencilFormat) ) )
				{
					assert( false && ieS("Device does not support required depth buffer.") );
					return false;
				}
			}
		}
	}

	// create device
	const LONG devTypeVector[] =
	{
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		D3DCREATE_MIXED_VERTEXPROCESSING,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
	};

	const UInt32 devTypeVectorCount = sizeof(devTypeVector) / sizeof(devTypeVector[0]);

	for (UInt32 i = 0; i < devTypeVectorCount; ++i)
	{
		Long DriverType = devTypeVector[i]; // D3DCREATE_FPU_PRESERVE |

		hr = m_pData->m_pID3D->CreateDevice( adapter, devtype, m_pData->m_hWnd,
				 DriverType, &m_pData->m_present, &m_pData->m_pID3DDevice );

		if ( SUCCEEDED(hr) )
			break;	
	}

	if ( FAILED(hr) || !m_pData->m_pID3DDevice )
	{
		String msg = DXGetErrorString(hr);
		assert( false && ieS("Could not create device") && msg.c_str() );
		return false;
	}

	// get caps
	m_pData->m_pID3DDevice->GetDeviceCaps(&m_pData->m_caps);

	// disable stencilbuffer if necessary
	if ( m_stencilBuffer &&
		( !(m_pData->m_caps.StencilCaps & D3DSTENCILCAPS_DECRSAT) ||
		!(m_pData->m_caps.StencilCaps & D3DSTENCILCAPS_INCRSAT) ||
		!(m_pData->m_caps.StencilCaps & D3DSTENCILCAPS_KEEP) ) )
	{
		assert( false && ieS("Device not able to use stencil buffer, disabling stencil buffer. \n") );
		m_stencilBuffer = false;
	}

	IntialiseSubSystems();

	return true;
}

void CRendererD3D9::Release()
{
	m_indexBuffers.clear();
	m_vertexBuffers.clear();
	m_vertexFormats.clear();

	SafeDelete(m_pDefaultwireState);
	SafeDelete(m_pDefaultStencilState);
	SafeDelete(m_pDefaultOffsetState);
	SafeDelete(m_pDefaultDepthState);
	SafeDelete(m_pDefaultCullState);
	SafeDelete(m_pDefaultAlphaState);

	SafeRelease(m_pData->m_pID3DDevice);
	SafeRelease(m_pData->m_pID3D);
	FreeLibrary(m_pData->m_d3dInst);
}

void CRendererD3D9::IntialiseSubSystems()
{
	m_pDefaultAlphaState = new CAlphaState();
	m_pDefaultCullState = new CCullState();
	m_pDefaultDepthState = new CDepthState();
	m_pDefaultOffsetState = new COffsetState();
	m_pDefaultStencilState = new CStencilState();
	m_pDefaultwireState	= new CWireState();

	m_pData->m_currentState.Initialise(m_pData->m_pID3DDevice, m_pDefaultAlphaState,
		m_pDefaultCullState, m_pDefaultDepthState, m_pDefaultOffsetState,
		m_pDefaultStencilState, m_pDefaultwireState);
}



//----------------------------------------------------------------------------
// Rendering TODO : iterate over the shaders, set approriate shaders then draw that primitive
//----------------------------------------------------------------------------
bool CRendererD3D9::PreDraw()
{
    HRESULT hr = m_pData->m_pID3DDevice->TestCooperativeLevel();
    if (hr == D3DERR_DEVICELOST)
    {
      //  if (!m_pData->m_pID3DDevice->mDeviceLost)
       // {
            // This is the first time we noticed that the device was lost.
      //      mData->mDeviceLost = true;
      //      mData->mFont->OnLostDevice();
      //  }
        return false;
    }

	hr = m_pData->m_pID3DDevice->BeginScene();
	if (hr == D3D_OK)
	{
		return true;
	}

	assert( false && ieS("BeginScene failed: %s\n") && DXGetErrorString(hr) );
	return false;
}

void CRendererD3D9::PostDraw()
{
	HRESULT hr = m_pData->m_pID3DDevice->EndScene();
    UNREFERENCED_PARAMETER(hr);
    assert( hr == D3D_OK && ieS("EndScene failed: %s\n") && DXGetErrorString(hr) );
}


// This function draws a scenenode. It retrieves its geometry
// and sets the appropriate states within the render API in order
// to render the object.
void CRendererD3D9::Draw(CSceneNodePtr pNode)
{
	CIndexBufferPtr pIBuffer = pNode->GetIndexBuffer();
	CVertexBufferPtr pVBuffer = pNode->GetVertexBuffer();
	CVertexFormatPtr pVFormat = pNode->GetVertexFormat();

	// Enable geometry
	Enable( pVBuffer ); // vertex buffer
	Enable( pVFormat ); // vertex format
	if (pIBuffer)
	{
		Enable( pIBuffer ); // index buffer
	}
	
    // Set up world matrix
	Matrix4f world = pNode->GetWorldTransform();
	m_pData->m_pID3DDevice->SetTransform( D3DTS_WORLD, (D3DMATRIX*)&world[0] );

	// Set up view matrix
	Matrix4f view = m_pCamera->GetViewMatrix();
	m_pData->m_pID3DDevice->SetTransform( D3DTS_VIEW, (D3DMATRIX*)&view[0] );

	// Set up projection matrix
	Matrix4f proj = m_pCamera->GetProjectionMatrix();
	m_pData->m_pID3DDevice->SetTransform( D3DTS_PROJECTION, (D3DMATRIX*)&proj[0] ); 

	// Draw the node
	DrawPrimitive(pNode);

	// Disable geometry
	if (pIBuffer)
	{
		Disable( pIBuffer );
	}
	Disable( pVFormat );
	Disable( pVBuffer );
}
//----------------------------------------------------------------------------
void CRendererD3D9::DrawPrimitive(CSceneNodePtr pNode)
{
	CSceneNode::PrimitiveType type = pNode->GetType();
	CVertexBufferPtr pVBuffer = pNode->GetVertexBuffer();
	CVertexFormatPtr pVFormat = pNode->GetVertexFormat();
	CIndexBufferPtr pIBuffer = pNode->GetIndexBuffer();

	UINT numPrimatives = 0;

	switch (type)
	{
	case CSceneNode::PT_TRISTRIP:
	case CSceneNode::PT_TRIMESH:
	case CSceneNode::PT_TRIFAN:
		{
			UINT numVerts = (UINT)pVBuffer->GetNumElements();
			CTrianglesPtr pTriangle = dynamic_pointer_cast<CTriangles>(pNode);
			numPrimatives = pTriangle->GetTriangleCount();

			if (numVerts && numPrimatives)
			{
				// UINT indicesOffset = pIBuffer->GetBytesStride();

				m_pData->m_pID3DDevice->DrawIndexedPrimitive(D3D9PrimitiveType[type], 0, 0,
					numVerts, 0, numPrimatives);
			}
		}
		break;

	default:
		assert( false && ieS("Invalid type") );
		break;
	} // switch

}


//----------------------------------------------------------------------------
// Frame Control
//----------------------------------------------------------------------------
void CRendererD3D9::SetViewport(const Viewporti& view)
{
	D3DVIEWPORT9 viewport;
	HRESULT hr = m_pData->m_pID3DDevice->GetViewport(&viewport);

	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && msg.c_str() );
	}
	
	viewport.X = (DWORD)view.left();
	viewport.Y = (DWORD)view.top();
    viewport.Width = (DWORD)view.right();
	viewport.Height = (DWORD)view.bottom();
	viewport.MinZ = 0.0f;
	viewport.MaxZ = 1.0f;
    hr = m_pData->m_pID3DDevice->SetViewport(&viewport);
	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && ieS("Failed to set new viewport \n") && msg.c_str() );
	}
}

void CRendererD3D9::Resize(Int32 width, Int32 height)
{
	D3DVIEWPORT9 viewport;
	HRESULT hr = m_pData->m_pID3DDevice->GetViewport(&viewport);

	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && msg.c_str() );
	}

	viewport.Width = (DWORD)width;
	viewport.Height = (DWORD)height;

    hr = m_pData->m_pID3DDevice->SetViewport(&viewport);
	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && ieS("Failed to set new viewport \n") && msg.c_str() );
	}

}

Viewporti CRendererD3D9::GetViewport() const
{
	D3DVIEWPORT9 viewport;
	HRESULT hr = m_pData->m_pID3DDevice->GetViewport(&viewport);

	if ( FAILED(hr) )
	{
		String msg = DXGetErrorDescription(hr);
		assert( false && msg.c_str() );
	}

	Viewporti view( (int)viewport.X, (int)viewport.Y, (int)viewport.Width, (int)viewport.Height );
	return view;
}

// ----------------------------------------------------------------------------
// Support for clearing the color, depth, and stencil buffers.
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearColorBuffer()
{
    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	HRESULT hr = m_pData->m_pID3DDevice->Clear(0, 0, D3DCLEAR_TARGET,
		clearColor, 1.0f, 0);

    assert( hr == D3D_OK && ieS("Clear failed: %s\n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearDepthBuffer()
{
    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	HRESULT hr = m_pData->m_pID3DDevice->Clear(0, 0, D3DCLEAR_ZBUFFER,
		clearColor, 1.0f, 0);

    assert( hr == D3D_OK && ieS("Clear failed: %s\n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearStencilBuffer()
{
    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	HRESULT hr = m_pData->m_pID3DDevice->Clear(0, 0, D3DCLEAR_STENCIL,
		clearColor, 1.0f, 0);

    assert( hr == D3D_OK && ieS("Clear failed: %s\n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearBuffers ()
{
    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	DWORD flags = 0;

	if (m_backBuffer)
		flags |= D3DCLEAR_TARGET;

	// if (zBuffer)
		flags |= D3DCLEAR_ZBUFFER;

	if (m_stencilBuffer)
		flags |= D3DCLEAR_STENCIL;

    HRESULT hr = m_pData->m_pID3DDevice->Clear(0, 0, flags,
        clearColor, m_clearDepth, (DWORD)m_clearStencil);

    assert( hr == D3D_OK && ieS("Clear failed: \n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearColorBuffer (Int32 x, Int32 y, Int32 w, Int32 h)
{
    D3DRECT rect;
    rect.x1 = (long)x;
    rect.y1 = (long)y;
    rect.x2 = (long)(x + w - 1);
    rect.y2 = (long)(y + h - 1);

    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	HRESULT hr = m_pData->m_pID3DDevice->Clear(1, &rect, D3DCLEAR_TARGET,
		clearColor, 1.0f, 0);

    assert( hr == D3D_OK && ieS("Clear failed: %s\n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearDepthBuffer (Int32 x, Int32 y, Int32 w, Int32 h)
{
    D3DRECT rect;
    rect.x1 = (long)x;
    rect.y1 = (long)y;
    rect.x2 = (long)(x + w - 1);
    rect.y2 = (long)(y + h - 1);

    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	HRESULT hr = m_pData->m_pID3DDevice->Clear( 1, &rect, D3DCLEAR_ZBUFFER,
		clearColor, 1.0f, static_cast<DWORD>(m_clearDepth) );

    assert( hr == D3D_OK && ieS("Clear failed: %s\n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearStencilBuffer (Int32 x, Int32 y, Int32 w, Int32 h)
{
    D3DRECT rect;
    rect.x1 = (long)x;
    rect.y1 = (long)y;
    rect.x2 = (long)(x + w - 1);
    rect.y2 = (long)(y + h - 1);

    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	HRESULT hr = m_pData->m_pID3DDevice->Clear(1, &rect, D3DCLEAR_STENCIL,
		clearColor, 1.0f, static_cast<DWORD>(m_clearStencil) );

    assert( hr == D3D_OK && ieS("Clear failed: %s\n") && DXGetErrorString(hr) );
}
// ----------------------------------------------------------------------------
void CRendererD3D9::ClearBuffers (Int32 x, Int32 y, Int32 w, Int32 h)
{
	D3DRECT rect;
    rect.x1 = (long)x;
    rect.y1 = (long)y;
    rect.x2 = (long)(x + w - 1);
    rect.y2 = (long)(y + h - 1);

    DWORD clearColor = D3DCOLOR_COLORVALUE(m_clearColor[0], m_clearColor[1],
        m_clearColor[2], m_clearColor[3]);

	DWORD flags = 0;

	if (m_backBuffer)
		flags |= D3DCLEAR_TARGET;

	// if (zBuffer)
		flags |= D3DCLEAR_ZBUFFER;

	if (m_stencilBuffer)
		flags |= D3DCLEAR_STENCIL;

    HRESULT hr = m_pData->m_pID3DDevice->Clear(1, &rect, flags,
        clearColor, m_clearDepth, (DWORD)m_clearStencil);

    assert( hr == D3D_OK && ieS("Clear failed: \n") && DXGetErrorString(hr) );
}
//----------------------------------------------------------------------------
void CRendererD3D9::DisplayColorBuffer()
{
    HRESULT hr = m_pData->m_pID3DDevice->Present(0, 0, 0, 0);
    if (hr != D3DERR_DEVICELOST)
    {
        assert( hr == D3D_OK && ieS("Present failed: \n") && DXGetErrorString(hr) );
    }
}

// -------------------------------------------------------------------------------------------
// VertexBuffer
// -------------------------------------------------------------------------------------------
void CRendererD3D9::Bind(CVertexBufferPtr pVBuffer)
{
	if ( m_vertexBuffers.find(pVBuffer) == m_vertexBuffers.end() )
	{
		m_vertexBuffers[pVBuffer] = CVertexBufferD3D9Ptr( new CVertexBufferD3D9( m_pData->m_pID3DDevice, pVBuffer ) );
	}
}
//----------------------------------------------------------------------------
void CRendererD3D9::Unbind(CVertexBufferPtr pVBuffer)
{
	VertexBufferMap::iterator itor = m_vertexBuffers.find(pVBuffer);
	if ( itor != m_vertexBuffers.end() )
	{
		CVertexBufferD3D9Ptr pVBufferD3D9 = itor->second;
		m_vertexBuffers.erase(itor);
	}
}
//----------------------------------------------------------------------------
void CRendererD3D9::Enable(CVertexBufferPtr pVBuffer, UInt streamIndex, UInt offset)
{
	// search to find if vertex buffer is active
	VertexBufferMap::iterator itor = m_vertexBuffers.find( pVBuffer );
	CVertexBufferD3D9Ptr pVBufferD3D9 = CVertexBufferD3D9Ptr();
	if ( itor != m_vertexBuffers.end() )
	{
		pVBufferD3D9 = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVBufferD3D9 = CVertexBufferD3D9Ptr( new CVertexBufferD3D9( m_pData->m_pID3DDevice, pVBuffer ) );
		m_vertexBuffers[pVBuffer] = pVBufferD3D9;
	}

	pVBufferD3D9->Enable( m_pData->m_pID3DDevice, pVBuffer->GetBytesStride(), streamIndex, offset );
}

void CRendererD3D9::Disable(CVertexBufferPtr pVBuffer, UInt streamIndex)
{
	// search to find if vertex buffer is active
	VertexBufferMap::iterator itor = m_vertexBuffers.find( pVBuffer );
	if ( itor != m_vertexBuffers.end() )
	{
		CVertexBufferD3D9Ptr pVBufferD3D9 = (*itor).second;
		pVBufferD3D9->Disable( m_pData->m_pID3DDevice, streamIndex );
	}
}

// -------------------------------------------------------------------------------------------
// VertexFormat
// -------------------------------------------------------------------------------------------
void CRendererD3D9::Bind(CVertexFormatPtr pVFormat)
{
	if ( m_vertexFormats.find(pVFormat) == m_vertexFormats.end() )
	{
		m_vertexFormats[pVFormat] = CVertexFormatD3D9Ptr( new CVertexFormatD3D9( m_pData->m_pID3DDevice, pVFormat ) );
	}
}
//----------------------------------------------------------------------------
void CRendererD3D9::Unbind(CVertexFormatPtr pVFormat)
{
	VertexFormatMap::iterator itor = m_vertexFormats.find(pVFormat);
	if ( itor != m_vertexFormats.end() )
	{
		CVertexFormatD3D9Ptr pVBufferD3D9 = itor->second;
		m_vertexFormats.erase(itor);
	}
}

void CRendererD3D9::Enable(CVertexFormatPtr pVFormat)
{
	// search to find if vertex buffer is active
	VertexFormatMap::iterator itor = m_vertexFormats.find(pVFormat);
	CVertexFormatD3D9Ptr pVFormatD3D9 = CVertexFormatD3D9Ptr();
	if ( itor != m_vertexFormats.end() )
	{
		pVFormatD3D9 = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pVFormatD3D9 = CVertexFormatD3D9Ptr( new CVertexFormatD3D9( m_pData->m_pID3DDevice, pVFormat ) );
		m_vertexFormats[pVFormat] = pVFormatD3D9;
	}

	pVFormatD3D9->Enable( m_pData->m_pID3DDevice );
}

void CRendererD3D9::Disable(CVertexFormatPtr pVFormat)
{
	VertexFormatMap::iterator itor = m_vertexFormats.find( pVFormat );
	if ( itor != m_vertexFormats.end() )
	{
		CVertexFormatD3D9Ptr pVFormatD3D9 = (*itor).second;
		pVFormatD3D9->Disable( m_pData->m_pID3DDevice );
	}
}

// -------------------------------------------------------------------------------------------
// IndexBuffer
// -------------------------------------------------------------------------------------------
void CRendererD3D9::Bind(CIndexBufferPtr pIBuffer)
{
	if ( m_indexBuffers.find(pIBuffer) == m_indexBuffers.end() )
	{
		m_indexBuffers[pIBuffer] = CIndexBufferD3D9Ptr( new CIndexBufferD3D9( m_pData->m_pID3DDevice, pIBuffer ) );
	}
}

void CRendererD3D9::Unbind(CIndexBufferPtr pIBuffer)
{
	CIndexBufferMap::iterator itor = m_indexBuffers.find(pIBuffer);
	if ( itor != m_indexBuffers.end() )
	{
		CIndexBufferD3D9Ptr pIBufferD3D9 = itor->second;
		m_indexBuffers.erase(itor);
	}
}

void CRendererD3D9::Enable(CIndexBufferPtr pIBuffer)
{
	CIndexBufferMap::iterator itor = m_indexBuffers.find(pIBuffer);
	CIndexBufferD3D9Ptr pIBufferD3D9 = CIndexBufferD3D9Ptr();
	if ( itor != m_indexBuffers.end() )
	{
		pIBufferD3D9 = (*itor).second;
	}
	else // if not found, lazy create it
	{
		pIBufferD3D9 = CIndexBufferD3D9Ptr( new CIndexBufferD3D9( m_pData->m_pID3DDevice, pIBuffer ) );
		m_indexBuffers[pIBuffer] = pIBufferD3D9;
	}

	pIBufferD3D9->Enable( m_pData->m_pID3DDevice );
}

void CRendererD3D9::Disable(CIndexBufferPtr pIBuffer)
{
	CIndexBufferMap::iterator itor = m_indexBuffers.find(pIBuffer);
	if ( itor != m_indexBuffers.end() )
	{
		CIndexBufferD3D9Ptr pIBufferD3D9 = (*itor).second;
		pIBufferD3D9->Disable( m_pData->m_pID3DDevice );
	}
}