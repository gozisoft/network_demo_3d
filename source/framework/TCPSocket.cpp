#include "frameafx.h"
#include "TCPSocket.h"
#include "Packet.h"
#include "Winnet.h"
#include "StringFunctions.h"

#include <WS2tcpip.h>

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CTCPSocket, "CTCPSocket", "CSocket");

CTCPSocket::CTCPSocket() : 
CSocket(),
m_recvOfs(0),
m_recvBegin(0)
{
 
}

CTCPSocket::CTCPSocket(SOCKET newsocket) : 
CSocket(newsocket),
m_recvOfs(0),
m_recvBegin(0)
{
	setsockopt(m_socket, SOL_SOCKET, SO_DONTLINGER, NULL, 0);

}

CTCPSocket::~CTCPSocket()
{
	m_recvBuf.clear();
}

bool CTCPSocket::Connect(const String& ip, const String& port, bool coalesce)
{
	// No Nagle Algorithim by default
	int iResult = TCP::NETConnectSock(m_socket, ip, port); 
	if (iResult != 0) 
	{
		Cout << ieS("TCP NETConnectSock failed: ") << iResult << std::endl;
		return false;
	}

	return true;
}

bool CTCPSocket::Listen(const String& port)
{
	int iResult = TCP::NETListenSock(m_socket, port); 
	if (iResult != 0) 
	{
		Cout << ieS("TCP NETListenSock failed: ") << iResult << std::endl;
		return false;
	}

	return true;
}

SOCKET CTCPSocket::AcceptConnection(String& address, String& port)
{
	return TCP::NETAcceptConnection(address, port, m_socket);
}

void CTCPSocket::Send(IPacketPtr pIPacket, bool clearTimeOut)
{
	if (clearTimeOut)
		m_timeOut = 0;

	m_outList.push_back(pIPacket);
}

void CTCPSocket::HandleInput(IEventManagerPtr pEevntMgr)
{
	UNREFERENCED_PARAMETER(pEevntMgr);

	int errorFlag = 0;

	if ( !m_recvBuf.capacity() )
	{
		m_recvBuf.resize(RECV_BUFFER_SIZE);
	}

	CharVector::pointer pRecBuff = &m_recvBuf[0];
	pRecBuff += m_recvBegin;
	pRecBuff += m_recvOfs;

	int result = recv(m_socket, pRecBuff, RECV_BUFFER_SIZE - m_recvOfs, 0);
	if (result == SOCKET_ERROR)
	{
		Cout << "recv() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
		NETPrintError();
		SetDeleteFlag(DF_DESTROY, true);
	}
	else if (result == 0)
	{
		Cout << ieS("Connection closed") << std::endl;
		errorFlag = 1;
	}

#if defined(DEBUG) | defined(_DEBUG)	
	OStringStream oss;
	oss << ieS("Incoming: ") << result << ieS(" bytes.") << ieS(" ");
	oss << ieS("Begin ") << m_recvBegin << ieS(" ");
	oss << ieS("Offset ") << m_recvOfs;
	Cout << oss.str() << std::endl;
#endif

	// Current size in buffer + next lot of bytes recieved
	int newData = m_recvOfs + result;
	int packetSize = 0;
	int processedData = 0;
	bool pktRecieved = false;

	while ( newData > (sizeof(u_short) * 2) )
	{
		CharVector::pointer pRead = &m_recvBuf[0];

		// extract packet size from data.
		packetSize = *(reinterpret_cast<u_short *>(pRead));
		packetSize = static_cast<int>( ntohs(packetSize) );

		// Don't have enough new data to grab the next packet
		if (packetSize > newData)
			break;

		if (packetSize > RECV_BUFFER_SIZE)
		{
			SetDeleteFlag(DF_DESTROY, true);
			return;
		}

		// we know how big the packet is...and we have the whole thing
		if (newData >= packetSize)
		{
			// Advance the pointer along 2 bytes to get the type
			u_short sType = ntohs( *(reinterpret_cast<u_short *>( pRead + sizeof(u_short) )) );
			IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

			const size_t dataOffset = sizeof(u_short) * 2;

			switch (type)
			{
			case IPacket::PT_BINARY:
				{
					// we know how big the packet is...and we have the whole thing
					CBinaryPacketPtr pkt = CBinaryPacketPtr(new CBinaryPacket( &pRecBuff[m_recvBegin + dataOffset], packetSize - dataOffset ));
					m_inList.push_back(pkt);
					pktRecieved = true;
					processedData += packetSize;
					m_recvBegin += packetSize;
					// determines if any data from the next packet has been recieved (bytesRecv - packetSize)
					newData -= packetSize;
				}
				break;
			case IPacket::PT_TEXT:
				{
					// Convert the recieved text to generic form (Unicode or Ansi)
					String recievedText;
					recievedText.resize( packetSize - dataOffset ); // Dont need the packet size or its type. Also no null character needed
					AnsiToGeneric( &recievedText[0], &pRecBuff[m_recvBegin + dataOffset], packetSize - dataOffset, packetSize - dataOffset );

					if ( !recievedText.empty() )
					{			
						// push back the text packet
						CTextPacketPtr pkt = CTextPacketPtr( new CTextPacket( recievedText ) );
						m_inList.push_back(pkt);
						pktRecieved = true;
						processedData += packetSize;
						m_recvBegin += packetSize;
						// determines if any data from the next packet has been recieved (bytesRecv - packetSize)
						newData -= packetSize; 
					}
				}
				break;
			default:
				assert( false && ieS("Unkown packet type") );
				break;
			}
		} // if

	}
	
	// Left over data size
	m_recvOfs = newData;

	if (pktRecieved)
	{
		if (m_recvOfs == 0)
		{
			m_recvOfs = 0;
			m_recvBegin = 0;
			m_recvBuf.clear();
		}
		else if (m_recvOfs + m_recvBegin +  MAX_PACKET_SIZE > RECV_BUFFER_SIZE)
		{
			// we don't want to overrun the buffer - so we copy the leftover bits 
			// to the beginning of the recieve buffer and start over
			int leftover = m_recvOfs;
			memcpy(&m_recvBuf[0], &m_recvBuf[m_recvBegin], m_recvOfs);
			m_recvBegin = 0;
		} 
	}

}

void CTCPSocket::HandleOutput()
{
	bool sent = false;
	int errorFlag = 0;

	do
	{
		char* pData = 0;
		u_long length = 0;

		if ( !m_outList.empty() )
		{
			PacketList::iterator itor = m_outList.begin();
			IPacketPtr pIPacket = *itor;
			pData = pIPacket->GetData();
			length = pIPacket->GetSize();
		}

		int result = send(m_socket, pData+m_sendOfs, length-m_sendOfs, 0);
		if (result == SOCKET_ERROR)
		{
			Cout << "send() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
			SetDeleteFlag(DF_DESTROY, true);
			sent = false;
		}
		else if ( WSAGetLastError() != WSAEWOULDBLOCK )
		{
			NETPrintError();
			SetDeleteFlag(DF_DESTROY, true);
			sent = false;
		}
		else if (result >= 0)
		{
			m_sendOfs += result; // bytes sent, increase the offset.
			sent = true;
		}
		else
		{
			sent = false;
		}

		// if the amount of sent data has reace the same length of that of the packet
		// size, then pop the top packet and send the next one.
		if ( m_sendOfs == length && !m_outList.empty() )
		{
			m_outList.pop_front();
			m_sendOfs = 0; // reset the sent offset
		}

	} while( sent && !m_outList.empty() );

}

