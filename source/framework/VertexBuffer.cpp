#include "Frameafx.h"
#include "VertexBuffer.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CVertexBuffer, "CVertexBuffer", "CBuffer");

CVertexBuffer::CVertexBuffer() : CBuffer()
{

}

CVertexBuffer::CVertexBuffer(UInt numVertices, UInt stride, BufferUse usage) : 
CBuffer(numVertices, stride, usage, BT_VERTEX)
{


}

CVertexBuffer::CVertexBuffer(const CVertexBuffer &attSet) : CBuffer(attSet)
{

}







/*

CVertexBuffer::CVertexBuffer() : m_totalStride(0)
{

}

CVertexBuffer::CVertexBuffer(const CVertexBuffer &attSet)
{

}

CVertexBuffer::~CVertexBuffer()
{
	m_vattribs.clear();
	m_enableFlags.reset();
}

void CVertexBuffer::SetVertexData(VertexSemantic attribute, std::size_t channels, VertexType type, const void *data, std::size_t count)
{
	// if enabled and if not a colour or texture.
	if (IsEnabled(attribute) && ( (attribute != VERTEX_COLOUR) || (attribute != VERTEX_TEXTURE_COORDINATES) ) )
	{
		for (AttItor itor = m_vattribs.begin(); itor != m_vattribs.end(); ++itor)
		{
			if ( (*itor)->GetVertexType() == attribute )
			{
				// decrement the total stide as this vertexattribute is going to be removed
				m_totalStride -= (*itor)->GetBytesStride();

				// remove the vertex
				m_vattribs.erase(itor);

				// create the new vertex and increment the totalstride with the new attribute
				CVertexAttributePtr pVertex = CVertexAttributePtr(new CVertexAttribute(channels, type, data, count));
				m_totalStride += pVertex->GetBytesStride();

				// insert the new vertex over the location of the
				// removed one.
				m_vattribs.insert( itor, pVertex );
			}
		}
	}
	else // First time adding this attribute to the list. (or it is a colour or a texture)
	{
		// Create the vertex attribute and set its data.
		CVertexAttributePtr pVertex = CVertexAttributePtr(new CVertexAttribute(channels, type, data, count));
		m_totalStride += pVertex->GetBytesStride();
		m_vattribs.push_back( pVertex );
	}

}

const void* CVertexBuffer::GetVertexData(VertexSemantic attribute) const
{
	for (Const_AttItor itor = m_vattribs.begin(); itor != m_vattribs.end(); ++itor)
	{
		if ( (*itor)->GetVertexType() == attribute )
			return (*itor)->GetData();
	}

	return 0;
}

UInt CVertexBuffer::GetVertexChannels(VertexSemantic attribute) const
{
	for (Const_AttItor itor = m_vattribs.begin(); itor != m_vattribs.end(); ++itor)
	{
		if ( (*itor)->GetVertexType() == attribute )
			return (*itor)->GetVertexChannels();
	}

	assert(!"warning, attribute type not in vertex buffer"); // this may be a bit harsh
	return 0;
}

UInt CVertexBuffer::GetVertexCount(VertexSemantic attribute) const
{
	for (Const_AttItor itor = m_vattribs.begin(); itor != m_vattribs.end(); ++itor)
	{
		if ( (*itor)->GetVertexType() == attribute )
			return (*itor)->GetNumElements();
	}

	assert(!"warning, attribute type not in vertex buffer"); // this may be a bit harsh
	return 0;
}

VertexType CVertexBuffer::GetVertexDataType(VertexSemantic attribute) const
{
	for (Const_AttItor itor = m_vattribs.begin(); itor != m_vattribs.end(); ++itor)
	{
		if ( (*itor)->GetVertexType() == attribute )
			return (*itor)->GetVertexType();
	}

	assert(!"warning, attribute type not in vertex buffer"); // this may be a bit harsh
	return V_UNKNOWN;
}

void CVertexBuffer::RemoveVertexData(VertexSemantic attribute)
{
	for (AttItor itor = m_vattribs.begin(); itor != m_vattribs.end(); ++itor)
	{
		if ( (*itor)->GetVertexType() == attribute )
			m_vattribs.erase(itor);
	}
}

*/