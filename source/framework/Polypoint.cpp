#include "frameafx.h"
#include "Polypoint.h"
#include "VertexBuffer.h"

using namespace Engine;


DEFINE_HIERARCHICALHEAP(CPolypoint, "CPolypoint", "CSceneNode");

CPolypoint::CPolypoint(CVertexFormatPtr pVFormat, CVertexBufferPtr pVBuffer) : 
CSceneNode( PT_POLYPOINTS, pVFormat, pVBuffer, CIndexBufferPtr() ),
m_numPoints( m_pVBuffer->GetNumElements() )
{


}

CPolypoint::~CPolypoint()
{

}

void CPolypoint::SetMaxNumPoints(UInt numPoints)
{
    UInt numVertices = m_pVBuffer->GetNumElements();
    if (0 <= numPoints && numPoints <= numVertices)
    {
        m_numPoints = numPoints;
    }
    else
    {
        m_numPoints = numVertices;
    }
}

UInt CPolypoint::GetMaxNumPoints() const
{
	return m_pVBuffer->GetNumElements();
}