#ifndef __CVERTEX_ATTRIBUTESET_INL__
#define __CVERTEX_ATTRIBUTESET_INL__


inline UInt8* CVertexBuffer::GetData()
{
	return const_cast<UInt8*>( static_cast<const CBuffer&>(*this).GetData() );
}




#endif


/*

inline UInt	CVertexBuffer::GetTotalStride() const
{
	return m_totalStride;
}

// Position Data
// Can be of 2 - 4 channales. 
inline void CVertexBuffer::SetPosition(const Vector2f *vertices, UInt count)
{
	SetVertexData(VERTEX_POSITION, 2, V_FLOAT, vertices, count);
	SetEnabled(VERTEX_POSITION, true);
}

inline void CVertexBuffer::SetPosition(const Vector3f *vertices, UInt count)
{
	SetVertexData(VERTEX_POSITION, 3, V_FLOAT, vertices, count);
	SetEnabled(VERTEX_POSITION, true);
}

inline void CVertexBuffer::SetPosition(const Vector4f *vertices, UInt count)
{
	SetVertexData(VERTEX_POSITION, 4, V_FLOAT, vertices, count);
	SetEnabled(VERTEX_POSITION, true);
}

inline const Vector2f *CVertexBuffer::GetPosition2() const
{
	assert(!GetVertexData(VERTEX_POSITION) || GetVertexChannels(VERTEX_POSITION) == 2); 
	return static_cast<const Vector2f*>( GetVertexData(VERTEX_POSITION) ); 
}

inline const Vector3f *CVertexBuffer::GetPosition3() const
{
	assert(!GetVertexData(VERTEX_POSITION) || GetVertexChannels(VERTEX_POSITION) == 3); 
	return static_cast<const Vector3f*>( GetVertexData(VERTEX_POSITION) ); 
}

inline const Vector4f *CVertexBuffer::GetPosition4() const
{
	assert(!GetVertexData(VERTEX_POSITION)|| GetVertexChannels(VERTEX_POSITION) == 4); 
	return static_cast<const Vector4f*>( GetVertexData(VERTEX_POSITION) ); 
}

inline UInt CVertexBuffer::GetPostionCount() const
{
	return GetVertexCount(VERTEX_POSITION);
}

// Blendweights
// Can be of 1 - 5, floats or doubles to representing vertex blending.
template <typename T> 
inline void CVertexBuffer::SetBlendWeights(const T *weights, UInt count)
{
	SetVertexData(VERTEX_BLEND_WEIGHTS, 1, Type2EnumType<T>::type, weights, count);
	SetEnabled(VERTEX_BLEND_WEIGHTS, true);
}

// Normals
// Can be of 1 - 3, float or double.
inline void CVertexBuffer::SetNormals(const Vector3f *normals, UInt count)
{
	SetVertexData(VERTEX_NORMAL, 3, V_FLOAT, normals, count);
	SetEnabled(VERTEX_NORMAL, true);
}

inline const Vector3f *CVertexBuffer::GetNormals() const
{
	return static_cast<const Vector3f*>( GetVertexData(VERTEX_NORMAL) );
}

inline UInt CVertexBuffer::GetNormalCount() const
{
	return GetVertexCount(VERTEX_NORMAL);
}

// Colours
// Can be of 3 - 4, float, double or unsigned byte.
template <typename T> 
inline void CVertexBuffer::SetColours(const ColourRGB<T> *colours, UInt count)
{
	SetVertexData(VERTEX_COLOUR, 3, Type2EnumType<T>::type, colours, count);
	SetEnabled(VERTEX_COLOUR, true);
}

template <typename T> 
inline void CVertexBuffer::SetColours(const ColourRGBA<T> *colours, UInt count)
{
	SetVertexData(VERTEX_COLOUR, 4, Type2EnumType<T>::type, colours, count);
	SetEnabled(VERTEX_COLOUR, true);
}

// Fog
// Can be of 1 floats or doubles.
template <typename T> 
inline void CVertexBuffer::SetFogCoords(const T *coords, UInt count)
{
	SetVertexData(VERTEX_FOG, 1, Type2EnumType<T>::type, coords, count);
	SetEnabled(VERTEX_FOG, true);
}

// Point
// The size of a point.
template <typename T> 
inline void CVertexBuffer::SetPointSize(const T *PSizes, UInt count)
{
	SetVertexData(VERTEX_PSIZE, 1, Type2EnumType<T>::type, PSizes, count);
	SetEnabled(VERTEX_PSIZE, true);
}

// Index Blending
template <typename T> 
inline void CVertexBuffer::SetBlendidIndices(const T *indices, UInt count)
{
	SetVertexData(VERTEX_BLEND_INDICES, 1, Type2EnumType<T>::type, indices, count);
	SetEnabled(VERTEX_BLEND_INDICES, true);
}

// Texture Coordinates
template <typename T> 
inline void CVertexBuffer::SetTCoords(const T *tcoords, UInt count)
{
	SetVertexData(VERTEX_TEXTURE_COORDINATES, 1, Type2EnumType<T>::type, tcoords, count);
	SetEnabled(VERTEX_TEXTURE_COORDINATES, true);
}

template <typename T> 
inline void CVertexBuffer::SetTCoords(const Vector2<T> *tcoords, UInt count)
{
	SetVertexData(VERTEX_TEXTURE_COORDINATES, 2, Type2EnumType<T>::type, tcoords, count);
	SetEnabled(VERTEX_TEXTURE_COORDINATES, true);
}

template <typename T> 
inline void CVertexBuffer::SetTCoords(const Vector3<T> *tcoords, UInt count)
{
	SetVertexData(VERTEX_TEXTURE_COORDINATES, 3, Type2EnumType<T>::type, tcoords, count);
	SetEnabled(VERTEX_TEXTURE_COORDINATES, true);
}

template <typename T> 
inline void CVertexBuffer::SetTCoords(const Vector4<T> *tcoords, UInt count)
{
	SetVertexData(VERTEX_TEXTURE_COORDINATES, 4, Type2EnumType<T>::type, tcoords, count);
	SetEnabled(VERTEX_TEXTURE_COORDINATES, true);
}

// Tangent Coordinates
template <typename T> 
inline void CVertexBuffer::SetTangents(const T *tangents, UInt count)
{
	SetVertexData(VERTEX_TANGENT, 1, Type2EnumType<T>::type, tangents, count);
	SetEnabled(VERTEX_TANGENT, true);
}

template <typename T>
inline void CVertexBuffer::SetBiTangents(const T *biTangents, UInt count)
{
	SetVertexData(VERTEX_BITANGENT, 1, Type2EnumType<T>::type, biTangents, count);
	SetEnabled(VERTEX_BITANGENT, true);
}

inline void CVertexBuffer::SetEnabled(VertexSemantic attribute, bool enable)
{
	assert(attribute >= VERTEX_POSITION && attribute <= VERTEX_TANGENT);
	m_enableFlags[attribute] = enable;

	//m_enableFlags &= ~(1 << attribute);
	//m_enableFlags |= ((!!enable) << attribute);
}

inline bool CVertexBuffer::IsEnabled(VertexSemantic attribute) const
{
	assert(attribute >= VERTEX_POSITION && attribute <= VERTEX_TANGENT);
	return m_enableFlags[attribute];
	//return !!(m_enableFlags & (1 << attribute));
}

*/