#ifndef __CBUFFER_H__
#define __CBUFFER_H__

#include "Core.h"

namespace Engine
{

class CBuffer
{
public:
	// Usage flags for vertex buffers, index buffers, and textures.
	enum BufferUse
	{
		BU_GPU,					// CPU cannont read or write to GPU
		BU_STATIC,				// CPU can write to GPU on creation
		BU_DYNAMIC,				// CPU can write to GPU frequently 
		BU_STAGING,				// GPU can read and write to the GPU
		NUM_BUFFER_USES
	};

	// Locking modes for accessing video memory for a buffer or texture.
	enum Locking
	{
		BL_READ_ONLY,
		BL_WRITE_ONLY,
		BL_READ_WRITE,
		BL_DISCARD,
		BL_NO_OVERWRITE,
		NUM_LOCKING_STATES
	};

	// Type of buffer in use
	enum BufferType
	{
		BT_VERTEX,
		BT_INDEX
	};

	virtual ~CBuffer();

	// The number of elements within the Buffer ie 24 Vector3's
	UInt GetNumElements() const;

	// The data member size in bytes
	UInt GetBytesSize() const;

	// The stride in bytes
	// VertexAttribute :: Returns the stride which is calculated by m_channels * SizeOfType(type) .
	UInt GetBytesStride() const;

	// Return a const pointer to the stored data
	const UInt8* GetData() const;

	BufferUse GetUsage() const;

	BufferType GetBufferType() const;

protected:
	CBuffer();
	CBuffer(UInt count, UInt stride, BufferUse usage, BufferType bufferType);
	CBuffer(const CBuffer& buffer);

	UInt			m_count;	// the number of added elements, size independant
	UInt			m_stride;	// the element size
	UInt8Vector		m_pData;
	BufferUse		m_usage;
	BufferType		m_bufferType;

private:
	DECLARE_HEAP;
};


#include "Buffer.inl"


}


#endif