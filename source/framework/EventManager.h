#ifndef CEVENT_MANAGER_H
#define CEVENT_MANAGER_H

#include "IEventManager.h"


namespace Engine
{

	class CEventManager : public IEventManager
	{
	public:
		enum { NUMQUEUES = 2 };

		static const ULong INFINITE_TIMEOUT = 0xffffffff;

		CEventManager();

		virtual ~CEventManager();

		virtual void AddRegisteredEventType(const EventType & eventType);

		virtual bool AddListener(const IEventListenerPtr& inListener, const EventType& inType);

		virtual bool DelListener(const IEventListenerPtr& inListener, const EventType& inType);

		virtual bool Trigger (const IEventData& inEvent) const;

		virtual bool QueueEvent(const IEventDataPtr& inEvent);

		virtual bool AbortEvent(const EventType& inType, bool allOfType = false);

		virtual bool Tick(ULong maxMillis = INFINITE_TIMEOUT);

		virtual bool ValidateType(const EventType& inType) const;

	private:
		// one instance of the event type
		typedef std::set<EventType> EventTypeSet;

		// insert result into event type set
		typedef std::pair<EventTypeSet::iterator, bool> EventTypeSetIRes;

		// one list per event (stored in the map)
		typedef std::list<IEventListenerPtr> IEventListenerTable;

		// mapping of event identities to listner list
		typedef std::map<ULong, IEventListenerTable> IEventListenerMap;

		// Event listener map entry
		typedef std::pair<ULong, IEventListenerTable> IEventListenerMapEnt;

		// insert result into listener map
		typedef std::pair<IEventListenerMap::iterator, bool> IEventListenerMapIRes;

		// Queue of pending- or processing-events
		typedef std::list<IEventDataPtr> EventQueue;

		// Registerd events
		EventTypeSet m_eventTypeSet;

		// Mappping of event types to listners
		IEventListenerMap m_registry;

		// Double buffered event processing queue
		EventQueue m_queues[NUMQUEUES];

		// Which queue is activly processing
		UInt32 m_activeQueue;

	private:
		DECLARE_HEAP;


	};


}

#endif