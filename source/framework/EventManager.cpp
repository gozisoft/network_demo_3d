#include "frameafx.h"
#include "EventManager.h"
#include "StringFunctions.h"
#include "IEventReciever.h"
#include "Timer.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CEventManager, "CEventManager", "IEventManager");

const char* const wildCardEventType = "*";

CEventManager::CEventManager() : 
IEventManager(),
m_activeQueue(0)
{


}


CEventManager::~CEventManager()
{

}

bool CEventManager::AddListener(const IEventListenerPtr& inListener, const EventType& inType)
{
	if ( !ValidateType(inType) )
		return false;

	// Find lisner map entry, create one if no table exists
	IEventListenerMap::iterator elmIT = m_registry.find( inType.GetHashValue() );

	// Not found in the registry
	if ( elmIT == m_registry.end() )
	{
		IEventListenerMapEnt mapEntryPair = IEventListenerMapEnt( inType.GetHashValue(), IEventListenerTable() );
		IEventListenerMapIRes elmIRes = m_registry.insert(mapEntryPair);

		// Count not insert into map
		if ( !elmIRes.second )
			return false;

		// Should not be possible, how did we insert and create an empty table.
		if ( elmIRes.first == m_registry.end() )
			return false;

		// Store the result in order to update the mapped list next...
		elmIT = elmIRes.first;
	}

	// Check for duplicate addition of listeneres. A bit more costly, but worth it.
	IEventListenerTable &evlTable = (*elmIT).second;

	for (IEventListenerTable::iterator it = evlTable.begin(); it != evlTable.end(); ++it)
	{
		if ( *it == inListener )
			return false;
	}

	evlTable.push_back(inListener);
	return true;
}

bool CEventManager::DelListener(const IEventListenerPtr& inListener, const EventType& inType)
{
	if ( !ValidateType(inType) )
		return false;

	bool rc = false;

	// Iterate trough all existing mapping entries
	// looking for the matching listener and remove it.
	for (IEventListenerMap::iterator mapIT = m_registry.begin(); mapIT != m_registry.end(); ++mapIT)
	{
		ULong eventID = (*mapIT).first;
		IEventListenerTable &evlTable = (*mapIT).second;

		for (IEventListenerTable::iterator eltIT = evlTable.begin(); eltIT != evlTable.end(); ++eltIT)
		{
			if ( *eltIT == inListener )
			{
				// found match, remove table
				evlTable.erase(eltIT);

				//update return status
				rc = true;

				// break from the loop as we have removed the event type from the table
				break;
			}
		}
	}

	return rc;
}

bool CEventManager::Trigger (const IEventData& inEvent) const
{
	if ( !ValidateType( inEvent.GetEventType() ) )
		return false;

	IEventListenerMap::const_iterator itWC = m_registry.find(0);

	if ( itWC != m_registry.end() )
	{
		const IEventListenerTable& table = itWC->second;
		for (IEventListenerTable::const_iterator it = table.begin(); it != table.end(); ++it)
		{
			(*it)->HandleEvent(inEvent);
		}
	}

	itWC = m_registry.find( inEvent.GetEventType().GetHashValue() );

	if ( itWC == m_registry.end() )
		return false;

	bool processed = false;

	const IEventListenerTable& table = itWC->second;
	for (IEventListenerTable::const_iterator it = table.begin(); it != table.end(); ++it)
	{
		if ( (*it)->HandleEvent(inEvent) )
			processed = true; // only set to true, if processing eats the messages
	}	

	return processed;
}

bool CEventManager::QueueEvent(const IEventDataPtr& inEvent)
{
	assert(m_activeQueue >= 0);
	assert(m_activeQueue < NUMQUEUES);

	if ( !ValidateType( inEvent->GetEventType() ) )
		return false;

	IEventListenerMap::const_iterator elmIT = m_registry.find( inEvent->GetEventType().GetHashValue() );

	if ( elmIT == m_registry.end() )
	{
		// if globabl listener is not active, then abort queue add
		IEventListenerMap::const_iterator elmIT1 = m_registry.find(0);

		// If no listenser for this event skip it.
		if ( elmIT1 == m_registry.end() )
			return false;
	}

	m_queues[m_activeQueue].push_back(inEvent);

	return true;
}

bool CEventManager::AbortEvent(const EventType& inType, bool allOfType)
{
	assert(m_activeQueue >= 0);
	assert(m_activeQueue < NUMQUEUES);

	if ( !ValidateType(inType) )
		return false;


}

bool CEventManager::Tick(ULong maxMillis)
{
	ULong currMS = static_cast<ULong>( OS::Timer::GetRealTime() );
	ULong maxMS = (maxMillis == INFINITE_TIMEOUT) ? INFINITE_TIMEOUT : (currMS + maxMillis);

	IEventListenerMap::const_iterator itWC = m_registry.find(0);

	// Swap active queues, make sure new queue is empty after first swap
	UInt32 queueToProcess = m_activeQueue;
	m_activeQueue = (m_activeQueue + 1) % NUMQUEUES;
	m_queues[m_activeQueue].clear();

	while ( m_queues[queueToProcess].empty() != true )
	{
		IEventDataPtr event = m_queues[queueToProcess].front();
		m_queues[queueToProcess].pop_front();

		if ( itWC != m_registry.end() )
		{
			const IEventListenerTable& table = itWC->second;
			bool processsed = false;

			for (IEventListenerTable::const_iterator eltIT = table.begin(); eltIT != table.end(); ++eltIT)
			{
				(*eltIT)->HandleEvent(*event);
			}
		}

		const EventType& eventType = event->GetEventType();
		IEventListenerMap::const_iterator itListeners = m_registry.find( eventType.GetHashValue() );

		// no listeners currently for this event type, skip it.
		if ( itListeners == m_registry.end() )
			continue;

		const ULong eventID = itListeners->first;
		const IEventListenerTable& table = itListeners->second;

		for (IEventListenerTable::const_iterator eltIT = table.begin(); eltIT != table.end(); ++eltIT)
		{
			if ( !(*eltIT)->HandleEvent(*event) )
			{
				OStringStream oss;
				oss << ieS("Failed to handle event of type") << std::endl;
				OutputDebugString( oss.str().c_str() );
			}
		}

		currMS = static_cast<ULong>( OS::Timer::GetRealTime() );

		if (maxMillis != INFINITE_TIMEOUT)
		{
			if (currMS >= maxMS)
			{
				// time ran out, break from processing loop.
				break;
			}
		}
	}

	// If any events left to process, push them onto the active
	// queue.
	bool queueEmptied = (m_queues[queueToProcess].size() == 0);
	if (!queueEmptied)
	{
		while (m_queues[queueToProcess].size() > 0)
		{
			IEventDataPtr event = m_queues[queueToProcess].back();
			m_queues[queueToProcess].pop_back();
			m_queues[m_activeQueue].push_back(event);
		}
	}

	// Queue emptied, can return now.
	return queueEmptied;
}

bool CEventManager::ValidateType(const EventType& inType) const
{
	if (!inType.GetStr().length())
		return false;

	if ( !inType.GetHashValue() && std::strcmp(inType.GetStr().c_str(), wildCardEventType) )
		return false;

	EventTypeSet::const_iterator evIT = m_eventTypeSet.find(inType); 
	if ( evIT == m_eventTypeSet.end() ) 
	{
		OStringStream oss;
		oss << ieS("Failed validation of an event type") << std::endl;
		OutputDebugString( oss.str().c_str() );
		Cout << oss.str();
		assert( false && ieS("Failed validation of an event type") );
		return false;
	}

	return true;
}


void CEventManager::AddRegisteredEventType(const EventType & eventType)
{
	EventTypeSet::const_iterator iter = m_eventTypeSet.find(eventType);
	if ( iter != m_eventTypeSet.end() )
	{
		OStringStream oss;
		oss << ieS("Attempted to register an event type that has already been registered!") << std::endl;
		OutputDebugString( oss.str().c_str() );
		Cout << oss.str();
		assert( false && ieS("Attempted to register an event type that has already been registered!") );
	}
	else
	{
		m_eventTypeSet.insert(eventType);
	}
}