#ifndef __CFILE_UTILITY_H__
#define __CFILE_UTILITY_H__

#include "Core.h"

namespace Engine
{

	class CFileUtility
	{
	public:
		static String GetMediaFile(const String& file);

	private:
		static String SearchFolders(const String& filename, Char* exeFolder, Char* exeName);
	};


}


#endif