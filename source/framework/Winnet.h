#ifndef __WIN_NET_H__
#define __WIN_NET_H__

#include "frameafx.h"

#include <winsock2.h>

_ENGINE_BEGIN

enum PacketConstraints
{ 
	MAX_PACKET_SIZE = 256, 
	RECV_BUFFER_SIZE = (MAX_PACKET_SIZE * 512) 
};

void NETSetBlocking(SOCKET sock, bool block);
void NETPrintError();

const Char* NETGetIpStr(const sockaddr *sa, Char *str, size_t maxlen);
const Char* NETInetNtop(int family, const void* pSrc, Char* pStrOut, size_t strSize);
UInt16 NetGetPort(const sockaddr *sa);
void NETGetPortStr(const sockaddr *sa, String &str);

namespace TCP
{
	int NETListenSock(SOCKET& newSocket, const String& protNum, bool block = false);
	int NETConnectSock(SOCKET& newSocket, const String& ip, const String& port, bool coalesce = false);
	SOCKET NETAcceptConnection(String& address, String& port, SOCKET listenSocket);

	int NETHandleOutput(std::list<IPacketPtr>& outList, SOCKET openSocket, int sendOfs);
	int NETHandleInput(std::list<IPacketPtr>& inList, std::vector<char>& recvBuf, SOCKET openSocket, size_t maxRecvSize = RECV_BUFFER_SIZE);

} // namespace TCP

namespace UDP
{
	int NETBind(SOCKET& newSocket, const String& port, bool block = false);
	int NETCreateSock(SOCKET& newSocket, const String& port, bool block= false);
	int NETConnectSock(SOCKET& newSocket, const String& ip, const String& port, bool block= false);
	int NETHandleOutput(std::list<IPacketPtr>& outList, SOCKET openSocket, const String& ip, const String& port);

} // namespace UDP


_ENGINE_END



#endif