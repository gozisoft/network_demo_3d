#ifndef __CPOLYLINES_INL__
#define __CPOLYLINES_INL__

inline UInt CPolyline::GetNumLines() const
{
	return m_numLines;
}

inline bool CPolyline::IsContiguous() const
{
	return m_contiguous;
}


#endif