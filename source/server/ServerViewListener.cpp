#include "appafx.h"
#include "ServerViewListener.h"
#include "ServerView.h"
#include "IActor.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CServerViewListener, "CServerViewListener", "IEventListener");

CServerViewListener::CServerViewListener(CServerGameView* pServerView) : 
m_pServerView(pServerView)
{


}

CServerViewListener::~CServerViewListener()
{


}

bool CServerViewListener::HandleEvent(const IEventData& event)
{
	/*
	if (event.GetEventType() == SEvtData_New_Actor::sk_EventType)
	{
		const SEvtData_New_Actor & castEvent = 
			static_cast<const SEvtData_New_Actor &>(event);

		SActorParams* pParams = castEvent.m_pActorParams;
		if (pParams == NULL)
		{
			assert( false && ieS("Invalid parameters specified for actor!") );
			return false;
		}

		// Create the actor with the paramaters
		IActorPtr pActor = IActor::CreateActor(pParams);

		// Create the mesh based on the paramaters recieved
		CSpatialPtr pSpatial = Engine::ActorToMesh::CreateGameMesh(pParams);

		// Map the actor to the SceneNode
		m_pServerView->m_actorSceneMap[pActor] = pSpatial;

		// Going to add a object to the scene
		CRootNodePtr pRootNode = m_pServerView->m_pScene;
		pRootNode->AddChild(pSpatial);

		return true;
	}
	*/
	if (event.GetEventType() == SEvtData_Spawn_Actor::sk_EventType)
	{
		const SEvtData_Spawn_Actor & castEvent = 
			static_cast<const SEvtData_Spawn_Actor &>(event);

		SActorParams* pActorParams = castEvent.m_pActorParams;
		if (!pActorParams)
		{
			assert( false && ieS("Invalid actor, unable to spawn!") );
			return false;
		}

		// Create the actor from the paramaters
		IActorPtr pActor = IActor::CreateActor(pActorParams);

		// Create the mesh based on the paramaters recieved
		CSpatialPtr pSpatial = Engine::ActorToMesh::CreateGameMesh(pActor);

		// Map the actor to the SceneNode
		m_pServerView->m_actorSceneMap[pActor] = pSpatial;

		// Going to add a object to the scene
		CRootNodePtr pRootNode = m_pServerView->m_pScene;
		pRootNode->AddChild(pSpatial);

		return true;
	}
	else if (event.GetEventType() == SEvtData_Destroy_Actor::sk_EventType)
	{


		return true;
	}
	else if (event.GetEventType() == SEvtData_Move_Actor::sk_EventType)
	{
		const SEvtData_Move_Actor& castEvent =
			static_cast<const SEvtData_Move_Actor &>(event);

		// ID of the actor be moved
		IActor::ActorID actorID = castEvent.m_actorID;

		CHumanView::ActorSceneMap::iterator itor = m_pServerView->m_actorSceneMap.begin();
		CHumanView::ActorSceneMap::iterator end = m_pServerView->m_actorSceneMap.end();
		for (; itor != end; ++itor)
		{
			IActorPtr pActor = (*itor).first;
			const SActorParams *pParams = pActor->GetParams();
			if (pParams->m_actorID == actorID )
			{
				CSpatialPtr pSpatial = (*itor).second;

				pSpatial->SetPosition(castEvent.m_position);
				pSpatial->SetRotation(castEvent.m_rotation);
				pSpatial->SetScale(castEvent.m_scale);
			}
		}

		return true;
	}

	return false;
}