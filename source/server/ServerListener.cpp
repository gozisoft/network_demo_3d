#include "appafx.h"
#include "ServerListener.h"
#include "ServerApp.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CServerListener, "CServerListener", "IEventListener");

CServerListener::CServerListener(CServerApp* pServerApp) : 
m_pServerApp(pServerApp)
{

}

CServerListener::~CServerListener()
{

}

// 1) remote client
// 2) request new actor
// 3) new actor
// 4) destroy actor
bool CServerListener::HandleEvent(const IEventData& event)
{
	IEventManagerPtr pEventMgr = m_pServerApp->m_pEventMgr;
	CServerNetoworkPtr pServerNet = m_pServerApp->m_pNetwork;

	if (event.GetEventType() == SEvtData_Remote_Client::sk_EventType)
	{
		const SEvtData_Remote_Client& ed = static_cast<const SEvtData_Remote_Client&>(event);
		const String ipAddress = ed.m_ipAddress;
		const UInt32 socketID = ed.m_socketId;

		CServerApp::ActorMap& actorMap = m_pServerApp->m_actorMap;

		// The teapot has already been created - we need to go find it.
		IActorPtr pActor = IActorPtr(); 
		for (CServerApp::ActorMap::iterator i = actorMap.begin(); i != actorMap.end(); ++i)
		{
			pActor = (*i).second;
			if (pActor->GetParams()->m_type == IActor::AT_CUBE)
			{
				const SActorParams* params = pActor->GetParams();
				const SCubeParams* cubeParams = static_cast<const SCubeParams*>(params);
				if (cubeParams->m_viewID == VIEWID_NO_VIEW_ATTACHED)
				{
					break;
				}
			}
		}

		if ( pActor != IActorPtr() )
		{			
			SActorParams* pParams = pActor->GetParams();
			IActor::ActorID actorID = pParams->m_actorID;

			CNetworkViewPtr pNetView = CNetworkViewPtr( new CNetworkView(pEventMgr, pServerNet, socketID) );
			m_pServerApp->m_pViewMgr->AddGameViewSingle( pNetView, actorID );

			// Build up a list of all the alive actors, these will be sent to the new client
			// to give it a view of the current server world.
			// Note :: This is a little sloppy, but what the hell.
			for (CServerApp::ActorMap::iterator i = actorMap.begin(); i != actorMap.end(); ++i)
			{
				pActor = (*i).second;
				const SActorParams* params = pActor->GetParams();

				if (pActor->GetParams()->m_type == IActor::AT_CUBE)
				{
					const SCubeParams* cubeParams = static_cast<const SCubeParams*>(params);

					if (cubeParams->m_viewID == VIEW_ATTACHED && cubeParams->m_spawned == true)
					{
						SEvtData_World_Update worldDataEvent(cubeParams->m_actorID, params);
						
						// now pass the event on to the network view
						pNetView->OnUpdate(worldDataEvent);
					}
				}
			}

			SCubeParams* cubeParams = static_cast<SCubeParams*>(pParams);
			cubeParams->m_viewID = VIEW_ATTACHED;
		}

		return true;
	}
	else if (event.GetEventType() == SEvtData_Spawn_Request::sk_EventType)
	{
		const SEvtData_Spawn_Request& ed = static_cast<const SEvtData_Spawn_Request&>(event);

		// All players have already spawnd
		if (m_pServerApp->m_spawnedPlayers >= m_pServerApp->m_maxPlayers)
			return false;

		// Spawn the player and make visibe to all other players
		CServerApp::ActorMap::iterator itor = m_pServerApp->m_actorMap.find(ed.m_actorID);
		if ( itor == m_pServerApp->m_actorMap.end() )
		{
			// Player is not in the actor map... somthing horrible has happend
			assert( false && ieS("Unable to find actor!") );
			return false;
		}

		// Found the actor
		IActorPtr pActor = (*itor).second;

		// Set the actor as spwaned as the request will happen
		SActorParams* pParams = pActor->GetParams();
		pParams->m_spawned = true; 

		// Trigger a spawn event
		shared_ptr<SEvtData_Spawn_Actor> spawnEvent = 
			shared_ptr<SEvtData_Spawn_Actor>( new SEvtData_Spawn_Actor( *pParams ) );

		bool success = pEventMgr->QueueEvent(spawnEvent);
		if (!success)
		{
			assert( false && ieS("Unable to trigger spawnEvent for actor!") );
			return false;
		}

		return true;
	}
	else if (event.GetEventType() == SEvtData_Request_New_Actor::sk_EventType)
	{
		  const SEvtData_Request_New_Actor & castEvent =
			  static_cast<const SEvtData_Request_New_Actor &>(event);

		  SActorParams* pParams = castEvent.m_pActorParams;
		  if (pParams == NULL)
		  {
			  assert( false && ieS("Invalid parameters specified for actor!") );
			  return false;
		  }

		  // Valid params.
		  IActor::ActorID actorID = m_pServerApp->m_actorMap.size(); //m_pServerApp->m_pViewMgr->GetNewActorID();
		  pParams->m_actorID = actorID;

		  // Create a new actor event and trigger it to run now
		  SEvtData_New_Actor actorEvent(actorID, pParams);
		  const bool success = m_pServerApp->m_pEventMgr->Trigger( actorEvent );
		  return success;
	}
	else if (event.GetEventType() == SEvtData_New_Actor::sk_EventType)
	{
		const SEvtData_New_Actor & castEvent =
			static_cast<const SEvtData_New_Actor &>(event);

		SActorParams* pParams = castEvent.m_pActorParams;
		if (pParams == NULL)
		{
			assert( false && ieS("Invalid parameters specified for actor!") );
			return false;
		}

		m_pServerApp->m_actorMap[pParams->m_actorID] = IActor::CreateActor(pParams);
		return true;
	}
	else if (event.GetEventType() == SEvtData_Request_Move_Actor::sk_EventType)
	{
		const SEvtData_Request_Move_Actor & castEvent =
			static_cast<const SEvtData_Request_Move_Actor &>(event);

		// check if playerID is in the map
		CServerApp::ActorMap::iterator i = m_pServerApp->m_actorMap.find(castEvent.m_actorID);
		if ( i == m_pServerApp->m_actorMap.end() )
		{
			Cout << ieS("Actor not found in map, unable to process request") << std::endl;
			return false;
		}

		// Update the actor paramaters
		IActorPtr pActor = (*i).second;
		SActorParams* pParams = pActor->GetParams();
		pParams->m_pos = castEvent.m_position;
		pParams->m_rotation = castEvent.m_rotation;
		pParams->m_scale = castEvent.m_scale;

		// Create a new actor event and trigger it to run now
		SEvtData_Move_Actor moveActorEvent(castEvent.m_actorID, castEvent.m_avgVelocity, castEvent.m_position,
			castEvent.m_rotation, castEvent.m_scale);

		const bool success = m_pServerApp->m_pEventMgr->Trigger(moveActorEvent);
		return success;
	}
	else if (event.GetEventType() == SEvtData_Destroy_Actor::sk_EventType)
	{

		return true;
	}

	return false;
}