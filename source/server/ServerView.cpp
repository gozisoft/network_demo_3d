#include "appafx.h"
#include "ServerView.h"
#include "ServerListener.h"

#include "Vector.h"

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CServerGameView, "CServerGameView", "IGameView");

CServerGameView::CServerGameView(IRendererPtr pRenderer, IMouseControllerPtr pMouse, IEventManagerPtr pEventMgr) :
CHumanView(pRenderer, pMouse, pEventMgr)
{
	// Create the camera listener
	m_pCamController = CCameraControllerFPSPtr( new CCameraControllerFPS(m_pMouse) );

	// Server view listener, listens for sever view events
	m_pServerViewListener = CServerViewListenerPtr( new CServerViewListener(this) );

	// Events that effect the view of the game (on screen)
	m_pEventMgr->AddListener(m_pServerViewListener, SEvtData_Spawn_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pServerViewListener, SEvtData_Destroy_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pServerViewListener, SEvtData_Move_Actor::sk_EventType);
}

CServerGameView::~CServerGameView()
{

}

void CServerGameView::OnCreate(GameViewID viewID, ActorID actorID)
{
	CHumanView::OnCreate(viewID, actorID);
	CreateScene();
}

void CServerGameView::OnRestore()
{
	CHumanView::OnRestore();

}

void CServerGameView::OnLostDevice()
{

}

void CServerGameView::OnUpdate(double time, double deltaTime)
{
	CHumanView::OnUpdate(time, deltaTime);

	m_pScene->Update(deltaTime);
}

void CServerGameView::OnRender(double time, double deltaTime)
{
	CHumanView::OnRender(time, deltaTime);

	if ( m_pRenderer->PreDraw() )
	{
		m_pCuller->ComputeVisibleSet(m_pScene);
		{
			m_pRenderer->ClearBuffers();
			m_pRenderer->Draw( m_pCuller->GetVisibleArray() );
		}

		m_pRenderer->PostDraw();
		m_pRenderer->DisplayColorBuffer();
	}
}

void CServerGameView::OnWindowEvent(const CWinEvent& event)
{
	bool absorebed = false;
	IEventDataPtr pEvent = IEventDataPtr();

	switch (event.EventType)
	{
	case CWinEvent::W_KEYBOARD_EVENT:
		pEvent = shared_ptr<SKeyEvent>( new SKeyEvent(event.KeyInput) );
		break;
	case CWinEvent::W_MOUSE_EVENT:
		pEvent = shared_ptr<SMouseEvent>( new SMouseEvent(event.MouseInput) );
		break;
	}

	if (m_pCameraNode)
		m_pCameraNode->OnEvent(*pEvent);
}

CServerGameView::GameViewType CServerGameView::GetType() const
{
	return GameViewType("CServerGameView");
}

void CServerGameView::CreateScene()
{
	// Create the scene.
	m_pScene = CRootNodePtr( new CRootNode() );

	// Create the Camera Node and assign it a controller
	m_pCamera->SetPosition( Vector3f(0.0f, 1.0f, -10.0f) );
	m_pCameraNode = CCameraNodeFPSPtr( new CCameraNodeFPS(m_pCamera) );
	m_pCameraNode->AddController(m_pCamController);

	// Create the objects in the scene (triangle format)
	CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
	pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
	pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);

	// Create the ground plane
	CTriMeshPtr pGround = CGeometry(pVFormat).Plane(1, 100, 100);

	// Create the triangle
	CTriMeshPtr pTriangle = CGeometry(pVFormat).Triangle(2.0, 2.0, 1.0f);
	pTriangle->SetPosition( Vector3f(0, 1, 5) );

	// Add the ground
	m_pScene->AddChild(pGround);

	// Add the triangle as a child to the scene
	m_pScene->AddChild(pTriangle);

	// Add the camera node
	m_pScene->AddChild(m_pCameraNode);

	// get absolute path
	String pathName = CFileUtility::GetMediaFile( ieS("media\\ControlPoints.txt") );
	IfStream inFile(pathName.c_str(), IfStream::in); 
	if ( inFile.fail() )
	{
		Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
		Cout << pathName << std::endl;
	}
	if ( inFile.fail() )
	{
		pathName = GetFullPath( ieS("ControlPoints.txt") );
		inFile.open(pathName.c_str(), IfStream::in); 
		if ( inFile.fail() )
		{
			Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
			Cout << pathName << std::endl;
		}
	}

	// Create the curve
	if ( inFile.is_open() )
	{
		using std::vector;
		vector<Vector3f> ctrlPoints;
		vector<BezierCurvef> bezierCurves;
		UInt numCtrlPoints = 0;
		while ( inFile.good() )
		{
			// read in number of ctrl points
			inFile >> numCtrlPoints;

			ctrlPoints.clear();
			ctrlPoints.resize(numCtrlPoints);
			for (UInt i = 0; i < numCtrlPoints; ++i)
			{
				inFile >> ctrlPoints[i][0];
				inFile >> ctrlPoints[i][1];
				inFile >> ctrlPoints[i][2];
			}

			// create the curve and push onto an array of curves
			bezierCurves.push_back( BezierCurvef(ctrlPoints) );
		}

		// Create the entire mesh from all the curves
		m_pBezierMesh = CPiecewiseBezierMeshPtr( new CPiecewiseBezierMesh(bezierCurves) );
		m_pBezierMesh->InsertToScene(m_pScene);

		// close the file
		inFile.close();
	}
	else
	{
		assert( false && ieS("Unable to locate ControlPoints.txt") );
		inFile.close();
		Cin.get();
		exit(1);
	}

}