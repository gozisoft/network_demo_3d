#pragma once

#include "targetver.h"

#define USING_OPENGL

// Windsock2 libs
#pragma comment (lib, "Ws2_32.lib")

#if defined (USING_DIRECT3D9) || defined (_USING_DIRECT3D9)
// Direct3D 9 libs
#pragma comment( lib, "DxErr.lib" )
#pragma comment( lib, "dxguid.lib" )
#pragma comment( lib, "d3d9.lib" )
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment( lib, "d3dx9d.lib" )
#else
#pragma comment( lib, "d3dx9.lib" )
#endif
#pragma comment( lib, "d3dcompiler.lib" )
#pragma comment( lib, "winmm.lib" )
#pragma comment( lib, "comctl32.lib" )
#elif defined (USING_OPENGL) || (_USING_OPENGL)
// OpenGL libs
#pragma comment( lib, "OpenGL32.lib" )
#pragma comment( lib, "glu32.lib" )
#endif


#include "apiheaders.h"
#include "mathsheaders.h"
#include "frameheaders.h"
#include "logicheaders.h"
#include "appheaders.h"

#if defined(WIN32) && defined(_DEBUG)
#ifndef USING_MEMORY_MGR
#define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
#define new DEBUG_NEW
#endif
#endif