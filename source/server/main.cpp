#include "appafx.h"
#include "ServerApp.h"

#define _CRTDBG_MAP_ALLOC
#include <tchar.h>
#include <crtdbg.h>



using namespace Engine;

// ------------------------------------------------------------------------------------------------------
// Summary: Application entry point
// ------------------------------------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	// Enable run-time memory check for debug builds.
    _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
    _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);

	int tmpDbgFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpDbgFlag |= _CRTDBG_DELAY_FREE_MEM_DF;
	tmpDbgFlag |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(tmpDbgFlag);		

    CServerAppPtr pApplication( new CServerApp() );
    CFrameworkPtr pFramework( new CFramework(pApplication, ieS("Server Window"), true) );

    // Initialize any application resources
	if ( !pApplication->Initialise() )
    {
        return 0;
    }

    // Initialize the Framework
    if ( !pFramework->Initialise() )
    {
        return 0;
    }

    // Rock and roll
	int result = pFramework->Run(0);

    return result;
}