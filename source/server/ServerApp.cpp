#include "appafx.h"
#include "ServerApp.h"
#include "ServerView.h"
#include "ServerListener.h"


using namespace Engine;

DEFINE_HIERARCHICALHEAP(CServerApp, "CServerApp", "IBaseApp");


// -------------------------------------------------------------------------------------------
// Summary: Default constructor
// -------------------------------------------------------------------------------------------
CServerApp::CServerApp() : 
m_maxPlayers(6),
m_spawnedPlayers(0)
{
	// Create the view manager
	m_pViewMgr = CGameViewManagerPtr( new CGameViewManager() );

	// Create the event manager
	m_pEventMgr = CEventManagerPtr( new CEventManager() );

	// serverlistener events
	m_pEventMgr->AddRegisteredEventType(SEvtData_Request_New_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_New_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Remote_Client::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Spawn_Request::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Request_Move_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Destroy_Actor::sk_EventType);

	// networkViewlistener / serverViewListener events
	m_pEventMgr->AddRegisteredEventType(SEvtData_Spawn_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Move_Actor::sk_EventType);

	// Create the application listener
	m_pServerListener = CServerListenerPtr( new CServerListener(this) );

	// Add the server listener and align it server specific events
	m_pEventMgr->AddListener(m_pServerListener, SEvtData_Request_New_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pServerListener, SEvtData_New_Actor::sk_EventType);	
	m_pEventMgr->AddListener(m_pServerListener, SEvtData_Remote_Client::sk_EventType);
	m_pEventMgr->AddListener(m_pServerListener, SEvtData_Spawn_Request::sk_EventType);
	m_pEventMgr->AddListener(m_pServerListener, SEvtData_Request_Move_Actor::sk_EventType);
	//m_pEventMgr->AddListener(m_pServerListener, SEvtData_Destroy_Actor::sk_EventType); 
}

CServerApp::~CServerApp()
{
	Release();
}

// -------------------------------------------------------------------------------------------
// Summary: Clean up resources
// -------------------------------------------------------------------------------------------
void CServerApp::Release()
{
	SafeRelease(m_pNetwork);
}

// -------------------------------------------------------------------------------------------
// Summary: Initialize application-specific resources and states here.
// -------------------------------------------------------------------------------------------
bool CServerApp::Initialise()
{
	// get absolute path
	String pathName = CFileUtility::GetMediaFile( ieS("media\\ServerPort.txt") );
	IfStream inFile(pathName.c_str(), IfStream::in); 
	if ( inFile.fail() )
	{
		Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
		Cout << pathName << std::endl;
		if ( inFile.fail() )
		{
			pathName.clear();
			pathName = GetFullPath( ieS("ServerPort.txt") );
			inFile.open(pathName.c_str(), IfStream::in); 
		}

	}

	String hostname;
	String port;
	if ( inFile.is_open() )
	{
		inFile >> hostname;
		inFile >> port;
		inFile.close();
	}
	else
	{
		Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
		Cout << pathName << std::endl;
		inFile.close();
		Cin.get();
		return false;
	}

	// Create the Networking
	m_pNetwork = CServerNetoworkPtr( new CServerNetowork(m_pEventMgr, port) );
	if ( !m_pNetwork->Initialise() )
	{
		assert( false && ieS("Unable to intialise networking component") );
		return false;
	}
	if ( !m_pNetwork->Listen() )
	{
		assert( false && ieS("Unable to Listen on network") );
		return false;
	}

	return true;
}

// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has been created. This is 
// the best location to create D3DPOOL_MANAGED resources. Resources created here should be released in 
// the OnDestroyDevice callback. 
// -------------------------------------------------------------------------------------------
void CServerApp::OnCreateDevice(IRendererPtr pRenderer, IMouseControllerPtr pMouse)
{
	// Create the GameView for viewing the server world
	m_pServerGameView = CServerGameViewPtr( new CServerGameView(pRenderer, pMouse, m_pEventMgr) );
	m_pServerGameView->OnCreate(0, 0);
	m_pServerGameView->OnRestore();

	for (int i = 0; i < 6; ++i)
	{
		SCubeParams cubeParams;
		cubeParams.m_viewID = VIEWID_NO_VIEW_ATTACHED;

		SEvtData_Request_New_Actor requestActor(cubeParams);
		m_pEventMgr->Trigger(requestActor);						// m_StartPosition += Vector3f(2, 0, 2);	
	}

}


// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has been created. This is 
// the best location to create D3DPOOL_DEFAULT resources. Resources created here should be released in 
// the OnLostDevice callback. 
// -------------------------------------------------------------------------------------------
void CServerApp::OnResetDevice(IRendererPtr pRenderer)
{
	// Queue a viewport size event
	if (m_pServerGameView)
		m_pServerGameView->OnRestore();

	for ( UInt32 i = 0; i < m_pViewMgr->GetViewCount(); ++i )
	{
		const CGameViewManager::IGameViewTable &table = m_pViewMgr->GetViewTable(i);
		CGameViewManager::IGameViewTable::const_iterator beginItor = table.begin();
		CGameViewManager::IGameViewTable::const_iterator endItor = table.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnRestore();
		}
	}

}

// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has entered a lost state 
// and before IDirect3DDevice9::Reset is called. Resources created in the OnResetDevice callback should 
// be released here, which generally includes all D3DPOOL_DEFAULT resources.
// -------------------------------------------------------------------------------------------
void CServerApp::OnLostDevice()
{


}

// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has been destroyed. 
// Resources created in the OnCreateDevice callback should be released here, which generally includes 
// all D3DPOOL_MANAGED resources.
// -------------------------------------------------------------------------------------------
void CServerApp::OnDestroyDevice()
{


}

// -------------------------------------------------------------------------------------------
// Summary: Updates the current frame.
// Parameters:
// -------------------------------------------------------------------------------------------
void CServerApp::OnUpdateFrame(double time, double deltaTime)
{
	if (m_pEventMgr)
		m_pEventMgr->Tick();

	if (m_pNetwork) // Asyncronous network processing - WSAEventSelect based
		m_pNetwork->DoSelect(0);

	if (m_pServerGameView)
		m_pServerGameView->OnUpdate(time, deltaTime);

	for ( UInt32 i = 0; i < m_pViewMgr->GetViewCount(); ++i )
	{
		const CGameViewManager::IGameViewTable &table = m_pViewMgr->GetViewTable(i);
		CGameViewManager::IGameViewTable::const_iterator beginItor = table.begin();
		CGameViewManager::IGameViewTable::const_iterator endItor = table.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnUpdate(time, deltaTime);
		}
	}

	// OStringStream oss;
	// oss << (ULong)time << std::endl;
	// OutputDebugString( oss.str().c_str() );

}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Renders the current frame.
Parameters:
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CServerApp::OnRenderFrame(IRendererPtr pRenderer, double time, double deltaTime)
{
	UNREFERENCED_PARAMETER(pRenderer);

	if (m_pServerGameView)
		m_pServerGameView->OnRender(time, deltaTime);

	for ( UInt32 i = 0; i < m_pViewMgr->GetViewCount(); ++i )
	{
		const CGameViewManager::IGameViewTable &table = m_pViewMgr->GetViewTable(i);
		CGameViewManager::IGameViewTable::const_iterator beginItor = table.begin();
		CGameViewManager::IGameViewTable::const_iterator endItor = table.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnRender(time, deltaTime);
		}
	}
}

void CServerApp::OnWindowEvent(const CWinEvent& event)
{
	if (m_pServerGameView)
		m_pServerGameView->OnWindowEvent(event);

	for ( UInt32 i = 0; i < m_pViewMgr->GetViewCount(); ++i )
	{
		const CGameViewManager::IGameViewTable &table = m_pViewMgr->GetViewTable(i);
		CGameViewManager::IGameViewTable::const_iterator beginItor = table.begin();
		CGameViewManager::IGameViewTable::const_iterator endItor = table.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnWindowEvent(event);
		}
	}
}

