#ifndef __CSERVER_VIEW_H__
#define __CSERVER_VIEW_H__


namespace Engine
{

	class CServerGameView : public CHumanView
	{
	public:
		friend class CServerViewListener;

		CServerGameView(IRendererPtr pRenderer, IMouseControllerPtr pMouse, IEventManagerPtr pEventMgr);
		~CServerGameView();

		void OnCreate(GameViewID viewID, ActorID actorID);
		void OnRestore();
		void OnLostDevice();
		void OnUpdate(double time, double deltaTime);
		void OnRender(double time, double deltaTime);
		void OnWindowEvent(const CWinEvent& event);
		GameViewType GetType() const;

	private:
		void CreateScene();

		CServerViewListenerPtr m_pServerViewListener;
		CPiecewiseBezierMeshPtr m_pBezierMesh;

	private:
		DECLARE_HEAP;

	};


}

#endif