#ifndef __APPLICATION_FORWARD_H__
#define __APPLICATION_FORWARD_H__

namespace Engine
{
	class CServerApp;
	class CServerGameView;
	class CServerListener;
	class CServerViewListener;

	typedef shared_ptr<CServerApp> CServerAppPtr;
	typedef shared_ptr<CServerGameView> CServerGameViewPtr;
	typedef shared_ptr<CServerListener> CServerListenerPtr;
	typedef shared_ptr<CServerViewListener> CServerViewListenerPtr;


}



#endif