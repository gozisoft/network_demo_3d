#ifndef __CSERVER_APP_H__
#define __CSERVER_APP_H__

#include "AppFwd.h"

namespace Engine
{


	class CServerApp : public IBaseApp
	{
	public:
		friend class CServerListener;
		typedef std::map<UInt32, IActorPtr> ActorMap;

		CServerApp();
		~CServerApp();

		bool Initialise();
		void Release();
		void OnCreateDevice(IRendererPtr pRenderer, IMouseControllerPtr pMouse);
		void OnResetDevice(IRendererPtr pRenderer);
		void OnLostDevice();
		void OnDestroyDevice();
		void OnUpdateFrame(double time, double deltaTime);
		void OnRenderFrame(IRendererPtr pRenderer, double time, double deltaTime);
		void OnWindowEvent(const CWinEvent& event);

	private:
		// The most important system
		CEventManagerPtr m_pEventMgr;

		// Netowrking manager
		CServerNetoworkPtr m_pNetwork;

		// Listener to handle events to the server logic
		CServerListenerPtr m_pServerListener;

		// Game views
		CGameViewManagerPtr m_pViewMgr;

		// Game actors
		ActorMap m_actorMap;
		UInt32 m_maxPlayers;
		UInt32 m_spawnedPlayers;

		// Server has a view cam that is not part of the game
		CServerGameViewPtr m_pServerGameView;

	private:
		DECLARE_HEAP;

	};



}


#endif

/*

CRootNodePtr m_pScene;

CCameraPtr m_pCamera;
CCameraNodeFPSPtr m_pCameraNode;
CCameraControllerFPSPtr m_pCamController;

CCullerPtr m_pCuller;



*/



/*
class CServerApp : public IBaseApp
{
public:
CServerApp();
~CServerApp();

bool Initialise();
void Release();
void OnCreateDevice(IRenderer* pRenderer, IMouseControllerPtr pMouse);
void OnResetDevice(IRenderer* pRenderer);
void OnLostDevice();
void OnDestroyDevice();
void OnUpdateFrame(IRenderer* pRenderer, double time, double deltaTime);
void OnRenderFrame(IRenderer* pRenderer, double time, double deltaTime);
bool PostEventFromUser(const IEventDataPtr& event);

private:

// owns these objects
IEventManagerPtr m_pEventMgr;
CNetworkPtr m_pNetwork;

CCameraPtr m_pCamera;
CCameraNodeFPSPtr m_pCameraNode;
CCameraControllerFPSPtr m_pCamController;

CRootNodePtr m_pScene;
CTriMeshPtr m_pTriangle;
CTriMeshPtr m_pBox;

};
*/