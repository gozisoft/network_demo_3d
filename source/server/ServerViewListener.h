#ifndef __CSERVER_VIEW_LISTENER_H__
#define __CSERVER_VIEW_LISTENER_H__

#include "AppFwd.h"

namespace Engine
{

	class CServerViewListener : public IEventListener
	{
	public:
		explicit CServerViewListener(CServerGameView* pServerView);
		~CServerViewListener();

		bool HandleEvent(const IEventData& event);

	private:
		CServerGameView* m_pServerView;

	private:
		DECLARE_HEAP;

	};






}

#endif