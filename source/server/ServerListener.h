#ifndef __CSERVER_LISTENER_H__
#define __CSERVER_LISTENER_H__

#include "AppFwd.h"

namespace Engine
{

	class CServerListener : public IEventListener
	{
	public:
		explicit CServerListener(CServerApp* pServerApp);
		~CServerListener();

		bool HandleEvent(const IEventData& event);

	private:
		CServerApp* m_pServerApp;

	private:
		DECLARE_HEAP;
	};

}



#endif