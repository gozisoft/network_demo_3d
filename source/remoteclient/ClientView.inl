#ifndef __CGAME_VIEW_INL__
#define __CGAME_VIEW_INL__

inline CHumanView::GameViewID CHumanView::GetViewId() const
{
	return m_gameViewID;
}

inline CHumanView::ActorID CHumanView::GetActorId() const
{
	return m_actorID;
}



#endif