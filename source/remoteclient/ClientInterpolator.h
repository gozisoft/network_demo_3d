#ifndef __CCLIENT_INTERPOLATOR_H__
#define __CCLIENT_INTERPOLATOR_H__

#include "ISceneController.h"
#include "IActor.h"
#include "Matrix.h"
#include "Beziercurve.h"

_ENGINE_BEGIN

// This class acts as the interpolator between movement packets
class CClientInterpolator : public ISceneController
{
public:
	CClientInterpolator(IActor::ActorID actorID, UInt curveSamples=1000U); // 10 ms default

	// Checks for node movement on the event of a key press or mouse press
	void AnimateNode(CSpatial* pSpatial, double deltaTime);

	// Does nothing
	bool HandleEvent(const IEventData& event);

private:
	shared_ptr<BezierCurvef> m_bezier;

	// start frame
	Vector3f m_startVelocity;
	Vector3f m_startPos;
	Vector3f m_startRot;
	Vector3f m_startScale;

	// end frame
	Vector3f m_endVelocity;
	Vector3f m_endPos;
	Vector3f m_endRot;
	Vector3f m_endScale;
	
	// Samples of the bezuer curve
	UInt m_curveSamples;

	// Reset every time a new move event is reiceved for this listener
	bool m_firstFrame;
	bool m_setEndPoint;

	// stepsize = 1 / static_cast<float>(curveSamples - 1);
	float m_stepSize;

	// Acumulate time and determine delta time [0, 1]
	float m_timeFactor;

	// actorID of the player 
	IActor::ActorID m_actorID;

};




_ENGINE_END


#endif