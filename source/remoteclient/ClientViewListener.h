#ifndef __CGAME_EVENT_RECIEVER_H__
#define __CGAME_EVENT_RECIEVER_H__

#include "IEventReciever.h"

namespace Engine
{

	class CClientViewListener : public IEventListener
	{
	public:
		explicit CClientViewListener(CClientView* pGameView);
		~CClientViewListener();

		bool HandleEvent(const IEventData& event);

	private:
		CClientView* m_pClientView;

	private:
		DECLARE_HEAP;

	};

	


}



#endif