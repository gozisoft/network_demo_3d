#ifndef __CPLAYER_EVENTS_H__
#define __CPLAYER_EVENTS_H__

#include "IEventReciever.h"
#include "ActorParamaters.h"
#include "BufferIO.h"

_ENGINE_BEGIN

namespace Serialisation
{
	SActorParams *CreateFromStream(std::istream &in);
	void SerialiseParams(std::ostream &out, const SActorParams *params);

}

// -----------------------------------------------------------------------------------------------
// Normal Events
// -----------------------------------------------------------------------------------------------
struct SEvtData_Remote_Connect : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Remote_Connect(String ipaddress)
		: m_ipAddress(ipaddress)
	{
	}

	SEvtData_Remote_Connect(std::istream &in)
	{

	}

	~SEvtData_Remote_Connect() 
	{

	}

	void Serialise(std::ostream& out) const
	{

	}

	void Serialise(CBufferIO& data) const
	{

	}

	String m_ipAddress;

private:
	DECLARE_HEAP;

};
// -----------------------------------------------------------------------------------------------

struct SEvtData_Remote_Client : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Remote_Client(UInt32 socketid, String ipaddress)
		: m_socketId(socketid), m_ipAddress(ipaddress)
	{
	}

	SEvtData_Remote_Client(std::istream &in)
	{

	}

	~SEvtData_Remote_Client() 
	{

	}

	void Serialise(std::ostream &out) const
	{

	}

	void Serialise(CBufferIO& data) const
	{

	}

	UInt32 m_socketId;
	String m_ipAddress;

private:
	DECLARE_HEAP;

};
// -----------------------------------------------------------------------------------------------

struct SEvtData_Network_Player_Actor_Assignment : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Network_Player_Actor_Assignment(IActor::ActorID socketid, IGameView::GameViewID playerID)
		: m_actorId(socketid), m_remotePlayerId(playerID)
	{
	}

	explicit SEvtData_Network_Player_Actor_Assignment(std::istream &in)
	{

	}

	~SEvtData_Network_Player_Actor_Assignment() 
	{

	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorId, sizeof(IActor::ActorID) );
		out.write( (char*)&m_remotePlayerId, sizeof(IGameView::GameViewID) );	
	}

	void Serialise(CBufferIO& data) const
	{
		data.Write( sizeof(IActor::ActorID), &m_actorId );
		data.Write( sizeof(IGameView::GameViewID), &m_remotePlayerId );	
	}

	IActor::ActorID m_actorId;
	IGameView::GameViewID m_remotePlayerId;

private:
	DECLARE_HEAP;

};
// -----------------------------------------------------------------------------------------------
struct SEvtData_World_Update : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_World_Update(IActor::ActorID actorID, const SActorParams& actorParams) : 
	m_actorID(actorID)
	{
		m_pActorParams = reinterpret_cast<SActorParams *>(new char[actorParams.m_size] );
		std::memcpy(m_pActorParams, &actorParams, actorParams.m_size);
	}

	explicit SEvtData_World_Update(IActor::ActorID actorID, const SActorParams* pActorParams) : 
	m_actorID(actorID)
	{
		m_pActorParams = reinterpret_cast<SActorParams *>(new char[pActorParams->m_size] );
		std::memcpy(m_pActorParams, pActorParams, pActorParams->m_size);
	}

	explicit SEvtData_World_Update(std::istream &in)
	{
		in.read( (char*)&m_actorID, sizeof(IActor::ActorID) );
		m_pActorParams = Serialisation::CreateFromStream(in);
	}

	~SEvtData_World_Update() 
	{
		SafeDelete(m_pActorParams);
	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorID, sizeof(IActor::ActorID) );
		Serialisation::SerialiseParams(out, m_pActorParams);
	}

	void Serialise(CBufferIO& data) const
	{
		// data.Write( sizeof(IActor::ActorID), &m_actorID );
		// data.Write( sizeof(IGameView::GameViewID), &m_remotePlayerId );		
	}

	IActor::ActorID m_actorID;		// id of the actor
	SActorParams* m_pActorParams;	// parameters for actor

private:
	DECLARE_HEAP;

};

// -----------------------------------------------------------------------------------------------
struct SEvtData_New_Actor : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_New_Actor(IActor::ActorID actorID, const SActorParams& actorParams) : 
	m_actorID(actorID)
	{
		m_pActorParams = reinterpret_cast<SActorParams *>(new char[actorParams.m_size] );
		std::memcpy(m_pActorParams, &actorParams, actorParams.m_size);
	}

	explicit SEvtData_New_Actor(IActor::ActorID actorID, const SActorParams* pActorParams) : 
	m_actorID(actorID)
	{
		m_pActorParams = reinterpret_cast<SActorParams *>(new char[pActorParams->m_size] );
		std::memcpy(m_pActorParams, pActorParams, pActorParams->m_size);
	}

	explicit SEvtData_New_Actor(std::istream &in)
	{
		in.read( (char*)&m_actorID, sizeof(IActor::ActorID) );
		m_pActorParams = Serialisation::CreateFromStream(in);
	}

	~SEvtData_New_Actor() 
	{
		SafeDelete(m_pActorParams);
	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorID, sizeof(IActor::ActorID) );
		Serialisation::SerialiseParams(out, m_pActorParams);
	}

	void Serialise(CBufferIO& data) const
	{
		// data.Write( sizeof(IActor::ActorID), &m_actorID );
		// data.Write( sizeof(IGameView::GameViewID), &m_remotePlayerId );		
	}

	IActor::ActorID m_actorID;		// id of the actor
	SActorParams* m_pActorParams;	// parameters for actor

private:
	DECLARE_HEAP;
};
// -----------------------------------------------------------------------------------------------

struct SEvtData_Destroy_Actor : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Destroy_Actor(IActor::ActorID actorID) : 
	m_actorID(actorID)
	{

	}

	~SEvtData_Destroy_Actor() 
	{

	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorID, sizeof(IActor::ActorID) );
	}

	void Serialise(CBufferIO& data) const
	{
		data.Write( sizeof(IActor::ActorID), &m_actorID );
	}

	IActor::ActorID m_actorID;		// id of the actor

private:
	DECLARE_HEAP;
};
// -----------------------------------------------------------------------------------------------


struct SEvtData_Spawn_Actor : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Spawn_Actor(const SActorParams& actorParams)  
	{
		m_pActorParams = reinterpret_cast<SActorParams *>(new char[actorParams.m_size] );
		std::memcpy(m_pActorParams, &actorParams, actorParams.m_size);		
	}

	explicit SEvtData_Spawn_Actor(std::istream &in) 
	{
		m_pActorParams = Serialisation::CreateFromStream(in);
	}

	~SEvtData_Spawn_Actor() 
	{

	}

	void Serialise(std::ostream &out) const
	{
		Serialisation::SerialiseParams(out, m_pActorParams);
	}

	void Serialise(CBufferIO& data) const
	{

	}

	SActorParams* m_pActorParams;	// parameters for actor

private:
	DECLARE_HEAP;

};

// -----------------------------------------------------------------------------------------------
struct SEvtData_Move_Actor : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Move_Actor(IActor::ActorID actorID, const Vector3f& avgVel, const Vector3f& pos,
		const Vector3f& rot, const Vector3f& scale)
		:
		m_actorID(actorID),
		m_avgVelocity(avgVel),
		m_position(pos),
		m_rotation(rot),
		m_scale(scale)
	{

	}

	explicit SEvtData_Move_Actor(std::istream& in)
	{
		in.read( (char*)&m_actorID, sizeof(IActor::ActorID) );
		//in.read( (char*)&m_avgVelocity, sizeof(float) );
		in.read( (char*)&m_avgVelocity[0], sizeof(Vector3f::type) * m_avgVelocity.GetSize() );
		in.read( (char*)&m_position[0], sizeof(Vector3f::type) * m_position.GetSize() );
		in.read( (char*)&m_rotation[0], sizeof(Vector3f::type) * m_rotation.GetSize() );
		in.read( (char*)&m_scale[0], sizeof(Vector3f::type) * m_scale.GetSize() ); 
	}

	~SEvtData_Move_Actor() 
	{

	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorID, sizeof(IActor::ActorID) );
		//out.write( (char*)&m_avgVelocity, sizeof(float) );
		out.write( (char*)&m_avgVelocity[0], sizeof(Vector3f::type) * m_avgVelocity.GetSize() );
		out.write( (char*)&m_position[0], sizeof(Vector3f::type) * m_position.GetSize() );
		out.write( (char*)&m_rotation[0], sizeof(Vector3f::type) * m_rotation.GetSize() );
		out.write( (char*)&m_scale[0], sizeof(Vector3f::type) * m_scale.GetSize() ); 
	}

	void Serialise(CBufferIO& data) const
	{
		data.Write( sizeof(IActor::ActorID), &m_actorID );
		data.Write( sizeof(Vector3f::type) * m_avgVelocity.GetSize(), &m_avgVelocity[0] );
		data.Write( sizeof(Vector3f::type) * m_position.GetSize(), &m_position[0] );
		data.Write( sizeof(Vector3f::type) * m_rotation.GetSize(), &m_rotation[0] );
		data.Write( sizeof(Vector3f::type) * m_scale.GetSize(), &m_scale[0] );
	}

	IActor::ActorID m_actorID;		// id of the actor
	Vector3f m_avgVelocity;
	Vector3f m_position;
	Vector3f m_rotation;
	Vector3f m_scale;

private:
	DECLARE_HEAP;

};
// -----------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------
// Requests
// -----------------------------------------------------------------------------------------------
struct SEvtData_Request_New_Actor : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Request_New_Actor(const SActorParams& actorParams)		
	{
		m_pActorParams = reinterpret_cast<SActorParams *>(new char[actorParams.m_size] );
		std::memcpy(m_pActorParams, &actorParams, actorParams.m_size);
	}

	~SEvtData_Request_New_Actor() 
	{
		SafeDelete(m_pActorParams);
	}

	void Serialise(std::ostream &out) const
	{

	}

	void Serialise(CBufferIO& data) const
	{

	}

	SActorParams* m_pActorParams;	// parameters for actor

private:
	DECLARE_HEAP;
};
// -----------------------------------------------------------------------------------------------

struct SEvtData_Spawn_Request : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Spawn_Request(IActor::ActorID actorID) : 
	m_actorID(actorID)
	{

	}

	explicit SEvtData_Spawn_Request(std::istream& in)
	{
		in.read( (char*)&m_actorID, sizeof(IActor::ActorID) );
	}

	~SEvtData_Spawn_Request() 
	{

	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorID, sizeof(IActor::ActorID) );
	}

	void Serialise(CBufferIO& data) const
	{
		data.Write( sizeof(IActor::ActorID), &m_actorID );
	}

	IActor::ActorID m_actorID;	// id of the actor wanting to spawn

private:
	DECLARE_HEAP;

};
// -----------------------------------------------------------------------------------------------

struct SEvtData_Request_Move_Actor : public IEventData
{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
		return sk_EventType;
	}

	explicit SEvtData_Request_Move_Actor(IActor::ActorID actorID, const Vector3f& avgVel, const Vector3f& pos,
		const Vector3f& rot, const Vector3f& scale) 
		: 
		m_actorID(actorID),
		m_avgVelocity(avgVel),
		m_position(pos),
		m_rotation(rot),
		m_scale(scale)
	{

	}

	explicit SEvtData_Request_Move_Actor(std::istream& in)
	{
		in.read( (char*)&m_actorID, sizeof(IActor::ActorID) );
		//in.read( (char*)&m_avgVelocity, sizeof(float) );
		in.read( (char*)&m_avgVelocity[0], sizeof(Vector3f::type) * m_avgVelocity.GetSize() );
		in.read( (char*)&m_position[0], sizeof(Vector3f::type) * m_position.GetSize() );
		in.read( (char*)&m_rotation[0], sizeof(Vector3f::type) * m_rotation.GetSize() );
		in.read( (char*)&m_scale[0], sizeof(Vector3f::type) * m_scale.GetSize() ); 
	}

	~SEvtData_Request_Move_Actor() 
	{

	}

	void Serialise(std::ostream &out) const
	{
		out.write( (char*)&m_actorID, sizeof(IActor::ActorID) );
		//out.write( (char*)&m_avgVelocity, sizeof(float) );
		out.write( (char*)&m_avgVelocity[0], sizeof(Vector3f::type) * m_avgVelocity.GetSize() );
		out.write( (char*)&m_position[0], sizeof(Vector3f::type) * m_position.GetSize() );
		out.write( (char*)&m_rotation[0], sizeof(Vector3f::type) * m_rotation.GetSize() );
		out.write( (char*)&m_scale[0], sizeof(Vector3f::type) * m_scale.GetSize() ); 
	}

	void Serialise(CBufferIO& data) const
	{
		data.Write( sizeof(IActor::ActorID), &m_actorID );
		data.Write( sizeof(Vector3f::type) * m_position.GetSize(), &m_position[0] );
		data.Write( sizeof(Vector3f::type) * m_rotation.GetSize(), &m_rotation[0] );
		data.Write( sizeof(Vector3f::type) * m_scale.GetSize(), &m_scale[0] );
	}

	IActor::ActorID m_actorID;		// id of the actor
	Vector3f m_avgVelocity;
	Vector3f m_position;
	Vector3f m_rotation;
	Vector3f m_scale;

private:
	DECLARE_HEAP;

};
// -----------------------------------------------------------------------------------------------


_ENGINE_END


#endif

	/*
	struct SEvtData_New_Actor : public IEventData
	{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
	return sk_EventType;
	}

	explicit SEvtData_New_Actor(IActor::ActorID actorID, SActorParams* pActorParams) : 
	m_actorID(actorID)
	{
	m_pActorParams = reinterpret_cast<SActorParams *>(new char[pActorParams->m_size] );
	std::memcpy(m_pActorParams, pActorParams, pActorParams->m_size);
	}

	virtual ~SEvtData_New_Actor() 
	{
	SafeDelete(m_pActorParams);
	}

	void Serialise(std::ostream &out) const
	{

	}

	IActor::ActorID m_actorID;		// id of the actor
	SActorParams* m_pActorParams;	// parameters for actor

	private:
	DECLARE_HEAP;
	};





	struct SEvtData_Request_New_Actor : public IEventData
	{
	static const EventType sk_EventType;
	virtual const EventType& GetEventType() const
	{
	return sk_EventType;
	}

	explicit SEvtData_Request_New_Actor(IActorPtr pActor) :
	m_pActor(pActor)
	{

	}

	virtual ~SEvtData_Request_New_Actor() 
	{

	}

	void Serialise(std::ostream &out) const
	{

	}

	IActorPtr m_pActor;

	private:
	DECLARE_HEAP;
	};

	*/