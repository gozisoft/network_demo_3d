#ifndef __NETWORK_EVENTS_H__
#define __NETWORK_EVENTS_H__

#include "IEventReciever.h"




namespace Engine
{

	struct SEvtData_Connect : public IEventData
	{
		static const EventType sk_EventType;
		virtual const EventType& GetEventType() const
		{
			return sk_EventType;
		}

		SEvtData_Connect() {}

		explicit SEvtData_Connect(const String& playerName)		
		{
			m_playerName = playerName;
		}

		void Serialise(std::ostream & os) const
		{
			// ar << m_playerName;
		}

		void Serialise(CBufferIO & data) const
		{
			
		}

		String m_playerName;

	private:
		DECLARE_HEAP;
	};
	


}

namespace boost 
{
	namespace serialization 
	{
		template<class Archive>
		void serialize(Archive & ar, Engine::SEvtData_Connect& evt, const unsigned int version)
		{
			ar & evt.m_playerName;
		}
	}
}

#endif