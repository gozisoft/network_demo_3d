#ifndef __ACTORS_PARAMATERS_H__
#define __ACTORS_PARAMATERS_H__

#include "IGameView.h"
#include "Vector.h"
#include "Colour.h"

namespace Engine
{


	struct SActorParams
	{
		typedef IActor::ActorType ActorType;
		typedef IActor::ActorID ActorID;

		SActorParams(ActorType type);

		void Init(std::istream &in);
		void Serialise(std::ostream &out) const;

		ActorType m_type;
		ActorID   m_actorID;
		size_t    m_size;
		bool	  m_spawned;
		Vector3f  m_pos;
		Vector3f  m_rotation;
		Vector3f  m_scale;
	};

	struct SCubeParams : public SActorParams
	{
		SCubeParams();

		void Init(std::istream &in);
		void Serialise(std::ostream &out) const;

		float m_xExtent;
		float m_yExtent;
		float m_zExtent;
		IGameView::GameViewID m_viewID;
	};

	namespace ParamsToActor
	{
		IActorPtr CreateGameActor(const SActorParams* pParams);

	}

}

#endif