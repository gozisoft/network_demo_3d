#include "logicafx.h"
#include "NetworkViewListener.h"
#include "NetworkManager.h"

#include "IEventReciever.h"
#include "StringFunctions.h"
#include "Packet.h"

using namespace Engine;

CNetworkViewListener::CNetworkViewListener(CBaseNetworkManagerPtr pNetMgr, UInt32 sockID, IActor::ActorID playerID) :
m_pNetworkMgr(pNetMgr),
m_sockID(sockID),
m_playerID(playerID)
{


}

CNetworkViewListener::~CNetworkViewListener()
{

}

bool CNetworkViewListener::HandleEvent(const IEventData& event)
{
	// Ignore player who sent the message
	if (event.GetEventType() == SEvtData_Move_Actor::sk_EventType)
	{
		const SEvtData_Move_Actor& castEvent =
			static_cast<const SEvtData_Move_Actor &>(event);

		if (m_playerID == castEvent.m_actorID)
		{
			// Cout << ieS("Ignore player who sent the message") << std::endl;
			Cout << ieS("PlayerID :") << castEvent.m_actorID << std::endl;
			return true;
		}
	}

	SocketEvent socketEventType = NetMsg_Event;
	ULong eventHash = event.GetEventType().GetHashValue();

	// serialise data
	using std::stringstream;
	stringstream ss( stringstream::in | stringstream::out | stringstream::binary );

	// 1) socketevent type (NetMsg_Event)
	// 2) event type (hash value)
	// 3) event data
	ss.write( (char*)&socketEventType, sizeof(SocketEvent) );
	ss.write( (char*)&eventHash, sizeof(ULong) );
	event.Serialise(ss);

	// get length of file:
	ss.seekg (0, stringstream::end);
	size_t length = static_cast<size_t>( ss.tellg() );
	ss.seekg (0, stringstream::beg);

	// read it into a vector
	using std::vector;
	vector<char> buffer(length);
	ss.read( &buffer[0], length );	

	CBinaryPacketPtr pPacket( new CBinaryPacket( &buffer[0], buffer.size() ) );
	bool sucess = m_pNetworkMgr->Send(m_sockID, pPacket);

	return sucess;
}