#ifndef __CLISTEN_SOCKET_H__
#define __CLISTEN_SOCKET_H__

#include "logicfwd.h"

#include <winsock2.h>

namespace Engine
{


	class CTCPListenSocket
	{		
	public:
		typedef std::list<CClientPtr> ClientList;

		CTCPListenSocket();
		~CTCPListenSocket();

		void HandleInput(ClientList& clientList, const IEventManagerPtr& pEevntMgr);

		SOCKET GetSocket() const { return m_socket; }
		SOCKET m_socket;


	private:
		DECLARE_HEAP;
	};

	class CUDPListenSocket
	{	
	public:
		typedef std::list<CClientPtr> ClientList;
		typedef std::vector<char> CharVector;

		CUDPListenSocket();
		~CUDPListenSocket();

		void HandleInput(ClientList& clientList, const IEventManagerPtr& pEventMgr);
		void HandleOutput(ClientList& clientList);

		SOCKET GetSocket() const { return m_socket; }
		SOCKET m_socket;

	private:
		// Check for connected client
		IPacketPtr ProcessIncoming(const char* pData, int bytesRecv);
		CClientPtr IsClientConnected(ClientList& clientList, const IEventManagerPtr& pEventMgr, SOCKADDR_STORAGE& sockStorage);	

		CharVector m_recvBuf;	// Buffered data from last recieve


		DECLARE_HEAP;	
	};


}


#endif