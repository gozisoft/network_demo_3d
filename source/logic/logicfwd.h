#ifndef LOGIC_FORWARD_H
#define LOGIC_FORWARD_H

#include "Core.h"
#include "MathsFwd.h"

namespace Engine
{
	// Game views (may not be used)
	class IGameView;
	class CNetworkView;
	class CGameViewManager;

	typedef shared_ptr<IGameView> IGameViewPtr;
	typedef shared_ptr<CNetworkView> CNetworkViewPtr;
	typedef shared_ptr<CGameViewManager> CGameViewManagerPtr;
	
	// View listeners
	class CNetworkListener;
	class CNetworkViewListener;

	typedef shared_ptr<CNetworkListener> CNetworkListenerPtr;
	typedef shared_ptr<CNetworkViewListener> CNetworkViewListenerPtr;

	// Actors
	class IActor;
	class CBaseActor;
	class CCubeActor;
	
	typedef shared_ptr<IActor> IActorPtr;
	typedef shared_ptr<CBaseActor> CBaseActorPtr;
	typedef shared_ptr<CCubeActor> CCubeActorPtr;

	struct SActorParams;
	struct SCubeParams;

	typedef shared_ptr<SActorParams> SActorParamsPtr;
	typedef shared_ptr<SCubeParams> SCubeParamsPtr;

	// Networking, applicaiton specific
	class CTCPEventSocket;
	class CTCPListenSocket;
	class CUDPEventSocket;
	class CUDPListenSocket;

	typedef shared_ptr<CTCPEventSocket> CTCPEventSocketPtr;
	typedef shared_ptr<CTCPListenSocket> CTCPListenSocketPtr;
	typedef shared_ptr<CUDPEventSocket> CUDPEventSocketPtr;
	typedef shared_ptr<CUDPListenSocket> CUDPListenSocketPtr;

	class CBaseNetworkManager;
	class CServerNetowork;
	class CClientNetwork;
	class CClient;

	typedef shared_ptr<CBaseNetworkManager> CBaseNetworkManagerPtr;
	typedef shared_ptr<CServerNetowork> CServerNetoworkPtr;
	typedef shared_ptr<CClientNetwork> CClientNetworkPtr;
	typedef shared_ptr<CClient> CClientPtr;

	// Scenegraph additions to interact with an actor
	class CObjectListener;

	typedef shared_ptr<CObjectListener> CObjectListenerPtr;

	// Bezier stuff
	class CBezierCurveMesh;
	class CPiecewiseBezierMesh;
	class CCameraControllerPieceWise;

	typedef shared_ptr<CBezierCurveMesh> CBezierCurveMeshPtr;
	typedef shared_ptr<CPiecewiseBezierMesh> CPiecewiseBezierMeshPtr;
	typedef shared_ptr<CCameraControllerPieceWise> CCameraControllerPieceWisePtr;

	typedef shared_ptr<BezierCurvef> BezierCurvefPtr;
	typedef shared_ptr<PieceWiseBezierf> PieceWiseBezierfPtr;


}


#endif