#ifndef CBEIZERCURVE_MESH_H
#define CBEIZERCURVE_MESH_H


#include "Beziercurve.h"

_ENGINE_BEGIN


class CBezierCurveMesh
{
public:
	typedef std::vector<Vector3f> ControlPointArray;
	typedef std::vector<CRootNodePtr> RootNodeArray;
	typedef shared_ptr<BezierCurvef> BezierCurvefPtr;

	CBezierCurveMesh(const Vector3f* pCtrlPoints, size_t numCtrlPoints);
	CBezierCurveMesh(const ControlPointArray& ctrlPoints);
	~CBezierCurveMesh();

	void UpdateMesh(double time, double deltaTime);

	const CRootNodePtr* GetCtrlPointArray() const;
	size_t GetCtrlPointCount() const;

	CRootNodePtr GetMesh();
	BezierCurvefPtr GetCurve();

private:
	void BuildCurve();
	void BuildConnectingLines();
	void BuildControlPoints();

	// Actual maths curve
	BezierCurvefPtr m_pCurve;

	// Mesh data [ control points, curve, lines joining points ]
	CRootNodePtr m_pRoot;
	RootNodeArray m_controlPoints;
	CPolylinePtr m_pCurveMesh;			// total curve line mesh
	CPolylinePtr m_pCtrlPointLines;
};

#include "BezierCurveMesh.inl"


_ENGINE_END


#endif