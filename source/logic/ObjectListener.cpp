#include "logicafx.h"
#include "ObjectListener.h"
#include "Spatial.h"
#include "IEventManager.h"
#include "Timer.h"
#include "StringFunctions.h"

#include "GameEvents.h"

using namespace Engine;

CObjectListener::CObjectListener(IEventManagerPtr pEventMgr, IActor::ActorID actorID, ULong intervalTime) : 
pEventMgr(pEventMgr),
m_actorID(actorID),
m_intervalTime(intervalTime),
m_prevTimeSent(0)
{


}

void CObjectListener::AnimateNode(CSpatial* pSpatial, double deltaTime)
{
	// not active node
	if (!pSpatial)
		return;

	ULong currMS = static_cast<ULong>( OS::Timer::GetGameTime() );
	ULong deltaMS = currMS - m_prevTimeSent;

	// not long enough since last check
	if (deltaMS < m_intervalTime)
		return;

	// has not moved
	if ( m_prevWorld == pSpatial->GetWorldTransform() )
		return;

	// Velocity of the object, based on world transform

	// s = ut + 1/2 at^2
	Vector3f difference = pSpatial->GetWorldTransform().GetTranslation() - m_prevWorld.GetTranslation();
	Vector3f avgVecVelocity = difference / (float)deltaMS;

	// Vector3f unitVector = Normalised( difference );
	// float avgeVel = Dot(unitVector, avgVecVelocity);

	// has moved so store the transform for next frame compare
	m_prevWorld = pSpatial->GetWorldTransform();

	// Get the moved to position, rotation and scale
	Vector3f postion = pSpatial->GetWorldTransform().GetTranslation();
	Vector3f rotation = pSpatial->GetWorldTransform().GetRotation();
	Vector3f scale = pSpatial->GetWorldTransform().GetScale();

	// Create a move event to interact with a game listener
	shared_ptr<SEvtData_Request_Move_Actor> moveEvent( new SEvtData_Request_Move_Actor(m_actorID, avgVecVelocity, postion, rotation, scale) );
	if ( !pEventMgr->QueueEvent(moveEvent) )
	{
		OStringStream oss;
		oss << ieS("Unable to queue movement event") << std::endl;
		OutputDebugString( oss.str().c_str() );
	}

	// Succesfully sent the movment event
	m_prevTimeSent = currMS;
}

bool CObjectListener::HandleEvent(const IEventData& event)
{




	return true;
}