#include "logicafx.h"
#include "Actors.h"
#include "ActorParamaters.h"

#include "SceneNode.h"
#include "GeometryCreator.h"
#include "VertexFormat.h"
#include "TriangleMesh.h"


_ENGINE_BEGIN

namespace ActorToMesh
{
	CSpatialPtr CreateGameMesh(const IActorPtr& pActor)
	{
		CSpatialPtr pSpatial = CSpatialPtr();

		const SActorParams* pParams = pActor->GetParams();
		if (!pParams)
		{
			assert( false && ieS("Invalid actor params : ActorToMesh::CreateGameMesh()") );
			return pSpatial;
		}

		switch (pParams->m_type)
		{
		case IActor::AT_CUBE:
			{
				// Create the objects in the scene (triangle format)
				CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
				pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
				pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);

				// Cast the paramaters to their original type
				const SCubeParams *pCube = static_cast<const SCubeParams *>(pParams);

				// Create the box
				CTriMeshPtr pBox = CGeometry(pVFormat).Box(pCube->m_xExtent, pCube->m_yExtent, pCube->m_zExtent);
				pBox->SetPosition( pParams->m_pos );

				// Set the spatial to the box
				pSpatial = static_pointer_cast<CSpatial>(pBox);
			} 
			break;
		};

		return pSpatial;
	}

	CSpatialPtr CreateGameMesh(const SActorParams* pParams)
	{
		CSpatialPtr pSpatial = CSpatialPtr();

		switch (pParams->m_type)
		{
		case IActor::AT_CUBE:
			{
				// Create the objects in the scene (triangle format)
				CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
				pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
				pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);

				// Cast the paramaters to their original type
				const SCubeParams *pCube = static_cast<const SCubeParams *>(pParams);

				// Create the box
				CTriMeshPtr pBox = CGeometry(pVFormat).Box(pCube->m_xExtent, pCube->m_yExtent, pCube->m_zExtent);
				pBox->SetPosition( pParams->m_pos );
				pBox->SetRotation( pParams->m_rotation );
				pBox->SetScale( pParams->m_scale );

				// Set the spatial to the box
				pSpatial = static_pointer_cast<CSpatial>(pBox);
			} 
			break;
		};

		return pSpatial;
	}

}


IActorPtr IActor::CreateActor(ActorType type)
{
	IActorPtr pActor = IActorPtr();

	switch (type)
	{
	case AT_CUBE:
		pActor = CCubeActorPtr( new CCubeActor()  );
		return pActor;
	case AT_GRID:
		return pActor;

	};

	return pActor;
}

IActorPtr IActor::CreateActor(const SActorParams* pParams)
{
	IActorPtr pActor = IActorPtr();

	switch (pParams->m_type)
	{
	case AT_CUBE:
		{
			const SCubeParams* pCubeParams = static_cast<const SCubeParams *>(pParams);
			pActor = CCubeActorPtr( new CCubeActor(*pCubeParams)  );
		}
		return pActor;
	case AT_GRID:
		{

		}
		return IActorPtr();

	};

	// keep the compiler happy
	return IActorPtr();
}


CBaseActor::CBaseActor()
{

}


CBaseActor::CBaseActor(const SActorParams& params) : 
m_actorParams(0)
{
	m_actorParams = reinterpret_cast<SActorParams *>(new char[params.m_size] );
	std::memcpy(m_actorParams, &params, params.m_size);
}

CBaseActor::~CBaseActor()
{
	SafeDelete(m_actorParams);
}


CCubeActor::CCubeActor()
{

}

CCubeActor::CCubeActor(const SCubeParams& params) :
CBaseActor(params)
{

}


_ENGINE_END