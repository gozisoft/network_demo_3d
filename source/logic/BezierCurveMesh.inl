#ifndef CBEIZERCURVE_MESH_INL
#define CBEIZERCURVE_MESH_INL


inline const CRootNodePtr* CBezierCurveMesh::GetCtrlPointArray() const
{
	return &m_controlPoints[0];
}

inline size_t CBezierCurveMesh::GetCtrlPointCount() const
{
	return m_controlPoints.size();
}

inline CRootNodePtr CBezierCurveMesh::GetMesh()
{
	return m_pRoot;
}

inline BezierCurvefPtr CBezierCurveMesh::GetCurve()
{
	return m_pCurve;
}

#endif