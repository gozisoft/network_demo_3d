#ifndef __IGAME_VIEW_H__
#define __IGAME_VIEW_H__

#include "Core.h"
#include "IActor.h"

namespace Engine
{

	class IGameView
	{
	public:
		typedef UInt32 GameViewID;
		typedef IActor::ActorID ActorID;
		typedef CHashedString GameViewType;

		virtual ~IGameView() {};
		virtual void OnCreate(GameViewID viewID, ActorID actorID) = 0;
		virtual void OnRestore() = 0;
		virtual void OnLostDevice() = 0;
		virtual void OnUpdate(double time, double deltaTime) = 0;
		virtual void OnRender(double time, double deltaTime) = 0;
		virtual void OnWindowEvent(const CWinEvent& event) = 0;
		virtual GameViewType GetType() const = 0;
		
	private:
		DECLARE_HEAP;

	};






}



#endif