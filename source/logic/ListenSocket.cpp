#include "logicafx.h"
#include "ListenSocket.h"
#include "Client.h"

#include "Winnet.h"
#include "IEventManager.h"
#include "StringFunctions.h"

#include <WS2tcpip.h>

using namespace Engine;

DEFINE_HIERARCHICALHEAP(CTCPListenSocket, "CTCPListenSocket", "CTCPSocket");
DEFINE_HIERARCHICALHEAP(CUDPListenSocket, "CUDPListenSocket", "CUDPSocket");


CTCPListenSocket::CTCPListenSocket() :
m_socket(0)
{

}

CTCPListenSocket::~CTCPListenSocket()
{

}


void CTCPListenSocket::HandleInput(ClientList& clientList, const IEventManagerPtr& pEevntMgr)
{
	String clientAddress;
	String portNumber;
	SOCKET newTCPSocket = TCP::NETAcceptConnection(clientAddress, portNumber, m_socket);

	bool oper = 1;
	if ( setsockopt( newTCPSocket, SOL_SOCKET, SO_DONTLINGER, (char *)&oper, sizeof(bool) ) == SOCKET_ERROR )
	{
		Cout << ieS("setsockopt has failed: SO_DONTLINGER") << std::endl;
		NETPrintError();
		closesocket(newTCPSocket);
		newTCPSocket = INVALID_SOCKET;
		assert(0);
	}

	// Print player connect
	OStringStream oss;
	oss << ieS("Client connected via TCP: ") << clientAddress << ieS("::") << portNumber;
	Cout << oss.str() << std::endl;

	if ( newTCPSocket != INVALID_SOCKET )
	{
		// Add the new client to the clientlist
		UInt32 sockID = clientList.size();
		clientList.push_back( CClientPtr( new CClient(sockID, clientAddress) ) );
	}
	
	// Not going to be using the tcp socket beyond accepting the connection
	closesocket(newTCPSocket);
}


CUDPListenSocket::CUDPListenSocket() :
m_socket(0)
{

}

CUDPListenSocket::~CUDPListenSocket()
{

}


void CUDPListenSocket::HandleInput(ClientList& clientList, const IEventManagerPtr& pEventMgr)
{
	CharVector recvBuf( MAX_PACKET_SIZE );
	CharVector::pointer pRecBuff = &recvBuf[0];

	SOCKADDR_STORAGE sockStorage;
	std::memset( &sockStorage, 0, sizeof(SOCKADDR_STORAGE) );
	int senderSize = sizeof(SOCKADDR_STORAGE);

	int result = recvfrom(m_socket, pRecBuff, MAX_PACKET_SIZE, 0, (SOCKADDR *)&sockStorage, &senderSize);
	if (result == SOCKET_ERROR)
	{
		Cout << "recvfrom() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
		NETPrintError();
		return;
	}
	else if (result == 0)
	{
		Cout << ieS("Connection closed") << std::endl;
		NETPrintError();
		return;
	}

	// Process packet to see if its valid
	IPacketPtr pPacket = ProcessIncoming(pRecBuff, result);
	if (!pPacket)
		return;

	// Check if this client has been previously connected
	CClientPtr pClient = IsClientConnected(clientList, pEventMgr, sockStorage);
	if (pClient)
	{
		#if defined(DEBUG) || defined(_DEBUG)	
		OStringStream oss;
		oss << ieS("Incoming: ") << result << ieS(" bytes. ");
		oss << ieS("From: ") << pClient->m_ipAddress << ieS("::") << pClient->m_port;
		Cout << oss.str() << std::endl;
		#endif

		pClient->m_inList.push_back(pPacket);
		pClient->ProcessPackets(pEventMgr);
	}
	else
	{
		// Get the IP address
		size_t ipAddressSize = (sockStorage.ss_family == AF_INET) ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN;
		Char* ipAddress = new Char[ipAddressSize];	
		NETGetIpStr( (SOCKADDR *)&sockStorage, ipAddress, ipAddressSize );

		// Get the port number
		UInt16 port = NetGetPort( (SOCKADDR *)&sockStorage );

		for (ClientList::iterator itor = clientList.begin(); itor != clientList.end(); ++itor)
		{
			CClientPtr pClient = (*itor);

			// Check if ipAddress has connected to server
			if (pClient->m_ipAddress == ipAddress && pClient->m_port == 0)
			{
				pClient->m_port = port;

				pClient->m_inList.push_back(pPacket);
				pClient->ProcessPackets(pEventMgr);

				bool result = pEventMgr->QueueEvent( IEventDataPtr( new SEvtData_Remote_Client(pClient->m_clientID, pClient->m_ipAddress) ) );
				if (!result)
					assert( false && ieS("Unable to queue SEvtData_Remote_Client event") );

#if defined(DEBUG) || defined(_DEBUG)	
				OStringStream oss;
				oss << ieS("New IP for UDP Below:") << std::endl;
				oss << ieS("Incoming: ") << result << ieS(" bytes. ");
				oss << ieS("From: ") << pClient->m_ipAddress << ieS("::") << pClient->m_port << std::endl;
				Cout << oss.str();
#endif
			}

		}

		SafeDeleteArray(ipAddress);
	}

}

IPacketPtr CUDPListenSocket::ProcessIncoming(const char* pData, int bytesRecv)
{
	IPacketPtr pPacketOut = IPacketPtr();

	// Current size in buffer + next lot of bytes recieved
	int newData = bytesRecv;
	int packetSize = 0;
	int processedData = 0;
	bool pktRecieved = false;

	while ( newData > (sizeof(u_short) * 2) )
	{
		// measured offset of the base header
		size_t dataOffset = 0;

		// extract packet size from data.
		packetSize = *( reinterpret_cast<const u_short *>( &pData[dataOffset] ) );
		packetSize = static_cast<int>( ntohs(packetSize) );

		// increase the offset by 2 bytes
		dataOffset += sizeof(u_short);

		// Don't have enough new data to grab the next packet
		if (packetSize > newData)
		{
			// Throw the recieved data away
			m_recvBuf.clear();
			break;
		}

		if (packetSize > CSocket::MAX_PACKET_SIZE)
		{
			// prevent nasty buffer overruns!
			// SetDeleteFlag(DF_DESTROY, true);
			return pPacketOut;
		}

		// we know how big the packet is...and we have the whole thing
		if (newData >= packetSize)
		{
			// Advance the pointer along 2 bytes to get the type
			u_short sType = ntohs( *( reinterpret_cast<const u_short *>( &pData[dataOffset] ) ) );
			IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

			// increase the offset by 2 bytes
			dataOffset += sizeof(u_short);

			switch (type)
			{
			case IPacket::PT_BINARY:
				{
					// we know how big the packet is...and we have the whole thing
					CBinaryPacketPtr pkt = CBinaryPacketPtr(new CBinaryPacket( &pData[dataOffset], packetSize - dataOffset ));
					pPacketOut = pkt;

					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;
					pPacketOut = pkt;
				}
				break;
			case IPacket::PT_TEXT:
				{
					// Convert the recieved text to generic form (Unicode or Windows Ansi)
					String recievedText;
					recievedText.resize( packetSize - dataOffset ); // Dont need the packet size or its type. Also no null character needed
					AnsiToGeneric( &recievedText[0], &pData[dataOffset], packetSize - dataOffset, packetSize - dataOffset );

					if ( !recievedText.empty() )
					{			
						// push back the text packet
						CTextPacketPtr pkt = CTextPacketPtr( new CTextPacket( recievedText ) );
						pPacketOut = pkt;

						pktRecieved = true;
						processedData += packetSize;
						newData -= packetSize;
						pPacketOut = pkt;
					}			
				}
				break;
			case IPacket::PT_SAFEBINARY:
				{
					// 4 bytes in : to get the sequence num
					u_short sequenceNum = *( reinterpret_cast<const u_short *>( &pData[dataOffset] ) );
					sequenceNum = ntohs(sequenceNum);

					// offset is 2 more bytes
					dataOffset += sizeof(u_short);

					// 6 bytes in : to get the ack
					u_long ack = *( reinterpret_cast<const u_long *>( &pData[dataOffset] ) );
					ack = ntohl(sequenceNum);

					// offset is 4 more bytes
					dataOffset += sizeof(u_long);

					// 10 bytes in : to get the ack bit field
					u_long ack_bit = *( reinterpret_cast<const u_long *>( &pData[dataOffset] ) );
					ack_bit = ntohl(sequenceNum);

					CSafeBinaryPacketPtr pkt = CSafeBinaryPacketPtr ( 
						new CSafeBinaryPacket( &pData[dataOffset], packetSize - dataOffset, sequenceNum, ack, ack_bit) );

					pPacketOut = pkt;

					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;

					pPacketOut = pkt;
				}
				break;
			default:
				assert( false && ieS("Unkown packet type") );
				break;

			} // switch

		} // if newData >= packetSize

	} // while newData > (sizeof(u_short) * 2)

	return pPacketOut;
}

CClientPtr CUDPListenSocket::IsClientConnected(ClientList& clientList, const IEventManagerPtr& pEventMgr, SOCKADDR_STORAGE& sockStorage)
{
	// Get the IP address
	size_t ipAddressSize = (sockStorage.ss_family == AF_INET) ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN;
	Char* ipAddress = new Char[ipAddressSize];	
	NETGetIpStr( (SOCKADDR *)&sockStorage, ipAddress, ipAddressSize );

	// Get the port number
	UInt16 port = NetGetPort( (SOCKADDR *)&sockStorage );

	for (ClientList::iterator itor = clientList.begin(); itor != clientList.end(); ++itor)
	{
		CClientPtr pClient = (*itor);

		// Check if ipAddress has connected to server
		if (pClient->m_ipAddress == ipAddress && pClient->m_port == port)
		{
			// We have found the client and now can break from the loop
			// No longer need the ipAddress array
			SafeDeleteArray(ipAddress);		
			return pClient;
		}
	}

	// No longer need the ipAddress array
	SafeDeleteArray(ipAddress);
	return CClientPtr();
}

void CUDPListenSocket::HandleOutput(ClientList& clientList)
{
	CClientPtr pClient = CClientPtr();
	for (ClientList::iterator itor = clientList.begin(); itor != clientList.end(); ++itor)
	{
		pClient = (*itor);

		// Client has no data, skip client
		if ( pClient->m_outList.empty() )
			continue;

		bool packetSent = false;
		int sendOfs = 0;

		// first, load up address structs with getaddrinfo()
		ADDRINFOT hints;
		std::memset( &hints, 0, sizeof(hints) );
		hints.ai_family = AF_UNSPEC;			// use IPv4 or IPv6, whichever
		hints.ai_socktype = SOCK_DGRAM;
		hints.ai_flags = AI_PASSIVE;			// fill in my IP for me

		ADDRINFOT* pResult = 0;
		String port = ToString(pClient->m_port);	
		GetAddrInfo( pClient->m_ipAddress.c_str(), port.c_str(), &hints, &pResult );

		for (PADDRINFOT p = pResult; p != NULL; p = pResult->ai_next)
		{
			CClient::PacketList& outList = pClient->m_outList;
			do
			{								
				CClient::PacketItor itor = outList.begin();
				IPacketPtr pIPacket = *itor;
				const char* pData = pIPacket->GetData();
				int length = (int)pIPacket->GetSize();

				// For message-oriented sockets (address family of AF_INET or AF_INET,
				// type of SOCK_DGRAM, and protocol of IPPROTO_UDP, for example), care
				// must be taken not to exceed the maximum packet size of the underlying provider
				int optVal = 0;
				int optLen = sizeof(int);
				if ( getsockopt(m_socket, SOL_SOCKET, SO_MAX_MSG_SIZE, (char*)&optVal, &optLen) == SOCKET_ERROR )
				{
					FreeAddrInfo(pResult);
					Cout << ieS("Problem with determining max message size of the provider") << std::endl;
					NETPrintError();
					return;
				}
				if (length > optVal)
				{
					FreeAddrInfo(pResult);
					Cout << ieS("Packet is bigger than the SO_MAX_MSG_SIZE of the socket") << std::endl;
					return;
				}

				// For message-oriented sockets (address family of AF_INET or AF_INET,
				// type of SOCK_DGRAM, and protocol of IPPROTO_UDP, for example), care
				// must be taken not to exceed the maximum packet size of the underlying provider
				int result = sendto(m_socket, pData+sendOfs, length-sendOfs, 0, p->ai_addr, p->ai_addrlen);
				if (result == SOCKET_ERROR)
				{
					FreeAddrInfo(pResult);
					Cout << ieS("sendto() SOCKET_ERROR failed with error: ") << WSAGetLastError() << std::endl;
					NETPrintError();
					packetSent = false;
					return;
				}
				else if (result >= 0)
				{
					sendOfs += result; // bytes sent, increase the offset.
					packetSent = true;
				}
				else
				{
					packetSent = false;
				}

				NETPrintError();

				OStringStream oss;
				oss << ieS("Sending packet to: ") << pClient->m_ipAddress << ieS("::") ;
				oss << pClient->m_port << std::endl;
				Cout << oss.str();

				// if the amount of sent data has reace the same length of that of the packet
				// size, then pop the top packet and send the next one.
				if ( sendOfs == length )
				{		
					outList.pop_front();
					sendOfs = 0; // reset the sent offset
				}

			} while( packetSent && !outList.empty() );

		} // for
	} // for

}


/*
void CUDPListenSocket::ProcessIncoming(Int8Ptr pData, int bytesRecv, PacketList& inlist)
{
// Current size in buffer + next lot of bytes recieved
int newData = bytesRecv;
int packetSize = 0;
int processedData = 0;
bool pktRecieved = false;

while ( newData > (sizeof(u_short) * 2) )
{
// measured offset of the base header
size_t dataOffset = 0;

// extract packet size from data.
packetSize = *( reinterpret_cast<u_short *>(&pData[0] + dataOffset) );
packetSize = static_cast<int>( ntohs(packetSize) );

// increase the offset by 2 bytes
dataOffset += sizeof(u_short);

// Don't have enough new data to grab the next packet
if (packetSize > newData)
{
// Throw the recieved data away
m_recvBuf.clear();
break;
}

if (packetSize > MAX_PACKET_SIZE)
{
// prevent nasty buffer overruns!
SetDeleteFlag(DF_DESTROY, true);
return;
}

// we know how big the packet is...and we have the whole thing
if (newData >= packetSize)
{
// Advance the pointer along 2 bytes to get the type
u_short sType = ntohs( *( reinterpret_cast<u_short *>(&pData[0] + dataOffset) ) );
IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

// increase the offset by 2 bytes
dataOffset += sizeof(u_short);

switch (type)
{
case IPacket::PT_BINARY:
{
// we know how big the packet is...and we have the whole thing
CBinaryPacketPtr pkt = CBinaryPacketPtr(new CBinaryPacket( &pData[dataOffset], packetSize - dataOffset ));
inlist.push_back(pkt);
pktRecieved = true;
processedData += packetSize;
newData -= packetSize;
}
break;
case IPacket::PT_TEXT:
{
// Convert the recieved text to generic form (Unicode or Windows Ansi)
String recievedText;
recievedText.resize( packetSize - dataOffset ); // Dont need the packet size or its type. Also no null character needed
AnsiToGeneric( &recievedText[0], &pData[dataOffset], packetSize - dataOffset, packetSize - dataOffset );

if ( !recievedText.empty() )
{			
// push back the text packet
CTextPacketPtr pkt = CTextPacketPtr( new CTextPacket( recievedText ) );
inlist.push_back(pkt);
pktRecieved = true;
processedData += packetSize;
newData -= packetSize;
}			
}
break;
case IPacket::PT_SAFEBINARY:
{
// 4 bytes in : to get the sequence num
u_short sequenceNum = *( reinterpret_cast<u_short *>(&pData[0] + dataOffset) );
sequenceNum = ntohs(sequenceNum);

// offset is 2 more bytes
dataOffset += sizeof(u_short);

// 6 bytes in : to get the ack
u_long ack = *( reinterpret_cast<u_long *>(&pData[0] + dataOffset) );
ack = ntohl(sequenceNum);

// offset is 4 more bytes
dataOffset += sizeof(u_long);

// 10 bytes in : to get the ack bit field
u_long ack_bit = *( reinterpret_cast<u_long *>(&pData[0] + dataOffset) );
ack_bit = ntohl(sequenceNum);

CSafeBinaryPacketPtr pkt = CSafeBinaryPacketPtr ( 
new CSafeBinaryPacket( &pData[dataOffset], packetSize - dataOffset, sequenceNum, ack, ack_bit) );

inlist.push_back(pkt);

pktRecieved = true;
processedData += packetSize;
newData -= packetSize;
}
break;
default:
assert( false && ieS("Unkown packet type") );
break;

} // switch

} // if newData >= packetSize

} // while newData > (sizeof(u_short) * 2)

}
*/
/*

CUDPListenSocket::CUDPListenSocket(CNetworkPtr pNetMgr, const String& portNum) :
CUDPSocket(),
m_pNetMgr(pNetMgr),
m_port(portNum)
{
	Initialise(m_port);
}

CUDPListenSocket::~CUDPListenSocket()
{

}

void CUDPListenSocket::Initialise(const String& portNum)
{
	if ( UDP::NETBind(m_socket, portNum) )
	{
		assert(false && ieS("Unable to setup a listen socket on TCP") );
		closesocket(m_socket);
		std::exit(1);
	}
}

void CUDPListenSocket::HandleInput(IEventManagerPtr pEevntMgr)
{
	m_recvBuf.resize( MAX_PACKET_SIZE );
	Int8Ptr pRecBuff = &m_recvBuf[0];

	SOCKADDR_STORAGE sockStorage;
	std::memset( &sockStorage, 0, sizeof(SOCKADDR_STORAGE) );
	int senderSize = sizeof(SOCKADDR_STORAGE);

	int result = recvfrom(m_socket, pRecBuff, RECV_BUFFER_SIZE, 0, (SOCKADDR *)&sockStorage, &senderSize);
	if (result == SOCKET_ERROR)
	{
		Cout << "recvfrom() SOCKET_ERROR failed with error: " << WSAGetLastError() << std::endl;
		NETPrintError();
		return;
	}

	// Process the incoming packt and see if its a connect packet
	bool loginPacket = ProcessIncoming(pRecBuff, result);

	if (loginPacket)
	{
		String clientAddress;
		clientAddress.resize( (sockStorage.ss_family == AF_INET) ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN );
		String::pointer pStr = &clientAddress[0];
		NETGetIpStr( (SOCKADDR *)&sockStorage, pStr, clientAddress.capacity() );

		String port;
		NETGetPortStr((SOCKADDR *)&sockStorage, port);

		// We know where the connect socket is, now connect a UDP socket to use send() / recv() with.
		SOCKET newSocket = 0;
		if ( UDP::NETBind(newSocket, port) != 0 )
			Cout << ieS("Unable to bind a UDP socket for new client: ") << clientAddress << std::endl;

		if ( UDP::NETConnectSock(newSocket, clientAddress, port) != 0 )
			Cout << ieS("Unable to connect a UDP socket for new client: ") << clientAddress << std::endl;

		if (newSocket != INVALID_SOCKET)
		{
			CUDPEventSocketPtr pUDPEvent = CUDPEventSocketPtr( new CUDPEventSocket(newSocket) );
			UInt32 socketID = m_pNetMgr->AddSocket(pUDPEvent);

			bool result = pEevntMgr->QueueEvent( IEventDataPtr( new SEvtData_Remote_Client(socketID, clientAddress) ) );
			if (!result)
			{
				m_recvBuf.clear();
				assert( false && ieS("Unable to queue SEvtData_Remote_Client event") );
				return;
			}
		}
	}

	m_recvBuf.clear();
}

bool CUDPListenSocket::ProcessIncoming(const Int8* pData, int bytesRecv)
{
	CBinaryPacketPtr pkt = CBinaryPacketPtr();

	// Current size in buffer + next lot of bytes recieved
	int newData = bytesRecv;
	int packetSize = 0;
	int processedData = 0;
	bool pktRecieved = false;

	while ( newData > (sizeof(u_short) * 2) )
	{
		// measured offset of the base header
		size_t dataOffset = 0;

		// extract packet size from data.
		packetSize = *( reinterpret_cast<const u_short *>(pData + dataOffset) );
		packetSize = static_cast<int>( ntohs(packetSize) );

		// increase the offset by 2 bytes
		dataOffset += sizeof(u_short);

		// Don't have enough new data to grab the next packet
		if (packetSize > newData)
		{
			// Throw the recieved data away
			m_recvBuf.clear();
			break;
		}

		if (packetSize > MAX_PACKET_SIZE)
		{
			// prevent nasty buffer overruns!
			SetDeleteFlag(DF_DESTROY, true);
			return false;
		}

		// we know how big the packet is...and we have the whole thing
		if (newData >= packetSize)
		{
			// Advance the pointer along 2 bytes to get the type
			u_short sType = ntohs( *( reinterpret_cast<const u_short *>(pData + dataOffset) ) );
			IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

			// increase the offset by 2 bytes
			dataOffset += sizeof(u_short);

			switch (type)
			{
			case IPacket::PT_BINARY:
				{
					// we know how big the packet is...and we have the whole thing
					pkt = CBinaryPacketPtr(new CBinaryPacket( &pData[dataOffset], packetSize - dataOffset ));
					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;
				}
				break;

			default:
				assert( false && ieS("Unkown packet type") );
				break;

			} // switch

		} // if newData >= packetSize

	} // while newData > (sizeof(u_short) * 2)

	if (pkt)
	{
		// extract data and size from packet
		size_t size = pkt->GetSize();
		size_t offset = pkt->GetOffset();
		const Int8 *pData = (Int8*)pkt->GetData() + offset;

		Int8Array data(size - offset);
		std::memcpy(&data[0], pData, size - offset);

		// Deserialise data		
		// extract messageType type from data.
		offset = 0;
		SocketEvent messageType;
		std::memcpy( &messageType, &data[offset], sizeof(SocketEvent) );

		if (messageType == NetMsg_PlayerLoginRequest)
			return true;
		else
			return false;
	}

	return false;
}

*/