#ifndef __CVIEW_MANAGER_INL__
#define __CVIEW_MANAGER_INL__

inline UInt32 CGameViewManager::GetViewCount() const
{
	return m_gameViewMap.size();
}

inline UInt32 CGameViewManager::GetNewActorID()
{	
	++m_requestedViewCount;
	UInt32 result = m_requestedViewCount + m_gameViewMap.size()-1;
	return m_gameViewMap.size();
}


#endif