#include "logicafx.h"
#include "CameraControllerPiecewise .h"
#include "PiecewiseBezier.h"

#include "Spatial.h"
#include "CameraNodeFPS.h"

using namespace Engine;
using std::vector;


const float CCameraControllerPieceWise::GRAVITY = 9.81f;

CCameraControllerPieceWise::CCameraControllerPieceWise(PieceWiseBezierfPtr pPiecewise, UInt currentCurve, UInt curveSamples, float maxSpeed,
	float moveSpeed, float minSpeed) :
m_pPieceWiseCurve(pPiecewise),
m_currentCurve(currentCurve),
m_curveSamples(curveSamples),
m_maxSpeed(maxSpeed),
m_moveSpeed(moveSpeed),
m_minSpeed(minSpeed),
m_stepSize( 1 / static_cast<float>(m_curveSamples - 1) ),
m_t(0.0f)
{

}


void CCameraControllerPieceWise::AnimateNode(CSpatial* pSpatial, double deltaTime)
{
	// no object to follow
	if (!pSpatial)
		return;

	// The curve to follow
	BezierCurvef curve = m_pPieceWiseCurve->GetCurve(m_currentCurve);
	const Vector3f* pCtrPoints = curve.GetControlPoints();
	
	// Potential energy at start 
	// PE = 1/2 ( m.v^2 ) + ( m.g.h )
	Vector3f curPos = curve.GetPosition(m_t);
	float potentialEnergy = ( 0.5f * Sqr(m_moveSpeed) ) + ( GRAVITY * curPos.y() ); 
	float distanceRemaining = m_moveSpeed * (float)deltaTime;

	bool reset = false;
	while(distanceRemaining > 0.0f)
	{
		m_t += m_stepSize;

		if (m_t > 1.0)		
		{
			// Past the last curve, reset to the first curve
			if ( m_currentCurve == m_pPieceWiseCurve->GetNumCurves()-1 )
			{
				m_t = 0.0f;
				m_currentCurve = 0;
				distanceRemaining = -1.0f;
				reset = true;
			}
			else
			{
				++m_currentCurve;
				curve = m_pPieceWiseCurve->GetCurve(m_currentCurve);
				m_t = 0.0f;
			}
		}

		Vector3f newPos = curve.GetPosition(m_t);
		float distance = Dist(newPos, curPos);
		distanceRemaining -= distance;
		curPos = newPos;
	}

	Vector3f nextPos = curve.GetPosition(m_t);
	if (reset)
	{
		m_moveSpeed = m_minSpeed;
	}
	else
	{
		float finalSpeedSqrd = 2*(potentialEnergy - ( GRAVITY * nextPos.y() ));
		if (finalSpeedSqrd < 0)
			m_moveSpeed = m_minSpeed;
		else
		{
			m_moveSpeed = sqrt(finalSpeedSqrd);

			if (m_moveSpeed > m_maxSpeed)
				m_moveSpeed = m_maxSpeed;

			if (finalSpeedSqrd < m_minSpeed)
				m_moveSpeed = m_minSpeed;
		}
	}

	pSpatial->SetPosition(nextPos);

	// cast the spatial to a camera
	CCameraNodeFPS *pCameraNode = static_cast<CCameraNodeFPS*>(pSpatial);
	
	// Set the object to look along the curve
	Vector3f tangent = curve.GetFirstDerivative(m_t);

	// write target
	Vector3f lookat = nextPos + tangent;
	pCameraNode->SetTarget(lookat); 
}

bool CCameraControllerPieceWise::HandleEvent(const IEventData& event)
{

	return true;
}

float CCameraControllerPieceWise::CurveLength(const BezierCurvef& curve)
{
	float singleLength = 0.0f;
	float t = 0.0f;

	for (UInt i = 0; i < m_curveSamples; ++i, t += m_stepSize)
	{
		if ( IsZero(t) )
			continue;
		else
		{
			Vector3f a = curve.GetPosition(t);
			Vector3f b = curve.GetPosition(t - m_stepSize);
			singleLength += Dist(b, a);
		}
	}

	return singleLength;
}



/*
float CCameraControllerPieceWise::LengthBetweenPoints(const BezierCurvef& curve, float t)
{
	float lengthBetweenPoints = 0.0f;
	static Vector3f previousPoint;

	if ( IsZero(t) )
	{
		previousPoint = curve.GetPosition(t);
		return lengthBetweenPoints;
	}
	else
	{
		Vector3f currentPoint = curve.GetPosition(t);
		lengthBetweenPoints = Dist(currentPoint, previousPoint);
		previousPoint = currentPoint;
	}
	
	return lengthBetweenPoints;
}
*/

/*

*/


/*
void CCameraControllerPieceWise::AnimateNode(CSpatial* pSpatial, double deltaTime)
{
	// no object to follow
	if (!pSpatial)
		return;

	// Past the last curve, reset to the first curve
	if ( m_currentCurve >= m_pPieceWiseCurve->GetNumCurves() )
		m_currentCurve = 0;

	// If t has is equal to or greater than 1.0
	if (m_t >= 1.0)												
		m_t -= 1.0;

	// The curve to follow
	const BezierCurvef& curve = m_pPieceWiseCurve->GetCurve(m_currentCurve);
	const Vector3f* pCtrPoints = curve.GetControlPoints();

	Vector3f curvePos = curve.GetPosition(m_t);
	Vector3f tangent = curve.GetFirstDerivative(m_t);
	tangent.Normalise();
	
	Vector3f xaxis, yaxis, zaxis;

	if (m_firstLoop)
	{
		pSpatial->SetPosition(curvePos);

		// cross product of two tangents for the side vector
		xaxis = Cross(tangent, curve.GetFirstDerivative(m_t + m_stepSize));
		xaxis.Normalise();	

		yaxis = Cross(tangent, xaxis);
		yaxis.Normalise();
		
		Matrix4f cheat( xaxis.x(), yaxis.x(), tangent.x(), 0,
						xaxis.y(), yaxis.y(), tangent.y(), 0,
						xaxis.z(), yaxis.z(), tangent.z(), 0,
						0, 0, 0, 0 );

		pSpatial->SetRotation( cheat.GetRotation() );
		
		m_firstLoop = false;
		return;
	}

	// cross product of two tangents for the side vector
	xaxis = Cross(tangent, curve.GetFirstDerivative(m_t - m_stepSize));
	xaxis.Normalise();	

	yaxis = Cross(tangent, xaxis);
	yaxis.Normalise();

	Matrix4f cheat( xaxis.x(), yaxis.x(), tangent.x(), 0,
					xaxis.y(), yaxis.y(), tangent.y(), 0,
					xaxis.z(), yaxis.z(), tangent.z(), 0,
					0, 0, 0, 0 );

	pSpatial->SetRotation( cheat.GetRotation() );	

	float distance = 0;
	distance += m_moveSpeed * deltaTime;

	// Potential energy at start
	float potentialEnergy = ( 0.5f * Sqr(m_moveSpeed) ) + ( GRAVITY * curvePos.y() ); // pe = 1/2 x m x v^2 + m x g x h

	UInt index = IndexOfLargestValueSmallerThan(m_arcLengths, distance);
	float tt = index / (float) (m_arcLengths.size() - 1);
	// m_t = index / (float) (m_arcLengths.size() - 1);
	m_t += tt;

	Vector3f nextPos = curve.GetPosition(m_t);
	float finalSpeed = 2*(potentialEnergy - ( GRAVITY * nextPos.y() ));


	pSpatial->SetPosition(nextPos);

	// increment along the cuve
	// m_t += m_stepSize;


}

*/

/*

	// no object to follow
	if (!pSpatial)
		return;

	// Past the last curve, reset to the first curve
	if ( m_currentCurve >= m_pPieceWiseCurve->GetNumCurves() )
		m_currentCurve = 0;

	// If t has is equal to or greater than 1.0
	if (m_t >= 1.0)												
		m_t -= 1.0;

	// The curve to follow
	const BezierCurvef& curve = m_pPieceWiseCurve->GetCurve(m_currentCurve);
	const Vector3f* pCtrPoints = curve.GetControlPoints();

	// get the target arcLength for curve for parameter m_t
	float targetArcLength = m_t * m_arcLengths.back();

	// the next function would be a binary search, for efficiency
	UInt index = IndexOfLargestValueSmallerThan(m_arcLengths, targetArcLength);

	// if exact match, return t based on exact index
	if (m_arcLengths[index] == targetArcLength)
	{
		m_t = index / (float) ( m_arcLengths.back() );
	}
	else  // need to interpolate between two points
	{
		float lengthBefore = m_arcLengths[index];
		float lengthAfter = m_arcLengths[index+1];
		float segmentLength = lengthAfter - lengthBefore;

		// determine where we are between the 'before' and 'after' points.
		float segmentFraction = (targetArcLength - lengthBefore) / segmentLength;

		// add that fractional amount to t 
		m_t = (index + segmentFraction) / (float) ( m_arcLengths.back() );
	}

	Vector3f pos = curve.GetPosition(m_t);
	Vector3f tangent = curve.GetFirstDerivative(m_t);
	tangent.Normalise();

	Vector3f xaxis, yaxis, zaxis;

	if (m_firstLoop)
	{
		pSpatial->SetPosition(pos);

		// cross product of two tangents for the side vector
		xaxis = Cross(tangent, curve.GetFirstDerivative(m_t + m_stepSize));
		xaxis.Normalise();	

		yaxis = Cross(tangent, xaxis);
		yaxis.Normalise();
		
		Matrix4f cheat( xaxis.x(), yaxis.x(), tangent.x(), 0,
						xaxis.y(), yaxis.y(), tangent.y(), 0,
						xaxis.z(), yaxis.z(), tangent.z(), 0,
						0, 0, 0, 0 );

		pSpatial->SetRotation( cheat.GetRotation() );
		
		m_firstLoop = false;
		return;
	}

	// cross product of two tangents for the side vector
	xaxis = Cross(tangent, curve.GetFirstDerivative(m_t - m_stepSize));
	xaxis.Normalise();	

	yaxis = Cross(tangent, xaxis);
	yaxis.Normalise();

	Matrix4f cheat( xaxis.x(), yaxis.x(), tangent.x(), 0,
					xaxis.y(), yaxis.y(), tangent.y(), 0,
					xaxis.z(), yaxis.z(), tangent.z(), 0,
					0, 0, 0, 0 );

	pSpatial->SetRotation( cheat.GetRotation() );	
	pSpatial->SetPosition(pos);

	// increment along the cuve
	m_t += m_stepSize;

*/