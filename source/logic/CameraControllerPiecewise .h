#ifndef CCAMERA_CONTROLLER_PIECEWISE_CURVE_H
#define CCAMERA_CONTROLLER_PIECEWISE_CURVE_H

_ENGINE_BEGIN


class CCameraControllerPieceWise : public ISceneController
{
public:
	CCameraControllerPieceWise( PieceWiseBezierfPtr pPiecewise, UInt currentCurve=0, UInt curveSamples=1000U, float maxSpeed=0.0025f,
		float moveSpeed=0.0008f, float minSpeed = 0.0008f );

	void AnimateNode(CSpatial* pSpatial, double deltaTime);

	bool HandleEvent(const IEventData& event);

protected:
	// Return the length of a single curve
	float CurveLength(const BezierCurvef& curve);

	PieceWiseBezierfPtr m_pPieceWiseCurve;
	
	UInt m_currentCurve;
	UInt m_curveSamples;

	float m_maxSpeed;
	float m_moveSpeed;
	float m_minSpeed;
	float m_stepSize;  // float stepsize = 1 / static_cast<float>(curveSamples - 1);
	float m_t;		   // value of t on the curve

	static const float GRAVITY;

};

_ENGINE_END


#endif