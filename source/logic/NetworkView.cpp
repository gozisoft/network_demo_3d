#include "logicafx.h"
#include "NetworkView.h"
#include "NetworkViewListener.h"
#include "NetworkManager.h"
#include "GameEvents.h"
#include "EventManager.h"
#include "Packet.h"
#include "StringFunctions.h"

using namespace Engine;



CNetworkView::CNetworkView(IEventManagerPtr pEventMgr, CServerNetoworkPtr pNetwork, UInt32 socketID):
m_pEventMgr(pEventMgr),
m_pNetwork(pNetwork),
m_sockID(socketID)
{

}

CNetworkView::~CNetworkView()
{

}


void CNetworkView::OnCreate(GameViewID viewID, ActorID actorID)
{
	// Register a listener for this view
	m_pNetworkListener = 
		CNetworkViewListenerPtr( new CNetworkViewListener(m_pNetwork, m_sockID, actorID) );

	// Add events that effect the network view
	// pEventMgr->AddListener(pListener, SEvtData_New_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pNetworkListener, SEvtData_Spawn_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pNetworkListener, SEvtData_Destroy_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pNetworkListener, SEvtData_Move_Actor::sk_EventType);

	// assign the viewID and actorID
	m_gameViewID = viewID;
	m_actorID = actorID;
	SocketEvent messageType = NetMsg_PlayerLoginOk;

	// serialise data
	using std::stringstream;
	stringstream oss( stringstream::in | stringstream::out | stringstream::binary );

	// 1) messageType
	// 2) m_gameViewID
	// 3) actorID
	oss.write( (char*)&messageType, sizeof(SocketEvent) );
	oss.write( (char*)&m_gameViewID, sizeof(GameViewID) );
	oss.write( (char*)&m_actorID, sizeof(ActorID) );

	// get length of file:
	oss.seekg (0, stringstream::end);
	size_t length = static_cast<size_t>( oss.tellg() );
	oss.seekg (0, stringstream::beg);

	// read it into a vector
	using std::vector;
	vector<char> buffer( length );
	oss.read( &buffer[0], length );

	CBinaryPacketPtr pPacket( new CBinaryPacket( &buffer[0], buffer.size() ) );
	m_pNetwork->Send(m_gameViewID, pPacket);

	// Now send world data
	
}

void CNetworkView::OnRestore()
{


}

void CNetworkView::OnLostDevice()
{

}

void CNetworkView::OnUpdate(double time, double deltaTime)
{

}

void CNetworkView::OnRender(double time, double deltaTime)
{

}

void CNetworkView::OnWindowEvent(const CWinEvent& event)
{

}

CNetworkView::GameViewType CNetworkView::GetType() const
{
	return GameViewType("CNetworkView");
}

CNetworkView::GameViewID CNetworkView::GetId() const
{ 
	return m_gameViewID;
}

void CNetworkView::OnUpdate(const IEventData& worldEvent)
{
	SocketEvent socketEventType = NetMsg_Event;
	ULong eventHash = worldEvent.GetEventType().GetHashValue();

	// serialise data
	using std::stringstream;
	stringstream ss( stringstream::in | stringstream::out | stringstream::binary );

	// 1) socketevent type (NetMsg_Event)
	// 2) event type (hash value)
	// 3) event data
	ss.write( (char*)&socketEventType, sizeof(SocketEvent) );
	ss.write( (char*)&eventHash, sizeof(ULong) );
	worldEvent.Serialise(ss);

	// get length of file:
	ss.seekg (0, stringstream::end);
	size_t length = static_cast<size_t>( ss.tellg() );
	ss.seekg (0, stringstream::beg);

	// read it into a vector
	using std::vector;
	vector<char> buffer(length);
	ss.read( &buffer[0], length );	

	CBinaryPacketPtr pPacket( new CBinaryPacket( &buffer[0], buffer.size() ) );
	m_pNetwork->Send(m_sockID, pPacket);
}