#ifndef __COBJECT_LISTENER_H__
#define __COBJECT_LISTENER_H__

#include "ISceneController.h"
#include "IActor.h"
#include "Matrix.h"

_ENGINE_BEGIN

// This class acts as a state change detection controller
// for any scene controller. The purpose of this is to act as an
// intermediery class that will dispatch game specifc events
// to the rest of application. Specifically in this case the network
class CObjectListener : public ISceneController
{
public:
	CObjectListener(IEventManagerPtr pEventMgr, IActor::ActorID actorID, ULong intervalTime = 100); // 10 ms default

	// Checks for node movement on the event of a key press or mouse press
	void AnimateNode(CSpatial* pSpatial, double deltaTime);

	// Does nothing
	bool HandleEvent(const IEventData& event);

private:
	// Event managaer instance
	IEventManagerPtr pEventMgr;

	// previous frames positional data for the spatial
	Matrix4f m_prevWorld;

	// Acumulate time and determine delta time *used for networking
	ULong m_intervalTime;
	ULong m_prevTimeSent;

	// actorID of the player 
	IActor::ActorID m_actorID;

};




_ENGINE_END


#endif