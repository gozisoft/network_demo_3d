#ifndef __CNETWORK_VIEW_H__
#define __CNETWORK_VIEW_H__


#include "IGameView.h"
#include "GameEvents.h"

namespace Engine
{


	class CNetworkView : public IGameView
	{
	public:
		typedef std::vector<SEvtData_World_Update> WorldDataArray;

		CNetworkView(IEventManagerPtr pEventMgr, CServerNetoworkPtr pNetwork, UInt32 socketID);
		~CNetworkView();

		void OnCreate(GameViewID viewID, ActorID actorID);
		void OnRestore();
		void OnLostDevice();
		void OnUpdate(double time, double deltaTime);
		void OnRender(double time, double deltaTime);
		void OnWindowEvent(const CWinEvent& event);
		GameViewType GetType() const;
		GameViewID GetId() const;


		void OnUpdate(const IEventData& worldEvent);

	private:
		void CreateScene();

		IEventManagerPtr m_pEventMgr;
		CServerNetoworkPtr m_pNetwork;
		CNetworkViewListenerPtr m_pNetworkListener; 
		UInt32 m_sockID;

		GameViewID m_gameViewID;
		ActorID m_actorID;

	private:
		DECLARE_HEAP;

	};


}

#endif