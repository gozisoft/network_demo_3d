#ifndef __ACTORS_H__
#define __ACTORS_H__

#include "IActor.h"


_ENGINE_BEGIN

// Set when no view is attched to an actor
const UInt32 VIEWID_NO_VIEW_ATTACHED = 0;
// Set when a view is attached to an actor
const UInt32 VIEW_ATTACHED = 1;

class CBaseActor : public IActor
{
public:
	CBaseActor();
	CBaseActor(const SActorParams& params);
	virtual ~CBaseActor();

	const SActorParams* GetParams() const;
	SActorParams* GetParams();
	
	
protected:
	SActorParams* m_actorParams;

private:
	DECLARE_HEAP;
};

class CCubeActor : public CBaseActor
{
public:
	CCubeActor();
	CCubeActor(const SCubeParams& params);

private:
	DECLARE_HEAP;
};

namespace ActorToMesh
{
	CSpatialPtr CreateGameMesh(const IActorPtr& pActor);
	CSpatialPtr CreateGameMesh(const SActorParams* pParams);
}


#include "Actors.inl"


_ENGINE_END



#endif