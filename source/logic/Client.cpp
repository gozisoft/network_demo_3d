#include "logicafx.h"
#include "Client.h"
#include "Packet.h"
#include "Socket.h"

#include "IEventManager.h"
#include "StringFunctions.h"

using namespace Engine;

DEFINE_HEAP(CClient, "CClient");

CClient::CClient(UInt32 clientID, const String& ipAddress) :
m_ipAddress(ipAddress),
m_clientID(clientID),
m_port(0),
m_sendOfs(0),
m_timeOut(0)
{

}

CClient::CClient(UInt32 clientID, const String& ipAddress, UInt16 port) :
m_ipAddress(ipAddress),
m_clientID(clientID),
m_port(port),
m_sendOfs(0),
m_timeOut(0)
{


}

IPacketPtr CClient::ProcessIncoming(const char* pData, int bytesRecv)
{
	IPacketPtr pPacketOut = IPacketPtr();

	// Current size in buffer + next lot of bytes recieved
	int newData = bytesRecv;
	int packetSize = 0;
	int processedData = 0;
	bool pktRecieved = false;

	while ( newData > (sizeof(u_short) * 2) )
	{
		// measured offset of the base header
		size_t dataOffset = 0;

		// extract packet size from data.
		packetSize = *( reinterpret_cast<const u_short *>( &pData[dataOffset] ) );
		packetSize = static_cast<int>( ntohs(packetSize) );

		// increase the offset by 2 bytes
		dataOffset += sizeof(u_short);

		// Don't have enough new data to grab the next packet
		if (packetSize > newData)
		{
			// Throw the recieved data away
			m_recvBuf.clear();
			break;
		}

		if (packetSize > CSocket::MAX_PACKET_SIZE)
		{
			// prevent nasty buffer overruns!
			// SetDeleteFlag(DF_DESTROY, true);
			return pPacketOut;
		}

		// we know how big the packet is...and we have the whole thing
		if (newData >= packetSize)
		{
			// Advance the pointer along 2 bytes to get the type
			u_short sType = ntohs( *( reinterpret_cast<const u_short *>( &pData[dataOffset] ) ) );
			IPacket::PacketType type = static_cast<IPacket::PacketType>(sType);

			// increase the offset by 2 bytes
			dataOffset += sizeof(u_short);

			switch (type)
			{
			case IPacket::PT_BINARY:
				{
					// we know how big the packet is...and we have the whole thing
					CBinaryPacketPtr pkt = CBinaryPacketPtr(new CBinaryPacket( &pData[dataOffset], packetSize - dataOffset ));
					m_inList.push_back(pkt);
					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;
					pPacketOut = pkt;
				}
				break;
			case IPacket::PT_TEXT:
				{
					// Convert the recieved text to generic form (Unicode or Windows Ansi)
					String recievedText;
					recievedText.resize( packetSize - dataOffset ); // Dont need the packet size or its type. Also no null character needed
					AnsiToGeneric( &recievedText[0], &pData[dataOffset], packetSize - dataOffset, packetSize - dataOffset );

					if ( !recievedText.empty() )
					{			
						// push back the text packet
						CTextPacketPtr pkt = CTextPacketPtr( new CTextPacket( recievedText ) );
						m_inList.push_back(pkt);
						pktRecieved = true;
						processedData += packetSize;
						newData -= packetSize;
						pPacketOut = pkt;
					}			
				}
				break;
			case IPacket::PT_SAFEBINARY:
				{
					// 4 bytes in : to get the sequence num
					u_short sequenceNum = *( reinterpret_cast<const u_short *>( &pData[dataOffset] ) );
					sequenceNum = ntohs(sequenceNum);

					// offset is 2 more bytes
					dataOffset += sizeof(u_short);

					// 6 bytes in : to get the ack
					u_long ack = *( reinterpret_cast<const u_long *>( &pData[dataOffset] ) );
					ack = ntohl(sequenceNum);

					// offset is 4 more bytes
					dataOffset += sizeof(u_long);

					// 10 bytes in : to get the ack bit field
					u_long ack_bit = *( reinterpret_cast<const u_long *>( &pData[dataOffset] ) );
					ack_bit = ntohl(sequenceNum);

					CSafeBinaryPacketPtr pkt = CSafeBinaryPacketPtr ( 
						new CSafeBinaryPacket( &pData[dataOffset], packetSize - dataOffset, sequenceNum, ack, ack_bit) );

					m_inList.push_back(pkt);

					pktRecieved = true;
					processedData += packetSize;
					newData -= packetSize;

					pPacketOut = pkt;
				}
				break;
			default:
				assert( false && ieS("Unkown packet type") );
				break;

			} // switch

		} // if newData >= packetSize

	} // while newData > (sizeof(u_short) * 2)

	return pPacketOut;
}

void CClient::ProcessPackets(const IEventManagerPtr& pEventMgr)
{
	while( !m_inList.empty() )
	{
		IPacketPtr pPacket = *m_inList.begin();
		m_inList.pop_front();

		// extract data and size from packet
		size_t size = pPacket->GetSize();
		size_t offset = pPacket->GetOffset();
		const char* pData = pPacket->GetData();

		std::stringstream ss( std::ios_base::in | std::ios_base::out | std::ios_base::binary );
		ss.write( &pData[offset], size - offset);

		// Deserialise data		
		// extract messageType type from data.
		SocketEvent messageType;
		ss.read( (char*)&messageType, sizeof(SocketEvent) );

		switch (messageType)
		{
		case NetMsg_Event:
			{
				ULong eventHash = 0;
				ss.read( (char*)&eventHash, sizeof(ULong) );

				if ( SEvtData_World_Update::sk_EventType.GetHashValue() == eventHash )
					pEventMgr->QueueEvent( shared_ptr<SEvtData_World_Update>( new SEvtData_World_Update(ss) )  );

				else if ( SEvtData_Spawn_Request::sk_EventType.GetHashValue() == eventHash )
					pEventMgr->QueueEvent( shared_ptr<SEvtData_Spawn_Request>( new SEvtData_Spawn_Request(ss) )  );
				
				else if ( SEvtData_Spawn_Actor::sk_EventType.GetHashValue() == eventHash )
					pEventMgr->QueueEvent( shared_ptr<SEvtData_Spawn_Actor>( new SEvtData_Spawn_Actor(ss) )  );
			
				else if ( SEvtData_Request_Move_Actor::sk_EventType.GetHashValue() == eventHash )
					pEventMgr->QueueEvent( shared_ptr<SEvtData_Request_Move_Actor>( new SEvtData_Request_Move_Actor(ss) )  );
				
				else if ( SEvtData_Move_Actor::sk_EventType.GetHashValue() == eventHash )
					pEventMgr->QueueEvent( shared_ptr<SEvtData_Move_Actor>( new SEvtData_Move_Actor(ss) )  );

				else if ( SEvtData_New_Actor::sk_EventType.GetHashValue() == eventHash )
					pEventMgr->QueueEvent( shared_ptr<SEvtData_New_Actor>( new SEvtData_New_Actor(ss) )  );
				
			}
			break;

		case NetMsg_PlayerLoginRequest:
			break;

		case NetMsg_PlayerLoginOk:
			{
				IGameView::GameViewID viewID = 0;
				ss.read( (char*)&viewID,  sizeof(IGameView::GameViewID) );
				IActor::ActorID actorID = 0;
				ss.read( (char*)&actorID,  sizeof(IActor::ActorID) );
								
				// Queue Player assign event
				IEventDataPtr pEvent( new SEvtData_Network_Player_Actor_Assignment(actorID, viewID) );
				pEventMgr->QueueEvent(pEvent);
			}
			break;
		};

	}
}

/*
void CClient::ProcessPackets(const IEventManagerPtr& pEventMgr)
{
	while( !m_inList.empty() )
	{
		IPacketPtr pPacket = *m_inList.begin();
		m_inList.pop_front();

		// extract data and size from packet
		size_t size = pPacket->GetSize();
		size_t offset = pPacket->GetOffset();
		const char* pData = (Int8*)pPacket->GetData() + offset;

		CharVector data(size - offset);
		std::memcpy(&data[0], pData, size - offset);

		// Deserialise data		
		// extract messageType type from data.
		offset = 0;
		SocketEvent messageType;
		std::memcpy( &messageType, &data[offset], sizeof(SocketEvent) );
		offset += sizeof(SocketEvent);

		switch (messageType)
		{
		case NetMsg_Event:
			break;

		case NetMsg_PlayerLoginRequest:
			break;

		case NetMsg_PlayerLoginOk:
			{
				IGameView::GameViewID viewID = 0;
				std::memcpy( &viewID, &data[offset], sizeof(IGameView::GameViewID) );
				offset += sizeof(IGameView::GameViewID);

				IActor::ActorID actorID = 0;
				std::memcpy( &actorID, &data[offset], sizeof(IActor::ActorID) );
				
				// Queue Player assign event
				IEventDataPtr pEvent( new SEvtData_Network_Player_Actor_Assignment(actorID, viewID) );
				pEventMgr->QueueEvent(pEvent);
			}
			break;
		};

	}
}
*/