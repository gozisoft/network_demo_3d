#include "logicafx.h"
#include "NetworkEvents.h"
#include "StringFunctions.h"

using namespace Engine;


DEFINE_HIERARCHICALHEAP(SEvtData_Connect, "SEvtData_Connect", "IEventData");

const SEvtData_Connect::EventType SEvtData_Connect::sk_EventType("SEvtData_Connect");
