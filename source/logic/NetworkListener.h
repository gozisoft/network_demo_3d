#ifndef __CNETWORK_LISTENER_H__
#define __CNETWORK_LISTENER_H__

#include "IEventReciever.h"

namespace Engine
{
	class CNetworkListener : public IEventListener
	{
	public:
		explicit CNetworkListener(CBaseNetworkManagerPtr pNetMgr, UInt32 sockID);
		~CNetworkListener();

		bool HandleEvent(const IEventData& event);

	private:
		CBaseNetworkManagerPtr m_pNetworkMgr;
		UInt32 m_sockID;
	};

	class CReliableNetworkListener : public IEventListener
	{
	public:
		explicit CReliableNetworkListener(UInt32 sockID);
		~CReliableNetworkListener();

		bool HandleEvent(const IEventData& event);

	private:
		UInt32 m_sockID;

	};

}


#endif