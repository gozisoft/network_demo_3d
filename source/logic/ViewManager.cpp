#include "logicafx.h"
#include "ViewManager.h"
#include "StringFunctions.h"

using namespace Engine;

DEFINE_HEAP(CGameViewManager, "CGameViewManager");


CGameViewManager::CGameViewManager() : 
m_actualViewCount(0),
m_requestedViewCount(0)
{


}

CGameViewManager::~CGameViewManager()
{


}

bool CGameViewManager::AddGameViewSingle(IGameViewPtr pView, ActorID actorID)
{
	UInt32 viewId = m_gameViewMap.size();
	IGameViewMap::iterator itor = m_gameViewMap.find(viewId);

	if ( itor == m_gameViewMap.end() )
	{
		// Build a new table and asscoiate it with a view id
		IGameViewMapEnt mapEntryPair = IGameViewMapEnt( viewId, IGameViewTable() );
		IGameViewMapIRes elmIRes = m_gameViewMap.insert(mapEntryPair);

		// Count not insert into map
		if ( !elmIRes.second )
			return false;

		// Should not be possible, how did we insert and create an empty table.
		if ( elmIRes.first == m_gameViewMap.end() )
			return false;

		// Store the result in order to update the mapped list next...
		itor = elmIRes.first; 
	} 

	// Check for duplicate addition of views.
	IGameViewTable &igvTable = (*itor).second;
	for (IGameViewTable::iterator it = igvTable.begin(); it != igvTable.end(); ++it)
	{
		if ( *it == pView || (*it)->GetType() == pView->GetType() )
			return false;
	}

	pView->OnCreate(viewId, actorID);
	pView->OnRestore();
	igvTable.push_back(pView);

	return true;
}

bool CGameViewManager::AddGameViewGroup(const IGameViewTable& table, ActorID actorID)
{
	UInt32 viewId = m_gameViewMap.size();
	IGameViewMap::iterator itor = m_gameViewMap.find(viewId);

	if ( itor == m_gameViewMap.end() )
	{
		// Build a new table and asscoiate it with a view id
		IGameViewMapEnt mapEntryPair = IGameViewMapEnt( viewId, table );
		IGameViewMapIRes elmIRes = m_gameViewMap.insert(mapEntryPair);

		// Count not insert into map
		if ( !elmIRes.second )
			return false;

		// Should not be possible, how did we insert and create an empty table.
		if ( elmIRes.first == m_gameViewMap.end() )
			return false;

		// Store the result in order to update the mapped list next...
		itor = elmIRes.first; 
	} 

	IGameViewTable &igvTable = (*itor).second;
	for (IGameViewTable::iterator it = igvTable.begin(); it != igvTable.end(); ++it)
	{
		(*it)->OnCreate(viewId, actorID);
		(*it)->OnRestore();
	}

	return true;
}


const CGameViewManager::IGameViewTable& CGameViewManager::GetViewTable(UInt32 viewID)
{
	IGameViewMap::iterator itor = m_gameViewMap.find(viewID);
	if ( itor != m_gameViewMap.end() )
	{
		return (*itor).second;
	}
	else
	{
		assert( false && ieS("Value not in ViewMap") );
		return IGameViewTable();
	}
}