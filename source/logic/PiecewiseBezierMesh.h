#ifndef CPIECEWISE_BEIZERCURVE_MESH_H
#define CPIECEWISE_BEIZERCURVE_MESH_H

#include "PiecewiseBezier.h"

_ENGINE_BEGIN


class CPiecewiseBezierMesh
{
public:
	typedef std::vector<BezierCurvef> bezierArray;

	
	CPiecewiseBezierMesh(const bezierArray& curves);
	CPiecewiseBezierMesh(const BezierCurvef* pCurves, size_t numCurves);

	// Used for adding an array of curves
	void AddBezierC0(const BezierCurvef* pCurves, size_t numCurves);

	// Used for adding a single curve
	void AddBezierC0(const BezierCurvef& curve);

	// use to update all the meshes
	void Update(double deltaTime);

	// Handle mouse clicks
	bool HandleEvent(const IEventData& event);

	// Insert curves into scene
	void InsertToScene(CRootNodePtr& pScene);

	// Access to the piecewise curve
	PieceWiseBezierfPtr GetPieceWiseCurve();

private:
	typedef std::pair<const BezierCurvef*, CBezierCurveMeshPtr> CurveMeshPair;
	typedef std::vector<CurveMeshPair> CurveMeshArray;

	// Piece wise cruve
	PieceWiseBezierfPtr m_pPieceWiseCurve;
	// Array of curve mesh's
	CurveMeshArray m_curveMeshes;

	// looking for right clicks
	bool m_mouseButtons[2];

};



#include "PiecewiseBezierMesh.inl"



_ENGINE_END



#endif