#ifndef __ACTORS_INL__
#define __ACTORS_INL__


inline const SActorParams* CBaseActor::GetParams() const
{
	return m_actorParams;
}


inline SActorParams* CBaseActor::GetParams()
{
	return m_actorParams;
}



#endif