#include "logicafx.h"
#include "BaseView.h"

// Components
#include "IRenderer.h"
#include "IEventManager.h"

// Scenegraph
#include "RootNode.h"
#include "Culler.h"
#include "CameraControllerFPS.h"

// Events
#include "WinMsgEvent.h"

#include "StringFunctions.h"

namespace Engine
{
	namespace GameEvents
	{
		// GameViewEvents
		// 1) new actor
		// 2) destroy actor
		// 3) move actor
		// 4) rotate actor
		// 5) scale actor
		void ListenForGameViewEvents(IEventManagerPtr pEventMgr, IEventListenerPtr pListener)
		{
			pEventMgr->AddListener(pListener, SEvtData_New_Actor::sk_EventType);
			pEventMgr->AddListener(pListener, SEvtData_Destroy_Actor::sk_EventType);
			pEventMgr->AddListener(pListener, SEvtData_Move_Actor::sk_EventType);
			//pEventMgr->AddListener(pListener, SEvtData_Rotate_Actor::sk_EventType);
			//pEventMgr->AddListener(pListener, SEvtData_Scale_Actor::sk_EventType);
		}

		// WindowEvents
		// 1) key input
		// 2) mouse input
		// 3) window rect change
		void ListenForWindowEvents(IEventManagerPtr pEventMgr, IEventListenerPtr pListener)
		{
			pEventMgr->AddListener(pListener, SKeyEvent::sk_EventType);
			pEventMgr->AddListener(pListener, SMouseEvent::sk_EventType);
			pEventMgr->AddListener(pListener, SWindowSizeEvent::sk_EventType);
		}

		void ListenForGeneralGameEvents(IEventManagerPtr pEventMgr, IEventListenerPtr pListener)
		{
			pEventMgr->AddListener(pListener, SEvtData_Spawn_Request::sk_EventType);
			pEventMgr->AddListener(pListener, SEvtData_Spawn_Actor::sk_EventType);
			pEventMgr->AddListener(pListener, SEvtData_New_Actor::sk_EventType);
			pEventMgr->AddListener(pListener, SEvtData_Destroy_Actor::sk_EventType);
			pEventMgr->AddListener(pListener, SEvtData_Move_Actor::sk_EventType);
		}
	}
}

using namespace Engine;

const float fov = 60.0f;
const float zNear = 1.0f;
const float zFar = 100.0f;

DEFINE_HIERARCHICALHEAP(CHumanView, "CHumanView", "IGameView");


CHumanView::CHumanView(IRendererPtr pRenderer, IMouseControllerPtr pMouse, IEventManagerPtr pEventMgr) :
m_pRenderer(pRenderer),
m_pMouse(pMouse),
m_pEventMgr(pEventMgr)
{
	// Create the camera
	m_pCamera = CCameraPtr( new CCamera() );

	// give the renderer a pointer to the camera
	m_pRenderer->SetCamera(m_pCamera);

	// Create the scene culler
	m_pCuller = CCullerPtr( new CCuller(m_pCamera) );
}

CHumanView::~CHumanView()
{

}

void CHumanView::OnCreate(GameViewID viewID, ActorID actorID)
{
	m_gameViewID = viewID;
	m_actorID = actorID;
}

void CHumanView::OnRestore()
{
	Viewporti view = m_pRenderer->GetViewport();
	float ratio = AspectRatio( Point2i( view.width(), view.height() ) );
	m_pCamera->SetFrustum(fov, ratio, zNear, zFar);
}

void CHumanView::OnLostDevice()
{

}

void CHumanView::OnUpdate(double time, double deltaTime)
{
	UNREFERENCED_PARAMETER(time);
	UNREFERENCED_PARAMETER(deltaTime);



}

void CHumanView::OnRender(double time, double deltaTime)
{
	UNREFERENCED_PARAMETER(time);
	UNREFERENCED_PARAMETER(deltaTime);


}

void CHumanView::OnWindowEvent(const CWinEvent& event)
{
	UNREFERENCED_PARAMETER(event);

}

inline CHumanView::GameViewType CHumanView::GetType() const
{
	return GameViewType("CHumanView");
}