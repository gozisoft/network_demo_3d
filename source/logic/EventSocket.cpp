#include "logicafx.h"
#include "EventSocket.h"
#include "Packet.h"
#include "IEventManager.h"

using namespace Engine;


DEFINE_HIERARCHICALHEAP(CTCPEventSocket, "CTCPEventSocket", "CTCPSocket");
DEFINE_HIERARCHICALHEAP(CUDPEventSocket, "CUDPEventSocket", "CUDPSocket");

CTCPEventSocket::CTCPEventSocket(SOCKET socket) : CTCPSocket(socket)
{


}

CTCPEventSocket::~CTCPEventSocket()
{


}

void  CTCPEventSocket::HandleInput(IEventManagerPtr pEevntMgr)
{


}

void CTCPEventSocket::HandleOutput()
{


}

CUDPEventSocket::CUDPEventSocket() : 
CUDPSocket()
{


}

CUDPEventSocket::CUDPEventSocket(SOCKET sock) : 
CUDPSocket(sock)
{


}

CUDPEventSocket::~CUDPEventSocket()
{


}

void CUDPEventSocket::HandleInput(const IEventManagerPtr& pEventMgr)
{
	CUDPSocket::HandleInput(pEventMgr);

	while( !m_inList.empty() )
	{
		IPacketPtr pPacket = *m_inList.begin();
		m_inList.pop_front();

		// extract data and size from packet
		size_t size = pPacket->GetSize();
		size_t offset = pPacket->GetOffset();
		const char* pData = pPacket->GetData() + offset;

		CharVector data(size - offset);
		std::memcpy(&data[0], pData, size - offset);

		// Deserialise data		
		// extract messageType type from data.
		offset = 0;
		SocketEvent messageType;
		std::memcpy( &messageType, &data[offset], sizeof(SocketEvent) );
		offset += sizeof(SocketEvent);

		switch (messageType)
		{
		case NetMsg_Event:
			break;

		case NetMsg_PlayerLoginOk:
			{
				IGameView::GameViewID viewID = 0;
				std::memcpy( &viewID, &data[offset], sizeof(IGameView::GameViewID) );
				offset += sizeof(IGameView::GameViewID);

				IActor::ActorID actorID = 0;
				std::memcpy( &actorID, &data[offset], sizeof(IActor::ActorID) );
				
				// Queue Player assign event
				IEventDataPtr pEvent( new SEvtData_Network_Player_Actor_Assignment(actorID, viewID) );
				pEventMgr->QueueEvent(pEvent);
			}
		}
	}

}

void CUDPEventSocket::HandleOutput()
{
	CUDPSocket::HandleOutput();

}

void CUDPEventSocket::DecryptEvent(const char* pData)
{




}