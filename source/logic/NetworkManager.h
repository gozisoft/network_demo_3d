#ifndef __CNETWORK_MANGER_H__
#define __CNETWORK_MANGER_H__


#include "logicfwd.h"
#include "framefwd.h"

#include <winsock2.h>
#include <tuple>

namespace Engine
{
	class CBaseNetworkManager
	{
	public:
		typedef std::list<CClientPtr> ClientList;

		virtual ~CBaseNetworkManager();

		bool Initialise();
		void Release();

		// Send to a client based on socket id
		bool Send(UInt32 sockId, IPacketPtr packet);

		// Find a client based on socket id
		CClientPtr FindClient(UInt32 sockId);

		// Search clients for any output
		bool HasOutput(const ClientList& clientList); 

	protected:	
		CBaseNetworkManager();

		WSADATA m_wsaData;
		ClientList m_clients;

	private:
		DECLARE_HEAP;
	};


	class CServerNetowork : public CBaseNetworkManager
	{
	public:
		CServerNetowork(IEventManagerPtr pEventMgr, const String& port);
		~CServerNetowork();

		bool Listen();
		void DoSelect(double pauseMicroSecs, bool handleInput = true);

	private:	
		IEventManagerPtr m_pEventMgr;
		CUDPListenSocketPtr m_UDPsocket;
		CTCPListenSocketPtr m_TCPSocket;
		String m_listenPort;

	private:
		DECLARE_HEAP;

	};


	// CClientSocketManager's job is to create a single socket that attachs
	// to a known server.
	class CClientNetwork : public CBaseNetworkManager
	{
	public:
		CClientNetwork(IEventManagerPtr pEventMgr, const String& hostName, const String& port);
		~CClientNetwork();

		bool Connect();
		void DoSelect(double pauseMicroSecs, bool handleInput = true);

	private:
		IEventManagerPtr m_pEventMgr;
		CUDPListenSocketPtr m_pUDPSocket;	
		String m_ipAddress;
		String m_port;

	private:
		DECLARE_HEAP;

	};

}


#endif



/*

	class CNetwork
	{
	public:
		typedef std::vector<WSAEVENT> EventArray;
		typedef EventArray::iterator EvItor;
		// typedef std::list<CAppSocketPtr> SocketList;
		typedef std::tuple<CTCPSocket, CUDPSocket> SocketTuple;
		typedef std::list<SocketTuple> SocketList;
		typedef SocketList::iterator ScktItor;

		CNetwork();
		virtual ~CNetwork();

		bool Initialise();
		void Release();

		void DoSelect(double currentTime);

		UInt32 AddAppSocket(CAppSocketPtr pAppSocket);
		void RemoveSocket(CAppSocketPtr pAppSocket);

	protected:
		WSADATA m_wsaData;
		WSANETWORKEVENTS NetworkEvents; // For use with WSAWaitForMultipleEvents

		EventArray m_TCPEvents;
		EventArray m_UDPEvents;

		SocketList m_socketList;

		UInt32 m_sktIDCount;
	};

	// CClientSocketManager's job is to create a single socket that attachs
	// to a known server.
	class CClientNetwork : public CNetwork
	{
	public:
		CClientNetwork(const String& hostName, const String& port);
		~CClientNetwork();

		bool Connect();

	private:
		String m_ipAddress;
		String m_port;

	};

	*/


/*
	class CNetwork
	{
	public:
		typedef std::vector<WSAEVENT> EventArray;
		typedef EventArray::iterator EvItor;

		typedef std::tr1::tuple<CTCPSocketPtr, CUDPSocketPtr, UInt32> SocketTuple;

		typedef std::vector<SocketTuple> SocketArray;
		typedef SocketArray::iterator ScktItor;

		CNetwork();
		virtual ~CNetwork();

		bool Initialise();
		void Release();

		void DoSelect(IEventManagerPtr pEevntMgr, double pauseMicroSecs, bool handleInput = true);

		UInt32 AddAppSocket( CTCPSocketPtr pTCPSocket = CTCPSocketPtr(), CUDPSocketPtr pUDPSocket = CUDPSocketPtr() );
		void RemoveSocket(SocketTuple& appSocket);

		String GetIpAddress(UInt32 sockId) const; 

	protected:
		WSADATA m_wsaData;
		WSANETWORKEVENTS NetworkEvents; // For use with WSAWaitForMultipleEvents

		EventArray m_TCPEvents;
		EventArray m_UDPEvents;

		SocketArray m_socketArray;

		UInt32 m_sktIDCount;

	private:
		DECLARE_HEAP;

	};
	*/