#ifndef __CNETWORK_VIEW_LISTENER_H__
#define __CNETWORK_VIEW_LISTENER_H__

#include "IEventReciever.h"
#include "IActor.h"

namespace Engine
{
	class CNetworkViewListener : public IEventListener
	{
	public:
		explicit CNetworkViewListener(CBaseNetworkManagerPtr pNetMgr, UInt32 sockID, IActor::ActorID playerID);
		~CNetworkViewListener();

		bool HandleEvent(const IEventData& event);

	private:
		CBaseNetworkManagerPtr m_pNetworkMgr;
		UInt32 m_sockID;
		IActor::ActorID m_playerID;
	};


}

#endif