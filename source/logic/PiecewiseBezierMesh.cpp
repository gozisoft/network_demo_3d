#include "logicafx.h"
#include "PiecewiseBezierMesh.h"
#include "BezierCurveMesh.h"

#include "RootNode.h"

using namespace Engine;

CPiecewiseBezierMesh::CPiecewiseBezierMesh(const bezierArray& curves) : 
m_pPieceWiseCurve( new PieceWiseBezierf(curves) )
{
	// set all buttons to zero
	std::memset( &m_mouseButtons, 0, sizeof(m_mouseButtons) );

	// resize the array to the number of curves in the piecewise set
	m_curveMeshes.resize( m_pPieceWiseCurve->GetNumCurves() );

	for (size_t i = 0; i < m_pPieceWiseCurve->GetNumCurves(); ++i)
	{
		const BezierCurvef* curve = &m_pPieceWiseCurve->GetCurve(i);

		// create the mesh
		CBezierCurveMeshPtr pCuveMesh( new CBezierCurveMesh( curve->GetControlPoints(), curve->GetNumCtrlPoints() ) );

		// pairing a curve with its mesh
		m_curveMeshes[i] = CurveMeshPair(curve, pCuveMesh);		
	}

}

CPiecewiseBezierMesh::CPiecewiseBezierMesh(const BezierCurvef* pCurves, size_t numCurves) :
m_pPieceWiseCurve( new PieceWiseBezierf(pCurves, numCurves) )
{


}

void CPiecewiseBezierMesh::AddBezierC0(const BezierCurvef& curve)
{
/*	// Create  the curve mesh
	shared_ptr<CBezierCurveMesh> pCurveMesh( new CBezierCurveMesh( curve.GetControlPoints(), curve.GetNumCtrlPoints() ) );

	// Add mesh curve to the piecewise curve
	m_pPieceWiseCurve.AddSingleCurveC0( pCurveMesh->GetCurve() );

	// Now rebuild the 
	*/
}

bool CPiecewiseBezierMesh::HandleEvent(const IEventData& event)
{

	return false;
}

void CPiecewiseBezierMesh::InsertToScene(CRootNodePtr& pScene)
{
	CurveMeshArray::iterator itor = m_curveMeshes.begin();

	for (; itor != m_curveMeshes.end(); ++itor)
	{
		CurveMeshPair& pair = *itor;
		CBezierCurveMeshPtr pCurveMesh = pair.second;
		pScene->AddChild( pCurveMesh->GetMesh() );
	}

}