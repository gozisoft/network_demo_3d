#ifndef __CCLIENT_H__
#define __CCLIENT_H__

#include "Core.h"

namespace Engine
{

	class CClient
	{
	public:
		friend class CBaseNetworkManager;

		friend class CTCPListenSocket;
		friend class CUDPListenSocket;

		CClient(UInt32 clientID, const String& ipAddress);
		CClient(UInt32 clientID, const String& ipAddress, UInt16 port);

		// Process a incoming data and reconstruct it to the correct packet type:
		// 1) Binary Packet
		// 2) Safe Binary Packet
		// 3) Text Packet
		// 4) Safe Text Packet
		IPacketPtr ProcessIncoming(const char* pData, int bytesRecv);


		void ProcessPackets(const IEventManagerPtr& pEventMgr);

	private:
		typedef std::list<IPacketPtr> PacketList;
		typedef PacketList::iterator PacketItor;
		typedef std::vector<char> CharVector;

		String m_ipAddress;
		UInt16 m_port;
		UInt32 m_clientID;

		CharVector m_recvBuf;	// Buffered data from last recieve
		PacketList m_outList;	// packet list out 
		PacketList m_inList;	// packet list in

		int m_sendOfs;
		int m_timeOut; // time in which a packet is added to outlist

	private:
		DECLARE_HEAP;

	};




}



#endif