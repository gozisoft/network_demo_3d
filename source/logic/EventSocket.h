#ifndef __CEVENT_SOCKET_H__
#define __CEVENT_SOCKET_H__

#include "TCPSocket.h"
#include "UDPSocket.h"



namespace Engine
{
	enum SocketEvent
	{
		NetMsg_Event,
		NetMsg_PlayerLoginRequest,
		NetMsg_PlayerLoginOk
	};

	class CTCPEventSocket : public CTCPSocket
	{
	public:
		CTCPEventSocket(SOCKET socket);
		~CTCPEventSocket();

		void HandleInput(IEventManagerPtr pEevntMgr);
		void HandleOutput();


	private:
		DECLARE_HEAP;

	};

	class CUDPEventSocket : public CUDPSocket
	{
	public:
		CUDPEventSocket();
		CUDPEventSocket(SOCKET sock);
		virtual ~CUDPEventSocket();

		void HandleInput(const IEventManagerPtr& pEevntMgr);
		void HandleOutput();

	private:
		void DecryptEvent(const char* pData);

		DECLARE_HEAP;

	};


}



#endif


/*
class CTCPEventSocket : public CTCPSocket
{
public:

// server accepting a client
CTCPEventSocket(SOCKET new_sock, const String& hostIP);

// client attach to server
CTCPEventSocket();

void HandleInput();

protected:
void CreateEvent(std::istrstream &in);
};
*/