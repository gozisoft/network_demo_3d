#include "logicafx.h"
#include "ActorParamaters.h"
#include "Actors.h"


_ENGINE_BEGIN

namespace ParamsToActor
{
	IActorPtr CreateGameActor(const SActorParams* pParams)
	{
		IActorPtr pActor = IActorPtr();

		switch (pParams->m_type)
		{
		case IActor::AT_CUBE:
			{
				const SCubeParams *cubeParams = static_cast<const SCubeParams*>(pParams);
				pActor = shared_ptr<CCubeActor>( new CCubeActor(*cubeParams) ); 
			} 
			break;
		};


		return pActor;
	}

}

SActorParams::SActorParams(ActorType type) : 
m_type(type),
m_actorID(0),
m_size(0),
m_spawned(false),
m_scale(Vector3f::ONE)
{


}

void SActorParams::Init(std::istream &in)
{
//	in.read( (char*)&m_type, sizeof(ActorType) );
	in.read( (char*)&m_actorID, sizeof(ActorID) );
	in.read( (char*)&m_size, sizeof(size_t) );
	in.read( (char*)&m_spawned, sizeof(bool) );
	in.read( (char*)&m_pos, sizeof(Vector3f::type) * Vector3f::m_arraySize );
	in.read( (char*)&m_rotation, sizeof(Vector3f::type) * Vector3f::m_arraySize );
	in.read( (char*)&m_scale, sizeof(Vector3f::type) * Vector3f::m_arraySize );
}

void SActorParams::Serialise(std::ostream &out) const
{
	out.write( (char*)&m_type, sizeof(ActorType) );
	out.write( (char*)&m_actorID, sizeof(ActorID) );
	out.write( (char*)&m_size, sizeof(size_t) );
	out.write( (char*)&m_spawned, sizeof(bool) );
	out.write( (char*)&m_pos, sizeof(Vector3f::type) * Vector3f::m_arraySize );
	out.write( (char*)&m_rotation, sizeof(Vector3f::type) * Vector3f::m_arraySize );
	out.write( (char*)&m_scale, sizeof(Vector3f::type) * Vector3f::m_arraySize );
}


SCubeParams::SCubeParams() : 
SActorParams(IActor::AT_CUBE),
m_xExtent(1),
m_yExtent(1),
m_zExtent(1),
m_viewID(0)
{
	m_size = sizeof(SCubeParams);


}

void SCubeParams::Init(std::istream &in) 
{
	SActorParams::Init(in);

	in.read( (char*)&m_xExtent, sizeof(float) );
	in.read( (char*)&m_yExtent, sizeof(float) );
	in.read( (char*)&m_zExtent, sizeof(float) );
	in.read( (char*)&m_viewID, sizeof(IGameView::GameViewID) );
}

void SCubeParams::Serialise(std::ostream &out) const
{
	SActorParams::Serialise(out);

	out.write( (char*)&m_xExtent, sizeof(float) );
	out.write( (char*)&m_yExtent, sizeof(float) );
	out.write( (char*)&m_zExtent, sizeof(float) );
	out.write( (char*)&m_viewID, sizeof(IGameView::GameViewID) );
}



_ENGINE_END