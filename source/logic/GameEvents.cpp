#include "logicafx.h"
#include "GameEvents.h"
#include "StringFunctions.h"
#include "ActorParamaters.h"


_ENGINE_BEGIN

namespace Serialisation
{
	SActorParams *CreateFromStream(std::istream &in)
	{
		IActor::ActorType actorType;
		in.read( (char*)&actorType, sizeof(IActor::ActorType) );

		SActorParams *pActorParams = 0;
		switch (actorType)
		{
		case IActor::AT_CUBE:
			pActorParams = new SCubeParams();
			break;

		case IActor::AT_GENERICMESHOBJECT:
			break;

		case IActor::AT_GRID:
			break;

		case IActor::AT_TEAPOT:
			break;

		default:
			assert( false && ieS("Unimplemented actor type in stream\n") );
			return 0;
		}

		if (pActorParams)
		{
			pActorParams->Init(in);
		}

		return pActorParams;
	}

	void SerialiseParams(std::ostream &out, const SActorParams *params)
	{
		if (!params)
		{
			assert( false && ieS("Actor params not valids\n") );
			return;
		}

		switch (params->m_type)
		{
		case IActor::AT_UNKNOWN:
			{
				params->Serialise(out);
			}
			break;
		case IActor::AT_CUBE:
			{
				const SCubeParams* pCubeParams = static_cast<const SCubeParams* >( params );
				pCubeParams->Serialise(out);
			}
			break;
		case IActor::AT_GENERICMESHOBJECT:
			{


			}
			break;

		case IActor::AT_GRID:
			break;

		case IActor::AT_TEAPOT:
			break;

		default:
			assert( false && ieS("Unimplemented actor type in stream\n") );
			return;
		}

	}


}


// -------------------------------------------------------------------------
// Normal Events
// -------------------------------------------------------------------------
DEFINE_HIERARCHICALHEAP(SEvtData_Remote_Connect, "SEvtData_Remote_Connect", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Remote_Client, "SEvtData_Remote_Client", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Network_Player_Actor_Assignment, "SEvtData_Network_Player_Actor_Assignment", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_World_Update, "SEvtData_World_Update", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_New_Actor, "SEvtData_New_Actor", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Destroy_Actor, "SEvtData_Destroy_Actor", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Move_Actor, "SEvtData_Move_Actor", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Spawn_Actor, "SEvtData_Spawn_Actor", "IEventData");


// -------------------------------------------------------------------------
// Requests
// -------------------------------------------------------------------------
DEFINE_HIERARCHICALHEAP(SEvtData_Request_New_Actor, "SEvtData_Request_New_Actor", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Spawn_Request, "SEvtData_Spawn_Request", "IEventData");
DEFINE_HIERARCHICALHEAP(SEvtData_Request_Move_Actor, "SEvtData_Request_Move_Actor", "IEventData");

// -------------------------------------------------------------------------
// Normal Events
// -------------------------------------------------------------------------
const SEvtData_Remote_Connect::EventType SEvtData_Remote_Connect::sk_EventType("SEvtData_Remote_Connect");
const SEvtData_Remote_Client::EventType SEvtData_Remote_Client::sk_EventType("SEvtData_Remote_Client");
const SEvtData_Network_Player_Actor_Assignment::EventType SEvtData_Network_Player_Actor_Assignment::sk_EventType("SEvtData_Network_Player_Actor_Assignment");
const SEvtData_World_Update::EventType SEvtData_World_Update::sk_EventType("SEvtData_World_Update");
const SEvtData_New_Actor::EventType SEvtData_New_Actor::sk_EventType("SEvtData_New_Actor");
const SEvtData_Destroy_Actor::EventType SEvtData_Destroy_Actor::sk_EventType("SEvtData_Destroy_Actor");
const SEvtData_Move_Actor::EventType SEvtData_Move_Actor::sk_EventType("SEvtData_Move_Actor");
const SEvtData_Spawn_Actor::EventType SEvtData_Spawn_Actor::sk_EventType("SEvtData_Spawn_Actor");


// -------------------------------------------------------------------------
// Requests (usually sent to the server)
// -------------------------------------------------------------------------
const SEvtData_Request_New_Actor::EventType SEvtData_Request_New_Actor::sk_EventType("SEvtData_Request_New_Actor");
const SEvtData_Spawn_Request::EventType SEvtData_Spawn_Request::sk_EventType("SEvtData_Spawn_Request");
const SEvtData_Request_Move_Actor::EventType SEvtData_Request_Move_Actor::sk_EventType("SEvtData_Request_Move_Actor");


_ENGINE_END