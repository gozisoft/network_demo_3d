#ifndef __CVIEW_MANAGER_H__
#define __CVIEW_MANAGER_H__

#include "logicfwd.h"
#include "IGameView.h"

namespace Engine
{

	class CGameViewManager
	{
	public:
		typedef IActor::ActorID ActorID;
		typedef IGameView::GameViewID GameViewID;
		typedef std::list<IGameViewPtr> IGameViewTable;
		typedef std::map<GameViewID, IGameViewTable> IGameViewMap;
		typedef std::pair<GameViewID, IGameViewTable> IGameViewMapEnt;			// map entry
		typedef std::pair<IGameViewMap::iterator, bool> IGameViewMapIRes; // insert result into IGameViewMap map
		
		CGameViewManager();
		~CGameViewManager();

		bool AddGameViewSingle(IGameViewPtr pView, ActorID actorID = 0);
		bool AddGameViewGroup(const IGameViewTable& table, ActorID actorID = 0);

		const IGameViewTable& GetViewTable(UInt32 viewID);
		UInt32 GetViewCount() const;
		UInt32 GetNewActorID();

	private:
		IGameViewMap m_gameViewMap;

		UInt32 m_actualViewCount;
		UInt32 m_requestedViewCount;

	private:
		DECLARE_HEAP;

	};


#include "ViewManager.inl"


}


#endif