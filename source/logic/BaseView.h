#ifndef __CBASE_VIEW_H__
#define __CBASE_VIEW_H__

#include "IGameView.h"

namespace Engine
{

	class CHumanView : public IGameView
	{
	public:
		typedef std::map<IActorPtr, CSpatialPtr> ActorSceneMap;

		CHumanView(IRendererPtr pRenderer, IMouseControllerPtr pMouse, IEventManagerPtr pEventMgr);
		virtual ~CHumanView();

		virtual void OnCreate(GameViewID viewID, ActorID actorID);
		virtual void OnRestore();
		virtual void OnLostDevice();
		virtual void OnUpdate(double time, double deltaTime);
		virtual void OnRender(double time, double deltaTime);
		virtual void OnWindowEvent(const CWinEvent& event);
		virtual GameViewType GetType() const;

		GameViewID GetViewId() const;
		ActorID GetActorId() const;

	protected:
		// Subsystems
		IRendererPtr m_pRenderer;
		IMouseControllerPtr m_pMouse;
		IEventManagerPtr m_pEventMgr;

		// Object Culler
		CCullerPtr m_pCuller;

		// The visual aspect of things
		ActorSceneMap m_actorSceneMap;
		CRootNodePtr m_pScene;
		CCameraPtr m_pCamera;
		CCameraNodeFPSPtr m_pCameraNode;
		CCameraControllerFPSPtr m_pCamController;

		// Used to identify the view and the actor it is attached to.
		GameViewID m_gameViewID;
		ActorID m_actorID;

	private:
		DECLARE_HEAP;
	};


	// helper functions
	namespace GameEvents
	{
		void ListenForGameViewEvents(IEventManagerPtr pEventMgr, IEventListenerPtr pListener);
		void ListenForWindowEvents(IEventManagerPtr pEventMgr, IEventListenerPtr pListener);
		void ListenForGeneralGameEvents(IEventManagerPtr pEventMgr, IEventListenerPtr pListener);
	}


}

#endif