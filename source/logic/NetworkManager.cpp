#include "logicafx.h"
#include "NetworkManager.h"
#include "EventSocket.h"
#include "ListenSocket.h"
#include "UDPSocket.h"

#include "Winnet.h"

#include "NetworkEvents.h"

#include "IEventManager.h"

#include "Client.h"
#include "StringFunctions.h"

#include <WS2tcpip.h>

#ifdef max
#undef max
#endif

using namespace Engine;

DEFINE_HEAP(CBaseNetworkManager, "CBaseNetworkManager")
DEFINE_HIERARCHICALHEAP(CServerNetowork, "CServerNetowork", "CBaseNetworkManager");
DEFINE_HIERARCHICALHEAP(CClientNetwork, "CClientNetwork", "CBaseNetworkManager");

// ------------------------------------------------------------------------------------------------------------------
// CBaseNetworkManager
// ------------------------------------------------------------------------------------------------------------------
CBaseNetworkManager::CBaseNetworkManager()
{

}

CBaseNetworkManager::~CBaseNetworkManager() 
{

}

bool CBaseNetworkManager::Initialise()
{
	// Initialize Winsock
	int iResult = WSAStartup( MAKEWORD(2,2), &m_wsaData );
	if (iResult != 0) 
	{
		Cout << ieS("WSAStartup failed: ") << iResult << std::endl;
		return false;
	}
	return true;
}

void CBaseNetworkManager::Release()
{
	int result = WSACleanup();
	if (result == SOCKET_ERROR)
	{
		// WSANOTINITIALISED | A successful WSAStartup call must occur before using this function. 
		// WSAENETDOWN		 | The network subsystem has failed. 
		// WSAEINPROGRESS	 | A blocking Winsock call is in progress, or the service provider is still processing a callback function. 
		NETPrintError();
		assert( false && ieS("Failed to close winsock: \n") );
	}
}

bool CBaseNetworkManager::Send(UInt32 sockId, IPacketPtr packet)
{
	CClientPtr pClient = FindClient(sockId);
	if (!pClient)
		return false;

	pClient->m_timeOut = 0;
	pClient->m_outList.push_back(packet);
	return true;
}

CClientPtr CBaseNetworkManager::FindClient(UInt32 sockId)
{
	ClientList::iterator itor = m_clients.begin();
	for (UInt i = 0; i < sockId; ++i)
		++itor;

	return (*itor);
}

bool CBaseNetworkManager::HasOutput(const ClientList& clientList)
{
	for (ClientList::const_iterator itor = clientList.begin(); itor != clientList.end(); ++itor)
	{
		CClientPtr pClient = (*itor);
		if ( !pClient->m_outList.empty() )
			return true;
	}

	return false;
}


// ------------------------------------------------------------------------------------------------------------------
// CServerNetowork
// ------------------------------------------------------------------------------------------------------------------
CServerNetowork::CServerNetowork(IEventManagerPtr pEventMgr, const String& port) : 
m_pEventMgr(pEventMgr),
m_listenPort(port)
{
	m_UDPsocket = CUDPListenSocketPtr( new CUDPListenSocket() ); 
	m_TCPSocket = CTCPListenSocketPtr( new CTCPListenSocket() );
}

CServerNetowork::~CServerNetowork()
{
	
}

bool CServerNetowork::Listen()
{
	if ( TCP::NETListenSock(m_TCPSocket->m_socket, m_listenPort) )
	{
		assert(false && ieS("Unable to setup a listen socket on TCP") );
		closesocket(m_UDPsocket->m_socket);
		return false;
	}

	if ( UDP::NETBind(m_UDPsocket->m_socket, m_listenPort) )
	{
		assert(false && ieS("Unable to setup a listen socket on UDP") );
		closesocket(m_UDPsocket->m_socket);
		return false;
	}

	return true;
}

void CServerNetowork::DoSelect(double pauseMicroSecs, bool handleInput)
{
	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = (long)pauseMicroSecs;    // 100 microseconds is 0.1 milliseconds or .0001 seconds

	fd_set inp_set, out_set, exc_set;

	if (m_TCPSocket)
	{
		FD_ZERO(&inp_set);
		FD_ZERO(&exc_set);

		int maxdesc = 0;

		// set everything up for the select
		if (handleInput)				
			FD_SET(m_TCPSocket->GetSocket(), &inp_set);

		FD_SET(m_TCPSocket->GetSocket(), &exc_set);

		if ((int)m_TCPSocket->GetSocket() > maxdesc)
			maxdesc = (int)m_TCPSocket->GetSocket();

		// do the select (duration passed in as tv, NULL to block until event)
		int selRet = select(maxdesc+1, &inp_set, NULL, &exc_set, &tv);
		if (selRet == SOCKET_ERROR)
		{
			NETPrintError();
			return;
		}

		OStringStream oss;
		oss << selRet << std::endl;
		OutputDebugString;

		if ( FD_ISSET(m_TCPSocket->GetSocket(), &exc_set) )
			assert(0);

		// Deal with UDP input
		if ( handleInput && FD_ISSET(m_TCPSocket->GetSocket(), &inp_set) )
		{
			m_TCPSocket->HandleInput(m_clients, m_pEventMgr);
		}
	}


	if (m_UDPsocket)
	{
		FD_ZERO(&inp_set);
		FD_ZERO(&out_set);
		FD_ZERO(&exc_set);

		int maxdesc = 0;

		// set everything up for the select
		if (handleInput)				
			FD_SET(m_UDPsocket->GetSocket(), &inp_set);

		if ( HasOutput(m_clients) )
			FD_SET(m_UDPsocket->GetSocket(), &out_set);

		FD_SET(m_UDPsocket->GetSocket(), &exc_set);

		if ((int)m_UDPsocket->GetSocket() > maxdesc)
			maxdesc = (int)m_UDPsocket->GetSocket();

		// do the select (duration passed in as tv, NULL to block until event)
		int selRet = select(maxdesc+1, &inp_set, &out_set, &exc_set, &tv);
		if (selRet == SOCKET_ERROR)
		{
			NETPrintError();
			return;
		}
		
		if ( FD_ISSET(m_UDPsocket->GetSocket(), &exc_set) )
			assert(0);

		// Deal with UDP Output
		if ( FD_ISSET(m_UDPsocket->GetSocket(), &out_set) )
		{
			m_UDPsocket->HandleOutput(m_clients);
		}

		// Deal with UDP input
		if ( handleInput && FD_ISSET(m_UDPsocket->GetSocket(), &inp_set) )
		{
			m_UDPsocket->HandleInput(m_clients, m_pEventMgr);
		}
	}	

}


// ------------------------------------------------------------------------------------------------------------------
// CClientNetwork
// ------------------------------------------------------------------------------------------------------------------
CClientNetwork::CClientNetwork(IEventManagerPtr pEventMgr, const String& hostName, const String& port) : 
m_pEventMgr(pEventMgr),
m_ipAddress(hostName),
m_port(port)
{
	m_pUDPSocket = CUDPListenSocketPtr( new CUDPListenSocket() );

}

CClientNetwork::~CClientNetwork()
{
}

bool CClientNetwork::Connect()
{
	// Try connecting via TCP :: its soo fluffy !!
	SOCKET tempTCPSocet = 0;
	if( TCP::NETConnectSock(tempTCPSocet, m_ipAddress, m_port) != 0)
		return false;

	// dont need the TCP socket anymore
	closesocket(tempTCPSocet);

	// Add server to the client list.
	UInt16 port = FromString<UInt16>(m_port);
	UInt32 sockID = (UInt32)m_clients.size();
	CClientPtr pClient( new CClient( sockID, m_ipAddress, port) );
	m_clients.push_back(pClient);

	// Connect via UDP
	if( UDP::NETCreateSock(m_pUDPSocket->m_socket, m_port) != 0)
		return false;

	// Create a connect event (sends connect message over network)
	shared_ptr<SEvtData_Remote_Connect> connectEvent( new SEvtData_Remote_Connect(m_ipAddress) );
	bool success = m_pEventMgr->QueueEvent(connectEvent);
	if (!success)
		return false;

	return true;
}

void CClientNetwork::DoSelect(double pauseMicroSecs, bool handleInput)
{
	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = (long)pauseMicroSecs;    // 100 microseconds is 0.1 milliseconds or .0001 seconds

	fd_set inp_set, out_set, exc_set;

	if (m_pUDPSocket)
	{
		FD_ZERO(&inp_set);
		FD_ZERO(&out_set);
		FD_ZERO(&exc_set);

		int maxdesc = 0;

		// set everything up for the select
		if (handleInput)				
			FD_SET(m_pUDPSocket->GetSocket(), &inp_set);

		FD_SET(m_pUDPSocket->GetSocket(), &exc_set);

		if ( HasOutput(m_clients) )
		FD_SET(m_pUDPSocket->GetSocket(), &out_set);

		if ((int)m_pUDPSocket->GetSocket() > maxdesc)
			maxdesc = (int)m_pUDPSocket->GetSocket();

		// do the select (duration passed in as tv, NULL to block until event)
		int selRet = select(maxdesc+1, &inp_set, &out_set, &exc_set, &tv);
		if (selRet == SOCKET_ERROR)
		{
			NETPrintError();
			return;
		}

		// handle input, output, and exceptions
		if ( FD_ISSET(m_pUDPSocket->GetSocket(), &exc_set) )
		{
			assert(0);
		}

		// Deal with UDP Output
		if ( FD_ISSET(m_pUDPSocket->GetSocket(), &out_set) )
		{
			m_pUDPSocket->HandleOutput(m_clients);
		}

		// Deal with UDP input
		if ( handleInput && FD_ISSET(m_pUDPSocket->GetSocket(), &inp_set) )
		{
			m_pUDPSocket->HandleInput(m_clients, m_pEventMgr);
		}
	}	


}


/*
CNetwork::CNetwork() : 
m_sktIDCount(0)
{

}

CNetwork::~CNetwork()
{

}

bool CNetwork::Initialise()
{
	// Initialize Winsock
	int iResult = WSAStartup( MAKEWORD(2,2), &m_wsaData );
	if (iResult != 0) 
	{
		Cout << ieS("WSAStartup failed: ") << iResult << std::endl;
		return false;
	}

	return true;
}

void CNetwork::Release()
{
	int result = WSACleanup();
	if (result == SOCKET_ERROR)
	{
		// WSANOTINITIALISED | A successful WSAStartup call must occur before using this function. 
		// WSAENETDOWN		 | The network subsystem has failed. 
		// WSAEINPROGRESS	 | A blocking Winsock call is in progress, or the service provider is still processing a callback function. 
		NETPrintError();
		assert( false && ieS("Failed to close winsock: \n") );
	}
}



bool CNetwork::Send(UInt32 sockId, IPacketPtr packet)
{
	CUDPSocketPtr pUDPsock = FindSocket(sockId);

	if (!pUDPsock)
		return false;

	pUDPsock->Send(packet);
	return true;
}

CUDPSocketPtr CNetwork::FindSocket(UInt32 sockId)
{
	ScktItor itor = m_socketArray.begin();
	for (UInt i = 0; i < sockId; ++i)
		++itor;

	return (*itor);
}

void CNetwork::AddListenSocket(CTCPListenSocketPtr pTCPSocket)
{
	m_listenSocket = pTCPSocket;
}

UInt32 CNetwork::AddSocket(CUDPSocketPtr pUDPSocket)
{
	UInt32 currentID = m_sktIDCount;

	// set the socket id and push on the list
	m_socketArray.push_back(pUDPSocket);

	// increment socket id count
	++m_sktIDCount;

	return currentID;
}

void CNetwork::RemoveSocket(UInt32 appSocketID)
{
	ScktItor itor = m_socketArray.begin();
	for (UInt i = 0; i < appSocketID; ++i)
		++itor;

	m_socketArray.erase(itor);

	--m_sktIDCount;
}
*/




/*

#include "logicafx.h"
#include "NetworkMgr.h"
#include "AppSocket.h"
#include "AppEventSocket.h"

#include "TCPSocket.h"
#include "UDPSocket.h"
#include "Winnet.h"


using namespace Engine;

CNetwork::CNetwork() : 
m_sktIDCount(0)
{

}

bool CNetwork::Initialise()
{
	// Initialize Winsock
	int iResult = WSAStartup( MAKEWORD(2,2), &m_wsaData );
	if (iResult != 0) 
	{
		Cout << ieS("WSAStartup failed: ") << iResult << std::endl;
		return false;
	}

	return true;
}



void CNetwork::Release()
{
	int result = WSACleanup();
	if (result == SOCKET_ERROR)
	{
		// WSANOTINITIALISED | A successful WSAStartup call must occur before using this function. 
		// WSAENETDOWN		 | The network subsystem has failed. 
		// WSAEINPROGRESS	 | A blocking Winsock call is in progress, or the service provider is still processing a callback function. 
		NETPrintError();
		assert( false && ieS("Failed to close winsock: \n") );
	}
}

void CNetwork::DoSelect(double currentTime)
{
	//
	// TCP scokets and events first.
	//
	// Wait for one of the sockets to receive I/O notification and 
	DWORD Event = WSAWaitForMultipleEvents( (DWORD)m_TCPEvents.size(), &m_TCPEvents[0], FALSE, WSA_INFINITE, FALSE);
	if (Event == WSA_WAIT_FAILED)
	{
		Cout << ieS("WSAWaitForMultipleEvents() failed with error") << WSAGetLastError();
		return;
	}
	Event = Event - WSA_WAIT_EVENT_0;

	ScktItor itor = m_socketList.begin();
	for (UInt32 i = Event; i < m_TCPEvents.size(); ++i, ++itor)
	{
		CTCPSocketPtr pTCPSock = (*itor)->GetTCPSocket();

		if (pTCPSock)
		{
			SOCKET socket = pTCPSock->GetSocket();

			Event = WSAEnumNetworkEvents(socket, m_TCPEvents[i], &NetworkEvents);

			if ((Event == WSA_WAIT_FAILED) || (Event == WSA_WAIT_TIMEOUT))
				continue;

			if (NetworkEvents.lNetworkEvents & FD_READ)
			{
				(*itor)->TCPHandleInput();
			}

			if (NetworkEvents.lNetworkEvents & FD_WRITE)
			{
				(*itor)->TCPHandleOutput();
			}
		}
	}

	//
	// UDP socket and events second.
	//
	Event = WSAWaitForMultipleEvents( (DWORD)m_UDPEvents.size(), &m_UDPEvents[0], FALSE, WSA_INFINITE, FALSE);
	if (Event == WSA_WAIT_FAILED)
	{
		Cout << ieS("WSAWaitForMultipleEvents() failed with error") << WSAGetLastError();
		return;
	}
	Event = Event - WSA_WAIT_EVENT_0;

	itor = m_socketList.begin();
	for (UInt32 i = Event; i < m_TCPEvents.size(); ++i, ++itor)
	{
		CUDPSocketPtr pUDPSock = (*itor)->GetUDPSocket();

		if (pUDPSock)
		{
			SOCKET socket = pUDPSock->GetSocket();

			Event = WSAEnumNetworkEvents(socket, m_TCPEvents[i], &NetworkEvents);

			if ((Event == WSA_WAIT_FAILED) || (Event == WSA_WAIT_TIMEOUT))
				continue;

			if (NetworkEvents.lNetworkEvents & FD_READ)
			{
				(*itor)->UDPHandleInput();
			}

			if (NetworkEvents.lNetworkEvents & FD_WRITE)
			{
				(*itor)->UDPHandleOutput();
			}
		}
	}

}

UInt32 CNetwork::AddAppSocket(CAppSocketPtr pAppSocket)
{
	// Setup the events for the OS to recieve for the TCP socket.
	CTCPSocketPtr pTCPSock = pAppSocket->GetTCPSocket();

	if (pTCPSock)
	{
		SOCKET used_sock = pAppSocket->GetTCPSocket()->GetSocket();
		WSAEVENT event = WSACreateEvent();
		int result = WSAEventSelect(used_sock, event, FD_READ | FD_WRITE);
		if (result != 0) 
		{
			Cout << ieS("TCP WSAEventSelect failed: ") << result << std::endl;
			NETPrintError();
			return false;
		}
		m_TCPEvents.push_back(event);
	}

	// Setup the events for the OS to recieve for the UDP socket.
	CUDPSocketPtr pUDPSock = pAppSocket->GetUDPSocket();

	if (pUDPSock)
	{
		SOCKET used_sock = pUDPSock->GetSocket();
		WSAEVENT event = WSACreateEvent();
		int result = WSAEventSelect(used_sock, event, FD_READ | FD_WRITE);
		if (result != 0) 
		{
			Cout << ieS("TCP WSAEventSelect failed: ") << result << std::endl;
			NETPrintError();
			return false;
		}
		m_UDPEvents.push_back(event);
	}

	// set the socket id and push on the list
	pAppSocket->SetID(m_sktIDCount);
	m_socketList.push_back(pAppSocket);

	// increment socket id count
	++m_sktIDCount;

	return pAppSocket->GetID();
}

void CNetwork::RemoveSocket(CAppSocketPtr pAppSocket)
{
	m_socketList.remove(pAppSocket);

	EvItor itor = m_UDPEvents.begin();
	for (UInt i = 0; i < pAppSocket->GetID(); ++i)
		++itor;

	m_UDPEvents.erase(itor);

	itor = m_TCPEvents.begin();
	for (UInt i = 0; i < pAppSocket->GetID(); ++i)
		++itor;

	m_TCPEvents.erase(itor);
}


CClientNetwork::CClientNetwork(const String& hostName, const String& port) : 
m_ipAddress(hostName),
m_port(port)
{


}

bool CClientNetwork::Connect()
{
	if ( !CNetwork::Initialise() )
		return false;

	CAppSocketPtr pAppSocket = CAppSocketPtr( new CAppSocket() );

	if ( !pAppSocket->Connect(m_ipAddress, m_port) )
	{
		pAppSocket.reset();
		return false;
	}

	AddAppSocket(pAppSocket);
	return true;
}


*/

/*
CNetwork::CNetwork() : 
m_sktIDCount(0)
{

}

CNetwork::~CNetwork()
{

}

bool CNetwork::Initialise()
{
	// Initialize Winsock
	int iResult = WSAStartup( MAKEWORD(2,2), &m_wsaData );
	if (iResult != 0) 
	{
		Cout << ieS("WSAStartup failed: ") << iResult << std::endl;
		return false;
	}

	return true;
}

void CNetwork::Release()
{
	int result = WSACleanup();
	if (result == SOCKET_ERROR)
	{
		// WSANOTINITIALISED | A successful WSAStartup call must occur before using this function. 
		// WSAENETDOWN		 | The network subsystem has failed. 
		// WSAEINPROGRESS	 | A blocking Winsock call is in progress, or the service provider is still processing a callback function. 
		NETPrintError();
		assert( false && ieS("Failed to close winsock: \n") );
	}
}

void CNetwork::DoSelect(IEventManagerPtr pEevntMgr, double pauseMicroSecs, bool handleInput)
{
	
	//
	// TCP scokets and events first.
	//
	// Wait for one or more of the sockets to receive I/O notification 
	DWORD Event = WSAWaitForMultipleEvents( (DWORD)m_TCPEvents.size(), &m_TCPEvents[0], FALSE, NULL, FALSE);
	if (Event == WSA_WAIT_FAILED)
	{
		Cout << ieS("WSAWaitForMultipleEvents() failed with error") << WSAGetLastError();
		return;
	}
	Event = Event - WSA_WAIT_EVENT_0;

	for (DWORD i = Event; i < (DWORD)m_TCPEvents.size(); ++i)
	{
		Event = WSAWaitForMultipleEvents(1, &m_TCPEvents[i], TRUE, NULL, FALSE);

		if ((Event == WSA_WAIT_FAILED) || (Event == WSA_WAIT_TIMEOUT))
			continue;
		else
		{
			SocketTuple& tuple = m_socketArray[i];
			CTCPSocketPtr pTCPSock = std::tr1::get<0>(tuple);

			if (pTCPSock)
			{
				SOCKET socket = pTCPSock->GetSocket();

				if( WSAEnumNetworkEvents(socket, m_TCPEvents[i], &NetworkEvents) == SOCKET_ERROR )
					NETPrintError();

				// Check for FD_READ messages
				if (NetworkEvents.lNetworkEvents & FD_READ || NetworkEvents.lNetworkEvents & FD_ACCEPT)
				{
					pTCPSock->HandleInput(pEevntMgr);
				}

				// Check for FD_WRITE messages
				if (NetworkEvents.lNetworkEvents & FD_WRITE)
				{
					pTCPSock->HandleOutput();
				}
			}
		}
	}

	//
	// UDP socket and events second.
	//
	Event = WSAWaitForMultipleEvents( (DWORD)m_UDPEvents.size(), &m_UDPEvents[0], FALSE, NULL, FALSE);
	if (Event == WSA_WAIT_FAILED)
	{
		Cout << ieS("WSAWaitForMultipleEvents() failed with error") << WSAGetLastError();
		return;
	}
	Event = Event - WSA_WAIT_EVENT_0;

	for (DWORD i = Event; i < (DWORD)m_TCPEvents.size(); ++i)
	{
		Event = WSAWaitForMultipleEvents(1, &m_UDPEvents[i], TRUE, NULL, FALSE);

		if ((Event == WSA_WAIT_FAILED) || (Event == WSA_WAIT_TIMEOUT))
			continue;
		else
		{
			SocketTuple& tuple = m_socketArray[i];
			CUDPSocketPtr pUDPSock = std::tr1::get<1>(tuple);

			if (pUDPSock)
			{
				SOCKET socket = pUDPSock->GetSocket();

				Event = WSAEnumNetworkEvents(socket, m_UDPEvents[i], &NetworkEvents);

				if ((Event == WSA_WAIT_FAILED) || (Event == WSA_WAIT_TIMEOUT))
					continue;

				if (NetworkEvents.lNetworkEvents & FD_READ)
				{
					pUDPSock->HandleInput(pEevntMgr);
				}

				if (NetworkEvents.lNetworkEvents & FD_WRITE)
				{
					pUDPSock->HandleOutput();
				}
			}
		}
	}


	timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = pauseMicroSecs;    // 100 microseconds is 0.1 milliseconds or .0001 seconds

	fd_set TCP_inp_set;
	fd_set TCP_out_set;
	fd_set TCP_exc_set;

	FD_ZERO(&TCP_inp_set);
	FD_ZERO(&TCP_out_set);
	FD_ZERO(&TCP_exc_set);

	fd_set UDP_inp_set;
	fd_set UDP_out_set;
	fd_set UDP_exc_set;

	FD_ZERO(&UDP_inp_set);
	FD_ZERO(&UDP_out_set);
	FD_ZERO(&UDP_exc_set);

	int TCPmaxdesc = 0;
	int UDPmaxdesc = 0;

  // set everything up for the select
	for (SocketArray::iterator itor = m_socketArray.begin(); itor != m_socketArray.end(); ++itor)
	{
		SocketTuple tuple = (*itor);

		CTCPSocketPtr pTCPSock = std::tr1::get<0>(tuple);
		if (pTCPSock)
		{
			if (handleInput)
				FD_SET(pTCPSock->GetSocket(), &TCP_inp_set);

			FD_SET(pTCPSock->GetSocket(), &TCP_exc_set);

			if (pTCPSock->HasOutput())
				FD_SET(pTCPSock->GetSocket(), &TCP_out_set);

			if ((int)pTCPSock->GetSocket() > TCPmaxdesc)
				TCPmaxdesc = (int)pTCPSock->GetSocket();
		}

		CUDPSocketPtr pUDPSock = std::tr1::get<1>(tuple);
		if (pUDPSock)
		{
			if (handleInput)				
				FD_SET(pUDPSock->GetSocket(), &UDP_inp_set);

			FD_SET(pUDPSock->GetSocket(), &UDP_exc_set);

			if (pUDPSock->HasOutput())
				FD_SET(pUDPSock->GetSocket(), &UDP_out_set);

			if ((int)pUDPSock->GetSocket() > UDPmaxdesc)
				UDPmaxdesc = (int)pUDPSock->GetSocket();
		}

	}
  
	int selRet = 0;

	// do the select (duration passed in as tv, NULL to block until event)
	selRet = select(TCPmaxdesc+1, &TCP_inp_set, &TCP_out_set, &TCP_exc_set, &tv) ;
	if (selRet == SOCKET_ERROR)
	{
		NETPrintError();
		return;
	}

	// handle input, output, and exceptions
	if (selRet)
	{
		for (SocketArray::iterator itor = m_socketArray.begin(); itor != m_socketArray.end(); ++itor)
		{
			SocketTuple tuple = (*itor);

			CTCPSocketPtr pTCPSock = std::tr1::get<0>(tuple);

			if (pTCPSock)
			{
				if ( FD_ISSET(pTCPSock->GetSocket(), &TCP_exc_set) )
				{
					// pTCPSock->HandleException();
					assert(0);
				}

				if ( FD_ISSET(pTCPSock->GetSocket(), &TCP_out_set) )
				{
					pTCPSock->HandleOutput();
				}

				if ( handleInput && FD_ISSET(pTCPSock->GetSocket(), &TCP_inp_set) )
				{
					pTCPSock->HandleInput(pEevntMgr);
				}

			}
		}	
	}

	// handle deleting any sockets
	for (SocketList::iterator i = m_SockList.begin(); i != m_SockList.end(); ++i)
	{
		pSock = *i;
		if (pSock->m_timeOut) 
		{
			if (pSock->m_timeOut < timeNow)
			{
				pSock->TimeOut();
			}
		}

		if (pSock->m_deleteFlag&1) 
		{
			switch (pSock->m_deleteFlag) 
			{
			  case 1:
//					--i;
					g_pSocketManager->RemoveSocket(pSock);
					break;
				case 3:
					pSock->m_deleteFlag = 2;
					if (pSock->m_sock != INVALID_SOCKET) 
					{
						closesocket(pSock->m_sock);
						pSock->m_sock = INVALID_SOCKET;
					}
					break;
			}
		}
	 }
	 
}

UInt32 CNetwork::AddAppSocket(CTCPSocketPtr pTCPSocket, CUDPSocketPtr pUDPSocket)
{
	if ( !pTCPSocket && !pUDPSocket )
	{
		assert( false && ieS("No valid TCP or UDP socket") );
		return std::numeric_limits<UInt32>::max(); 
	}

	// 1) Setup the events for the OS to recieve for the TCP socket.

	// create blank non signaled event
	WSAEVENT event = WSACreateEvent();

	// if socket is valid give it signals (TCP)
	if (pTCPSocket)
	{
		SOCKET used_sock = pTCPSocket->GetSocket();
		int result = WSAEventSelect(used_sock, event, FD_READ | FD_WRITE);
		if (result != 0) 
		{
			Cout << ieS("TCP WSAEventSelect failed: ") << result << std::endl;
			NETPrintError();
			return false;
		}
	}

	// push back the event to the TCP event array
	m_TCPEvents.push_back(event);

	// 2) Setup the events for the OS to recieve for the TCP socket.

	// create blank non signaled event
	event = WSACreateEvent();

	// if socket is valid give it signals (UDP)
	if (pUDPSocket)
	{
		SOCKET used_sock = pUDPSocket->GetSocket();
		int result = WSAEventSelect(used_sock, event, FD_READ | FD_WRITE);
		if (result != 0) 
		{
			Cout << ieS("TCP WSAEventSelect failed: ") << result << std::endl;
			NETPrintError();
			return false;
		}
	}

	// push back the event to the UDP event array
	m_UDPEvents.push_back(event);

	// 3) Create the tuple and assign ID socket
	SocketTuple tuple(pTCPSocket, pUDPSocket, m_sktIDCount);

	// set the socket id and push on the list
	m_socketArray.push_back(tuple);

	// increment socket id count
	++m_sktIDCount;

	return std::tr1::get<2>(tuple);
}

void CNetwork::RemoveSocket(SocketTuple& appSocket)
{
	for (ScktItor itor = m_socketArray.begin(); itor != m_socketArray.end(); ++itor)
	{
		if ( (*itor) == appSocket )
			m_socketArray.erase(itor);
	}
	
	EvItor itor = m_UDPEvents.begin();
	for (UInt i = 0; i < std::tr1::get<2>(appSocket); ++i)
		++itor;

	m_UDPEvents.erase(itor);

	itor = m_TCPEvents.begin();
	for (UInt i = 0; i < std::tr1::get<2>(appSocket); ++i)
		++itor;

	m_TCPEvents.erase(itor);
}

String CNetwork::GetIpAddress(UInt32 sockId) const
{
	for (SocketArray::const_iterator itor = m_socketArray.begin(); itor != m_socketArray.end(); ++itor)
	{
		SocketTuple tuple = (*itor);

		if ( std::tr1::get<2>(tuple) == sockId )
		{
			String address;

		
		}			
	}

	return String();
}


CClientNetwork::CClientNetwork(const String& hostName, const String& port) : 
m_ipAddress(hostName),
m_port(port)
{


}

CClientNetwork::~CClientNetwork()
{

}

bool CClientNetwork::Connect()
{
	if ( !CNetwork::Initialise() )
		return false;

	CTCPSocketPtr pTCPSocket = CTCPSocketPtr( new CTCPSocket() );

	if ( !pTCPSocket->Connect(m_ipAddress, m_port) )
	{
		pTCPSocket.reset();
		return false;
	}

	CUDPSocketPtr pUDPSocket = CUDPSocketPtr( new CUDPSocket() );

	if ( !pUDPSocket->Connect(m_ipAddress, m_port) )
	{
		pUDPSocket.reset();
		return false;
	}

	AddAppSocket(pTCPSocket, pUDPSocket);
	return true;
}
*/