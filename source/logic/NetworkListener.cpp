#include "logicafx.h"
#include "NetworkListener.h"
#include "NetworkManager.h"

#include "IEventReciever.h"
#include "StringFunctions.h"
#include "Packet.h"

using namespace Engine;

CNetworkListener::CNetworkListener(CBaseNetworkManagerPtr pNetMgr, UInt32 sockID) :
m_pNetworkMgr(pNetMgr),
m_sockID(sockID)
{


}

CNetworkListener::~CNetworkListener()
{

}

bool CNetworkListener::HandleEvent(const IEventData& event)
{
	SocketEvent socketEventType = NetMsg_Event;
	ULong eventHash = event.GetEventType().GetHashValue();

	// serialise data
	using std::stringstream;
	stringstream ss( stringstream::in | stringstream::out | stringstream::binary );

	// 1) socketevent type (NetMsg_Event)
	// 2) event type (hash value)
	// 3) event data
	ss.write( (char*)&socketEventType, sizeof(SocketEvent) );
	ss.write( (char*)&eventHash, sizeof(ULong) );
	event.Serialise(ss);

	// get length of file:
	ss.seekg (0, stringstream::end);
	size_t length = static_cast<size_t>( ss.tellg() );
	ss.seekg (0, stringstream::beg);

	// read it into a vector
	using std::vector;
	vector<char> buffer(length);
	ss.read( &buffer[0], length );	

	CBinaryPacketPtr pPacket( new CBinaryPacket( &buffer[0], buffer.size() ) );
	bool sucess = m_pNetworkMgr->Send(m_sockID, pPacket);

	return sucess;
}