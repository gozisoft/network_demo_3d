#include "logicafx.h"
#include "BezierCurveMesh.h"

#include "VertexBuffer.h"
#include "VertexFormat.h"
#include "VertexBufferAccess.h"

#include "RootNode.h"
#include "Polyline.h"
#include "TriangleMesh.h"
#include "GeometryCreator.h"

using namespace Engine;


CBezierCurveMesh::CBezierCurveMesh(const Vector3f* pCtrlPoints, size_t numCtrlPoints) :
m_pCurve( new BezierCurvef(pCtrlPoints, numCtrlPoints) ),
m_pRoot(new CRootNode() )
{
	BuildCurve();
	BuildConnectingLines();
	BuildControlPoints();
}


CBezierCurveMesh::CBezierCurveMesh(const ControlPointArray& ctrlPoints) :
m_pCurve( new BezierCurvef(ctrlPoints) ),
m_pRoot( new CRootNode() )
{
	BuildCurve();
	BuildConnectingLines();
	BuildControlPoints();
}

CBezierCurveMesh::~CBezierCurveMesh()
{

}

// The actual curve
void CBezierCurveMesh::BuildCurve()
{
	// Create thecurve vertex format (colours and pos)
	CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
	pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
	pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);
	UInt stride = pVFormat->GetTotalStride();

	// Used for the number of segments within the curve
	const UInt numSamples = 1000;
	const float stepsize = 1 / static_cast<float>(numSamples - 1);

	{
		// Set up the vertex buffer
		CVertexBufferPtr pVBuffer = CVertexBufferPtr( new CVertexBuffer(numSamples, stride, CBuffer::BU_DYNAMIC) );

		// Create a buffer accessor
		CVertexBufferAccess vba(pVFormat, pVBuffer);

		// Set a singal colour for the curve, yes its boring.
		Colour4f col(1.0f, 0.0f, 0.0f, 1.0f);

		float t = 0.0f;
		for (UInt i = 0; i < numSamples; ++i, t += stepsize)
		{
			vba.Position<Vector3f>(i) = m_pCurve->GetPosition(t);
			vba.Colour<Colour4f>(0, i) = col;
		}

		// create the curve mesh
		m_pCurveMesh = CPolylinePtr( new CPolyline(pVFormat, pVBuffer, true) );

		// Add curve mesh to the root
		m_pRoot->AddChild(m_pCurveMesh);
	}

}

// Create line mesh connecting nodes
void CBezierCurveMesh::BuildConnectingLines()
{
	// Create thecurve vertex format (colours and pos)
	CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
	pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
	pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);
	UInt stride = pVFormat->GetTotalStride();

	// Set up the vertex buffer
	CVertexBufferPtr pVBuffer = CVertexBufferPtr( new CVertexBuffer(m_pCurve->GetNumCtrlPoints()+1, stride, CBuffer::BU_DYNAMIC) );

	// Create a buffer accessor
	CVertexBufferAccess vba(pVFormat, pVBuffer);
	for (UInt i = 0; i < pVBuffer->GetNumElements()-1; ++i)
	{
		// Pointer to the control points
		const Vector3f* pCtrlPoints = m_pCurve->GetControlPoints();

		vba.Position<Vector3f>(i) = pCtrlPoints[i];
		vba.Colour<Colour4f>(0, i) = Colour4f( 1.0f, 1.0f, 0.0f, 1.0f );
	}

	m_pCtrlPointLines = CPolylinePtr( new CPolyline(pVFormat, pVBuffer, true) );

	// Add lines to the root node
	m_pRoot->AddChild(m_pCtrlPointLines);
}

// The control points
void CBezierCurveMesh::BuildControlPoints()
{
	// Create thecurve vertex format (colours and pos)
	CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
	pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
	pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);
	UInt stride = pVFormat->GetTotalStride();

	// Point mesh
	using std::vector;
	vector<CSceneNodePtr> MeshArray( m_pCurve->GetNumCtrlPoints() );
	for (UInt i = 0; i < m_pCurve->GetNumCtrlPoints(); ++i)
	{
		// Createa sphere mesh
		CTriMeshPtr pSphere = CGeometry(pVFormat).Sphere(64, 64, 0.125f);

		// Create the child mesh for the control point node
		MeshArray[i] = pSphere;
	}

	// create the polypoints nodes with mesh as child
	for (UInt i = 0; i < m_pCurve->GetNumCtrlPoints(); ++i)
	{
		// Pointer to the control points
		const Vector3f* pCtrlPoints = m_pCurve->GetControlPoints();

		// Create the actual node
		CRootNodePtr pCtrlPointNode = CRootNodePtr( new CRootNode() );
		pCtrlPointNode->SetPosition(pCtrlPoints[i]);
		pCtrlPointNode->AddChild(MeshArray[i]);

		// Create the bezier controller and pair it with the node Ctrl point

		// Add the control point to the array
		m_controlPoints.push_back(pCtrlPointNode);		
	}

	for (UInt i = 0; i < m_pCurve->GetNumCtrlPoints(); ++i)
		m_pRoot->AddChild(m_controlPoints[i]);		
}

void CBezierCurveMesh::UpdateMesh(double time, double deltaTime)
{



}