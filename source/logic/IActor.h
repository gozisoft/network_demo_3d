#ifndef __IACTORS_H__
#define __IACTORS_H__

#include "logicfwd.h"


namespace Engine
{

	class IActor
	{
	public:
		typedef UInt32 ActorID;

		enum ActorType
		{
			AT_UNKNOWN,
			AT_CUBE,
			AT_TEAPOT,
			AT_TESTOBJECT,
			AT_GRID,
			AT_GENERICMESHOBJECT
		};

		virtual ~IActor() {}

		static IActorPtr CreateActor(ActorType type);
		static IActorPtr CreateActor(const SActorParams* pParams);

		virtual const SActorParams* GetParams() const = 0;
		virtual SActorParams* GetParams() = 0;
	};

}


#endif