#ifndef __SPHERE_INL__
#define __SPHERE_INL__


template <typename T>
Sphere<T>::Sphere() : m_radius(0)
{

}

template <typename T> template < typename U >
Sphere<T>::Sphere (const Vector3<U>& center, const U& radius) : 
m_center(center),
m_radius(radius)
{

}

template <typename T> template < typename U >
Sphere<T>::Sphere (const Sphere<U>& sphere) : 
m_center(sphere.m_center),
m_radius(sphere.m_radius)
{

}

template <typename T> template < typename U >
const Sphere<T>& Sphere<T>::operator = (const Sphere<U>& sphere)
{	
	m_center = sphere.m_center;
	m_radius = sphere.m_radius;
	return *this;
}


template <typename T>
void Sphere<T>::TrasnfromBy(const Matrix4<T>& transform, Sphere<T>& sphere)
{
	sphere.m_center = m_center * transform;

	float radius = 0.0f;
	Vector3f scale = transform.GetScale();
	float maxScale = 0.0f;
	for (UInt i = 0; i < scale.GetSize(); ++i)
	{
		if ( Abs(scale[i]) > maxScale )
			maxScale = Abs(scale[i]);
	}
	radius = maxScale;

	sphere.m_radius = m_radius * radius;
}




#endif