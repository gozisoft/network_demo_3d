#ifndef __FRUSTUM_H__
#define __FRUSTUM_H__


#include "Intersections.h"
#include "Matrix.h"


namespace Engine
{


	template <typename T>
	class Frustum
	{
	public:
		enum
		{
			FRUSTUM_PLANE_NEAR = 0,
			FRUSTUM_PLANE_FAR,
			FRUSTUM_PLANE_BOTTOM,
			FRUSTUM_PLANE_TOP,
			FRUSTUM_PLANE_LEFT,
			FRUSTUM_PLANE_RIGHT,
			NUM_FRUSTUM_PLANES
		};

		Plane<T> planes[NUM_FRUSTUM_PLANES];

		template < typename U >
		Frustum(const U* pPlanes, size_t noPlanes);

		template < typename U >
		void ExtractPlanes(const Matrix4<U> &viewMatrix4, const Matrix4<U> &projMatrix4);

		template < typename U >
		bool BoxInFrustum(const Aabbox<U> &aabb) const;

		template < typename U >
		bool PointInFrustum(const Vector3<U> &point) const;

		template < typename U >
		bool SphereInFrustum(const Sphere<U> &sphere) const;

	};

#include "Frustum.inl"

};



#endif
