#ifndef __COLOUR_H__
#define __COLOUR_H__

#include "MathsFwd.h"
#include "FixedArray.h"

namespace Engine
{


template < typename T >
class ColourRGB : public FixedArray<3, T>
{
public: 
	typedef FixedArray<3, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

public:
	// const accessor methods
	const_reference r() const { return (*this)[0]; }
	const_reference g() const { return (*this)[1]; }
	const_reference b() const { return (*this)[2]; }

	// accessor methods
	reference r() { return (*this)[0]; }
	reference g() { return (*this)[1]; }
	reference b() { return (*this)[2]; }

	void Clamp();
	void ScaleByMax();

	void SetRGB(const ColourRGB & colourRGB);
	void SetRGB(const_reference red, const_reference green, const_reference blue);

	ColourRGB();
	ColourRGB(const_reference red, const_reference green, const_reference blue);
	ColourRGB(const ColourRGB & colourRGB);

	static const ColourRGB BLACK;
	static const ColourRGB WHITE;
	static const ColourRGB RED;
	static const ColourRGB GREEN;
	static const ColourRGB BLUE;
	static const ColourRGB INVALID;
};


template < typename T >
class ColourRGBA : public FixedArray<4, T>
{
public: 
	typedef FixedArray<4, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

public:
	// const accessor methods
	const_reference r() const { return (*this)[0]; }
	const_reference g() const { return (*this)[1]; }
	const_reference b() const { return (*this)[2]; }
	const_reference a() const { return (*this)[3]; }

	// accessor methods
	reference r() { return (*this)[0]; }
	reference g() { return (*this)[1]; }
	reference b() { return (*this)[2]; }
	reference a() { return (*this)[3]; }

	void Clamp();
	void ScaleByMax();

	void SetRGBA(const ColourRGBA & colourRGBA);
	void SetRGBA(const_reference red, const_reference green, const_reference blue, const_reference alpha);

	ColourRGBA();
	ColourRGBA(const_reference red, const_reference green, const_reference blue, const_reference alpha);
	ColourRGBA(const ColourRGBA & colourRGBA);

	static const ColourRGBA BLACK;
	static const ColourRGBA WHITE;
	static const ColourRGBA RED;
	static const ColourRGBA GREEN;
	static const ColourRGBA BLUE;
	static const ColourRGBA INVALID;
};



#include "Colour.inl"


}



#endif