#include "mathsafx.h"
#include "BitHacks.h"

namespace Engine
{

bool IsPowerOf2(UInt val)
{
	return (val > 0) && ((val & (val - 1)) == 0);
}

bool IsPowerOf2(Int val)
{
	return (val > 0) && ((val & (val - 1)) == 0);
}

UInt Log2OfPowerOfTwo(UInt powerOfTwo)
{
	UInt log2 = (powerOfTwo & 0xAAAAAAAA) != 0;
	log2 |= ((powerOfTwo & 0xFFFF0000) != 0) << 4;
	log2 |= ((powerOfTwo & 0xFF00FF00) != 0) << 3;
	log2 |= ((powerOfTwo & 0xF0F0F0F0) != 0) << 2;
	log2 |= ((powerOfTwo & 0xCCCCCCCC) != 0) << 1;
	return log2;
}

Int Log2OfPowerOfTwo(Int powerOfTwo)
{
	UInt log2 = (powerOfTwo & 0xAAAAAAAA) != 0;
	log2 |= ((powerOfTwo & 0xFFFF0000) != 0) << 4;
	log2 |= ((powerOfTwo & 0xFF00FF00) != 0) << 3;
	log2 |= ((powerOfTwo & 0xF0F0F0F0) != 0) << 2;
	log2 |= ((powerOfTwo & 0xCCCCCCCC) != 0) << 1;
	return static_cast<Int>(log2);
}

}
