#ifndef __SPHERE_H__
#define __SPHERE_H__

#include "Vector.h"

namespace Engine
{

	// The sphere is represented as |X-C| = R where C is the center and R is
	// the radius.
	template < typename T >
	class Sphere
	{
	public:
		Vector3<T> m_center;
		T m_radius;

		// Default ctor
		Sphere ();  

		template < typename U >
		Sphere (const Vector3<U>& center, const U& radius);

		template < typename U >
		Sphere (const Sphere<U>& sphere);

		// assignment
		template < typename U >
		const Sphere& operator = (const Sphere<U>& sphere);

		void TrasnfromBy(const Matrix4<T>& transform, Sphere<T>& sphere);

	};


#include "Sphere.inl"


}



#endif