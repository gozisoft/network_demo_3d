#ifndef __VIEWPORT_INL__
#define __VIEWPORT_INL__


template < typename T > const Viewport<T> Viewport<T>::ZERO(0,0,0,0);

template < typename T >
Viewport<T>::Viewport() : array_type()
{

}

template < typename T >
Viewport<T>::Viewport(const Viewport<T> &viewport) : array_type() 
{
	SetLTRB(viewport);
}

template < typename T >
void Viewport<T>::SetLTRB(const Viewport & viewport)
{
	SetLTRB ( viewport.left(), viewport.top(), viewport.right(), viewport.bottom() );
}

template < typename T >
Viewport<T>::Viewport(const_reference left, const_reference top, const_reference right, const_reference bottom)
{
	SetLTRB (left, top, right, bottom);
}

template < typename T >
void Viewport<T>::SetLTRB(const_reference left, const_reference top, const_reference right, const_reference bottom)
{
	this->left() = left;
	this->top() = top;
	this->right() = right;
	this->bottom() = bottom;
}

template < typename T >
float Viewport<T>::AspectRatio() const
{
	return ( (float)this->width() / (float)this->height() );
}

template < typename T >
T Viewport<T>::width() const
{
	return (this->right() - this->left());
}

template < typename T >
T Viewport<T>::height() const
{
	return (this->bottom() - this->top());
}

template < typename T >
bool Viewport<T>::IsPointInside(const Vector2<T>& point) const
{
	return ( this->left() <= point.x() && this->top() <= point.y() &&
			 this->right() >= point.x() && this->bottom() >= point.y() );
}







#endif