#ifndef __AABBOX_INL__
#define __AABBOX_INL__

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

template < typename T > template < typename U >
Aabbox<T>::Aabbox(const Aabbox<U>& aabb) : 
m_min(aabb.m_min),
m_max(aabb.m_max)
{
	
}

template < typename T > template < typename U >
Aabbox<T>::Aabbox(const Vector3<U>& min, const Vector3<U>& max) :
m_min(min),
m_max(max)
{

}

// get the six planes, normals pointing outwards
template < typename T > template < typename U >
void Aabbox<T>::GetPlanes(Plane<U>* pPlanes) 
{
	if (!pPlanes) 
		return;

	/*
	Edges are stored in this way:
	   3---------7	
	  /|	    /| 
	 / |       / |
	1---------5  |
	|  2- - - |- 6
	| /       | /
	|/        |/
	0---------4
	*/

	// right side
	pPlanes[0].Set(Vector3<T>(1, 0, 0), m_max);

	// left side
	pPlanes[1].Set(Vector3<T>(-1, 0, 0), m_min);

	// front side
	pPlanes[2].Set(Vector3<T>(0, 0, -1), m_min);

	// back side
	pPlanes[3].Set(Vector3<T>(0, 0, 1), m_max);

	// top side
	pPlanes[4].Set(Vector3<T>(0, 1, 0), m_max);

	// bottom side
	pPlanes[5].Set(Vector3<T>(0, -1, 0), m_min);
}

template < typename T >
Vector3<T> Aabbox<T>::GetCenter() const
{
	return (m_max + m_min) * T(0.5);
}

template < typename T >
const T& Aabbox<T>::GetSize() const
{
	return Mag(m_max - m_min);
}

template < typename T >
const T& Aabbox<T>::GetRadius() const
{
	return GetSize() * T(0.5);
}





#endif