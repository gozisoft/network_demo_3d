#ifndef __LINE2_INL__
#define __LINE2_INL__


template <typename T>
Line2<T>::Line2() {}

template <typename T>
Line2<T>::Line2(const Line2 &line) : m_origin(line.m_origin), m_direction(line.m_direction)
{

}

template <typename T>
Line2<T>::Line2(const Vector2<T> &origin, const Vector2<T> &direction) : m_origin(origin), m_direction(direction)
{

}

template <typename T>
Line2<T> & Line2<T>::operator = (const Line2 &line)
{
	m_origin = line.m_origin;
	m_direction = line.m_direction;

	return *this;
}


#endif