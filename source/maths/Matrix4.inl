#ifndef __MATRIX4_INL__
#define __MATRIX4_INL__


// static data member initialisations
template < typename T >
const Matrix4<T> Matrix4<T>::ZERO(T(0), T(0), T(0), T(0),
	T(0), T(0), T(0), T(0),
	T(0), T(0), T(0), T(0),
	T(0), T(0), T(0), T(0));


template < typename T >
const Matrix4<T> Matrix4<T>::IDENTITY (T(1), T(0), T(0), T(0),
	T(0), T(1), T(0), T(0),
	T(0), T(0), T(1), T(0),
	T(0), T(0), T(0), T(1));

//-----------------------------------------------------------------------
// Constructors
//-----------------------------------------------------------------------
template < typename T >
Matrix4<T>::Matrix4 () : array_type()
{

}

template < typename T > template < typename U >
Matrix4<T>::Matrix4 (const Matrix4<U> & mat)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), mat.GetData() );
}

template < typename T >
Matrix4<T>::Matrix4 (const_reference _m00, const_reference _m01, const_reference _m02, const_reference _m03,
					 const_reference _m10, const_reference _m11, const_reference _m12, const_reference _m13,
					 const_reference _m20, const_reference _m21, const_reference _m22, const_reference _m23,
					 const_reference _m30, const_reference _m31, const_reference _m32, const_reference _m33)
{
	m_data[0]  = _m00; m_data[1]  = _m01; m_data[2]  = _m02; m_data[3]  = _m03;
	m_data[4]  = _m10; m_data[5]  = _m11; m_data[6]  = _m12; m_data[7]  = _m13;
	m_data[8]  = _m20; m_data[9]  = _m21; m_data[10] = _m22; m_data[11] = _m23;
	m_data[12] = _m30; m_data[13] = _m31; m_data[14] = _m32; m_data[15] = _m33;
}

template < typename T >
Matrix4<T>::Matrix4 (const Vector3<type>& xaxis, const Vector3<type>& yaxis, const Vector3<type>& zaxis)
{
	m_data[0]  = xaxis[0]; m_data[1]  = xaxis[1]; m_data[2]  = xaxis[2]; 
	m_data[4]  = yaxis[0]; m_data[5]  = yaxis[1]; m_data[6]  = yaxis[2]; 
	m_data[8]  = zaxis[0]; m_data[9]  = zaxis[1]; m_data[10] = zaxis[2]; 
}

//-----------------------------------------------------------------------
// Operators = == != < <= < > >= + - * * / - += -= *= /=
//-----------------------------------------------------------------------
// assignment :: matrix

// Row major and post multiply : 
// | a b | | e f | = | ae + bg  af + bh |
// | c d | | g h | = | ce + dg  af + dh |
template < typename T > template < typename U > 
Matrix4<T> &Matrix4<T>::operator *= (const Matrix4<U>& mat)
{
	Matrix4<T> out; // temp to store data in

#if defined (PRE_MULTI)
	out[0] = (*this)[0]*mat[0] + (*this)[4]*mat[1] + (*this)[8]*mat[2] + (*this)[12]*mat[3];
	out[1] = (*this)[1]*mat[0] + (*this)[5]*mat[1] + (*this)[9]*mat[2] + (*this)[13]*mat[3];
	out[2] = (*this)[2]*mat[0] + (*this)[6]*mat[1] + (*this)[10]*mat[2] + (*this)[14]*mat[3];
	out[3] = (*this)[3]*mat[0] + (*this)[7]*mat[1] + (*this)[11]*mat[2] + (*this)[15]*mat[3];

	out[4] = (*this)[0]*mat[4] + (*this)[4]*mat[5] + (*this)[8]*mat[6] + (*this)[12]*mat[7];
	out[5] = (*this)[1]*mat[4] + (*this)[5]*mat[5] + (*this)[9]*mat[6] + (*this)[13]*mat[7];
	out[6] = (*this)[2]*mat[4] + (*this)[6]*mat[5] + (*this)[10]*mat[6] + (*this)[14]*mat[7];
	out[7] = (*this)[3]*mat[4] + (*this)[7]*mat[5] + (*this)[11]*mat[6] + (*this)[15]*mat[7];

	out[8] = (*this)[0]*mat[8] + (*this)[4]*mat[9] + (*this)[8]*mat[10] + (*this)[12]*mat[11];
	out[9] = (*this)[1]*mat[8] + (*this)[5]*mat[9] + (*this)[9]*mat[10] + (*this)[13]*mat[11];
	out[10] = (*this)[2]*mat[8] + (*this)[6]*mat[9] + (*this)[10]*mat[10] + (*this)[14]*mat[11];
	out[11] = (*this)[3]*mat[8] + (*this)[7]*mat[9] + (*this)[11]*mat[10] + (*this)[15]*mat[11];

	out[12] = (*this)[0]*mat[12] + (*this)[4]*mat[13] + (*this)[8]*mat[14] + (*this)[12]*mat[15];
	out[13] = (*this)[1]*mat[12] + (*this)[5]*mat[13] + (*this)[9]*mat[14] + (*this)[13]*mat[15];
	out[14] = (*this)[2]*mat[12] + (*this)[6]*mat[13] + (*this)[10]*mat[14] + (*this)[14]*mat[15];
	out[15] = (*this)[3]*mat[12] + (*this)[7]*mat[13] + (*this)[11]*mat[14] + (*this)[15]*mat[15];
#else
	out[0] = (*this)[0]*mat[0] + (*this)[1]*mat[4] + (*this)[2]*mat[8]  + (*this)[3]*mat[12];
	out[1] = (*this)[0]*mat[1] + (*this)[1]*mat[5] + (*this)[2]*mat[9]  + (*this)[3]*mat[13];
	out[2] = (*this)[0]*mat[2] + (*this)[1]*mat[6] + (*this)[2]*mat[10] + (*this)[3]*mat[14];
	out[3] = (*this)[0]*mat[3] + (*this)[1]*mat[7] + (*this)[3]*mat[11] + (*this)[3]*mat[15];

	out[4] = (*this)[4]*mat[0] + (*this)[5]*mat[4] + (*this)[6]*mat[8]  + (*this)[7]*mat[12];
	out[5] = (*this)[4]*mat[1] + (*this)[5]*mat[5] + (*this)[6]*mat[9]  + (*this)[7]*mat[13];
	out[6] = (*this)[4]*mat[2] + (*this)[5]*mat[6] + (*this)[6]*mat[10] + (*this)[7]*mat[14];
	out[7] = (*this)[4]*mat[3] + (*this)[5]*mat[7] + (*this)[6]*mat[11] + (*this)[7]*mat[15];

	out[8]  = (*this)[8]*mat[0] + (*this)[9]*mat[4] + (*this)[10]*mat[8]  + (*this)[11]*mat[12];
	out[9]  = (*this)[8]*mat[1] + (*this)[9]*mat[5] + (*this)[10]*mat[9]  + (*this)[11]*mat[13];
	out[10] = (*this)[8]*mat[2] + (*this)[9]*mat[6] + (*this)[10]*mat[10] + (*this)[11]*mat[14];
	out[11] = (*this)[8]*mat[3] + (*this)[9]*mat[7] + (*this)[10]*mat[11] + (*this)[11]*mat[15];

	out[12] = (*this)[12]*mat[0] + (*this)[13]*mat[4] + (*this)[14]*mat[8]  + (*this)[15]*mat[12];
	out[13] = (*this)[12]*mat[1] + (*this)[13]*mat[5] + (*this)[14]*mat[9]  + (*this)[15]*mat[13];
	out[14] = (*this)[12]*mat[2] + (*this)[13]*mat[6] + (*this)[14]*mat[10] + (*this)[15]*mat[14];
	out[15] = (*this)[12]*mat[3] + (*this)[13]*mat[7] + (*this)[14]*mat[11] + (*this)[15]*mat[15];
#endif

	(*this) = out;

	return *this;
}

template < typename T > template < typename U >
Matrix4<T> &Matrix4<T>::operator += (const Matrix4<U>& mat)
{
	return static_cast< Matrix4<T>& >( _AddAssign( static_cast< const Matrix4<U>::array_type& >(mat) ) );
}

template < typename T > template < typename U >
Matrix4<T> & Matrix4<T>::operator -= (const Matrix4<U>& mat)
{
	return static_cast< Matrix4<T>& >( _SubAssign( static_cast< const Matrix4<U>::array_type& >(mat) ) );
}

template < typename T >
Matrix4<T> & Matrix4<T>::operator *= (const_reference val)
{
	return static_cast< Matrix4<T>& >( _MulAssign( val ) );
} 

template < typename T > 
Matrix4<T> & Matrix4<T>::operator /= (const_reference val)
{
	return static_cast< Matrix4<T>& >( _DivAssign( val ) );
} 

// assignment :: set equal
template < typename T > template < typename U >
Matrix4<T>& Matrix4<T>::operator = (const Matrix4<U> & mat)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), mat.GetData() );
	return (*this);
}

//-----------------------------------------------------------------------
// External Operators
//-----------------------------------------------------------------------
// comparison
template < typename T, typename U > 
const bool operator == (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	return mat1.CompareArrays( static_cast< const Matrix4<U>::array_type& >(mat2) ) == 0;
}

template < typename T, typename U > 
const bool operator != (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	return mat1.CompareArrays( static_cast< const Matrix4<U>::array_type& >(mat2) ) != 0;
}

template < typename T, typename U > 
const bool operator < (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	return mat1.CompareArrays( static_cast< const Matrix4<U>::array_type& >(mat2) ) < 0;
}	

template < typename T, typename U > 
const bool operator <= (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	return mat1.CompareArrays( static_cast< const Matrix4<U>::array_type& >(mat2) ) <= 0;
}

template < typename T, typename U > 
const bool operator > (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	return mat1.CompareArrays( static_cast< const Matrix4<U>::array_type& >(mat2) ) > 0;
}

template < typename T, typename U > 
const bool operator >= (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	return mat1.CompareArrays( static_cast< const Matrix4<U>::array_type& >(mat2) ) >= 0;
}

// arithmatic :: scalar
template < typename T, typename U > 
const Matrix4<T> operator * (const Matrix4<T>& mat, const U &val)
{
	Matrix4<T> mat1(mat);
	return mat1 *= val;
}

template < typename T, typename U > 
const Matrix4<T> operator / (const Matrix4<T>& mat, const U &val)
{
	Matrix4<T> mat1(mat);
	return mat1 /= val;
} 

// assignment :: set negative
template < typename T > 
const Matrix4<T> operator - (Matrix4<T>& m1)
{
	return m1 *= T(-1);
}

// arithmatic :: matrix ( new = matrix + matrix )
template < typename T, typename U >
const Matrix4<T> operator + (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	Matrix4<T> mat(mat1);
	return mat += mat2;
} 

template < typename T, typename U >
const Matrix4<T> operator - (const Matrix4<T>& mat1, const Matrix4<U>& mat2)
{
	Matrix4<T> mat(mat1);
	return mat -= mat2;
}

template < typename T, typename U >
const Matrix4<T> operator * (const Matrix4<T>& mat1, const Matrix4<U>& mat2) 
{
	Matrix4<T> mat(mat1);
	return mat *= mat2;
}

/*
// arithmatic :: vector
template < typename T > 
Vector4<T> Matrix4<T>::operator * (const Vector4<T>& vec) const
{
	return Vector4<T> out((*this)[0]  * vec[0] + (*this)[1]  * vec[1] + (*this)[2]  * vec[2] + (*this)[3]  * vec[3],
		(*this)[4]  * vec[0] + (*this)[5]  * vec[1] + (*this)[6]  * vec[2] + (*this)[7]  * vec[3],
		(*this)[8]  * vec[0] + (*this)[9]  * vec[1] + (*this)[10] * vec[2] + (*this)[11] * vec[3],
		(*this)[12] * vec[0] + (*this)[13] * vec[1] + (*this)[14] * vec[2] + (*this)[15] * vec[3]);
}
*/

template < typename T, typename U >
const Vector3<T> operator * (const Matrix4<T>& mat, const Vector3<U>& vec) 
{
	Vector3<T> out( T(mat[0] * vec[0] + mat[1] * vec[1] + mat[2] * vec[2] + mat[3]),
					T(mat[4] * vec[0] +	mat[5] * vec[1] + mat[6] * vec[2] +	mat[7]),
					T(mat[8] * vec[0] +	mat[9] * vec[1] + mat[10] * vec[2] + mat[11]) );

	return out;
}

template < typename T, typename U >
const Vector3<T> operator * (const Vector3<T>& vec, const Matrix4<U>& mat) 
{
	Vector3<T> out( T( (vec[0] * mat[0]) + (vec[1] * mat[4]) + (vec[2] * mat[8]) + mat[12] ),
					T( (vec[0] * mat[1]) + (vec[1] * mat[5]) + (vec[2] * mat[9]) + mat[13] ),
					T( (vec[0] * mat[3]) + (vec[1] * mat[6]) + (vec[2] * mat[10]) + mat[14] ) );

	return out;
}


//-----------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------
// for simd investigate : 
// http://download.intel.com/design/PentiumIII/sml/24504301.pdf 
// laplace expansion theorem see :
// http://www.geometrictools.com/Documentation/LaplaceExpansionTheorem.pdf
template < typename T > 
Matrix4<T> Matrix4<T>::Inverse() const
{
	T fA0 = m_data[ 0] * m_data[ 5] - m_data[ 1] *	m_data[ 4];
	T fA1 = m_data[ 0] * m_data[ 6] - m_data[ 2] *	m_data[ 4];
	T fA2 = m_data[ 0] * m_data[ 7] - m_data[ 3] *	m_data[ 4];
	T fA3 = m_data[ 1] * m_data[ 6] - m_data[ 2] *	m_data[ 5];
	T fA4 = m_data[ 1] * m_data[ 7] - m_data[ 3] *	m_data[ 5];
	T fA5 = m_data[ 2] * m_data[ 7] - m_data[ 3] *	m_data[ 6];
	T fB0 = m_data[ 8] * m_data[13] - m_data[ 9] *	m_data[12];
	T fB1 = m_data[ 8] * m_data[14] - m_data[10] *	m_data[12];
	T fB2 = m_data[ 8] * m_data[15] - m_data[11] *	m_data[12];
	T fB3 = m_data[ 9] * m_data[14] - m_data[10] *	m_data[13];
	T fB4 = m_data[ 9] * m_data[15] - m_data[11] *	m_data[13];
	T fB5 = m_data[10] * m_data[15] - m_data[11] *	m_data[14];

	T fDet = fA0*fB5 - fA1*fB4 + fA2*fB3 + fA3*fB2 - fA4*fB1 + fA5*fB0;
	if ( Abs(fDet) <= Const<T>::EPSILON)
	{
		return Matrix4<T>::ZERO;
	}

	Matrix4<T> out;
	out.m_data[ 0] = + m_data[ 5] * fB5 - m_data[ 6] * fB4 + m_data[ 7] * fB3;
	out.m_data[ 4] = - m_data[ 4] * fB5 + m_data[ 6] * fB2 - m_data[ 7] * fB1;
	out.m_data[ 8] = + m_data[ 4] * fB4 - m_data[ 5] * fB2 + m_data[ 7] * fB0;
	out.m_data[12] = - m_data[ 4] * fB3 + m_data[ 5] * fB1 - m_data[ 6] * fB0;
	out.m_data[ 1] = - m_data[ 1] * fB5 + m_data[ 2] * fB4 - m_data[ 3] * fB3;
	out.m_data[ 5] = + m_data[ 0] * fB5 - m_data[ 2] * fB2 + m_data[ 3] * fB1;
	out.m_data[ 9] = - m_data[ 0] * fB4 + m_data[ 1] * fB2 - m_data[ 3] * fB0;
	out.m_data[13] = + m_data[ 0] * fB3 - m_data[ 1] * fB1 + m_data[ 2] * fB0;
	out.m_data[ 2] = + m_data[13] * fA5 - m_data[14] * fA4 + m_data[15] * fA3;
	out.m_data[ 6] = - m_data[12] * fA5 + m_data[14] * fA2 - m_data[15] * fA1;
	out.m_data[10] = + m_data[12] * fA4 - m_data[13] * fA2 + m_data[15] * fA0;
	out.m_data[14] = - m_data[12] * fA3 + m_data[13] * fA1 - m_data[14] * fA0;
	out.m_data[ 3] = - m_data[ 9] * fA5 + m_data[10] * fA4 - m_data[11] * fA3;
	out.m_data[ 7] = + m_data[ 8] * fA5 - m_data[10] * fA2 + m_data[11] * fA1;
	out.m_data[11] = - m_data[ 8] * fA4 + m_data[ 9] * fA2 - m_data[11] * fA0;
	out.m_data[15] = + m_data[ 8] * fA3 - m_data[ 9] * fA1 + m_data[10] * fA0;

	T fInvDet = T(1) / fDet;
	out.m_data[ 0] *= fInvDet;
	out.m_data[ 1] *= fInvDet;
	out.m_data[ 2] *= fInvDet;
	out.m_data[ 3] *= fInvDet;
	out.m_data[ 4] *= fInvDet;
	out.m_data[ 5] *= fInvDet;
	out.m_data[ 6] *= fInvDet;
	out.m_data[ 7] *= fInvDet;
	out.m_data[ 8] *= fInvDet;
	out.m_data[ 9] *= fInvDet;
	out.m_data[10] *= fInvDet;
	out.m_data[11] *= fInvDet;
	out.m_data[12] *= fInvDet;
	out.m_data[13] *= fInvDet;
	out.m_data[14] *= fInvDet;
	out.m_data[15] *= fInvDet;

	return out;
}

template < typename T > 
void Matrix4<T>::MakeZero()
{
	std::memset( this, 0, this->GetSize() * sizeof(type) );
}

template < typename T > 
void Matrix4<T>::MakeIdentity()
{
	this = Matrix4<T>::IDENTITY;
}

// Axis Angle rotation
//
//	   | 1 + (1 - cos(angle)) * (x^2 - 1)  | -z * sin(angle) + (1 - cos(angle)) * x * y | y * sin(angle) + (1 - cos(angle)) * x * z  |
// R = | z*sin(angle)+(1-cos(angle))*x*y   | 1 + (1 - cos(angle)) * (y^2 - 1)		    | -x * sin(angle) + (1 - cos(angle)) * y * z |
//	   | -y*sin(angle)+(1-cos(angle))*x*z  | x * sin(angle) + (1 - cos(angle)) * y * z  | 1 + (1-cos(angle))*(z*z-1)				 |
template < typename T > 
const Matrix4<T>& Matrix4<T>::SetRotationAxis(const Vector3<T>& axis, const_reference radians)
{
    double cs = cos(radians);
    double sn = sin(radians);
    double oneMinusCos = 1.0 - cs;
    double x2 = axis[0]*axis[0];
    double y2 = axis[1]*axis[1];
    double z2 = axis[2]*axis[2];
    double xym = axis[0]*axis[1]*oneMinusCos;
    double xzm = axis[0]*axis[2]*oneMinusCos;
    double yzm = axis[1]*axis[2]*oneMinusCos;
    double xSin = axis[0]*sn;
    double ySin = axis[1]*sn;
    double zSin = axis[2]*sn;

	(*this)[ 0] = static_cast<type>(x2*oneMinusCos + cs);
	(*this)[ 4] = static_cast<type>(xym - zSin);
	(*this)[ 8] = static_cast<type>(xzm + ySin);
	(*this)[12] = static_cast<type>(0);
	(*this)[ 1] = static_cast<type>(xym + zSin);
	(*this)[ 5] = static_cast<type>(y2*oneMinusCos + cs);
	(*this)[ 9] = static_cast<type>(yzm - xSin);
	(*this)[13] = static_cast<type>(0);
	(*this)[ 2] = static_cast<type>(xzm - ySin);
	(*this)[ 6] = static_cast<type>(yzm + xSin);
	(*this)[10] = static_cast<type>(z2*oneMinusCos + cs);
	(*this)[14] = static_cast<type>(0);
	(*this)[ 3] = static_cast<type>(0);
	(*this)[ 7] = static_cast<type>(0);
	(*this)[11] = static_cast<type>(0);
	(*this)[15] = static_cast<type>(1);

	return *this;
}


template < typename T > 
const Matrix4<T>& Matrix4<T>::SetRotateX(const_reference radians)
{
	(*this) = Matrix4<T>::IDENTITY; // set to identity matrix

	// apply x rotation (LHS)
	(*this)[5] = std::cos(radians);
	(*this)[6] = std::sin(radians);
	(*this)[9] = -(std::sin(radians));
	(*this)[10] = std::cos(radians);

	return *this;
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetRotateY(const_reference radians)
{
	(*this) = Matrix4<T>::IDENTITY; // set to identity matrix

	// apply y rotation (LHS)
	(*this)[0] = std::cos(radians);
	(*this)[2] = -(std::sin(radians));
	(*this)[8] = std::sin(radians);
	(*this)[10] = std::cos(radians);

	return *this;
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetRotateZ(const_reference radians)
{
	(*this) = Matrix4<T>::IDENTITY; // set to identity matrix

	// apply x rotation (LHS)
	(*this)[0] = std::cos(radians);
	(*this)[1] = std::sin(radians);
	(*this)[4] = -(std::sin(radians));
	(*this)[5] = std::cos(radians);

	return *this;
}

///////////////////////////////////////////////////////////////////////////////
// convert Euler angles(x,y,z) to axes(right, up, forward)
// Each column of the rotation matrix represents left, up and forward axis.
// The order of rotation is Roll->Yaw->Pitch (Rx*Ry*Rz)
// Rx: rotation about X-axis, pitch
// Ry: rotation about Y-axis, yaw(heading)
// Rz: rotation about Z-axis, roll
//      Rx           Ry          Rz
// |1   0    0| |Cy  0 -Sy| |Cz   Sz  0|   | CyCz         CySz        -Sy  |
// |0   Cx  Sx|*| 0  1  0 |*|-Sz  Cz  0| = | SxSyCz-CxSz  SxSySz+CxCz  SxCy|
// |0  -Sx  Cx| |Sy  0  Cy| | 0   0   1|   | CxSyCz+SxSz  CxSySz-SxCz  CxCy|
///////////////////////////////////////////////////////////////////////////////
template < typename T > 
const Matrix4<T>& Matrix4<T>::SetRotationRads(const Vector3<T>& rotation)
{
	// rotation angle about X-axis (pitch)
	double theta = rotation.x();
	double sx = sin(theta);
	double cx = cos(theta);

	// rotation angle about Y-axis (yaw)
	theta = rotation.y();
	double sy = sin(theta);
	double cy = cos(theta);

	// rotation angle about Z-axis (roll)
	theta = rotation.z();
	double sz = sin(theta);
	double cz = cos(theta);

	(*this)[0] = (T)( cy * cz );
	(*this)[1] = (T)( cy * sz );
	(*this)[2] = (T)( -sy );
	(*this)[3] = T(0);

	double sxsy = sx * sy;
	double cxsy = cx * sy;

	(*this)[4] = (T)( sxsy * cz - cx * sz );
	(*this)[5] = (T)( sxsy * sz + cx * cz );
	(*this)[6] = (T)( sx * cy );
	(*this)[7] = T(0);

	(*this)[8] = (T)( cxsy * cz + sx*sz );
	(*this)[9] = (T)( cxsy * sz - sx*cz );
	(*this)[10]	= (T)( cx * cy );
	(*this)[11] = T(0);

	(*this)[12] = T(0);
	(*this)[13] = T(0);
	(*this)[14] = T(0);
	(*this)[15] = T(1);
	
	return *this; 
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetRotationDegrees(const Vector3<T>& rotation)
{
	return SetRotationRads( rotation * Const<T>::TO_RAD() );
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetInverseRotationRads(const Vector3<T>& rotation)
{
	// rotation angle about X-axis (pitch)
	double theta = rotation.x();
	double sx = sin(theta);
	double cx = cos(theta);

	// rotation angle about Y-axis (yaw)
	theta = rotation.y();
	double sy = sin(theta);
	double cy = cos(theta);

	// rotation angle about Z-axis (roll)
	theta = rotation.z();
	double sz = sin(theta);
	double cz = cos(theta);

	(*this)[0] = (T)( cy * cz );
	(*this)[4] = (T)( cy * sz );
	(*this)[8] = (T)( -sy );
	(*this)[12] = T(0);

	double sxsy = sx * sy;
	double cxsy = cx * sy;

	(*this)[1 ] = (T)( sxsy * cz - cx * sz );
	(*this)[5 ] = (T)( sxsy * sz + cx * cz );
	(*this)[9 ] = (T)( sx * cy );
	(*this)[13] = T(0);

	(*this)[2 ] = (T)( cxsy * cz + sx*sz );
	(*this)[6 ] = (T)( cxsy * sz - sx*cz );
	(*this)[10]	= (T)( cx * cy );
	(*this)[14] = T(0);

	(*this)[3 ] = T(0);
	(*this)[7 ] = T(0);
	(*this)[11] = T(0);
	(*this)[15] = T(1);
	
	return *this; 
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetInverseRotationDegrees(const Vector3<T>& rotation)
{
	return SetInverseRotationRads( rotation * Const<T>::TO_RAD() );
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetTranslation(const Vector3<T>& translation)
{
	(*this)[12] = translation.x();
	(*this)[13] = translation.y();	
	(*this)[14] = translation.z();

	return *this;
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetInverseTranslation(const Vector3<T>& translation)
{
	(*this)[12] = -translation.x();
	(*this)[13] = -translation.y();	
	(*this)[14] = -translation.z();

	return *this;
}

template < typename T > 
const Matrix4<T>& Matrix4<T>::SetScale(const Vector3<T>& scale)
{
	(*this)[0] = scale.x();
	(*this)[5] = scale.y();
	(*this)[10] = scale.z();

	return *this;
}

// Get functions
template < typename T > 
Vector3<T> Matrix4<T>::GetRotation() const
{
	const Matrix4<T> &mat = *this;
	const Vector3<T> scale = mat.GetScale();
	const Vector3<double> invScale( (T)1 / scale.x(), (T)1 / scale.y(), (T)1 / scale.z() );

	double Y = -asin( mat[2] * invScale.x() );
	const double C = cos(Y);
	Y *= Const<double>::TO_DEG();

	double rotx = 0;
	double roty = 0;
	double X = 0;
	double Z = 0;

	if ( !IsZero<const double>(C) )
	{
		const double invC = (T)1 / C;
		rotx = mat[10] * invC * invScale.z();
		roty = mat[6] * invC * invScale.y();
		X = atan2( roty, rotx ) * Const<double>::TO_DEG();
		rotx = mat[0] * invC * invScale.x();
		roty = mat[1] * invC * invScale.x();
		Z = atan2( roty, rotx ) * Const<double>::TO_DEG();
	}
	else
	{
		X = 0.0;
		rotx = mat[5] * invScale.y();
		roty = -mat[4] * invScale.y();
		Z = atan2( roty, rotx ) * Const<double>::TO_DEG();
	}

	// fix values that get below zero
	// before it would set (!) values to 360
	// that were above 360:
	if (X < 0.0) X += 360.0;
	if (Y < 0.0) Y += 360.0;
	if (Z < 0.0) Z += 360.0;

	return Vector3<T>( (T)X, (T)Y, (T)Z );
}


template < typename T > 
Vector3<T> Matrix4<T>::GetTranslation() const
{
	return Vector3f( (*this)[12], (*this)[13], (*this)[14] );
}

template < typename T > 
Vector3<T> Matrix4<T>::GetScale() const
{
	// Deal with the 0 rotation case first
	if ( IsZero( (*this)[1] ) && IsZero( (*this)[2] ) &&
		 IsZero( (*this)[4] ) && IsZero( (*this)[6] ) &&
		 IsZero( (*this)[8] ) && IsZero( (*this)[9] ) )
	{
		 return Vector3f( (*this)[0], (*this)[5], (*this)[10] );
	}

	// We have to do the full calculation.
	return Vector3f( sqrt( Sqr( (*this)[0] ) +  Sqr( (*this)[1] ) +  Sqr( (*this)[2] ) ),
					 sqrt( Sqr( (*this)[4] ) +  Sqr( (*this)[5] ) +  Sqr( (*this)[6] ) ),
					 sqrt( Sqr( (*this)[8] ) +  Sqr( (*this)[9] ) +  Sqr( (*this)[10] ) ) );
}

template < typename T > 
Vector3<T> Matrix4<T>::GetRow(size_t row) const
{
	assert( row < GetRowSize() );
	return Vector3f( (*this)(row, 0), (*this)(row, 1), (*this)(row, 2) );
}

template < typename T > 
Vector3<T> Matrix4<T>::GetCol(size_t col) const
{
	assert( col < GetColSize() );
	return Vector3f( (*this)(0, col), (*this)(1, col), (*this)(2, col) );
}

template < typename T > 
void Matrix4<T>::TransformVectNormal(Vector3<T>& vect) const
{
	Vector3<T> out;

	out[0] = vect.x()*(*this)[0] + vect.y()*(*this)[4] + vect.z()*(*this)[8] + (*this)[12];
	out[1] = vect.x()*(*this)[1] + vect.y()*(*this)[5] + vect.z()*(*this)[9] + (*this)[13];
	out[2] = vect.x()*(*this)[2] + vect.y()*(*this)[6] + vect.z()*(*this)[10] + (*this)[14];

	vect = out;
}

template < typename T > 
void Matrix4<T>::TransformVectNormal(Vector3<T>& out, const Vector3<T>& in) const
{
	out[0] = in.x()*(*this)[0] + in.y()*(*this)[4] + in.z()*(*this)[8] + (*this)[12];
	out[1] = in.x()*(*this)[1] + in.y()*(*this)[5] + in.z()*(*this)[9] + (*this)[13];
	out[2] = in.x()*(*this)[2] + in.y()*(*this)[6] + in.z()*(*this)[10] + (*this)[14];
}

template < typename T > 
void Matrix4<T>::TransformVectCoord(Vector3<T>& vect) const
{
	T norm = T(0);
	norm = (*this)[3]*vect.x() + (*this)[7]*vect.y() + (*this)[11]*vect.z() + (*this)[15]; // preserve w component

	if (norm)
	{
		Vector3<T> out;

		T invNorm = T(1) / norm;

		out[0] = ( vect.x()*(*this)[0] + vect.y()*(*this)[4] + vect.z()*(*this)[8] + (*this)[12] ) * invNorm;
		out[1] = ( vect.x()*(*this)[1] + vect.y()*(*this)[5] + vect.z()*(*this)[9] + (*this)[13] ) * invNorm;
		out[2] = ( vect.x()*(*this)[2] + vect.y()*(*this)[6] + vect.z()*(*this)[10] + (*this)[14] ) * invNorm;

		vect = out;
	}
	else
	{
		vect = Vector3<T>::ZERO;
	}
}

template < typename T > 
void Matrix4<T>::TransformVectCoord(Vector3<T>& out, const Vector3<T>& in) const
{
	T norm = T(0);
	norm = (*this)[3]*in.x() + (*this)[7]*in.y() + (*this)[11]*in.z() + (*this)[15]; // preserve w component

	if (norm)
	{
		T invNorm = T(1) / norm;

		out[0] = ( in.x()*(*this)[0] + in.y()*(*this)[4] + in.z()*(*this)[8] + (*this)[12] ) * invNorm;
		out[1] = ( in.x()*(*this)[1] + in.y()*(*this)[5] + in.z()*(*this)[9] + (*this)[13] ) * invNorm;
		out[2] = ( in.x()*(*this)[2] + in.y()*(*this)[6] + in.z()*(*this)[10] + (*this)[14] ) * invNorm;
	}
	else
	{
		out = Vector3<T>::ZERO;
	}
}


//-----------------------------------------------------------------------
// External Functions
//-----------------------------------------------------------------------
// Transpose from row
// | a b | = | a c |
// | c d | = | b d |
template < typename T > 
Matrix4<T> Transpose(const Matrix4<T>& mat)
{
	return Matrix4<T> ( mat[0], mat[4], mat[8], mat[12],
					    mat[1], mat[5], mat[9], mat[13],
						mat[2], mat[6], mat[10], mat[14],
						mat[3], mat[7], mat[11], mat[15] );
}

template < typename T, typename U >
Vector3<T> TransformVectNormal(const Vector3<T>& vect, const Matrix4<U>& mat)
{
	return Vector3f( vect.x()*mat[0] + vect.y()*mat[4] + vect.z()*mat[8] + mat[12],
					 vect.x()*mat[1] + vect.y()*mat[5] + vect.z()*mat[9] + mat[13],
					 vect.x()*mat[2] + vect.y()*mat[6] + vect.z()*mat[10] + mat[14] );
}

/*
template < typename T > 
Vector3<T> TransformVectorNorm(const Matrix4<T>& mat)
{
	Vector3<T> out;

	out[0] = vect.x() * mat[0] + vect.y() * mat[4] + vect.z() * mat[8];
	out[1] = vect.x() * mat[1] + vect.y() * mat[5] + vect.z() * mat[9];
	out[2] = vect.x() * mat[2] + vect.y() * mat[6] + vect.z() * mat[10];

	vec.x() = out[0];
	vec.y() = out[1];
	vec.z() = out[2];
}
*/


// view based functions

// perspective, depth [0,1]
//   +-                                               -+
//   | 2*N/(R-L)  0           -(R+L)/(R-L)  0          |
//   | 0          2*N/(T-B)   -(T+B)/(T-B)  0          |
//   | 0          0           F/(F-N)       -N*F/(F-N) |
//   | 0          0           1             0          |
//   +-                                               -+
// produces a perpective normalised projection matrix left handed coordinate
template < typename T > 
Matrix4<T> MatrixPerspectiveNLH(const T& xleft, const T& xright, const T& ytop,
	const T& ybottom, const T& znear, const T& zfar)
{
	Matrix4<T> proj;

/*	proj[0] = (2 * znear) / (xright - xleft);
	proj[1] = T(0);
	proj[2] = T(0);
	proj[3] = T(0);

	proj[4] = T(0);
	proj[5] = (2 * znear) / (ytop - ybottom);
	proj[6] = T(0);
	proj[7] = T(0);

	proj[8] = -(xright + xleft) / (xright - xleft);
	proj[9] = -(ytop + ybottom) / (ytop - ybottom);
	proj[10] = zfar / (zfar - znear);
	proj[11] = T(1);

	proj[12] = T(0);
	proj[13] = T(0);
	proj[14] = -znear * zfar / (zfar - znear);
	proj[15] = T(0); */

	proj[0] = (2 * znear) / (xright - xleft);
	proj[1] = T(0);
	proj[2] = T(0);
	proj[3] = T(0);

	proj[4] = T(0);
	proj[5] = (2 * znear) / (ytop - ybottom);
	proj[6] = T(0);
	proj[7] = T(0);

	proj[8] = (xleft + xright) / (xleft - xright);
	proj[9] = (ytop + ybottom) / (ybottom - ytop);
	proj[10] = zfar / (zfar - znear);
	proj[11] = T(1);

	proj[12] = T(0);
	proj[13] = T(0);
	proj[14] = (znear * zfar) / (znear - zfar);
	proj[15] = T(0);

	return proj;
}


// perspective, depth [-1,1] 
//   +-                                                 -+
//   | 2*N/(R-L)  0           -(R+L)/(R-L)  0            |
//   | 0          2*N/(T-B)   -(T+B)/(T-B)  0            |
//   | 0          0           (F+N)/(F-N)   -2*F*N/(F-N) |
//   | 0          0           1             0			 |
//   +-                                                 -+
// produces a perpective projection matrix left handed coordinate (non-normalised)
template < typename T > 
Matrix4<T> MatrixPerspectiveLH(const T& xleft, const T& xright, const T& ytop,
	const T& ybottom, const T& znear, const T& zfar)
{
	Matrix4<T> proj;

/*	proj[0] = (2 * znear) / (xright - xleft);
	proj[1] = T(0);
	proj[2] = T(0);
	proj[3] = T(0);

	proj[4] = T(0);
	proj[5] = (2 * znear) / (ytop - ybottom);
	proj[6] = T(0);
	proj[7] = T(0);

	proj[8] = -(xright + xleft) / (xright - xleft);
	proj[9] = -(ytop + ybottom) / (ytop - ybottom);
	proj[10] = (zfar + znear) / (zfar - znear);
	proj[11] = T(1);

	proj[12] = T(0);
	proj[13] = T(0);
	proj[14] = -2 * zfar * znear / (zfar - znear);
	proj[15] = T(0); */

	proj[0] = (2 * znear) / (xright - xleft);
	proj[1] = T(0);
	proj[2] = T(0);
	proj[3] = T(0);

	proj[4] = T(0);
	proj[5] = (2 * znear) / (ytop - ybottom);
	proj[6] = T(0);
	proj[7] = T(0);

	proj[8] = (xleft + xright) / (xleft - xright);
	proj[9] = (ytop + ybottom) / (ybottom - ytop);
	proj[10] = zfar / (zfar - znear);
	proj[11] = T(1);

	proj[12] = T(0);
	proj[13] = T(0);
	proj[14] = (2 * zfar * znear) / (znear - zfar);
	proj[15] = T(0);

	return proj;
}

// produces a view matrix
template < typename T > 
Matrix4<T> MatrixLookAtLH(const Vector3<T> & up, const Vector3<T> & lookat, const Vector3<T> & pos)
{
	Vector3<T> zaxis = Normalised( lookat - pos );
	Vector3<T> xaxis = Normalised( Cross(up, zaxis) );
	Vector3<T> yaxis = Cross(zaxis, xaxis);

	const float epsilon = 0.01f;
	float det = Dot( lookat, Cross(up, right) );
	if (Abs(1.0f - det) > epsilon)
	{
		OrthoNormaliseVectors(lookat, up, right);
	}

	Matrix4<T> viewMat = Matrix4<T>::IDENTITY;
	viewMat(0,0) = xaxis.x();
	viewMat(0,1) = yaxis.x();
	viewMat(0,2) = zaxis.x();
	viewMat(1,0) = xaxis.y();
	viewMat(1,1) = yaxis.y();
	viewMat(1,2) = zaxis.y();
	viewMat(2,0) = xaxis.z();
	viewMat(2,1) = yaxis.z();
	viewMat(2,2) = zaxis.z();

	return viewMat;
}

/*
// produces a view matrix
template < typename T > 
Matrix4<T> MatrixLookAtLH(const Vector3<T> & up, const Vector3<T> & lookat, const Vector3<T> & pos)
{
	Vector3<T> right = Normalised( Cross(up, lookat) );

	const float epsilon = 0.01f;
	float det = Dot( lookat, Cross(up, right) );
	if (Abs(1.0f - det) > epsilon)
	{
		OrthoNormaliseVectors(lookat, up, right);
	}

	Matrix4<T> viewMat;
	viewMat(0,0) = m_right[0];
	viewMat(0,1) = m_right[1];
	viewMat(0,2) = m_right[2];
	viewMat(0,3) = -Dot(m_pos, m_right);
	viewMat(1,0) = m_up[0];
	viewMat(1,1) = m_up[1];
	viewMat(1,2) = m_up[2];
	viewMat(1,3) = -Dot(m_pos, m_up);
	viewMat(2,0) = m_lookat[0];
	viewMat(2,1) = m_lookat[1];
	viewMat(2,2) = m_lookat[2];
	viewMat(2,3) = -Dot(m_pos, m_lookat);
	viewMat(3,0) = 0.0f;
	viewMat(3,1) = 0.0f;
	viewMat(3,2) = 0.0f;
	viewMat(3,3) = 1.0f;

	return viewMat;
}

// Common names for orthographic projections include plan, cross-section, bird's-eye, and elevation.

// orthographic, depth [0,1]
//   +-                                       -+
//   | 2/(R-L)  0  0              -(R+L)/(R-L) |
//   | 0        2/(T-B)  0        -(T+B)/(T-B) |
//   | 0        0        1/(F-N)  -N/(F-N)  0  |
//   | 0        0        0        1            |
//   +-                                       -+
template < typename T > 
Matrix4<T> MatrixOrthoZeroOneLH(const T& left, const T& right,
	const T& top, const T& bottom, const T& near, const T& far)
{
	Matrix4<T> ortho;

	if ( (right == -left) && (top == -bottom) ) 
	{
		ortho(0,0) = T(2) / right - left;
		ortho(0,1) = T(0);
		ortho(0,2) = T(0);
		ortho(0,3) = T(0);
		ortho(1,0) = T(0);
		ortho(1,1) = T(2) / top - bottom;
		ortho(1,2) = T(0);
		ortho(1,3) = T(0);
		ortho(2,0) = T(0);
		ortho(2,1) = T(0);
		ortho(2,2) = T(-1) / far - near;
		ortho(2,3) = -(near / far - near);
		ortho(3,0) = T(0);
		ortho(3,1) = T(0);
		ortho(3,2) = T(0);
		ortho(3,3) = T(1);
	}
	else
	{
		ortho(0,0) = T(2) / right - left;
		ortho(0,1) = T(0);
		ortho(0,2) = T(0);
		ortho(0,3) = -(right + left / right - left);
		ortho(1,0) = T(0);
		ortho(1,1) = T(2) / top - bottom;
		ortho(1,2) = T(0);
		ortho(1,3) = -(top + bottom / top - bottom);
		ortho(2,0) = T(0);
		ortho(2,1) = T(0);
		ortho(2,2) = T(-1) / far - near;
		ortho(2,3) = -(near / far - near);
		ortho(3,0) = T(0);
		ortho(3,1) = T(0);
		ortho(3,2) = T(0);
		ortho(3,3) = T(1);
	}

	return ortho;
}

// orthographic, depth [-1,1]
//   +-                                       -+
//   | 2/(R-L)  0        0        -(R+L)/(R-L) |
//   | 0        2/(T-B)  0        -(T+B)/(T-B) |
//   | 0        0        2/(F-N)  -(F+N)/(F-N) |
//   | 0        0        0        1            |
//   +-                                       -+
template < typename T > 
Matrix4<T> MatrixOrthoMinusOneOneLH(const T& left, const T& right,
	const T& top, const T& bottom, const T& near, const T& far)
{
	Matrix4<T> out;

	if ( (right == -left) && (top == -bottom) ) 
	{
		out(0,0) = T(1) / right - left;
		out(0,1) = T(0);
		out(0,2) = T(0);
		out(0,3) = T(0);
		out(1,0) = T(0);
		out(1,1) = T(1) / top - bottom;
		out(1,2) = T(0);
		out(1,3) = T(0);
		out(2,0) = T(0);
		out(2,1) = T(0);
		out(2,2) = T(-2) / far - near;
		out(2,3) = -(far + near / far - near);
		out(3,0) = T(0);
		out(3,1) = T(0);
		out(3,2) = T(0);
		out(3,3) = T(1);
	}
	else
	{
		out(0,0) = T(2) / right - left;
		out(0,1) = T(0);
		out(0,2) = T(0);
		out(0,3) = -(right + left / right - left);
		out(1,0) = T(0);
		out(1,1) = T(2) / top - bottom;
		out(1,2) = T(0);
		out(1,3) = -(top + bottom / top - bottom);
		out(2,0) = T(0);
		out(2,1) = T(0);
		out(2,2) = T(-2) / far - near;
		out(2,3) = -(far + near / far - near);
		out(3,0) = T(0);
		out(3,1) = T(0);
		out(3,2) = T(0);
		out(3,3) = T(1);
	}

	return out;
}

*/

#endif // MATRIX_INL_



