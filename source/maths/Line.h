#ifndef __LINE2_H__
#define __LINE2_H__

namespace Engine
{

template <typename T>
class Line2
{
public:
	Line2();
	Line2(const Line2 &line);
	Line2(const Vector2<T> &origin, const Vector2<T> &direction);

	Line2 &operator = (const Line2 &line);

	Vector2<T> m_origin;
	// unit length direction vector.
	Vector2<T> m_direction;
};

template <typename T>
class Line3
{
public:
	Line3();
	Line3(const Line3 &line);
	Line3(const Vector3<T> &origin, const Vector3<T> &direction);

	Line3 &operator = (const Line3 &line);

	Vector3<T> m_origin;
	// unit length direction vector.
	Vector3<T> m_direction;
};



#include "Line2.inl"
#include "Line3.inl"


}


#endif