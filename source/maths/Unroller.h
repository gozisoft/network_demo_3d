#ifndef __UNROLLER_H__
#define __UNROLLER_H__

#include "MathsFwd.h"
#include <cstddef>
#include <cstring>


using std::size_t;

namespace Engine
{

//=============================================================================
// Unrolled operations
//=============================================================================
struct AssignOpAssign
{
	template <typename _lhs, typename _rhs >
	inline static void doOperation(_lhs & lhs, _rhs & rhs) { lhs = rhs; }
};

struct AssignOpAdd
{
	template <typename _lhs, typename _rhs >
	inline static void doOperation(_lhs & lhs, _rhs & rhs) { lhs += rhs; }
};

struct AssignOpSub
{
	template <typename _lhs, typename _rhs >
	inline static void doOperation(_lhs & lhs, _rhs & rhs) { lhs -= rhs; }
};

struct AssignOpMul
{
	template <typename _lhs, typename _rhs >
	inline static void doOperation(_lhs & lhs, _rhs & rhs) { lhs *= rhs; }
};

//=============================================================================
// The unroller struct for Vector * Vector or other operators
//=============================================================================
template <typename _lhs, typename _rhs, class _operator>
struct AssignOpLoopUnroller
{
	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = N - 1 };

		inline static void eval(_lhs *LArray, _rhs * __restrict RArray)
		{
			_operator::doOperation( LArray[Nxt_I], RArray[Nxt_I] );
			Loop<Nxt_I>::eval(LArray, RArray);
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static void eval(_lhs * __restrict aLArray, _rhs * __restrict aRArray) 
		{ 
			UNUSED_PARAMETER(aLArray);
			UNUSED_PARAMETER(aRArray);
		}
	};
};

//=============================================================================
// The unroller struct for Vector * scalar or other operators
//=============================================================================
template <typename _lhs, typename _rhs, class _operator>
struct ScalarOpLoopUnroller
{
	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = N - 1 };

		inline static void eval(_lhs *aLArray, _rhs scalarVal)
		{
			_operator::doOperation( aLArray[Nxt_I], scalarVal );
			Loop<Nxt_I>::eval(aLArray, scalarVal);
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static void eval(_lhs * __restrict aLArray, _rhs scalarVal)
		{ 
			UNUSED_PARAMETER(aLArray);
			UNUSED_PARAMETER(scalarVal);
		}
	};
};

//=============================================================================
// The unroller struct for Dot products : Vector Dot Vector
//=============================================================================
template <typename _lhs, typename _rhs>
struct DotProductUnroller
{
	template < size_t N >
	struct Loop
	{
		enum { Nxt_I = N - 1 };
		enum { Nxt_Nxt_I = Nxt_I - 1 };

		inline static _lhs eval(_lhs *LArray, _rhs *RArray)
		{
			return LArray[Nxt_I] * RArray[Nxt_I] + Loop<Nxt_I>::eval( LArray[Nxt_Nxt_I], RArray[Nxt_Nxt_I] );	
		}
	};

	template <> // specialisation for zero case (loop zero)
	struct Loop<0>
	{
		inline static void eval(_lhs *aLArray, _rhs *aRArray) 
		{ 
			return LArray[0] * RArray[0];
		}
	};
};


}

#endif