#ifndef __LINE3_INL__
#define __LINE3_INL__


template <typename T>
Line3<T>::Line3() {}

template <typename T>
Line3<T>::Line3(const Line3 &line) : m_origin(line.m_origin), m_direction(line.m_direction)
{

}

template <typename T>
Line3<T>::Line3(const Vector3<T> &origin, const Vector3<T> &direction) : m_origin(origin), m_direction(direction)
{

}

template <typename T>
Line3<T> & Line3<T>::operator = (const Line3 &line)
{
	m_origin = line.m_origin;
	m_direction = line.m_direction;

	return *this;
}




#endif