#ifndef __RAYCAST_H__
#define __RAYCAST_H__

#include "Vector.h"
#include "Plane.h"

namespace Engine
{


	template < typename T >
	class Raycast
	{
	public:
		Vector3<T> m_origin;				// ray origin
		Vector3<T> m_direction;				// ray direction

		Raycast();
		Raycast(const Vector3<T>& origin, const Vector3<T>& direction);

		// set the orgin and direction of the ray
		void set(const Vector3<T>& origin, const Vector3<T>& direction);

		// Triangle
		bool intersects(const Vector3<T>& vc0, const Vector3<T>& vc1,
			const Vector3<T>& vc2, bool cull,
			T &disRayOrigin) const;

		// Triange (ray with finite distance)
		// fl is length of ray for which you want to check an intersection
		// &disRayOrigin is the distance from the ray origin to the point of intersection 
		bool intersects(const Vector3<T>& vc0, const Vector3<T>& vc1,
			const Vector3<T>& vc2, bool cull,
			T raylength, T &disRayOrigin) const;

		// plane tests
		bool intersects(const Plane<T> &plane, bool cull,
			T &disRayOrigin, Vector3<T> &vcHit) const;

		bool intersects(const Plane<T> &plane, bool cull,
			T raylength, T &disRayOrigin, Vector3<T> &vcHit) const;

		// aabb tests
		bool intersects(const Aabbox<T> &aabb, T &disRayOrigin) const;

		bool intersects(const Aabbox<T> &aabb, T raylength, T &disRayOrigin) const;

	}; 


#include "Raycast.inl"


}

#endif