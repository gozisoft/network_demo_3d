#ifndef __PLANE_INL__
#define __PLANE_INL__

template < typename T >
Plane<T>::Plane() : m_disOrigin(T(0))
{

}

template < typename T > template < typename U >	
Plane<T>::Plane(const Plane<U>& plane) :
m_normal(plane.m_normal),
m_disOrigin(plane.m_disOrigin)
{

}

template < typename T > template < typename U >
const Plane<T>& Plane<T>::operator = (const Plane<U>& plane)
{
	m_normal = plane.m_normal;
	m_disOrigin = plane.m_disOrigin;
}

template < typename T >
Plane<T>::Plane(const T& x, const T& y, const T& z, const T& distance)
{
	Set(x, y, z, distance);
}

template < typename T >
Plane<T>::Plane(const Vector3<T>& point, const Vector3<T>& normal)
{
	FromPointNormal(point, normal);
}

template < typename T >
Plane<T>::Plane(const Vector3<T> &point1, const Vector3<T> &point2, const Vector3<T> &point3)
{
	FromPoints(point1, point2, point3);
}

template < typename T >
void Plane<T>::Set(const T& x, const T& y, const T& z, const T& distance)
{
	m_normal.SetXYZ(x, y, z);
	m_disOrigin = -distance;
}

template < typename T >
void Plane<T>::Set(const Vector3<T>& normal, const T& distance)
{
	m_normal = normal;
	m_disOrigin = -distance;
}

// Calculate distance to point. Plane normal must be normalized.
template < typename T >
const T& Plane<T>::Distance(const Vector3<T>& point) const
{
	return ( Abs( (m_normal * point) - m_disOrigin ) );
}

template < typename T >  
void Plane<T>::PlaneNormalise()
{
	T length = T(1) / Mag(m_normal);
    m_normal *= length;
    m_disOrigin *= length;
}

template < typename T >
void Plane<T>::FromPointNormal(const Vector3<T> &normal, const Vector3<T> &point)
{
	Set(normal, Dot(normal, point));
	PlaneNormalise();
}

template < typename T >
void Plane<T>::FromPoints(const Vector3<T> &point1, const Vector3<T> &point2, const Vector3<T> &point3)
{
	m_normal = Normalised( CrossProduct(point2 - point1, point3 - point1) );  
    m_disOrigin = -Dot(m_normal, point1);
}




#endif