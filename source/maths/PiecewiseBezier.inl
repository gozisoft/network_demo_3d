#ifndef __PIECEWISE_BEZIER_CURVE_INL__
#define __PIECEWISE_BEZIER_CURVE_INL__

template < typename Element >
PieceWiseBezier<Element>::PieceWiseBezier(const curve_array& bezierArray)
{
	// Go through array of curves
	for (constCurveItor itor = bezierArray.begin(); itor != bezierArray.end(); ++itor)
	{
		curve_type pCurve = *itor;

		AddSingleCurveC1(pCurve);	
	}

}

template < typename Element >
PieceWiseBezier<Element>::PieceWiseBezier(const curve_type* pArray, size_t size)
{


}

template < typename Element >
PieceWiseBezier<Element>::~PieceWiseBezier()
{

}

template < typename Element >
void PieceWiseBezier<Element>::AddSingleCurveC0(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		const curve_type &curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		const Vector3<type> *pCtrlPoints = curve.GetControlPoints();
		size_t lastControlPointIndex = curve.GetNumCtrlPoints();

		// Get the end control point of stored curve
		const Vector3<type> &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];

		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		using std::vector;
		vector< Vector3<type> > NewCtrlPoints( bezierCurve.GetControlPoints(), bezierCurve.GetControlPoints() + bezierCurve.GetNumCtrlPoints() );

		// Set it equal to the first control point of the new bezier curve (c0 continuity)
		NewCtrlPoints[0] = endCtrlPoint;

		// Create the new tweaked curve
		curve_type newCurve(NewCtrlPoints);

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(newCurve);
	}

}

template < typename Element >
void PieceWiseBezier<Element>::AddSingleCurveC1(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		curve_type &curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		const Vector3<type> *pCtrlPoints = curve.GetControlPoints();
		size_t lastControlPointIndex = curve.GetNumCtrlPoints();

		// Get the end control point of stored curve and second end point
		const Vector3<type> &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];
		const Vector3<type> &oneLessThanEndCtrlPoint = pCtrlPoints[ lastControlPointIndex - 2 ];
		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		using std::vector;
		vector< Vector3<type> > NewCtrlPoints( bezierCurve.GetControlPoints(), bezierCurve.GetControlPoints() + bezierCurve.GetNumCtrlPoints() );

		// Set it equal to the first control point of the new bezier curve (c1 continuity)
		NewCtrlPoints[0] = endCtrlPoint;

		// now set tangent of point of the previous to equal that of new.
		NewCtrlPoints[1] = (endCtrlPoint - oneLessThanEndCtrlPoint) + NewCtrlPoints[0];
	
		// Create the new tweaked curve
		curve_type newCurve(NewCtrlPoints);	

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(newCurve);
	}
}


template < typename Element >
void PieceWiseBezier<Element>::AddSingleCurveC2(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		curve_type &curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		const Vector3<type> *pCtrlPoints = curve.GetControlPoints();
		size_t lastControlPointIndex = curve.GetNumCtrlPoints();

		// Get the end control point of stored curve and second end point
		const Vector3<type> &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];
		const Vector3<type> &oneLessThanEndCtrlPoint = pCtrlPoints[ lastControlPointIndex - 2 ];
		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		using std::vector;
		vector< Vector3<type> > NewCtrlPoints( bezierCurve.GetControlPoints(), bezierCurve.GetControlPoints() + bezierCurve.GetNumCtrlPoints() );

		// Set it equal to the first control point of the new bezier curve (c1 continuity)
		NewCtrlPoints[0] = endCtrlPoint;

		// now set tangent of point of the previous to equal that of new.
		NewCtrlPoints[1] = (endCtrlPoint - oneLessThanEndCtrlPoint) + NewCtrlPoints[0];

		// Create the new tweaked curve
		curve_type newCurve(NewCtrlPoints);	

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(newCurve);
	}
}

template < typename Element >
const BezierCurve<Element>& PieceWiseBezier<Element>::GetCurve(size_t index) const
{
	return m_bezierCurves.at(index);
}

template < typename Element >
size_t PieceWiseBezier<Element>::GetNumCurves() const
{
	return m_bezierCurves.size();
}


#endif



/*

template < typename Element >
PieceWiseBezier<Element>::PieceWiseBezier(const curve_array& bezierArray) 
{
	// Go through array of curves
	for (curveItor itor = bezierArray.begin(); itor != bezierArray.end(); ++itor)
	{
		curve_type* pCurve = *itor;
		curve_array ctrlpoints = pCurve->GetControlPoints();

		if (m_pCtrlPoints.empty() == 0)
		{



		}


	}

}

template < typename Element >
PieceWiseBezier<Element>::PieceWiseBezier(const curve_type* pArray, size_t size)
{


}

template < typename Element >
PieceWiseBezier<Element>::~PieceWiseBezier()
{

}

template < typename Element >
void PieceWiseBezier<Element>::AddSingleCurveC0(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back( bezierCurve );
	}
	else // not the first curve
	{	
		// ----------------------------------------------------------
		// Previous curve
		// ----------------------------------------------------------
		// Get the end curve
		curve_type curve = m_bezierCurves.back();
		
		// Get the control points and index the last end one
		Vector3<type> *pCtrlPoints = curve->GetControlPoints();
		size_t lastControlPointIndex = curve->GetNumCtrlPoints();

		// Get the end control point of stored curve
		Vector3<type> &endCtrlPoint = pCtrlPoints[ lastControlPointIndex - 1 ];

		// ----------------------------------------------------------
		// Input curve
		// ----------------------------------------------------------
		// Get the control points and index the last first one
		const Vector3<type> *Const_pCtrlPoints = bezierCurve->GetControlPoints();
		size_t startControlPointIndex = bezierCurve->GetNumCtrlPoints();

		// Get the start control point of the new curve
		const Vector3<type> &startCtrlPoint = Const_pCtrlPoints[ lastControlPointIndex - 1 ];

		// Set it equal to the first control point of the new bezier curve (c0 continuity)
		endCtrlPoint = startCtrlPoint;

		// ----------------------------------------------------------
		// Adding the new curve to the list
		// ----------------------------------------------------------
		m_bezierCurves.push_back(bezierCurve);
	}

}

template < typename Element >
void PieceWiseBezier<Element>::AddSingleCurveC1(const curve_type& bezierCurve)
{
	// if the first curve
	if ( m_bezierCurves.empty() )
	{
		m_bezierCurves.push_back(bezierCurve);
	}
	else // not the first curve
	{




	}

}

template < typename Element >
const shared_ptr< BezierCurve<Element> >& PieceWiseBezier<Element>::GetCurve(size_t index) const
{
	return m_bezierCurves.get(index);
}
*/