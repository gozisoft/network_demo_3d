#ifndef __MATHS_FORWARD_H__
#define __MATHS_FORWARD_H__

#include <cstddef>

namespace Engine
{

// Const type
template < typename T = float > struct Const;

// Base vector class
template < size_t Size, typename Element > class FixedArray;
// Base matrix class
template < size_t Rows, size_t Cols, typename Element >	class TableArray;

// ----------------------------------------------------------------------------------------
// Linear Algebra
// ----------------------------------------------------------------------------------------
template < typename T = float > class Vector2;
template < typename T = float > class Vector3;
template < typename T = float > class Vector4;
template < typename T = float >	class Matrix4;

// vector typedefs
typedef Vector2<int> Vector2i;
typedef Vector2<long> Vector2l;
typedef Vector2<float> Vector2f;
typedef Vector2<double> Vector2d;
typedef Vector2<unsigned int> Vector2u;

// vector typedefs
typedef Vector2<int> Point2i;
typedef Vector2<long> Point2l;
typedef Vector2<float> Point2f;
typedef Vector2<double> Point2d;
typedef Vector2<unsigned int> Point2u;

// vector typedefs
typedef Vector3<int> Vector3i;
typedef Vector3<long> Vector3l;
typedef Vector3<float> Vector3f;
typedef Vector3<double> Vector3d;
typedef Vector3<unsigned int> Vector3u;

// vector typedefs
typedef Vector3<int> Point3i;
typedef Vector3<long> Point3l;
typedef Vector3<float> Point3f;
typedef Vector3<double> Point3d;
typedef Vector3<unsigned int> Point3u;

// vector typedefs
typedef Vector4<int> Vector4i;
typedef Vector4<long> Vector4l;
typedef Vector4<float> Vector4f;
typedef Vector4<double> Vector4d;
typedef Vector4<unsigned int> Vector4u;

// vector typedefs
typedef Vector4<int> Point4i;
typedef Vector4<long> Point4l;
typedef Vector4<float> Point4f;
typedef Vector4<double> Point4d;
typedef Vector4<unsigned int> Point4u;

// Matrix typedef
typedef Matrix4<int> Matrix4i;
typedef Matrix4<long> Matrix4l;
typedef Matrix4<float> Matrix4f;
typedef Matrix4<double> Matrix4d;
typedef Matrix4<unsigned int> Matrix4u;

// ----------------------------------------------------------------------------------------
// Engine Tools
// ----------------------------------------------------------------------------------------
template < typename T = float >	class ColourRGB;
template < typename T = float >	class ColourRGBA;
template < typename T = long >	class Viewport;
template < typename T = float > class Frustum;

// Colour typedefs
typedef ColourRGB<float> Colour3f;
typedef ColourRGB<double> Colour3d;

// Colour typedefs
typedef ColourRGBA<float> Colour4f;
typedef ColourRGBA<double> Colour4d;

// Viewport typedefs
typedef Viewport<int> Viewporti;
typedef Viewport<long> Viewportl;
typedef Viewport<float> Viewportf;
typedef Viewport<double> Viewportd;
typedef Viewport<unsigned int> Viewportu;

// Frustum typedefs
typedef Frustum<float> Frustumf;
typedef Frustum<double> Frustumd;

// ----------------------------------------------------------------------------------------
// Curves
// ----------------------------------------------------------------------------------------
template < typename T = float > class BezierCurve;
template < typename T = float > class PieceWiseBezier;

// Bezier Curve
typedef BezierCurve<float> BezierCurvef;
typedef BezierCurve<double> BezierCurved;
typedef PieceWiseBezier<float> PieceWiseBezierf;
typedef PieceWiseBezier<double> PieceWiseBezierd;


// ----------------------------------------------------------------------------------------
// Collision
// ----------------------------------------------------------------------------------------
template < typename T = float > class Raycast;
template < typename T = float > class Plane;
template < typename T = float > class Aabbox;
template < typename T = float > class Sphere;

// Shapes
template < typename T = float > class Triangle2;
template < typename T = float > class Triangle3;
template < typename T = float > class Line2;
template < typename T = float > class Line3;

// Raycast typedefs
typedef Raycast<float> Raycastf;
typedef Raycast<double> Raycastd;

// Plane typedefs
typedef Plane<float> Planef;
typedef Plane<double> Planed;

// Aabbox typedefs
typedef Aabbox<float> Aabboxf;
typedef Aabbox<double> Aabboxd;

// Sphere typedefs
typedef Sphere<float> Spheref;
typedef Sphere<double> Sphered;

// Triangle typedef
typedef Triangle2<float> Triangle2f;
typedef Triangle2<double> Triangle2d;
typedef Triangle3<float> Triangle3f;
typedef Triangle3<double> Triangle3d;

// Line typedef
typedef Line2<float> Line2f;
typedef Line2<double> Line2d;
typedef Line3<float> Line3f;
typedef Line3<double> Line3d;





}



#endif
