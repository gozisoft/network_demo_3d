#ifndef __VECTOR2_INL__
#define __VECTOR2_INL__


template < typename T > const Vector2<T> Vector2<T>::UNIT_X (T(1), T(0));
template < typename T > const Vector2<T> Vector2<T>::UNIT_Y (T(0), T(1));

template < typename T > const Vector2<T> Vector2<T>::ZERO (T(0), T(0));
template < typename T > const Vector2<T> Vector2<T>::ONE (T(1), T(1));

//-----------------------------------------------------------------------
// Constructors
//-----------------------------------------------------------------------
template < typename T >
Vector2<T>::Vector2() : array_type()
{
	SetXY(0,0);
}

template < typename T > template < typename U >
Vector2<T>::Vector2(const Vector2<U> & vector2)
{
	(*this) = vector2;
}

template < typename T >
Vector2<T>::Vector2(const_reference x, const_reference y)
{
	SetXY(x, y);	
}

//-----------------------------------------------------------------------
// Operators
//-----------------------------------------------------------------------
// assignment :: Vector
template < typename T > template < typename U >
Vector2<T> & Vector2<T>::operator += (const Vector2<U> & v1)
{
	return static_cast< Vector2<T>& >( this->_AddAssign( static_cast< const Vector2<U>::array_type& >(v1) ) );
}

template < typename T > template < typename U >
Vector2<T> & Vector2<T>::operator -= (const Vector2<U> & v1)
{
	return static_cast<Vector2<T>&>( this->_SubAssign( static_cast< const Vector2<U>::array_type& >(v1) ) );
}

template < typename T > template < typename U >
Vector2<T> & Vector2<T>::operator *= (const Vector2<U> & v1)
{
	return static_cast<Vector2<T>&>( this->_MulAssign( static_cast< const Vector2<U>::array_type& >(v1) ) );
}

// assignment :: scalar 
template < typename T >
Vector2<T> & Vector2<T>::operator *= (const_reference val)
{
	return static_cast<Vector2<T>&>( this->_MulAssign( val ) );
}

template < typename T >
Vector2<T> & Vector2<T>::operator /= (const_reference val)
{
	return static_cast<Vector2<T>&>( this->_DivAssign( val ) );
}

// assignment :: set equal
template < typename T > template < typename U > 
Vector2<T>& Vector2<T>::operator = (const Vector2<U>& v1)
{
	AssignOpLoopUnroller<type, const U, AssignOpAssign>::Loop<m_arraySize>::eval( GetData(), v1.GetData() );
	return (*this);
}


//-----------------------------------------------------------------------
// External Operators
//-----------------------------------------------------------------------
// Vector

template < typename T, typename U >
const Vector2<T> operator + (const Vector2<T> v1, const Vector2<U> &v2)
{
	Vector2<T> out(v1);
	return out += v2;
}

template < typename T, typename U >
const Vector2<T> operator - (const Vector2<T> v1, const Vector2<U> &v2)
{
	Vector2<T> out(v1);
	return out -= v2;
}

// Not mathimatically defined
template < typename T, typename U >
const Vector2<T> operator * (const Vector2<T> v1, const Vector2<U> &v2)
{
	Vector2<T> out(v1);
	return out *= v2;
}

// Scalar
//! multiplication by scalar
template < typename T, typename U >
const Vector2<T> operator * (const Vector2<T> v, const U &s)
{
	Vector2<T> out(v);
	return out *= s;
}

//! multiplication by scalar
template < typename T, typename U >
const Vector2<T> operator * (const U &s, const Vector2<T> v)
{
	Vector2<T> out(v);
	return out *= s;
}

//! division by scalar
template < typename T, typename U >
const Vector2<T> operator / (const Vector2<T> v, const U &s)
{
	Vector2<T> out(v);
	return out /= s;
}

//! division by scalar
template < typename T, typename U >
const Vector2<T> operator / (const U &s, const Vector2<T> v)
{
	Vector2<T> out(v);
	return out /= s;
}

// assignment :: set negative
template < typename T >
const Vector2<T> operator - (Vector2<T> v1)
{
	return v1 *= T(-1);
}

// comparison
template < typename T, typename U > 
const bool operator == (const Vector2<T> & vec1, const Vector2<U> & vec2)
{
	return vec1.CompareArrays( static_cast< const Vector2<U>::array_type& >(vec2) ) == 0;
}

template < typename T, typename U > 
const bool operator != (const Vector2<T> & vec1, const Vector2<U> & vec2)
{
	return vec1.CompareArrays( static_cast< const Vector2<U>::array_type& >(vec2) ) != 0;
}

template < typename T, typename U > 
const bool operator < (const Vector2<T> & vec1, const Vector2<U> & vec2)
{
	return vec1.CompareArrays( static_cast< const Vector2<U>::array_type& >(vec2) ) < 0;
}	

template < typename T, typename U > 
const bool operator <= (const Vector2<T> & vec1, const Vector2<U> & vec2)
{
	return vec1.CompareArrays( static_cast< const Vector2<U>::array_type& >(vec2) ) <= 0;
}

template < typename T, typename U > 
const bool operator > (const Vector2<T> & vec1, const Vector2<U> & vec2)
{
	return vec1.CompareArrays( static_cast< const Vector2<U>::array_type& >(vec2) ) > 0;
}

template < typename T, typename U > 
const bool operator >= (const Vector2<T> & vec1, const Vector2<U> & vec2)
{
	return vec1.CompareArrays( static_cast< const Vector2<U>::array_type& >(vec2) ) >= 0;
}

//-----------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------
template < typename T >
void Vector2<T>::SetXY(const_reference x, const_reference y)
{
	this->x() = x;
	this->y() = y;
}

template < typename T >
const Vector2<T> & Vector2<T>::Normalise()
{
	return !IsZero(*this) ? (*this) /= Mag(*this) : (*this);
}

//! modulus / length / magnitude
template < typename T >
const float Mag(const Vector2<T> &v)
{
	return sqrt(Dot(v,v));
}

//! modulus squared (removes square root for faster comparisons)
template < typename T >
const float MagSqrd(const Vector2<T> &v)
{
	return Dot(v,v);
}

//! euclidean distance
template < typename T, typename U >
const T Dist(const Vector2<T> &v1, const Vector2<U> &v2)
{
	return Mag(v1-v2);
}

//! euclidean distance squared (again, for comparisons)
template < typename T, typename U >
const T DistSqrd(const Vector2<T> &v1, const Vector2<U> &v2)
{
	return MagSqrd(v1-v2);
}

//! randomise a vector within the given range
template < typename T >
const Vector2<T> Random(Vector2<T> &v, const T &low, const T &high)
{
	for (size_t i = 0; i < 2; ++i)
		v[i] = Random(low, high);

	return v;
}

//! midpoint of two vectors
template < typename T, typename U >
const Vector2<T> Midpoint(const Vector2<T> &v1, const Vector2<U> &v2)
{
	Vector2<T> ret;
	for (size_t i = 0; i < 2; ++i)
		ret[i] = (v1[i] + v2[i]) / T(2);

	return ret;
}

//! component-wise absolute value
template < typename T >
const Vector2<T> Abs(const Vector2<T> &v)
{
	Vector2<T> ret;
	for (size_t i = 0; i < 2; ++i)
		ret[i] = Abs(v[i]);

	return ret;
}

//! component-wise minimum vector
template < typename T, typename U >
const Vector2<T> Min(const Vector2<T> &v1, const Vector2<U> &v2)
{
	Vector2<T> ret;
	for (size_t i = 0; i < 2; ++i)
		ret[i] = Min(v1[i], v2[i]);

	return ret;
}

//! component-wise maximum vector
template < typename T, typename U >
const Vector2<T> Max(const Vector2<T> &v1, const Vector2<U> &v2)
{
	Vector2<T> ret;
	for (size_t i = 0; i < 2; ++i)
		ret[i] = Max(v1[i], v2[i]);

	return ret;
}

//! linear interpolation
template < typename T, typename U >
const Vector2<T> Lerp(const Vector2<T> &v1, const Vector2<U> &v2, const T &t)
{
	return v1 + (v2 - v1) * t;
}

//! dot product
template < typename T, typename U >
const T Dot(const Vector2<T> &v1, const Vector2<U> &v2)
{
	T ret = 0;
	for (size_t i = 0; i < 2; ++i)
		ret += v1[i] * v2[i];

	return ret;
}

//! 2D cross product
template < typename T, typename U >
inline const T Cross(const Vector2<T> &v1, const Vector2<U> &v2)
{
	return v1[0] * v2[1] - v1[1] * v2[0];
}

//! scalar triple product / mixed product
template < typename T, typename U, typename V >
const T ScaTrip(const Vector2<T> &v1, const Vector2<U> &v2, const Vector2<V> &v3)
{
	return Dot(v1,Cross(v2,v3));
}

//! vector triple product
template < typename T, typename U, typename V >
const Vector2<T> VecTrip(const Vector2<T> &v1, const Vector2<U> &v2, const Vector2<V> &v3)
{
	// lagrange's formula [a x (b x c) = b(a . c) - c(a . b)]
	return v2 * Dot(v1,v3) - v3 * Dot(v1,v2);
}

//! angle between vectors in radians
template < typename T >
const T Angle(const Vector2<T> &v1, const Vector2<T> &v2)
{
	return acos(Dot(v1,v2));
}

//! project a vector onto another
template < typename T, typename U >
const Vector2<T> Project(const Vector2<T> &v1, const Vector2<U> &v2)
{
	assert( Dot(v2,v2) != T(0) );
	return (v2 * Dot(v1,v2)) / Dot(v2,v2);
}

//! reflect a vector about its normal
template < typename T, typename U >
const Vector2<T> Reflect(const Vector2<T> &v, const Vector2<U> &n)
{
	return v - (T(2) * Dot(v,n) * n);
}

//! unitised vector copy
template < typename T >
const Vector2<T> Normalised(Vector2<T> v)
{
	return v.Normalise();
}

//! test for null vector
template < typename T >
const bool IsZero(const Vector2<T> &v)
{
	for (size_t i = 0; i < 2; ++i)
		if ( !IsZero( v[i], Abs(v[i]) ) )
			return false;

	return true;
}

//! test for unit vector
template < typename T >
const bool IsUnit(const Vector2<T> &v)
{
	return IsEqual(T(1), Mag(v), Mag(v));
}

//! test for parallel vectors
template < typename T, typename U >
const bool IsParallel(const Vector2<T> &v1, const Vector2<U> &v2)
{
	return IsZero( Cross(v1,v2) );
}

//! test for perpendicular vectors
template < typename T, typename U >
const bool IsPerp(const Vector2<T> &v1, const Vector2<U> &v2)
{
	return IsZero( Dot(v1,v2), Abs( Dot(v1,v2) ) );
}

template < typename T >
float AspectRatio(const Vector2<T> &v1)
{
	return ( (float)v1.x() / (float)v1.y() );
}


#endif // VECTOR_INL_



