#ifndef __PLANE_H__
#define __PLANE_H__

#include "Vector.h"
#include "Const.h"

namespace Engine
{

	template < typename T >
	class Plane 
	{
	public:
		enum IntersectSide
		{
			ISS_FRONT = 0,
			ISS_BACK,     
			ISS_PLANAR
		};

	public:
		Vector3<T> m_normal;	// plane normal vector
		T	       m_disOrigin;	// distance to origin

		// Uninitalised
		Plane();

		// Copy Ctor
		template < typename U >	Plane(const Plane<U>& plane);

		// Assignment
		template < typename U > const Plane& operator = (const Plane<U>& plane);

		Plane(const T& x, const T& y, const T& z, const T& distance);
		Plane(const Vector3<T>& point, const Vector3<T>& normal);
		Plane(const Vector3<T>& point1, const Vector3<T>& point2, const Vector3<T>& point3);

		const T& Distance(const Vector3<T>& point) const;

		// Normalise the plane
		void PlaneNormalise();	

		// Set functions
		void Set(const T& x, const T& y, const T& z, const T& distance);
		void Set(const Vector3<T>& normal, const T& distance);

		// Point on a plane tests
		void FromPointNormal(const Vector3<T> &normal, const Vector3<T> &point);
		void FromPoints(const Vector3<T> &point1, const Vector3<T> &point2, const Vector3<T> &point3);
	};



#include "Plane.inl"

}


#endif