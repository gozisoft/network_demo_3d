#ifndef __RAYCAST_INL__
#define __RAYCAST_INL__

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif

template < typename T >
Raycast<T>::Raycast()
{

}

template < typename T >
Raycast<T>::Raycast(const Vector3<T>& origin, const Vector3<T> &direction) :
m_origin(origin),
m_direction(direction) 
{

}

// Does not check for a normalised direction!
template < typename T >
void Raycast<T>::set(const Vector3<T>& origin, const Vector3<T>& direction)
{
	m_origin = origin;
	m_direction = direction;
}


// Triagle intersect of infinite length
// returns the exact distance from the origin to the point of
// intersection.
template < typename T >
bool Raycast<T>::intersects(const Vector3<T> &vc0, const Vector3<T> &vc1, const Vector3<T> &vc2, bool cull, T &disRayOrigin) const
{
	Vector3f<T> edge1 = vc1 - vc0;
	Vector3f<T> edge2 = vc2 - vc0;

	Vector3f<T> pvec = CrossProduct(m_direction, edge2);

	// if close to 0 ray is parallel
	T det = edge1 * pvec;

	if (cull && det < Const<T>::EPSILON)
		return false;
	else if (det < Const<T>::EPSILON && det > -Const<T>::EPSILON)
		return false;

	// distance to plane. < 0 means ray behind the plane
	Vector3f<T> tvec = m_origin - vc0;
	T u = tvec * pvec;

	if (u < T(0) || u > det)
		return false;

	Vector3f<T> qvec = CrossProduct(tvec, edge1);
	T v = m_direction * qvec;

	if ( (v < T(0)) || ((u + v) > det) )
		return false;

	if (disRayOrigin)
	{
		disRayOrigin = edge2 * qvec;
		T InvDet = T(1) / det;
		disRayOrigin *= InvDet;
	}

	return true;
}

// test for intersection with triangle at certain length (line segment),
// same as above but test distance to intersection vs segment length.
template < typename T >
bool Raycast<T>::intersects(const Vector3<T>& vc0, const Vector3<T>& vc1, const Vector3<T>& vc2, bool cull, T raylength, T &disRayOrigin) const
{
	Vector3f<T> edge1 = vc1 - vc0;
	Vector3f<T> edge2 = vc2 - vc0;

	Vector3f<T> pvec = CrossProduct(m_direction, edge2);

	// if close to 0 ray is parallel
	T det = edge1 * pvec;

	if (cull && det < Const<T>::EPSILON)
		return false;
	else if (det < Const<T>::EPSILON && det > -Const<T>::EPSILON)
		return false;

	// distance to plane. < 0 means ray behind the plane
	Vector3f<T> tvec = m_origin - vc0;
	T u = Dot(tvec, pvec);

	if (u < T(0) || u > det)
		return false;

	Vector3f<T> qvec = CrossProduct(tvec, edge1);
	T v = Dot(m_direction, qvec);

	if ( (v < T(0)) || ((u + v) > det) )
		return false;

	if (disRayOrigin) 
	{
		disRayOrigin = Dot(edge2, qvec);
		T InvDet = T(1) / det;
		disRayOrigin *= fInvDet;
		// collision but not on segment?
		if (disRayOrigin > raylength)
			return false; 
	}
	else 
	{
		// collision but not on segment?
		T f = (edge2 * qvec) * (T(1) / det); // edge2 * qvec may need changed to dot(edge2, qvec)
		if (f > raylength)
			return false;
	}

	return true;
} // Intersects(Tri at length)


template < typename T >
bool Raycast<T>::intersects(const Plane<T> &plane, bool cull, T &disRayOrigin, Vector3<T> &vcHit) const
{
	T d = plane.m_normal * m_direction;

	// ray parallel to the plane
	if (Abs(d) < Const<T>::EPSILON)
		return false;

	// plane normal point away from ray direction
	// => intersection with back face if any
	if (cull && d > T(0))
		return false;

	T e = -( (plane.m_normal * m_origin) + plane.m_disOrigin );

	T f = e / d;

	// intersection before ray origin
	if (f < T(0))
		return false;

	if (vcHit)
	{
		vcHit = m_origin + (m_direction * f);
	}

	if (disRayOrigin)
		disRayOrigin = f;

	return true;
}

template < typename T >
bool Raycast<T>::intersects(const Plane<T> &plane, bool cull, T raylength, T &disRayOrigin, Vector3<T> &vcHit) const
{
	T d = plane.m_normal * m_direction;

	// ray parallel to the plane
	if (Abs(d) < Const<T>::EPSILON)
		return false;

	// plane normal point away from ray direction
	// => intersection with back face if any
	if (cull && d > T(0))
		return false;

	T e = -( (plane.m_normal * m_origin) + plane.m_disOrigin );

	T f = e / d;

   // intersection behind ray origin or beyond valid range
   if ( (f < T(0)) || (f > raylength) )
      return false;

	if (vcHit)
	{
		vcHit = m_origin + (m_direction * f);
	}

	if (disRayOrigin)
		disRayOrigin = f;

	return true;
}

// Intersects(Aabb)
// test for intersection with aabb, original code by Andrew Woo, 
// from "Geometric Tools...", Morgan Kaufmann Publ., 2002
template < typename T >
bool Raycast<T>::intersects(const Aabbox<T> &aabb, T &disRayOrigin) const
{
	//T near = std::numeric_limits<T>::min();
	//T far  = std::numeric_limits<T>::max();

	T near = -999999.9f;
	T far = 999999.9f;

	for (UInt i = 0; i < 3; ++i)
	{
		// first pair of planes
		if (Abs(m_direction[i]) < Const<T>::EPSILON) 
		{
			if ( (m_origin[i] < aabb.m_min[i]) || (m_origin[i] > aabb.m_max[i]) )
				return false;
		}

		T t0 = (aabb.m_min[i] - m_origin[i]) / m_direction[i];
		T t1 = (aabb.m_max[i] - m_origin[i]) / m_direction[i];

		if (t0 > t1)
		{ 
			T tmp = t0;
			t0 = t1;
			t1 = tmp;
		}

		if (t0 > near)
			near = t0;

		if (t1 < far) 
			far = t1;

		if (near > far) 
			return false;

		if (far < 0)
			return false;
	}

	if (near > 0)
		disRayOrigin = near;
	else 
		disRayOrigin = far;

	return true;
} // Intersects(Aabb)




// Intersects(Aabb) at length
// test for intersection with aabb, original code by Andrew Woo, 
// from "Geometric Tools...", Morgan Kaufmann Publ., 2002
template < typename T >
bool Raycast<T>::intersects(const Aabbox<T> &aabb, T raylength, T &disRayOrigin) const
{
	T near = std::numeric_limits<T>::min();
	T far  = std::numeric_limits<T>::max();

	for (UInt i = 0; i < 3; ++i)
	{
		// first pair of planes
		if (Abs(m_direction[i]) < Const<T>::EPSILON)
		{
			if ( (m_origin[i] < aabb.m_min[i]) || (m_origin[i] > aabb.m_max[i]) )
				return false;
		}

		T t0 = (aabb.m_min[i] - m_origin[i]) / m_direction[i];
		T t1 = (aabb.m_max[i] - m_origin[i]) / m_direction[i];

		if (t0 > t1)
		{ 
			T tmp = t0;
			t0 = t1;
			t1 = tmp;
		}

		if (t0 > near)
			near = t0;

		if (t1 < far) 
			far = t1;

		if (near > far) 
			return false;

		if (far < 0)
			return false;
	}

	T final = T(0);

	if (near > 0) 
		final = near;
	else 
		final = far;

	if (final > raylength)
		return false;

	if (disRayOrigin)
		disRayOrigin = final;

	return true;
}





#endif