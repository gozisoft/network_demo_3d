#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "MathsFwd.h"
#include "TableArray.h"
#include "Vector.h"
#include <limits>

#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif


namespace Engine
{

// The matrix is stored in row-major order.
// based on a left handed coordinate system.
template < typename T >
class Matrix4 : public TableArray<4, 4, T>
{
public:
	typedef TableArray<4, 4, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

public:
	// constructors
	Matrix4 ();

	Matrix4 (const_reference _m00, const_reference _m01, const_reference _m02, const_reference _m03,
			 const_reference _m10, const_reference _m11, const_reference _m12, const_reference _m13,
			 const_reference _m20, const_reference _m21, const_reference _m22, const_reference _m23,
			 const_reference _m30, const_reference _m31, const_reference _m32, const_reference _m33);

	// create a rotation matrix
	Matrix4 (const Vector3<type>& axis, type angle);

	// copy constructor
	template < typename U >	Matrix4 (const Matrix4<U> & mat);

	// assignment :: set equal
	template < typename U > Matrix4 & operator = (const Matrix4<U> & mat);

	// assignment :: matrix
	template < typename U > Matrix4 & operator *= (const Matrix4<U> & mat);
	template < typename U > Matrix4 & operator += (const Matrix4<U> & mat);
	template < typename U > Matrix4 & operator -= (const Matrix4<U> & mat);
	
	// assignment :: scalar
	Matrix4 & operator *= (const_reference val);
	Matrix4 & operator /= (const_reference val);

	// arithmatic :: vector
	// Vector4<T> operator * (const Vector4<T> & vec) const;  // M * v

	// functions
	void MakeZero();
	void MakeIdentity();

	// make into a rotation matrix (around axis by a certain degrees) :: axis angle rotation
	const Matrix4& SetRotationAxis(const Vector3<T>& axis, const_reference radians);

	// rotate matrix around by a deg in each axis
	// set function
	const Matrix4& SetRotateX(const_reference radians);
	const Matrix4& SetRotateY(const_reference radians);
	const Matrix4& SetRotateZ(const_reference radians);

	const Matrix4& SetRotationRads(const Vector3<T>& rotation);
	const Matrix4& SetRotationDegrees(const Vector3<T>& rotation);
	const Matrix4& SetInverseRotationRads(const Vector3<T>& rotation);
	const Matrix4& SetInverseRotationDegrees(const Vector3<T>& rotation);

	const Matrix4& SetTranslation(const Vector3<T>& translation); 
	const Matrix4& SetInverseTranslation(const Vector3<T>& translation);
	const Matrix4& SetScale(const Vector3<T>& scale);

	// get functions
	Vector3<T> GetRotation() const;
	Vector3<T> GetTranslation() const;

	// Returns the absolute valus of the scales of the matrix
	Vector3<T> GetScale() const;

	// Retrieves a row in Vector3 form: x,y,z
	Vector3<T> GetRow(size_t row) const;
	Vector3<T> GetCol(size_t col) const;

	void TransformVectNormal(Vector3<T>& vec) const;
	void TransformVectNormal(Vector3<T>& out, const Vector3<T>& in) const;
	void TransformVectCoord(Vector3<T>& vec) const;
	void TransformVectCoord(Vector3<T>& out, const Vector3<T>& in) const;

	// other
	Matrix4 Inverse() const;

	// static members
	static const Matrix4 ZERO;
	static const Matrix4 IDENTITY;

protected:
	// used for making rotation matrix
	Matrix4 (const Vector3<type>& xaxis, const Vector3<type>& yaxis, const Vector3<type>& zaxis);

};


#include "Matrix4.inl"


}


#endif // MATRIX_H_










/*!
A class for m by n row-major matrices. As template paramaters are known
at compile-time, for loops should be unrolled.
*/


/*
template < size_t R = 4, size_t C = 4, typename T = float >
class Matrix : public Vector < R, Vector<C,T> >
{
typedef T Degree, Radian;

public:

//! default ctor
Matrix(){}

//! overloaded ctors
Matrix(const Vector<2,T> &v1, const Vector<2,T> &v2): Vector(v1,v2){}
Matrix(const Vector<3,T> &v1, const Vector<3,T> &v2, const Vector<3,T> &v3): Vector(v1,v2,v3){}
Matrix(const Vector<4,T> &v1, const Vector<4,T> &v2, const Vector<4,T> &v3, const Vector<4,T> &v4): Vector(v1,v2,v3,v4){}

//! 2x2 overloaded ctor
Matrix(const T &_00, const T &_01,
const T &_10, const T &_11);

//! 3x3 overloaded ctor
Matrix(const T &_00, const T &_01, const T &_02,
const T &_10, const T &_11, const T &_12,
const T &_20, const T &_21, const T &_22);

//! 4x4 overloaded ctor
Matrix(const T &_00, const T &_01, const T &_02, const T &_03,
const T &_10, const T &_11, const T &_12, const T &_13,
const T &_20, const T &_21, const T &_22, const T &_23,
const T &_30, const T &_31, const T &_32, const T &_33);

// canonical rotations (note: use with identity matrix)
const Matrix & RotateX(Degree angle);
const Matrix & RotateY(Degree angle);
const Matrix & RotateZ(Degree angle);

// scaling (note: use with identity matrix)
const Matrix & Scale(const T &sx, const T &sy, const T &sz);

// translation (note: use with identity matrix)
const Matrix & Translate(const T &tx, const T &ty, const T &tz);

//! make this matrix an identity matrix
void Identity();

//! pointer to first element
T* GetPointer() { return &(*this)[0][0]; }

public:

//! identity matrix
static const Matrix<4,4,T> IDENTITY;

};

*/