#ifndef __AABBOX_H__
#define __AABBOX_H__

#include "Vector.h"
#include "Plane.h"

namespace Engine
{


template < typename T >
class Aabbox
{
public:
	Vector3<T> m_min;
	Vector3<T> m_max;		 // box extreme points

	template < typename U >
	Aabbox(const Aabbox<U>& aabb);

	template < typename U >
	Aabbox(const Vector3<U>& min, const Vector3<U>& max);

	// Function to retrive all the sides of the aabb
	template < typename U >
	void GetPlanes(Plane<U>* pPlanes);

	// Get the center point of the box
	Vector3<T> GetCenter() const;

	// Get its size
	const T& GetSize() const;

	// Get the area of the box
	const T& GetRadius() const;
}; 


#include "AABBox.inl"


}

#endif