#ifndef __CONST_H__
#define __CONST_H__

#include <limits>

namespace Engine
{
/*!
A struct of common mathematical constants. Template specialisation is 
employed to select between float or double precision.
*/
template < typename T >
struct Const
{
	static T PI() { return T(0); }
	static T TWO_PI() { return T(0); }
	static T HALF_PI() { return T(0); }
	static T INV_PI() { return T(0); }

	static T E() { return T(0); }
	static T LN2() { return T(0); }
	static T LN10() { return T(0); }
	static T EPSILON() { return T(0); }
	static T ROUNDING_ERROR() { return T(0); }

	static T TO_DEG() { return T(0); }
	static T TO_HALF_DEG() { return T(0); }
	static T TO_RAD() { return T(0); }
	static T TO_HALF_RAD() { return T(0); }
};

const float PI32 = 3.14159265359f;
const double PI64 = 3.1415926535897932384626433832795028841971693993751;

template <>
struct Const <float>
{
	typedef float T;

	static T PI ()
	{ 
		return PI32;
	}

	static T TWO_PI ()
	{ 
		return PI32 * 2.0f;
	}

	static T HALF_PI ()
	{ 
		return PI32 * 0.5f;
	}

	static T INV_PI ()
	{ 
		return 1.0f / PI32;
	}

	static T E ()
	{
		return 2.71828f; 
	}

	static T LN2 ()
	{ 
		return 0.693147f;
	}

	static T LN10 ()
	{ 
		return 2.30259f; 
	}

	static T EPSILON ()
	{ 
		return FLT_EPSILON; 
	}

	static T ROUNDING_ERROR ()
	{ 
		return 0.000001f; 
	}

	static T TO_DEG ()
	{
		return 180.0f / PI32; 
	}

	static T TO_HALF_DEG ()
	{ 
		return (180.0f / PI32) / 2.0f;
	}

	static T TO_RAD ()
	{ 
		return PI32 / 180.0f; 
	}

	static T TO_HALF_RAD ()
	{ 
		return (PI32 / 180.0f) / 2.0f; 
	}
};



template <>
struct Const <double>
{
	typedef double T;

	static T PI ()
	{ 
		return PI64;
	}

	static T TWO_PI ()
	{ 
		return PI64 * 2.0;
	}

	static T HALF_PI ()
	{ 
		return PI64 * 0.5;
	}

	static T INV_PI ()
	{ 
		return 1.0 / PI64;
	}

	static T E ()
	{
		return 2.71828182846;
	}

	static T LN2 ()
	{ 
		return 0.693147180560; 
	}

	static T LN10 ()
	{ 
		return 2.30258509299; 
	}

	static T EPSILON ()
	{ 
		return DBL_EPSILON; 
	}

	static T ROUNDING_ERROR ()
	{ 
		return 0.00000001; 
	}

	static T TO_DEG ()
	{
		return 180.0 / PI64; 
	}

	static T TO_HALF_DEG ()
	{ 
		return (180.0 / PI64) / 2.0;
	}

	static T TO_RAD ()
	{ 
		return PI64 / 180.0; 
	}

	static T TO_HALF_RAD ()
	{ 
		return (PI64 / 180.0) / 2.0; 
	}
};

}



#endif // CONST_H_


/*
template < typename T >
struct Const
{
	static const T PI;
	static const T TWO_PI;
	static const T HALF_PI;
	static const T INV_PI;

	static const T E;
	static const T LN2;
	static const T LN10;
	static const T EPSILON;
	static const T ROUNDING_ERROR;

	static const T TO_DEG;
	static const T TO_HALF_DEG;
	static const T TO_RAD;
	static const T TO_HALF_RAD;
};
*/