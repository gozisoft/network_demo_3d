#ifndef __INTERSECTIONS_H__
#define __INTERSECTIONS_H__

#include "Vector.h"
#include "AABBox.h"
#include "Sphere.h"
#include "Raycast.h"


namespace Engine
{
	//----------------------------------------------------------------------------------------
	// Plane
	//----------------------------------------------------------------------------------------

	// Check intersection side of plane Returns:
	//  > 0 if the point 'pt' lies in front of the plane 'p'
	//  < 0 if the point 'pt' lies behind the plane 'p'
	//    0 if the point 'pt' lies on the plane 'p'
	// The signed distance from the point 'pt' to the plane 'p' is returned.
	template < typename T >
	inline const T PlaneDotCoord(const Plane<T>& plane, const Vector3<T>& point)
	{
		return Dot(plane.m_normal, point) + plane.m_disOrigin;
	}


	//----------------------------------------------------------------------------------------
	// AABB Collisions
	//----------------------------------------------------------------------------------------

	// Test if AABB a intersects AABB b
	template < typename T, typename U >
	inline bool TestAABBAABB(const Aabbox<T>& a, const Aabbox<U>& b)
	{
		// Exit with no intersection if separated along an axis
		if (a.m_max[0] < b.m_min[0] || a.m_min[0] > b.m_max[0]) return false;
		if (a.m_max[1] < b.m_min[1] || a.m_min[1] > b.m_max[1]) return false;
		if (a.m_max[2] < b.m_min[2] || a.m_min[2] > b.m_max[2]) return false;

		// Overlapping on all axes means AABBs are intersecting
		return true;
	}

	// Test if AABB b intersects plane p
	template < typename T, typename U >
	inline bool TestAABBPlane(const Aabbox<T>& aabb, const Plane<U>& plane)
	{
		// These two lines not necessary with a (center, extents) AABB representation
		Vecto3<T> c = aabb.GetCenter();		// Compute AABB center
		Vecto3<T> e = aabb.m_max - c;		// Compute positive extents

		// Compute the projection interval radius of b onto L(t) = b.c + t * p.n
		T r = Dot( e, Abs(plane.m_normal) );
		// Compute distance of box center from plane
		T s = Dot( plane.m_normal, c) - p.d;

		// Intersection occurs when distance s falls within [-r,+r] interval
		return Abs(s) <= r;
	}

	//----------------------------------------------------------------------------------------
	// Sphere Collisions
	//----------------------------------------------------------------------------------------

	// Test if Sphere a intersects Sphere b.
	template < typename T, typename U >
	inline bool TestSphereSphere(const Sphere<T>& a, const Sphere<U>& b)
	{
		// Calculate squared distance between centers
		Vector3<T> d = a.m_center - b.m_center;
		T distance = Dot(d, d);

		// Spheres intersect if squared distance is less than squared sum of radii
		T radiusSum = a.m_radius + sphere.m_radius;

		return ( distance <= Sqr(radiusSum) );
	}

	template < typename T, typename U >
	inline void TransformSphere(Sphere<T>& out, const Sphere<U>& in, const Matrix4<U>& transform)
	{
		out.m_center = transform * in.m_center;

		Vector3<T> scale = transform.GetScale();
		T maxScale = T(0);
		for (UInt i = 0; i < scale.GetSize(); ++i)
		{
			if ( Abs(scale[i]) > maxScale )
				maxScale = Abs(scale[i]);
		}

		out.m_radius = maxScale;
	}

	// Computes the bounding sphere out of spheres a and b.
	template < typename T, typename U >
	inline void MergeWithSphere(Sphere<T>& out, const Sphere<U>& a, const Sphere<U>& b)
	{
		// Compute the squared distance between the sphere centers
		Vector3<T> d = b.m_center - a.m_center;
		T disSqr = Dot(d, d);
		T radiusDiff = b.m_radius - a.m_radius;

		// By squaring the difference of the sphere radii, you remove the
		// negative if the shperes overlap. This results in a value greator
		// than the distance between the two spheres
		if ( Sqr(radiusDiff) >= disSqr )
		{
			// The sphere with the larger radius encloses the other;
			// just set out to be the larger of the two spheres
			if(radiusDiff >= 0.0f)
			{
				out.m_center = b.m_center;
				out.m_radius = b.m_radius;
			}

			return;
		}
		else
		{
			// Spheres partially overlapping or disjoint
			T dist = std::sqrt(disSqr);
			
			if ( dist > Const<T>::EPSILON() )
			{
				T coeff = (dist + radiusDiff) / ( T(2.0) * dist );
				out.m_center += coeff * d;

			}

			out.m_radius = (dist + a.m_radius + b.m_radius) * T(0.5);
		}
	}

	template < typename T >
	inline void ComputeBoundingSphere(Sphere<T>& out, const T* pPosData, size_t numVertices)
	{
		// Caclulate center of the sphere - average value of all the vertex positions.
		out.m_center = Vector3f::ZERO;
		for (UInt i = 0; i < numVertices; ++i)
		{
			out.m_center[0] += pPosData[0];
			out.m_center[1] += pPosData[1];
			out.m_center[2] += pPosData[2];
		}
		// Same as [SumOfAllVerts / numVerts]
		T invNumVerts = 1.0f / static_cast<T>(numVertices);
		out.m_center *= invNumVerts;

		// The radius is the largest distance from the center to the positions.
		// Eqtn of a circle (x - ca)^2 + (y - cb)^2 + (z - cc)^2 = r^2
		// where [ca,cb,cc] are the xyz coords of the center.
		out.m_radius = 0.0f;
		for (UInt i = 0; i < numVertices; ++i)
		{
			float difference[3] = 
			{
				pPosData[0] - out.m_center[0],
				pPosData[1] - out.m_center[1],
				pPosData[2] - out.m_center[2]
			};
			float radiusSqrd = Sqr(difference[0]) + Sqr(difference[1]) + Sqr(difference[2]);

			// now check if this radius is greator than the last radius
			if (radiusSqrd > out.m_radius)
			{
				out.m_radius = radiusSqrd;
			}
		}

		out.m_radius = sqrt(out.m_radius);
	}

	//----------------------------------------------------------------------------------------
	// Ray Collisions
	//----------------------------------------------------------------------------------------
	template < typename T >
	inline bool TestRaySphere(const Raycast<T>& ray, const Sphere<T>& sphere)
	{
		Vector3<T> w(sphere.m_center - ray.m_origin);
		T wsq = Dot(w, w);
		T proj = Dot(w, direction);
		T rsq = sphere.m_radius * sphere.m_radius;

		// Early out: if sphere is behind the ray then there's no intersection.
		if (proj < T(0) && wsq > rsq)
			return false;

		T vsq = Dot(direction, direction);

		// Test length of difference vs. radius.
		return ( Sqr(vsq) - Sqr(proj) <= Sqr(vsq) );
	}


} // namespace Engine



#endif