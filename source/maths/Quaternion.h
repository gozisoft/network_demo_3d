#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#include "MathsFwd.h"
#include "FixedArray.h"

namespace Engine
{

template < typename T >
class Quaternion : public FixedArray<4, T>
{
public:
	typedef FixedArray<4, T> array_type;
	typedef typename array_type::type			 type;
	typedef typename array_type::pointer		 pointer;
	typedef typename array_type::reference		 reference;
	typedef typename array_type::const_pointer   const_pointer;
	typedef typename array_type::const_reference const_reference;

	typedef type Degree, Radian;

public:
	Quaternion();
	Quaternion(const_reference w, const_reference x, const_reference y, const_reference z);
	Quaternion(const Quaternion& quat);
	// build a quaternion from a matrix
	Quaternion(const Matrix4<T> mat);
	// build a quaternion from axis angle
	Quaternion(const Vector3<T> Axis, const_reference angle); 

	// const accessor methods
	const_reference x() const { return (*this)[0]; }
	const_reference y() const { return (*this)[1]; }
	const_reference z() const { return (*this)[2]; }
	const_reference w() const { return (*this)[3]; }

	// accessor methods
	reference x() { return (*this)[0]; }
	reference y() { return (*this)[1]; }
	reference z() { return (*this)[2]; }
	reference w() { return (*this)[3]; }

    // Arithmetic updates.
    Quaternion& operator+= (const Quaternion& quat);
    Quaternion& operator-= (const Quaternion& quat);
    Quaternion& operator*= (const_reference scalar);
    Quaternion& operator/= (const_reference scalar);

	// Conversion between quaternions, matrices, and axis-angle.
	void FromAxisAngle(const Vector3<T>& Axis, const_reference angle);
	void ToAxisAngle(Vector3<T>& Axis, reference angle) const;
	void FromRotationMatrix(const Matrix4<T>& rotation);
	void ToRotationMatrix(Matrix4<T>& rotation) const;
	



};

#include "Quaternion.inl"

}

#endif