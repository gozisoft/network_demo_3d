#ifndef __BIT_HACKS_H__
#define __BIT_HACKS_H__

#include "Core.h"

namespace Engine
{

	bool IsPowerOf2(UInt val);
	bool IsPowerOf2(Int val);

	UInt Log2OfPowerOfTwo(UInt powerOfTwo);
	Int Log2OfPowerOfTwo(Int powerOfTwo);



}




#endif