#include "clientafx.h"
#include "ClientListener.h"



using namespace Engine;


CClientListener::CClientListener(CClientApp* pClientApp) : 
m_pClientApp(pClientApp)
{

}

CClientListener::~CClientListener()
{

}

bool CClientListener::HandleEvent(const IEventData& event)
{	
	IEventManagerPtr pEventMgr = m_pClientApp->m_pEventMgr;
	CClientNetworkPtr pClientNet = m_pClientApp->m_pNetwork;

	if (event.GetEventType() == SEvtData_Remote_Connect::sk_EventType)
	{
		const SEvtData_Remote_Connect& ed = 
			static_cast<const SEvtData_Remote_Connect&>(event);

		SocketEvent messageType = NetMsg_PlayerLoginRequest;

		// serialise data
		using std::stringstream;
		stringstream ss( stringstream::in | stringstream::out | stringstream::binary );

		// 1) serilise the socketevent type (NetMsg_PlayerLoginRequest)
		ss.write( (char*)&messageType, sizeof(SocketEvent) );

		// get length of file:
		ss.seekg (0, stringstream::end);
		size_t length = static_cast<size_t>( ss.tellg() );
		ss.seekg (0, stringstream::beg);

		// Read it into a vector
		using std::vector;
		vector<char> buffer(length);
		ss.read( &buffer[0], length );	

		// Create a packet from the data and send
		IPacketPtr pPacket = CBinaryPacketPtr( new CBinaryPacket( &buffer[0], buffer.size() ) );
		m_pClientApp->m_pNetwork->Send(0, pPacket);

		return true;
	}
	else if (event.GetEventType() == SEvtData_Network_Player_Actor_Assignment::sk_EventType)
	{
		const SEvtData_Network_Player_Actor_Assignment& ed = 
			static_cast<const SEvtData_Network_Player_Actor_Assignment&>(event);
	
		// Remote client getting an actor assignment.
        // the server assigned us a playerId when we first attached (the server's socketId, actually)
		CClientApp::GameViewArray &viewArray = m_pClientApp->m_gameViews;
		IGameView::GameViewID viewID = static_cast<IGameView::GameViewID>( viewArray.size() );
		CClientViewPtr pClientView = m_pClientApp->m_pClientView;

		// push back the view and run the start functions
		viewArray.push_back(m_pClientApp->m_pClientView);
		pClientView->OnCreate(viewID, ed.m_actorId);
		pClientView->OnRestore();

		// Add events that effect the network view	
		CNetworkListenerPtr pNetListener = CNetworkListenerPtr( new CNetworkListener(pClientNet, viewID) );	
		pEventMgr->AddListener(pNetListener, SEvtData_Spawn_Request::sk_EventType);
		pEventMgr->AddListener(pNetListener, SEvtData_New_Actor::sk_EventType);
		pEventMgr->AddListener(pNetListener, SEvtData_Request_Move_Actor::sk_EventType);
		pEventMgr->AddListener(pNetListener, SEvtData_Destroy_Actor::sk_EventType);

		// queue a spawn request event
		shared_ptr<SEvtData_Spawn_Request> spawnRequest( new SEvtData_Spawn_Request(ed.m_actorId) );
		bool success = pEventMgr->QueueEvent(spawnRequest);
		if (!success)
		{
			assert( false && ieS("Unable to trigger spawnEvent for actor!") );
			return false;
		}

		return true;
	}
	else if (event.GetEventType() == SEvtData_World_Update::sk_EventType)
	{
		// This is not spawning this player, but rather its spawning any currently active players
		// within the server world.
		const SEvtData_World_Update& ed = static_cast<const SEvtData_World_Update&>(event);

		// Actor params
		const SActorParams* pParams = ed.m_pActorParams;

		// queue a spawn event
		shared_ptr<SEvtData_Spawn_Actor> spawnActor( new SEvtData_Spawn_Actor(*pParams) );
		bool success = pEventMgr->Trigger(*spawnActor);
		if (!success)
		{
			assert( false && ieS("Unable to trigger spawnEvent for actor!") );
			return false;
		}

		return true;
	}

	return false;
}