#include "clientafx.h"
#include "ClientView.h"
#include "ClientViewListener.h"

// Maths
#include "Viewport.h"


using namespace Engine;


DEFINE_HIERARCHICALHEAP(CClientView, "CClientView", "IGameView");



CClientView::CClientView(IRendererPtr pRenderer, IMouseControllerPtr pMouse, IEventManagerPtr pEventMgr) :
CHumanView(pRenderer, pMouse, pEventMgr)
{
	// Create the game views listener
	m_pClientViewListener = CClientViewListenerPtr( new CClientViewListener(this) );

	// Events that effect the view of the game (on screen)
	// Add the camcontroller listner and align it with keyevent, mouseevent, winSizeEvent.
	m_pEventMgr->AddListener(m_pClientViewListener, SEvtData_Request_New_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pClientViewListener, SEvtData_Spawn_Actor::sk_EventType);
	m_pEventMgr->AddListener(m_pClientViewListener, SEvtData_Move_Actor::sk_EventType);
}

CClientView::~CClientView()
{

}

void CClientView::OnCreate(UInt32 gameViewID, ActorID actorID)
{
	CHumanView::OnCreate(gameViewID, actorID);
	CreateScene();
}

void CClientView::OnRestore()
{
	CHumanView::OnRestore();
}

void CClientView::OnLostDevice()
{

}

void CClientView::OnUpdate(double time, double deltaTime)
{
	CHumanView::OnUpdate(time, deltaTime);

	if (m_pScene)
		m_pScene->Update(deltaTime);
}

void CClientView::OnRender(double time, double deltaTime)
{
	CHumanView::OnRender(time, deltaTime);

	if ( m_pRenderer->PreDraw() )
	{
		m_pCuller->ComputeVisibleSet(m_pScene);
		{
			m_pRenderer->ClearBuffers();
			m_pRenderer->Draw( m_pCuller->GetVisibleArray() );
		}

		m_pRenderer->PostDraw();
		m_pRenderer->DisplayColorBuffer();
	}
}

void CClientView::OnWindowEvent(const CWinEvent& event)
{
	bool absorebed = false;
	IEventDataPtr pEvent = IEventDataPtr();

	switch (event.EventType)
	{
	case CWinEvent::W_KEYBOARD_EVENT:
		pEvent = shared_ptr<SKeyEvent>( new SKeyEvent(event.KeyInput) );
		break;
	case CWinEvent::W_MOUSE_EVENT:
		pEvent = shared_ptr<SMouseEvent>( new SMouseEvent(event.MouseInput) );
		break;
	}

	if (m_pCameraNode)
		m_pCameraNode->OnEvent(*pEvent);
}

inline CClientView::GameViewType CClientView::GetType() const
{
	return GameViewType("CClientView");
}

void CClientView::CreateScene()
{
	m_pScene = CRootNodePtr( new CRootNode() );

	// Create the objects in the scene (triangle format)
	CVertexFormatPtr pVFormat = CVertexFormatPtr( new CVertexFormat() );
	pVFormat->AddElement(0, VT_FLOAT3, VS_POSITION);
	pVFormat->AddElement(0, VT_FLOAT4, VS_COLOUR);

	// Create the ground plane
	CTriMeshPtr pGround = CGeometry(pVFormat).Plane(1, 100, 100);

	// Create the triangle
	CTriMeshPtr pTriangle = CGeometry(pVFormat).Triangle(2.0, 2.0, 1.0f);
	pTriangle->SetPosition( Vector3f(0, 1, 5) );

	// Add the ground
	m_pScene->AddChild(pGround);

	// Add the triangle as a child to the scene
	m_pScene->AddChild(pTriangle);

	// get absolute path
	String pathName = CFileUtility::GetMediaFile( ieS("media\\ControlPoints.txt") );
	IfStream inFile(pathName.c_str(), IfStream::in); 
	if ( inFile.fail() )
	{
		Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
		Cout << pathName << std::endl;
	}
	if ( inFile.fail() )
	{
		pathName = GetFullPath( ieS("ControlPoints.txt") );
		inFile.open(pathName.c_str(), IfStream::in); 
		if ( inFile.fail() )
		{
			Cout << ieS("3) Unable to loacte file in: ") << std::endl;	
			Cout << pathName << std::endl;
		}
	}
	// Create the curve
	if ( inFile.is_open() )
	{
		using std::vector;
		vector<Vector3f> ctrlPoints;
		vector<BezierCurvef> bezierCurves;
		UInt numCtrlPoints = 0;
		while ( inFile.good() )
		{
			// read in number of ctrl points
			inFile >> numCtrlPoints;

			ctrlPoints.clear();
			ctrlPoints.resize(numCtrlPoints);
			for (UInt i = 0; i < numCtrlPoints; ++i)
			{
				inFile >> ctrlPoints[i][0];
				inFile >> ctrlPoints[i][1];
				inFile >> ctrlPoints[i][2];
			}

			// create the curve and push onto an array of curves
			bezierCurves.push_back( BezierCurvef(ctrlPoints) );
		}

		// Create the entire mesh from all the curves
		m_pBezierMesh = CPiecewiseBezierMeshPtr( new CPiecewiseBezierMesh(bezierCurves) );
		m_pBezierMesh->InsertToScene(m_pScene);

		// close the file
		inFile.close();
	}
	else
	{
		assert( false && ieS("Unable to locate ControlPoints.txt") );
		inFile.close();
		Cin.get();
		exit(1);
	}
}







/*
	IGameViewMap::iterator itor = m_gameViewMap.find(m_gameViewCount);
	if ( itor == m_gameViewMap.end() )
	{
		pView->OnCreate(m_gameViewCount, actorID);
		pView->OnRestore();

		IGameViewTable newTable;
		newTable.push_back(pView);
		m_gameViewMap[m_gameViewCount] = newTable;
	}
	else
	{
		GameViewTable table = (*itor).second;
		for (GameViewTable::iterator i = table.begin(); i != table.end(); ++i)
		{
			if ( (*i)->GetType() != pView->GetType() )
			{
				pView->OnCreate(m_gameViewCount, actorID);
				pView->OnRestore();
				table.push_back(pView);
			}
			else
			{
				++m_gameViewCount;
				AddGameView(pView, actorID);
			}
		}
	}
	*/