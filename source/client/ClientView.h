#ifndef __CGAME_VIEW_H__
#define __CGAME_VIEW_H__


#include "BaseView.h"

namespace Engine
{

	class CClientView : public CHumanView
	{
	public:
		friend class CClientViewListener;

		CClientView(IRendererPtr pRenderer, IMouseControllerPtr pMouse, IEventManagerPtr pEventMgr);
		~CClientView();

		void OnCreate(UInt32 gameViewID, ActorID actorID);
		void OnRestore();
		void OnLostDevice();
		void OnUpdate(double time, double deltaTime);
		void OnRender(double time, double deltaTime);
		void OnWindowEvent(const CWinEvent& event);
		GameViewType GetType() const;

	private:
		void CreateScene();

		CClientViewListenerPtr m_pClientViewListener;
		CPiecewiseBezierMeshPtr m_pBezierMesh;

	private:
		DECLARE_HEAP;

	};


#include "ClientView.inl"

}

#endif