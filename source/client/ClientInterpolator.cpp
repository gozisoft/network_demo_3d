#include "clientafx.h"
#include "ClientInterpolator.h"


using namespace Engine;


CClientInterpolator::CClientInterpolator(IActor::ActorID actorID, UInt curveSamples) :
m_actorID(actorID),
m_curveSamples(curveSamples),
m_timeFactor(0),
m_firstFrame(true),
m_stepSize( 1 / static_cast<float>(curveSamples - 1) )
{

}


void CClientInterpolator::AnimateNode(CSpatial* pSpatial, double deltaTime)
{
	if (!pSpatial)
		return;

	if (m_firstFrame)
	{
		// Set the initial positions	
		m_startPos = pSpatial->GetPostion();
		m_startRot = pSpatial->GetRotation();
		m_startScale = pSpatial->GetScale();

		// Bezier information
		// Coordinate 1 = Starting position
		// Coordinate 2	= Position after 1 second using starting velocity
		// 				= Coordinate1 + StartVelocity
		// Coordinate 3	= Position after 1 second using reversed ending velocity
		// 				= Coordinate4 � EndVelocity
		// Coordinate 4	= Ending position
		using std::vector;
		vector<Vector3f> controlPoints(4);
		controlPoints[0] = m_startPos;
		controlPoints[1] = controlPoints[0] + ( m_startVelocity * 1000 );
		controlPoints[2] = m_endPos - ( m_endVelocity * 1000 );
		controlPoints[3] = m_endPos;
		m_bezier.reset( new BezierCurvef(controlPoints) );
		m_timeFactor = 0.0f;
	}
	// Access to the ctrl points of the curve
	const Vector3f* pCtrlPoints = m_bezier->GetControlPoints();

	// finding velocity of the object: Normalised( endpos - startpos );
	Vector3f difference = Normalised( m_endPos - pCtrlPoints[0] );
	float averageVel = Dot(difference, m_startVelocity);

	// Determine the distance moved
	Vector3f curPos = m_bezier->GetPosition(m_timeFactor);
	float distanceRemaining = averageVel * static_cast<float>(deltaTime);

	bool reset = false;
	while(distanceRemaining > 0.0f)
	{
		m_timeFactor += m_stepSize;

		if (m_timeFactor >= 1.0f)		
		{
			m_firstFrame = true;
			m_timeFactor = 0.0f;
		}

		Vector3f newPos = m_bezier->GetPosition(m_timeFactor);
		float distance = Dist(newPos, curPos);
		distanceRemaining -= distance;
		curPos = newPos;
	}

	// set the position
	Vector3f nextPos = m_bezier->GetPosition(m_timeFactor);
	pSpatial->SetPosition(nextPos);

	// Set the object to look along the curve
	Vector3f tangent = m_bezier->GetFirstDerivative(m_timeFactor);
 	tangent.Normalise();

	if ( tangent != Vector3f::ZERO )
	{
		Vector3f zaxis = Normalised( tangent );
		Vector3f xaxis = Normalised( Cross(Vector3f::UNIT_Y, zaxis) );
		Vector3f yaxis = Cross(zaxis, xaxis);

		const float epsilon = 0.001f;
		float det = Dot( xaxis, Cross(yaxis, zaxis) );
		if (Abs(1.0f - det) > epsilon)
		{
			OrthoNormaliseVectors(zaxis, yaxis, xaxis);
		}

		Matrix4f view;
		view(0,0) = xaxis.x();
		view(0,1) = yaxis.x();
		view(0,2) = zaxis.x();
		view(1,0) = xaxis.y();
		view(1,1) = yaxis.y();
		view(1,2) = zaxis.y();
		view(2,0) = xaxis.z();
		view(2,1) = yaxis.z();
		view(2,2) = zaxis.z();
		view(3,3) = 1.0f;
	
		pSpatial->SetRotation( view.GetRotation() );
	}
	else
	{
		pSpatial->SetRotation( m_endRot );
	}

}

bool CClientInterpolator::HandleEvent(const IEventData& event)
{
	if (event.GetEventType() == SEvtData_Move_Actor::sk_EventType)
	{
		const SEvtData_Move_Actor& castEvent =
			static_cast<const SEvtData_Move_Actor &>(event);

		// ID of the actor be moved
		IActor::ActorID actorID = castEvent.m_actorID;

		if (m_actorID == actorID)
		{
			// set the start velocity to that of the end before updating the
			// the actual end veolocity
			m_startVelocity = m_endVelocity;
			m_endVelocity = castEvent.m_avgVelocity;
			m_endPos = castEvent.m_position;
			m_endRot = castEvent.m_rotation;
			m_endScale = castEvent.m_scale;
			m_firstFrame = true;
		}
	}


	return true;
}

