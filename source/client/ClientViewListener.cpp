#include "clientafx.h"
#include "ClientViewListener.h"
#include "ClientView.h"
#include "ClientInterpolator.h"

using namespace Engine;


DEFINE_HIERARCHICALHEAP(CClientViewListener, "CClientViewListener", "IEventListener");

CClientViewListener::CClientViewListener(CClientView* pGameView) : m_pClientView(pGameView)
{

}

CClientViewListener::~CClientViewListener()
{


}

bool CClientViewListener::HandleEvent(const IEventData& event)
{
	IEventManagerPtr pEventMgr = m_pClientView->m_pEventMgr;

	if (event.GetEventType() == SEvtData_Request_New_Actor::sk_EventType)
	{
		const SEvtData_New_Actor & castEvent =
			static_cast<const SEvtData_New_Actor &>(event);

		SActorParams* pParams = castEvent.m_pActorParams;
		if (pParams == NULL)
		{
			assert( false && ieS("Invalid parameters specified for actor!") );
			return false;
		}

		return true;
	}
	else if (event.GetEventType() == SEvtData_Spawn_Actor::sk_EventType)
	{
		const SEvtData_Spawn_Actor & castEvent =
			static_cast<const SEvtData_Spawn_Actor &>(event);

		SActorParams* pParams = castEvent.m_pActorParams;
		if (pParams == NULL)
		{
			assert( false && ieS("Invalid parameters specified for actor!") );
			return false;
		}

		// Create the actor with the paramaters
		IActorPtr pActor = IActor::CreateActor(pParams);

		// Create the mesh based on the paramaters recieved
		CSpatialPtr pSpatial = Engine::ActorToMesh::CreateGameMesh(pParams);

		// Map the actor to the SceneNode
		m_pClientView->m_actorSceneMap[pActor] = pSpatial;

		// Going to add a object to the scene
		CRootNodePtr pRootNode = m_pClientView->m_pScene;

		// Check if its this player's assignment
		if (pParams->m_actorID == m_pClientView->m_actorID)
		{
			// Object listener will listen for when the player object move
			CObjectListenerPtr pObjectListener = CObjectListenerPtr( new CObjectListener(pEventMgr, pParams->m_actorID) );
			pSpatial->AddController(pObjectListener);

			// Create the camera listener and pass in the camera node (camera requires control of mouse).
			IMouseControllerPtr pMouse = m_pClientView->m_pMouse;
			CCameraControllerFPSPtr pCamController = CCameraControllerFPSPtr( new CCameraControllerFPS(pMouse) );
			m_pClientView->m_pCamController = pCamController;

			// Create the camera node, attach the controller and add the mesh as a child to the camera.
			CCameraPtr pCamera = m_pClientView->m_pCamera;
			pCamera->SetPosition( Vector3f(0.0f, 1.0f, -2.0f) );
			CCameraNodeFPSPtr pCameraNode = CCameraNodeFPSPtr( new CCameraNodeFPS(pCamera) );
			pCameraNode->AddController(pCamController);

			// Reset the spatials parent and re-assign its parent as the cameranode
			pCameraNode->AddChild(pSpatial);
			pCameraNode->SetPosition( pSpatial->GetPostion() ); 
			pCameraNode->SetRotation( pSpatial->GetRotation() ); 

			// Add the camera node to the sceneGraph
			pRootNode->AddChild(pCameraNode);

			// Set the view camera node as that what we created
			m_pClientView->m_pCameraNode = pCameraNode;	

			// The camera node is now the new scene actor (its the client player btw)
			// pSpatial = static_pointer_cast<CSpatial>(pCameraNode);
		}
		else
		{
			// Not a player spawn
			// Add a position interpolator (Dead reckoning)
			// listen for move events
			CClientInterpolatorPtr pInterp( new CClientInterpolator(pParams->m_actorID) );
			pEventMgr->AddListener(pInterp, SEvtData_Move_Actor::sk_EventType);
			pSpatial->AddController(pInterp);
			pRootNode->AddChild(pSpatial);
		}

		return true;
	}
	else if (event.GetEventType() == SEvtData_Move_Actor::sk_EventType)
	{
		const SEvtData_Move_Actor& castEvent =
			static_cast<const SEvtData_Move_Actor &>(event);

		// ID of the actor be moved
		IActor::ActorID actorID = castEvent.m_actorID;

		CHumanView::ActorSceneMap::iterator itor = m_pClientView->m_actorSceneMap.begin();
		CHumanView::ActorSceneMap::iterator end = m_pClientView->m_actorSceneMap.end();
		for (; itor != end; ++itor)
		{
			IActorPtr pActor = (*itor).first;
			const SActorParams *pParams = pActor->GetParams();
			if (pParams->m_actorID == actorID )
			{
				CSpatialPtr pSpatial = (*itor).second;

				// perfrom dead reckoning
				// pSpatial->SetPosition(castEvent.m_position);
				// pSpatial->SetRotation(castEvent.m_rotation);
				// pSpatial->SetScale(castEvent.m_scale);
			}
		}

		return true;
	}

	return false;
}