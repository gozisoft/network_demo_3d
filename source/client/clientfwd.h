#ifndef __CLIENT_FORWARD_H__
#define __CLIENT_FORWARD_H__

#include "Core.h"

_ENGINE_BEGIN // namespace Engine


class CClientApp;
class CClientListener;
class CClientView;
class CClientViewListener;
class CClientInterpolator;


typedef shared_ptr<CClientApp> CClientAppPtr;
typedef shared_ptr<CClientListener> CClientListenerPtr;
typedef shared_ptr<CClientView> CClientViewPtr;
typedef shared_ptr<CClientViewListener> CClientViewListenerPtr;
typedef shared_ptr<CClientInterpolator> CClientInterpolatorPtr;

_ENGINE_END // namespace Engine



#endif