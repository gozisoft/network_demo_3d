#ifndef __CCLIENT_LISTENER_H__
#define __CCLIENT_LISTENER_H__

#include "clientfwd.h"

namespace Engine
{

	class CClientListener : public IEventListener
	{
	public:
		explicit CClientListener(CClientApp* pClientApp);
		~CClientListener();

		bool HandleEvent(const IEventData& event);

	private:
		CClientApp* m_pClientApp;
	};

}



#endif