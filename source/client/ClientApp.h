#ifndef __CCLIENT_APP_H__
#define __CCLIENT_APP_H__


namespace Engine
{

	class CClientApp : public IBaseApp
	{
	public:
		friend class CClientListener;
		typedef std::map<UInt32, IActorPtr> ActorMap;
		typedef std::vector<IGameViewPtr> GameViewArray;
		typedef IActor::ActorID ActorID;

		CClientApp();
		~CClientApp();

		bool Initialise();
		void Release();
		void OnCreateDevice(IRendererPtr pRenderer, IMouseControllerPtr pMouse);
		void OnResetDevice(IRendererPtr pRenderer);
		void OnLostDevice();
		void OnDestroyDevice();
		void OnUpdateFrame(double time, double deltaTime);
		void OnRenderFrame(IRendererPtr pRenderer, double time, double deltaTime);
		void OnWindowEvent(const CWinEvent& event);

	private:
		// owns these objects
		CClientNetworkPtr m_pNetwork;

		// The most important system
		CEventManagerPtr m_pEventMgr;

		// Listener to handle events to the client logic
		CClientListenerPtr m_pClientListener;

		// Game views
		GameViewArray m_gameViews;

		// Game actors
		ActorMap m_actorMap;

		// Player game view, added to game by server
		CClientViewPtr m_pClientView;

	private:
		DECLARE_HEAP;
	};



}


#endif