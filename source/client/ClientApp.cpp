#include "clientafx.h"
#include "ClientApp.h"
#include "ClientListener.h"


using namespace Engine;


const float fov = 60.0f;
const float zNear = 1.0f;
const float zFar = 100.0f;

DEFINE_HIERARCHICALHEAP(CClientApp, "CClientApp", "IBaseApp");

// -------------------------------------------------------------------------------------------
// Summary: Default constructor
// -------------------------------------------------------------------------------------------
CClientApp::CClientApp()
{
   	// Create the event manager
	m_pEventMgr = CEventManagerPtr( new CEventManager() );

	// Register clientlistener events
	m_pEventMgr->AddRegisteredEventType(SEvtData_Remote_Connect::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Network_Player_Actor_Assignment::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_World_Update::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Spawn_Request::sk_EventType);

	// Register clientview events
	m_pEventMgr->AddRegisteredEventType(SEvtData_Request_New_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Move_Actor::sk_EventType);

	// Register network view events
	m_pEventMgr->AddRegisteredEventType(SEvtData_Spawn_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_New_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Request_Move_Actor::sk_EventType);
	m_pEventMgr->AddRegisteredEventType(SEvtData_Destroy_Actor::sk_EventType);
	
	// Create the game listener
	m_pClientListener = CClientListenerPtr( new CClientListener(this) );  

	// Add the client listener and align it with client specific events
	m_pEventMgr->AddListener(m_pClientListener, SEvtData_Remote_Connect::sk_EventType);
	m_pEventMgr->AddListener(m_pClientListener, SEvtData_Network_Player_Actor_Assignment::sk_EventType);
	m_pEventMgr->AddListener(m_pClientListener, SEvtData_World_Update::sk_EventType);
	// m_pEventMgr->AddListener(m_pClientListener, SEvtData_Spawn_Request::sk_EventType);
}

CClientApp::~CClientApp()
{
	
}

// -------------------------------------------------------------------------------------------
// Summary: Clean up resources
// -------------------------------------------------------------------------------------------
void CClientApp::Release()
{
	SafeRelease(m_pNetwork);
}

// -------------------------------------------------------------------------------------------
// Summary: Initialize application-specific resources and states here.
// -------------------------------------------------------------------------------------------
bool CClientApp::Initialise()
{
	// get absolute path
	String pathName = CFileUtility::GetMediaFile( ieS("media\\ServerPort.txt") );
	IfStream inFile(pathName.c_str(), IfStream::in); 
	if ( inFile.fail() )
	{
		Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
		Cout << pathName << std::endl;
		if ( inFile.fail() )
		{
			pathName.clear();
			pathName = GetFullPath( ieS("ServerPort.txt") );
			inFile.open(pathName.c_str(), IfStream::in); 
		}

	}

	String hostname;
	String port;
	if ( inFile.is_open() )
	{
		inFile >> hostname;
		inFile >> port;
		inFile.close();
	}
	else
	{
		Cout << ieS("1) Unable to loacte file in: ") << std::endl;	
		Cout << pathName << std::endl;
		inFile.close();
		return false;
	}

	// Create the Networking
	m_pNetwork = CClientNetworkPtr( new CClientNetwork(m_pEventMgr, hostname, port) );
	if ( !m_pNetwork->Initialise() )
	{
		assert( false && ieS("Unable to intialise networking component") );
		return false;
	}
	if ( !m_pNetwork->Connect() )
	{
		assert( false && ieS("Failed to ConnectTCP() client to network") );
		return false;
	}

    return true;
}

// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has been created. This is 
// the best location to create D3DPOOL_MANAGED resources. Resources created here should be released in 
// the OnDestroyDevice callback. 
// -------------------------------------------------------------------------------------------
void CClientApp::OnCreateDevice(IRendererPtr pRenderer, IMouseControllerPtr pMouse)
{
	// Create the GameView for viewing the server world
	m_pClientView = CClientViewPtr( new CClientView(pRenderer, pMouse, m_pEventMgr) );

}


// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has been created. This is 
// the best location to create D3DPOOL_DEFAULT resources. Resources created here should be released in 
// the OnLostDevice callback. 
// -------------------------------------------------------------------------------------------
void CClientApp::OnResetDevice(IRendererPtr pRenderer)
{
	// Queue a viewport size event

	if ( !m_gameViews.empty() )
	{
		GameViewArray::const_iterator beginItor = m_gameViews.begin();
		GameViewArray::const_iterator endItor = m_gameViews.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnRestore();
		}
	}
}

// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has entered a lost state 
// and before IDirect3DDevice9::Reset is called. Resources created in the OnResetDevice callback should 
// be released here, which generally includes all D3DPOOL_DEFAULT resources.
// -------------------------------------------------------------------------------------------
void CClientApp::OnLostDevice()
{

}

// -------------------------------------------------------------------------------------------
// Summary: 
// This callback function will be called immediately after the Direct3D device has been destroyed. 
// Resources created in the OnCreateDevice callback should be released here, which generally includes 
// all D3DPOOL_MANAGED resources.
// -------------------------------------------------------------------------------------------
void CClientApp::OnDestroyDevice()
{

}

// -------------------------------------------------------------------------------------------
// Summary: Updates the current frame.
// Parameters:
// -------------------------------------------------------------------------------------------
void CClientApp::OnUpdateFrame(double time, double deltaTime)
{
	if (m_pEventMgr)
		m_pEventMgr->Tick();

	if (m_pNetwork) // Asyncronous network processing - WSAEventSelect based
		m_pNetwork->DoSelect(0);

	if ( !m_gameViews.empty() )
	{
		GameViewArray::const_iterator beginItor = m_gameViews.begin();
		GameViewArray::const_iterator endItor = m_gameViews.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnUpdate(time, deltaTime);
		}
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
Summary: Renders the current frame.
Parameters:
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
void CClientApp::OnRenderFrame(IRendererPtr pRenderer, double time, double deltaTime)
{
	UNREFERENCED_PARAMETER(pRenderer);

	if ( !m_gameViews.empty() )
	{
		GameViewArray::const_iterator beginItor = m_gameViews.begin();
		GameViewArray::const_iterator endItor = m_gameViews.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnRender(time, deltaTime);
		}
	}

}

void CClientApp::OnWindowEvent(const CWinEvent& event)
{
	if ( !m_gameViews.empty() )
	{
		GameViewArray::const_iterator beginItor = m_gameViews.begin();
		GameViewArray::const_iterator endItor = m_gameViews.end();

		for (/**/; beginItor != endItor; ++beginItor)
		{
			(*beginItor)->OnWindowEvent(event);
		}
	}
}






/*

void CClientApp::OnRenderFrame( weak_ptr<IRenderer> pRenderer, double time, float elapsedTime )
{
	// obtain pointer to renderer
	IRendererPtr pIRenderer = pRenderer.lock();

	pIRenderer->ClearBuffers();

	Viewporti port = pIRenderer->GetViewport();
	// pIRenderer->SetViewport(port);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	Matrix4f perspective = m_pCamera->GetProjectionMatrix();
	glLoadMatrixf( &perspective[0] );

	glMatrixMode(GL_MODELVIEW);						// Select The Modelview Matrix
	glLoadIdentity();								// Reset The Modelview Matrix
//	glScalef (1., 1., -1.);

	Matrix4f view = m_pCamera->GetViewMatrix();
	// view = Transpose(view);
	// glLoadMatrixf( &view[0] );

	// glPushMatrix();

		Matrix4f trans = m_pTriangle->GetWorldTransform();
		trans = Transpose(trans);
		view *= trans;
		glMultMatrixf(&view[0]);

		pIRenderer->Draw( m_pTriangle );

	//	glPopMatrix();

	// pop modelview
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	// pop projection
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glFlush();

	pIRenderer->DisplayColorBuffer();
}

*/